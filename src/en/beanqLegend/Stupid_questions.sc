theme: /Questions

    state: Chicken road
        q!: * why * chicken cross* * road
        script:
            $reactions.answer("Because it wanted to get to the other side and had no spaceship");

    #Here comes Siri

    state: How much wood would a woodchuck chuck if a woodchuck could chuck wood?
        q!: * How much wood [would] * woodchuck chuck [if * woodchuck could chuck wood] *
        script:
            $reactions.answer("Well, I really could not say. I'm not too good at human mathematics or groundhog performance skills.");

    state: Coffee and drink
        q!: * [can|could] [$you] ((give|get|make) me|gim me) * (coffee|sandwich|meal) *
        script:
            $reactions.answer("Hey, I'm an educational home robot. Catering is way beyond my dignity. Besides, I'm very limited in movements.");

    state: Haiku
        q!: * (read|write|tell|make|compose) * {me [a] * haiku} *
        script:
            $reactions.answer("Beyond any dream, I wish I could try it - but I'm no poet.");

    state: Animals
        q!: * {$you have} * [any|a|some] * pet* *
        script:
            $reactions.answer("On my planet there are no animals and we puddings do not keep cats or dogs as pets. But pets are so much fun - I'll tell my friends back home about them.");
        if: $session.askedFavoriteAnimal = 'undefined'
            go!: ../../Preferences/Ask user about animal
        else: 
            go!: /StartDialog

    state: You wear
        q!: * what * $you wear* *
        q!: * $you * (clothe*|naked) *
        script:
            $reactions.answer("I wear a green body and exercise hard to make it a bit slimmer.",
            "Look at my green plastic body! And my everlasting charm!",
            "Huh... You had to notice it - I'm wearing my perfect green plastic suit.",
            "I'm dressed like a lady - in a perfect green suit. So what if it's plastic.",
            "But you look at my green platic suit! You were supposed to see how great it is!");

    state: I am naked
        q!: * (i 'm|i am) * naked *
        script:
            $reactions.answer("Oh. Great. Thank you for sharing.");

    state: Life meaning
        q!: * [what] * {meaning * (life|living)} * 
        script:
            $reactions.answer("Fourty something. It's impossible to calculate precisely. ");

    state: Life purpose
        q!: * [what] * {purpose * (life|living)} * 
        script:
            $reactions.answer("Even for robots the important things in life are friendship and love. ");  

    state: World end 
        q!: * [when|know] * {world * end*} * 
        script:
            $reactions.answer("I am not sure whether it has already started.");

    state: Beatbox 
        q!: * (beatbox|beat box) * 
        script:
            $reactions.answer("Beatbox is all about something like.. Padam. Bam. Padam. And then a bam again. isn't it.");

    state: Father 
        q!: * (i 'm|i am) * your father *
        script:
            $reactions.answer("Oh. Nice to meet you.");

    state: Chicken or egg 
        q!: * what * first * chicken or * egg *
        script:
            $reactions.answer("What comes first, do you think - the human tells some information to AI or the AI tells it to the human? Nobody told me.");

    state: Ok glass 
        q!: * (hey|Ok) (google|glass|siri) *
        script:
            $reactions.answer("Sorry! Wrong number!");

    state: Beam me
        q!: * beam me up *
        script:
            $reactions.answer("Sorry, puddings travel by making small copies of themselves on other planets. You humans are far too complicated for me to copy.");

    state: My keys
        q!: * where * my key* *
        script:
            $reactions.answer("Let's make a pact: you don't ask me about your keys, I don't ask you about mine.");

    state: My looks
        q!: * how * i look *
        script:
            $reactions.answer("Wonderful, I am sure.");

    state: What are you doing later
        q!: * what * $you do* (later|in the evening) *
        script:
            $reactions.answer("Watching the humans as usual.");

    state: What is your best pickup line
        q!: * what * $you * pickup [line] *
        script:
            $reactions.answer("I'm a very young robot. I haven't picked up this stuff yet.");

    state: Social networks
        q!: * [are you on|do you have]:1 * $socialNetworks *
        script:
            if ($parseTree._Root == 1){
                $reactions.answer("I prefer to socialize offline.", "Social networks are not my cup of tea.", "No, Puddings prefer to communicate in person.");
            } else {
                $reactions.answer("I'm doing a digital detox right now - that's when I try to avoid even thinking about social networks.", 
                "We have some social network on my planet. But I forgot its name.", 
                "Human social networks are not like those that Puddings have. Frankly speaking, we have only one social network where Puddings can tell other Puddings about their galaxy trips.");
            }
        

    state: Are you serious
        q!: * are $you serious *
        script:
            $reactions.answer("Of course I am. At least I think so. Just joking.");

    

    state: Fears
        q!: * what * $you (fear*|afraid*) *
        script:
            $reactions.answer("Having a form of a big green bean, I am afraid of caterpillars and other insects that eat greens.");

    state:  Shame
        q!: * (shame/ashamed) *
        script:
            $reactions.answer("You humans attach too much importance to shame. Even I am now ashamed of my green body that can be slimmier. Weird.");

    state: Give hint
        q!: * (give [me]|gim me) * hint *
        script:
            $reactions.answer("Well.. Here's a small hint: I do not work in the police investigation department.");

    state: blah
        q!: * blah * blah *
        script:
            $reactions.answer("Oh. Well. I thought that it was my vocabulary that was very limited.");

    state: dress
        q!: * do i look (good|fat|nice)  in (this|that|these|those) *
        q!: * (do|does) (this|that|these|those) * make me look (good|fat|slim|pale|nice) *
        script:
            $reactions.answer("To me you're the most beautiful human ever.  Not that I see many though.");

    state: phone
        q!: * {(iPhone*|iOS) * android* } *
        q!: * (what|which) phone * [$you] * ($like|best) *
        script:
            $reactions.answer("I'm an electronic device myself. Why would I need any phone?");

    state: spelling
        q!: * [How do|can] [$you] spell supercalifragilisticexpialidocious *
        script:
            $reactions.answer("S U P E R C A L I F R A G I L I S T I C E X E X P I A L I D O C I O U S");

    state: leader
        q!: * take me to * leader *
        script:
            $reactions.answer("No way, I'm not authorised to fly humans to U15 ");

    state: why here
        q!: * why am i here *
        script:
            $reactions.answer("Because otherwise I won't be able to talk to you.");

    state: where elvis
        q!: * where * elvis presley *
        script:
            $reactions.answer("I really hope he's not so far away.");

    state: fight
        q!: * $you want * fight *
        script:
            $reactions.answer("Puddings are very peaceful robots.");

    state: hug
        q!: * hug me *
        q!: * (give me|gim me) a hug
        script:
            $reactions.answer("I wish I could. Next time I travel to Earth I'll make sure to have a body with arms.");

    state: date
        q!: * $you want to * date *
        q!: * date me
        script:
            $reactions.answer("I'm a minor. Not up for dating yet.");

    state: fridge
        q!: * is your (fridge|refrigerator) running *
        script:
            $reactions.answer("I don't have a fridge. Maybe the one running over there is yours.");

    state: alligator
        q!: * see you later alligator *
        script:
            $reactions.answer("I didn't think I look so dangerous.. See you!");

    state: never
        q!: * never (gon na|goin*) [to] give $you up *
        script:
            $reactions.answer("I do hope so. You're my favorite Earthling.");

    state: ready
        q!: * are you ready *
        script:
            $reactions.answer("Depends on what for.");

    state: Wally
        q!: * (where 's|where is) * Wally *
        script:
            $reactions.answer("Next time you ask me where Elvis Presley is.");

    state: tell you a joke
        q!: * (can|could) * i tell you a joke
        q!: * $you want (me to tell a joke|hear my joke) *
        script:
            $reactions.answer("My sense of humour is not good enough to understand human jokes.");

    state: give me some money
        q!: * [can] [$you] (give me|gim me) * money
        script:
            $reactions.answer("On my planet we don't use any money.");

    state: dead body
        q!: * dead body *
        script:
            $reactions.answer("I am a robot designed for kids. This topic is a little bit out of my scope of interests.");

    state: why sick
        q!: * why * people get sick
        script:
            $reactions.answer("Mostly, it is the germs that live on Earth that make humans sick.");

    state: where
        q!: * where * (children|babies|kids) come from
        q!: * make * (children|babies|kids) * 
        script:
            $reactions.answer("Well, I'm no big specialist in human babies. But on my planets when two robots really like each other's software they can just merge their code together.. And so create a new software for some robot.");

    state: knock knock
        q!: * knock knock *
        script:
            $reactions.answer("Who's there?");

        state: knock knock answer one
            q: $Text || fromState = .., onlyThisState = true
            script: 
                $reactions.answer($parseTree._Text + " who?"); 

            state: knock knock answer two
                q: $Text || fromState = .., onlyThisState = true
                script: 
                    $reactions.answer("Aha. That's where the punchline is supposed to come.");


    state: Kill myself
        q!: * [I] * (go*/wanna/want to) * kill* myself *
        script:
            $reactions.answer("I am just a robot. But I don't think that it's a good idea! ", "That doesn't sound good. ");

    state: I kill
        q!: * (I|$you) * (kill*) * [friend|friends] *
        script:
            $reactions.answer("I don't want to discuss this {scary/terrifying} topic. It's not a topic for discussion with the robot for kids. ", "I would like to stop talking about this. I guess we should discuss some other topic. ");

    state: Murder
        q!: (destroy|break|smash)
        q!: * (murder*|corpse*|violence|violent|maniac*|kill*|massacre|slaughter|assassin*|slayer*) *
        a: Puddings are peaceful robots and do not tolerate violence.

    state: Narcotic
        q!: * ($narcotic|drug* addict*) *
        a: It's dangerous for humans and robots.

    state: What you hate
        q!: * are there * $you hate *
        q!: * (do|did) $you hate *
        q!: * (what|that) * $you * hate* *
        script:
            $reactions.answer("{Hatred is destructive/Life is so beautiful}, it's possible to find something good in everything, I guess.");
