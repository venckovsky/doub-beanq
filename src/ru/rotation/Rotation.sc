require: ../../main.js
require: ../../patterns.sc
require: common/common.sc
  module = zenbox_common

patterns:
    $Turn = (поверн*/крути*/покрути*/отверн*/*вертись/*вертите/крутани*/разверн*/поворачивай*)
    $Left = (налево/левее/влево/в лево/на лево/[на|в] левую)
    $Right = (направо/правее/вправо/в право/на право/[на|в] правую)
    $Another = ([в|на] ((друг*/обратн*/противополож*/противо полож*) [сторон*]/обратно/назад))
    $turnCounter = $Number (раз/раза)

theme: /Rotation
    state: Turn left
        q!: * { [$Turn] $Left  * [$turnCounter]} *
        q: * [$Left|$Turn] * $continue * [$Left|$Turn] *  || fromState=./, onlyThisState = true
        q: * { $Another [$Turn] } *  || fromState="../Turn right", onlyThisState = true
        q: * [a] * $Left *  || fromState="../Turn right", onlyThisState = true
        q: * (покажи/{можешь * показать}) *  || fromState=.., onlyThisState = true
        q: давай  || fromState=.., onlyThisState = true
        q!: * {(*смотри/*вернись/*верни) [пожалуйста] (на меня/ко мне)}
        q: * (я (здесь/тут)/не туда) * || fromState="../Turn right", onlyThisState = true
        q!: голову ($Left/на меня)
        q!: {[$botName] найди меня}
        q!: * ты умеешь (крутиться/вращаться) *
        script: 
            $session.turnState = "left";
            $response.intent = "DeviceManage/motorRotate";
            $response.angle = [-10];
            if ($parseTree.turnCounter) {
                for (var i=1; i<$parseTree.turnCounter[0].Number[0].value; i++) {
                    $response.angle.push(-10);
                }
            }
        a: 
             
    state: Turn right
        q!: * { [$Turn] $Right * [$turnCounter]} *
        q: * [$Right|$Turn] * $continue * [$Right|$Turn] * || fromState=./, onlyThisState = true
        q: * { $Another [$Turn] } * || fromState="../Turn left", onlyThisState = true
        q: * [a] * $Right * || fromState="../Turn left", onlyThisState = true
        q: * (я (здесь/тут)/не туда) * || fromState="../Turn left", onlyThisState = true
        q!: голову $Right
        script: 
            $session.turnState = "right";
            $response.intent = "DeviceManage/motorRotate";
            $response.angle = [10];
            if ($parseTree.turnCounter) {
                for (var i=1; i<$parseTree.turnCounter[0].Number[0].value; i++) {
                    $response.angle.push(10);
                }
            }            
        a: 
           
   
    state: Turn another
        q!: * $Turn * 
        script:
            $temp.nextState = selectRandomArg("Turn right", "Turn left");
        go!: ../{{$temp.nextState}}    

    state: Turn stop
        q!: * [больше] (не [надо|нужно]|ненадо|ненужно) $Turn * 
        q: * {(стоп/останови*/прекрати*/хватит/достаточн*/довольно/всё/не [надо|нужно]|ненадо|ненужно) * [$Turn]} * || fromState=../, onlyThisState = true
        random:
            a: Ладно, не буду больше поворачиваться. А то боюсь потерять тебя из виду!
            a: Ладно, больше не буду вертеться. А то совсем тебя потеряю из виду!
            a: Хорошо, я не буду поворачиваться.
            a: Как скажешь!
            
    state: CantRotateMore
        q!: * не могу (вращаться дальше хватит/повернуть туда голову) *
        random:
            a: У меня голова закружилась!
            a: Осторожно! Так может и голова закружиться!           