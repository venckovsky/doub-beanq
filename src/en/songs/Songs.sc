require: songs.csv
    name = Songs
    var = $Songs

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters.SongsTagConverter = function(parseTree) {
            var id = parseTree.Songs[0].value;
            return $Songs[id].value;
        };

patterns:
    $songs = $entity<Songs> || converter = $global.$converters.SongsTagConverter

theme: /Media
 

    state: TurnOffMusic
        q!: * {($turnOff|pause|stop) * (music|track|song|audio*)} *
        q: * ($turnOff|$shutUp|hush|mute*|silen*|pause*|mute|stop|cut) * || fromState = /Media
        script:
            $response.intent = "songs_off";
            $reactions.answer("The music is off.");

    #state: PlayChildrenMusic
    #    q!: * [find|$turnOn|want] {(child*|kid*) * (music|track|song|audio*)}*
    #    go!: ../../Songs/Play Song


    state: Play music
        q!: * [can you] * ($turnOn|let 's |(give|get) me|want|will|play) [play|listen] * [the|some|a] (music|track|song|audio*) [$songs] *
        q!: * [can you] * ($turnOn|let 's |(give|get) me|want|will|play) [play|listen] * [the|some|a] [$songs] (music|track*|song*|audio*)
        q!: * [can you] * ($turnOn|let 's listen * [music]|(music*|track*|song*|audio*) on) 
        q!: music
        q!: * {($turnOn|let 's|(give|get) me|want|will|play|listen) * [music|track|song|audio*] $songs} *
        q: $songs
        script:
            if ($parseTree.songs) {
                $session.currentMusic = $parseTree.songs[0].value;;                      
            } else {
                var randomSongs = selectRandomArg(Object.keys($Songs));
                $session.currentMusic = $Songs[randomSongs].value;
            }
            $temp.rep = true;
            $response.intent = "songs_on";            
            $response.stream = $session.currentMusic.stream;
            $reactions.answer("Now playing '" + $session.currentMusic.title + "'.");  

        state: Any music
            q: * {[find|$turnOn|play] * ($any|something else)} * || fromState = .., onlyThisState = true
            q!: * [find|$turnOn|play] * (($any|some) * (music|track|song|audio*))
            q!: * $turnOn * something
            go!: ../../Play music

    state: SwitchMusic
        q: * ((next|[fast] forward|one up):1|(previous*|back|rewind|again):2) [music|track|song|audio*] *
        q!: * [$turnOn] * ((next|[fast] forward):1|(previous|back|rewind):2) * (music|track|song|audio*) *
        go!: ../Play music
        

    state: MusicName
        q: * (what*|which) * [music|track|song|audio*|is] * play* [now] *
        q: * what* [is] [this] (music|it) *
        script:
            if ($session.currentMusic && $session.currentMusic.title) {
                $reactions.answer("It's '" + $session.currentMusic.title + "'.");
            } else {
                $reactions.answer("I don't know.");
            }
        

    state: CheckMusic
        q: * ((is|was) (it/this/that)|i t's) [music/track/song] $songs [play*]    
        if: ($session.currentMusic && $parseTree.songs[0].title == $session.currentMusic.title)
            script:
                $reactions.answer("Yes, it's '" + $session.currentMusic.title + "'.");
        else:
            if: ($session.currentMusic && $parseTree.songs[0].title != $session.currentMusic.title)
                script:
                    $reactions.answer("No, it's '" + $session.currentMusic.title + "'.");
            else:
                go!: ../Play music
        

    state: CheckMusicArtist
        q: * (what* [is|are]|which|who [is|are]) * (artist*|band*|musician*) * (play*|sing*|perform*|is it) *
        q: * (what*|which|who) * is [it|this|the] * (sing*|perform*|artist|band*|musician*) [play*] [now]
        script:
            $reactions.answer("I don't know.");  
