var learnEnglishFairytales = (function() {

    function nextTale() {
        var $session = $jsapi.context().session;
        var $parseTree = $jsapi.context().parseTree;

        $session.englishFairytales.last = $session.englishFairytales.next;  // В первый раз это undefined

        var nextTaleId = selectRandomArg($session.englishFairytales.left);
        $session.englishFairytales.next = ($parseTree.learnEnglishFairytale) ? $parseTree._learnEnglishFairytale : $learnEnglishFairytales[nextTaleId].value;

        nextTaleId = $session.englishFairytales.left.indexOf($session.englishFairytales.next.id);
        $session.englishFairytales.left.splice(nextTaleId, 1);
    }

    function changeTheme() {
        var $request = $jsapi.context().request;
        var res = $nlp.match($request.query, "/");

        if (res.targetState) {
            this.cleanUp();
            $reactions.transition(res.targetState);
        } else {
            $reactions.transition("../CatchAll");
        }
    }

    function cleanUp() {
        var $session = $jsapi.context().session;
        delete $session.englishFairytales;
    }

    return {
        nextTale: nextTale,
        changeTheme: changeTheme,
        cleanUp: cleanUp
    };

})();
