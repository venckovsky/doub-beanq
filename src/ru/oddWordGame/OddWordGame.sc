require: OddWordGame.js

require: ../games/BeanqGames.sc

require: ../../dictionaries/oddWordGameQuestions.csv
  name = oddWordGameQuestions
  var = $oddWordGameQuestions

require: answers.yaml
  var = OddWordGameCommonAnswers

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .oddWordGameQuestionsConverter = function(parseTree) {
            var id = parseTree.oddWordGameQuestions[0].value;
            return $oddWordGameQuestions[id].value;
        };


    $global.themes = $global.themes || [];
    $global.themes.push("Odd word game");
    $global.$OddWordGameAnswers = (typeof OddWordCustomAnswers != 'undefined') ? applyCustomAnswers(OddWordGameCommonAnswers, OddWordCustomAnswers) : OddWordGameCommonAnswers;

patterns:
    $oddWordGameQuestions = $entity<oddWordGameQuestions> || converter = $converters.oddWordGameQuestionsConverter
    $changeThemeWords = (хочу/давай/включи/поиграем/поиграй/поговори/расскажи/скажи/создай/загадай/открой/запусти/поставь);

    $oddWordGameOrder = ((~первый/один/1 слово/1):1|
             (~второй/два/2 слово/2):2|
             (~третий/три/3 слово/третье/3):3|
             (~четвертый/четыре/4 слово/4):4|
             (~последний):lst|
             (предпоследний/предпоследняя/предпоследнее):blst|
             ($Number))
    
    $oddWordGameStrictOrder = (1|2|3|4)
         
theme: /Odd word game BeanQ

    state: Odd word game start
        q!: * [хочу/давай/открой] {(~играть|~сыграть|~поиграть|~игра) * [в/про] ([найди/найти] [~один] ~лишний [~слово])} *
        q!: * [хочу/давай/открой] * [в/про] * (~лишний ~слово|[найди|один|~слово] ~лишний)
        q: * (лишнее/найди) * || fromState=/BeanqGames/Games, modal=true
        script:
            oddWordGameInit();
            $session.repeat = false;
        a: {{selectRandomArg($OddWordGameAnswers["Odd word game start"])}} || question = true
        go: ../YesOrNo

    state: YesOrNo || modal=true
        
        state: No Odd Word game
            q: * ($disagree|$notNow|не помогу) * || fromState = .., onlyThisState = true
            q: * ($disagree|$notNow) * || fromState = "/BeanqGames/Games/How to play Odd word game", onlyThisState = true
            a: {{selectRandomArg($OddWordGameAnswers["YesOrNo"]["No Odd Word game"])}}
            go!: /Odd word game BeanQ/YesOrNo/Odd word game stop

        state: Yes Odd Word game
            q: * ($agree|начин*|начн*|помогу|говори|угадаю|найду|отгадаю|найдем|попроб*|давай) * || fromState = .., onlyThisState = true
            q: * ($agree|начин*|начн*|говори|давай) * || fromState = "/BeanqGames/Games/How to play Odd word game", onlyThisState = true
            q: * [$agree|$maybe|$sure] * ((найди/найти) лишнее) * [$agree|$maybe|$sure] * || fromState = /BeanqGames/Games, onlyThisState = true
            q: * (сыграем/еще/продолжай/называй/вперед/продолж*) *        
            script:
                $temp.start = true;
                $session.flagForNonsense = false;
            if: $session.repeat
                a: {{selectRandomArg($OddWordGameAnswers["YesOrNo"]["Yes Odd Word game"]["if"])}}
            else:
                a: {{selectRandomArg($OddWordGameAnswers["YesOrNo"]["Yes Odd Word game"]["else"])}}
                script:
                    $session.repeat = true;
            go!: ../../Odd word game BeanQ ask question   


        state: Odd word game stop
            q: * ($stopGame/(не хочу/надоело/не буду) * играть/отстань) *
            if: $session.oddWordGame.rightAnswersCounter == 0
                a: {{selectRandomArg($OddWordGameAnswers["Odd word game stop 0"])}} 
            else:
                a: {{selectRandomArg($OddWordGameAnswers["Odd word game stop"])}} 
            script:
                $session.repeat = false;
            go: /

        state: HowManyStars
            q: * {(сколько|как много) [у меня] [уже] [есть|набралось] * (~звездочка|~звезда|~очко|~балл/угадал*)} *
            a: У тебя {{$session.oddWordGame.rightAnswersCounter}} {{ $nlp.conform("звёздочка", $session.oddWordGame.rightAnswersCounter) }}.
            a: {{selectRandomArg($OddWordGameAnswers["RightAnswer"]["again"])}}
            go: /Odd word game BeanQ/YesOrNo

        state: HowManyWrongAnswers
            q: * {(сколько|как много) * [у меня] [уже] [есть|набралось] ((~неправильный|~правильный) ~ответ/не угадал*)} *
            a: У тебя {{$session.oddWordGame.rightAnswersCounter}} {{ $nlp.conform("правильный", $session.oddWordGame.rightAnswersCounter) }} {{ $nlp.conform("ответ", $session.oddWordGame.rightAnswersCounter) }} из {{$session.oddWordGame.answersCounter}}.
            a: {{selectRandomArg($OddWordGameAnswers["RightAnswer"]["again"])}}
            go: /Odd word game BeanQ/YesOrNo

        state: LocalCatchAll
            q: * [$changeThemeWords] *
            script:
                if ($parseTree.changeThemeWords) {
                    var res = $nlp.match($request.query, "/");
                    if (res.targetState) {
                        $reactions.transition(res.targetState);
                    }
                }
            if: $session.flagForNonsense
                script:
                    $reactions.answer('Кажется, стало скучновато, и мы уходим от темы. Во что другое хочешь поиграть?');
                go: /
            else:
                script:
                    $session.flagForNonsense = true;
                    $reactions.answer('Кажется, мы отвлеклись. Я как робот считаю, что если играть, то играть по правилам. Я называю слова, а ты находишь лишнее. Сыграем?');
                go: ../../YesOrNo


    state: Odd word game BeanQ ask question || modal=true
        script:          
            OddWordGame.chooseQuestion();
            $session.flagForNonsense = false;
        a: {{$session.oddWordGame.currentListText}}
        a: Какое слово здесь лишнее?


        state: Odd Word game get answer
            q: * [$maybe|$sure|думаю] [что] [это] [$changeThemeWords] ($oddWordGameOrder/$AnyWord) [это] [$maybe|$sure] *
            script:
                if ($parseTree.changeThemeWords && $request.query.indexOf("ответ")  == -1) {
                    var res = $nlp.match($request.query, "/");
                    if (res.targetState) {
                        $reactions.transition(res.targetState);
                    }
                } else {
                    $session.flagForNonsense = false;
                    if ($request.query.indexOf(" или ")  > -1) {
                        $reactions.transition("/Odd word game BeanQ/WrongAnswer");
                    } else {
                        OddWordGame.processAnswerBeanq();
                    }
                }


        state: Odd Word game repeat list
            q: * {[повтори/скажи/прочитай/какие были/повтори-ка] [еще] [раз] (список/запрос/вопрос/варианты/слова/список слов/что [там/в списке] было)} *
            q: * {(повтори/скажи/прочитай/повтори-ка) * [пожалуйста] [еще] [раз]}
            q: [* повтори/повтори-ка] еще раз
            q: * (*медленнее/(очень/слишком) быстро) *
            q: * из чего * (выбрать/выбирать) *
            a: {{capitalize($session.oddWordGame.currentListText)}} || question = true
            a: Какое слово здесь лишнее?
            go: /Odd word game BeanQ/Odd word game BeanQ ask question


        state: HowManyStars
            q: * {(сколько|как много) [у меня] [уже] [есть|набралось] * (~звездочка|~звезда|~очко|~балл|угадал*)} *
            a: У тебя {{$session.oddWordGame.rightAnswersCounter}} {{ $nlp.conform("звёздочка", $session.oddWordGame.rightAnswersCounter) }}.
            go!: /Odd word game BeanQ/Odd word game BeanQ ask question/Odd word game BeanQ repeat list

        state: HowManyWrongAnswers
            q: * {(сколько|как много) * [у меня/я] [уже] [есть|набралось] ((~неправильный|~правильный) ~ответ)} *
            a: У тебя {{$session.oddWordGame.rightAnswersCounter}} {{ $nlp.conform("правильный", $session.oddWordGame.rightAnswersCounter) }} {{ $nlp.conform("ответ", $session.oddWordGame.rightAnswersCounter) }} из {{$session.oddWordGame.answersCounter}}.
            go!: /Odd word game BeanQ/Odd word game BeanQ ask question/Odd word game BeanQ repeat list


        state: Odd word game stop
            q: * ($stopGame/(не хочу/надоело/не буду) * играть/отстань) *
            if: $session.oddWordGame.rightAnswersCounter == 0
                a: {{selectRandomArg($OddWordGameAnswers["Odd word game stop 0"])}} 
            else:
                a: {{selectRandomArg($OddWordGameAnswers["Odd word game stop"])}} 
            script:
                $session.repeat = false;
            go: /


    state: RightAnswer
        script:
            $session.oddWordGame.answersCounter += 1;
            $session.oddWordGame.rightAnswersCounter += 1;
        if: ($session.oddWordGame.usedQuestions.length == 45)
            a: {{selectRandomArg($OddWordGameAnswers["RightAnswer"]["if"])}}
            go: / 
        else:
            a: {{selectRandomArg($OddWordGameAnswers["RightAnswer"]["else"])}} 
            a: {{$session.oddWordGame.explanation}} 
            a: {{selectRandomArg($OddWordGameAnswers["RightAnswer"]["again"])}}
            go: /Odd word game BeanQ/YesOrNo

    state: WrongAnswer
        script:
            $session.oddWordGame.answersCounter += 1;


        a: {{selectRandomArg($OddWordGameAnswers["WrongAnswer"]["if"])}} лишнее слово - {{$session.oddWordGame.rightAnswer}}. 
        a: {{$session.oddWordGame.explanation}} 
        if: ($session.oddWordGame.usedQuestions.length == 45)
            a: {{selectRandomArg($OddWordGameAnswers["WrongAnswer"]["end"])}}
            go: / 
        else:
            a: {{selectRandomArg($OddWordGameAnswers["WrongAnswer"]["again"])}}
            go: /Odd word game BeanQ/YesOrNo   
