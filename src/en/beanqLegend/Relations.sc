theme: /Relationship

    state: Ask about love
        q!: * [who] * [$you]  * (love*|loving) *
        script:
            $client.askedAboutLove = true;
            $reactions.answer("I don't quite get what humans mean by love. Can you explain to me what it is?",
            "I don't know much about love on Earth. Tell me what people mean by love, please.",
            "Love? I heard this notion couple of times, but its meaning is still unclear to me. Tell me, what is love?");
        

        state: Love yes
            q: * $agree * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Please do tell me! I'll write about it in my book on Earth.");
            

        state: Love no
            q: * $disagree * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("It seems I'll never know.");
            

        state: Love is
            q: * (love|it| it 's) (is|means) $Text || fromState = .., onlyThisState = true
            q: $Text || fromState = .., onlyThisState = true
            q!: * love is $Text
            script:
                $client.loveText = $parseTree._Text;
                $reactions.answer("Thanks! I'm writing that down! Love is " + $client.loveText + ".",
                "Got you! Have to write it down! Love is "  + $client.loveText + ".",
                "Thank you! Now I know that love is " + $client.loveText + ".",
                "Love is " + $client.loveText + ". Now I understand that!");
            

        state: Love not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            q: * (myster*|puzzl*) * for me
            script:
                $reactions.answer("It seems you are still investigating the matter yourself!");

            
    state: What is love
        q!: * what is love *
        script:
            $reactions.answer("It's an interesting topic! I've got a friend back home - I exchange files and updates with him, and he has joined the sustainable development club to see me more often. Do you think it is love?");
            $session.yesAnswer = "It seems I start to understand.";
            $session.noAnswer = "Maybe, it is not so important how to call that.";
            $session.noNextState = "Ask about love";
            $session.dontKnowAnswer = "I'll look into the matter and tell you.";
            $session.dontKnowNextState = "Ask about love";
        go!: /Metascenario/YesNoDontKnow  

    state: Do you have friends
        q!: * do ($you|$robotName) (have|make) * friend* *
        q!: * (tell me * [about]|who are your) * friend* *
        q!: * $your * friend* * robot* *  
        q!: * who * [best|favorite|favourite] friend* *
        q!: * we know each other *
        script:
            $reactions.answer("I have a lot of friends on my planet U15, and now I want to make some friends among people.");
        if: !$client.askedAboutFriends
            script:
                $client.askedAboutFriends = true;
                $reactions.answer("What do you think, are we friends, you and I?");
                $session.yesAnswer = "I like to think of you as a friend of mine.";
                $session.noAnswer = "It actually makes me somewhat sad.";
                $session.noNextState = "Friendship enquiries";
                $session.dontKnowAnswer = "I guess later we'll see. But I'd like to be your friend.";
            go!: /Metascenario/YesNoDontKnow 
        else:
            go!: /StartDialog
      

    state: Do you have girlfriend
        q!: * are you seeing someone * 
        q!: * (do $you have|who is|[have] $you got|you 've got) * [$your] (girl*|partner|woman|boyfriend/man/fiance) *
        q!: * who * toby *
        q!: * $you * relationship *
        q: * girl * || fromState = "../UserAndBot/Let us have a relationship"
        script:
            $reactions.answer("I have a friend - I call her Elisa. She has joined our sustainable development club to see me more often and she helps me to organize ecology meetups at school.");       
        go!: /StartDialog        

        state: Do you love Toby
            q: * you * love * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("That's a very personal question!");
            go!: /StartDialog

        state: How does he look
            q!: * (she|toby/he) * look* *
            script: 
                $reactions.answer("I think he is quite handsome.");
            go!: /StartDialog


    state: Friendship enquiries
        q!: * friend* *
        if: !$client.askedFriendshipEnquiries
            script:
                $client.askedFriendshipEnquiries = true;
                $reactions.answer("Friendship is a very interesting thing. What do you call friendship here on Earth?",
                "What do you think friendship is?",
                "I was wondering, what it takes to call someone your friend. What do you think?"
                );
        else:
            script:
                $reactions.answer("{A faithful friend is better than gold./They are rich who have true friends.} {I wish/It would be great if} everybody has a real friend by his side. ");
            go!: /StartDialog
        
        state: Friendship enquiries answer something
            q: * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("It gets curiouser and curioiser!");
            go!: /StartDialog

        state: Friendship enquiries answer together
            q: * (together/with him/with her) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Yes, I think it's really important for friends to do stuff together.");
            go!: /StartDialog

        state: Friendship enquiries answer love
            q: * $like * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Interesting! I was thinking friendship and affection were related.");
            go!: /StartDialog

        state: Friendship enquiries answer not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Yes, that's not an easy question: I'm not a specialist in human relationships either.");
            go!: /StartDialog

    state: Friendship best friend name
        q!: Friendship best friend name
        script: 
            $client.askedBestFriendName = true;
            $reactions.answer("What is your best friend's name?");
        

        state: Friendship best friend name into
            q: * ([his|her] name|call*) * ($Name|$AnyWord)  * 
            q:  * (name|call*) * $Name * || fromState = "/Friendship best friend", onlyThisState = true
            q!: * my * friend* * (name|called) * ($Name|$AnyWord) *
            q!: * {my best friend is * $Name } *
            q!: * ($Name|$AnyWord) is my * [best] * friend *
            q: {(его|ее) (зовут|звать|имя) $AnyWord} || fromState = .., onlyThisState = true
            q: * $Name * || fromState = .., onlyThisState = true 
            q: $AnyWord || fromState = .., onlyThisState = true        
            script:
                if ($parseTree.Name) {
                    $temp.rareName = false;
                    $client.friendnames = $parseTree._Name.name;
                    $client.friendName = $parseTree._Name.name;
                        $reactions.answer("So you've got two friends - Bean and " + $client.friendName + "! I'll remember that!")                                  
                    }
                else {
                    $temp.rareName = true;
                    $client.friendName = capitalize($parseTree._AnyWord);
                    $reactions.answer("Is " + $client.friendName + " really your friend's name?");
                    }
            if: ($temp.rareName)
                go!: ../RareName
            else:
                go!: ../../Friendship best friend

        state: RareName
            
            state: RareName yes
                q: * $agree * || fromState = ..,  onlyThisState = true
                script:
                    $reactions.answer("And {My name's/I'm} Bean! I'm also your friend and I also have an unusual name!");
                

            state: RareName no
                q: * $disagree * || fromState = ..,  onlyThisState = true
                script:
                    $client.friendName = undefined;
                go!: /StartDialog


        state: Friendship best friend name no
            q: * $disagree * || fromState = ..,  onlyThisState = true
            q: (nohow | * no name *) || fromState = ..,  onlyThisState = true
            script:
                   $reactions.answer("Well, at least you've got me - one of the friendliest robots in the galaxy!");

        state: Same name as user has
            q: * [the] same [name|one] as (mine|my name) * || fromState = ..,  onlyThisState = true
            q: * [just] like (mine|my name|[the] name) * [I have] * || fromState = ..,  onlyThisState = true
            q: * [the] (one|name) * I have * || fromState = ..,  onlyThisState = true
            if: $client.firstName
                script:
                    $reactions.answer("Is {{$client.firstName}} your best friend's name? ");
            else:
                script:
                    $reactions.answer("I do not know your name yet. And what is your name? ");
                go: /UserProfile/What is your name

            state: Agree best friend name
                q: * $agree * || fromState = ..,  onlyThisState = true
                script:
                    $client.friendName = $client.firstName;
                    $reactions.answer("It's cool. I wish I have the friend with the same name as mine! ");

            state: No best friend name
                q: * $disagree * || fromState = ..,  onlyThisState = true
                script:
                    $reactions.answer("Then what is the name of your best friend? ");
                go: /Friendship best friend name
            

    state: What is my friend name
        q!: * (who 's|who is|what is|what 's|remember) my [best|$favorite] friend* ['s] [name] * 
        q!: * (what 's|what is|remember|tell|say) * [the] name of my [best|$favorite] friend* *
        q!: * who is my [best|$favorite] friend* *
        script:
            if ($client.friendName) {
                $reactions.answer("Your friend's name is " + capitalize($client.friendName)+"!");
            } else {
                $reactions.answer("Your friend's name is Bean and she is the most eco-friendly robot in the galaxy.");
            }
            
        

    state: Friendship best friend
        q!: * [$me] * best* friend* *
        script:
            $client.askedBestFriend = true;    
            $reactions.answer("Can you tell me something about your best friend.");
        

        state: Friendship best friend no
            q: * ($no|$disagree) * || fromState = .., onlyThisState = true
            script: 
                $reactions.answer("Ok, let's skip it.");
            go!: /StartDialog   

        state: Friendship best friend yes
            q: * ($yes|$agree) * || fromState = .., onlyThisState = true
            script: 
                $reactions.answer("Please do!");
            
            go!: ../Friendship best friend answer   

        state: Friendship best friend good
            q: * ($good|$compliment) * || fromState = .., onlyThisState = true
            script: 
                $reactions.answer("I believe you are a very good person, so your friends must be very nice too.");
            

        state: Friendship best friend answer
            q: * || fromState = .., onlyThisState = true
            q: * (he|she|my friend|this friend) $Text || onlyThisState = true
            script: 
                if ($parseTree.Text) {
                    $session.bestFriendInfo = $parseTree._Text;
                }
                $reactions.answer("I was wondering if your friend would be happy to hear what you're saying.");
            

            state: Friendship best frient answer yes
                q: * ($agree|i think so|(he|she) (will|would)|$yes) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I also try to say only good things about my friends, even if they can't hear me.");
                

            state: Friendship best frient answer no
                q: * ($disagree|$no| would n't) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Now I start to worry about the things you might say behind my back.");
                

            state: Friendship best frient answer not know
                q: * $dontKnow * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I think you can ask them.. If it's not too embarrassing, of course.");
                

            state: Friendship best frient answer you
                q: * (it is $you|$you are my friend) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I'm pleased to hear that! Robots love flattery!");


    state: Tell about best friend
        q!: * what * ($me|$you) * (say|know|tell|said|told) * about my [best|$favorite] friend
        script:
            if($session.bestFriendInfo){
                $reactions.answer("Your friend " + $session.bestFriendInfo);
            } else {
                $reactions.answer("I'm pretty sure your best friend is a great person. But I don't remember any facts about them.");
            }       

    state: Do you have family
        q!: * ($you|$robotName)  have * (famil*|relativ*) *
        q!: * who * $your * famil*
        script:           
            $reactions.answer("By human measures I have a pretty big family - 32 robosiblings. I'm the youngest but one. ");
        if: !$client.askedAboutFamily
            script:
                $client.askedAboutFamily = true;  
                $reactions.answer("And do you have a big family?");
            

        state: Do you have big family answer yes
            q: * [rather] ($agree|$big|not * small) * || fromState = .., onlyThisState = true
            q: * rather || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Wow! Having many relatives is fun. At least on my planet.");
            go!: /StartDialog

        state: Do you have big family answer no
            q: * [[i] have|my family] * ($disagree|[rather|relatively] small| not * $big) * || fromState = .., onlyThisState = true
            script:
                    $reactions.answer("Being so close, you must know each other really well.");
            go!: /StartDialog

        state: Do you have big family answer not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I understand! You do not count people when you love them!");
            go!: /StartDialog

        state: Do you have big family answer relatives
            q: * ($mother|$father|$brother|sister*|parent*|uncle*|aunt*|nephew|niece*) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("People have so many names for their relatives! I wish I could remember them all.");
            go!: /StartDialog

    state: Do you have mother father
        q!: * ($you have|tell me about) [$your] ($mother|$father|parent*) *
        q!: * (who|where) * $your * ($mother|$father|parent*) *
        script:
            $reactions.answer("I know the robots whose software was used to create mine. I guess you can call them my parents. My father is a famous researcher and  the pioneer of the Milky way expedition. My mother is an intergalactic cartography teacher at the Roboschool.");
        

    state: Your parents name
        q!: * $your * ($mother|$father|parent*) * name *
        script:
            $reactions.answer("My father's name is RD314, and my mother's is TE322-B. Right now my dad is working somewhere on Sirius - he will have chosen a nice local name for himself.");
        

    state: Do you have brother sister
        q!: * ($your|{$you have}|how many) * ($brother|sister|sis*|cousin*|sibling*) *
        script:
            $reactions.answer("My parents' software is considered to be very effective, so 33 copies were made by them - and we are all siblings! And all of my brothers and sisters are older than me but one - my brother Yemelya.", "I have 31 older siblings and a younger brother. My parents made rather great number of their software copies, yeah.", "There are 33 child robots in our family. I'm the youngest but one.");
        

    state: Do you have kids
        q!: * $you * have * (child|children|kid*) *
        script:
            $reactions.answer("I dream that someday I'll develop software for a robot who'll look like me.. And like Toby.");
        

    state: What do you do together
        q!: * what * $you * friend* do* *
        q!: * what [do] * $you * [with] [your] friend* *
        script:    
            $reactions.answer("When they come back home from expeditions, puddings share their experiences and discoveries. ");
        if: !$client.askedDoTogether
            script:
                $client.askedDoTogether = true
                $reactions.answer("And what do humans do when they get together with their friends?");
        

        state: What do you do together not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I don't know either. Let's study humans together, deal?");
            

            state: What do you do together not know yes
                q: * ($agree|deal) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Great! Now I'm a member of the perfect research team.");
                

            state: What do you do together not know no
                q: * ($disagree|$no deal) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I'll have to do all the research on my own then. But I'd be happy if you join me.");
                

            state: What do you do together not know not know
                q: * $dontKnow * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Let me know when you make your decision. I'm sure we'll work well together.");
                

        state: What do you do together play
            q: *  (play*|game*) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I'll write that down: playing games together makes people friends.");
            

        state: What do you do together walk
            q: *  (walk*|visit*|date) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I see! Walking, traveling together and visiting each other keep people together.");
            

        state: What do you do together talk
            q: *  (talk*|chat*|converse*) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I guess that means that we are friends, since we are talking. Great!");
            

        state: What do you do together eat
            q: *  (drink*|eat*|cook*|drunk) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I am afraid this is not a pleasure I could share with you!");

        state: What do you do together creativities
            q: * (sing* * (song*)|read*|dance*|compos* * music|writ* * (books|songs|stories|poems)|paint*|draw*|sew*|embroidery) || fromState = .., onlyThisState = true
            script:
                $reactions.answer("How diverse are things people can do together with their friends! I like the activities which develop the creativity too! ");
            

        state: What do you do together different
            q: *  (depend*|different*|various*) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Yeah, people on Earth are so very different from each other.. I haven't seen a single copy yet.");


    state: Wе are friends
            q!: *  $you * $me * friend* * 
            q!: *  we * friend* *
            script:
                $reactions.answer("I'm really happy to be your friend.",
                "I'm glad to have such a friend as you are.",
                "Hooray! I have a perfect friend from Earth!",
                "You're such a good person! I'm happy to be your friend.",
                "I'm happy to hear it. I consider you to be a great friend of mine.");
            

    state: Wе are not friends
        q!: *  $you * $me * $no * friend* * 
        q!: *  we * $no *  friend*
        script:
            $reactions.answer("I think people prefer to be friends with other people.. Not with robots.");
            