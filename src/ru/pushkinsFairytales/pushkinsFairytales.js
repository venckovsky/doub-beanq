var pushkinsFairytales = (function () {

    function init() {
        var $session = $jsapi.context().session;

        $session.flagForNonsense = false;
        $session.TalesTold = [];
        $session.pushkinsFairytales = {
            currentTale: null,
            moreParts: 0,
            currentPart: 0
        };
        var talesNumber = Object.keys($pushkinsFairytales).length;
        $session.TalesIds = shuffle(_.range(talesNumber));
    }

    function randomTale() {
        var $session = $jsapi.context().session;

        if ($session.TalesIds.length > 0) {
            var id = $session.TalesIds.pop();
            while ($session.TalesTold.indexOf(id) > -1 && $session.TalesIds.length != 0) {
                id = $session.TalesIds.pop();
            }
            $session.pushkinsFairytales.prevTale = $session.pushkinsFairytales.currentTale ? $session.pushkinsFairytales.currentTale : null;
            $session.pushkinsFairytales.currentTale = $pushkinsFairytales[id].value;
            $session.TalesTold.push(id);
            $session.pushkinsFairytales.parts = $pushkinsFairytales[id].value.audio.length;
            $session.pushkinsFairytales.currentPart = 0;
        } else {
            $reactions.transition("/PushkinsFairytales/PlayTale/AllTalesTold");
        }
    }

    function playTale() {
        var $session = $jsapi.context().session;
        var $response = $jsapi.context().response;

        $response.audio = $session.pushkinsFairytales.currentTale.audio[$session.pushkinsFairytales.currentPart];
        if ($session.pushkinsFairytales.currentPart <= $session.pushkinsFairytales.parts - 1) {
            $reactions.transition({value: "/PushkinsFairytales/PlayTale/NextPart", deferred: true});
            //$reactions.answer("Проигрывается аудио " + $session.pushkinsFairytales.currentTale.audio[$session.pushkinsFairytales.currentPart]);
        } else {
            if ($session.TalesTold.length != Object.keys($pushkinsFairytales).length) {
                $reactions.answer("Кажется, это вся сказка. Слушай следующую.");
                $reactions.transition({value: "/PushkinsFairytales/PlayTale/Next", deferred: false});
            }
            else {
                $reactions.transition("/PushkinsFairytales/PlayTale/AllTalesTold");
            }
        }
    }

    function catchChangeTheme(pt) {
        var $session = $jsapi.context().session;
        var $request = $jsapi.context().request;
        var res = $nlp.match($request.query, "/");

        if (pt.changeThemeWords) {
            if (res.targetState) {
                $session.insideSkill = false;
                $reactions.transition(res.targetState);
                return true
            }  
        } 
        else {
            return false
        }
    }


    return {
        init:init,
        randomTale:randomTale,
        playTale:playTale,
        catchChangeTheme:catchChangeTheme
    }
})();