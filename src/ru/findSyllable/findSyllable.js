var findSyllable = (function () {

    function init() {
        var $session = $jsapi.context().session;
        var $client = $jsapi.context().client;

        if ($client.age > 5) {
            $session.dictionary = syllableHard;
        } else {
            $session.dictionary = syllableEasy;
        }

        $session.numberOfQuestions = Object.keys($session.dictionary).length;
        $session.questionsIds = shuffleArr(_.range(1, $session.numberOfQuestions + 1));
    }


    function getSyllable(){
        var $session = $jsapi.context().session;
        
        if ($session.questionsIds.length == 0) {
                $reactions.answer("Кажется, мы отгадали все слоги, даже самые трудные. У нас с тобой отличный слух и хорошее внимание! Во что хочешь поиграть сейчас?");
                $reactions.transition("/BeanqGames");
        } else {
            var id = $session.questionsIds.pop();
            $session.lastGroup = $session.dictionary[id].value;
            $session.lastGroupWords = shuffleArr($session.dictionary[id].value.words);
            $session.words = "";

            for (var i = 0; i < $session.lastGroupWords.length; i++) {
                $session.lastGroupWords.length - i == 1 ? $session.words += $session.lastGroupWords[i] : $session.words += $session.lastGroupWords[i] + ", ";
            } 
        }
    }

    function checkAnswer(answer){                           //проверка совпадения ответа пользователя с вариантами ответов
        var $session = $jsapi.context().session;
        var res = $nlp.matchExamples(answer, $session.lastGroupWords);

        if (res){
            return true;
        } else {
            return false;
        }
    }

    return {
        init:init,
        getSyllable:getSyllable,
        checkAnswer:checkAnswer
    };
})();