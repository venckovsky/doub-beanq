require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: patterns.sc
    module = zb_common

require: pushkinsFairytales.js

require: ../../dictionaries/PushkinsFairytales.csv
    name = pushkinsFairytales
    var = $pushkinsFairytales

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .pushkinsFairytales = function(parseTree) {
            var id = parseTree.pushkinsFairytales[0].value;
            return $pushkinsFairytales[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("pushkinsFairytales");

patterns:
    $fairytalesName = $entity<pushkinsFairytales> || converter = $converters.pushkinsFairytales
    $dontCare = (все равно|пофиг*|плевать|неважно|не [$AnyWord] важно|побоку|(без|никакой|какая) разниц*|как* угодно|без понятия)
    $changeThemeWords = (хочу/давай/включи/поиграем/поиграй/играть/поговори/расскажи/скажи/создай/загадай/открой/запусти/поставь/давай (играть/поиграем/слушать/послушаем/поговорим/говорить))

theme: /PushkinsFairytales

    state: Enter || modal = true
        q!: * {[*скажи|скажи*|*читай|*чти|включ*|~слушать|послуша*|хоч*|хотел*|расскажи] * (~сказки|~сказка|сказочк*|что-нибудь) * ~пушкин} *
        q!: * {(*скажи|скажи*|*читай|*чти|включ*|~слушать|послуша*|хоч*|хотел*|расскажи) * [~сказки|~сказка|сказочк*|что-нибудь] * ~пушкин} *
        q: * {(*скажи|скажи*|*читай|*чти|включ*|~слушать|послуша*|хоч*|хотел*) * (~сказка|сказочк*|что-нибудь)} * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
        script:
            $session.insideSkill = true;
            if ($client.opened == undefined) {
                $client.opened = true;
                pushkinsFairytales.init();
                $reactions.answer('У меня в памяти много сказок, которые давным-давно написал талантливый землянин Александр Сергеевич Пушкин. Я слышал, что это был выдающийся поэт и просто очень интересный человек. Давай послушаем вместе. Если захочешь остановиться и поиграть во что-то другое, просто скажи "хватит". Какую сказку ты хочешь?')
            }
            else {
                $temp.rep = true;
                $session.flagForNonsense = false;
                pushkinsFairytales.init();
                $reactions.answer('Какую сказку хочешь послушать?')
            }
            

        state: WhatDoYouHave
            q: * (что|какие|какой|че) * (есть|знаешь|умеешь|можешь|хочешь) *
            q: * (назови|перечисли|предложи|выбери|выбирай|на твой выбор|на твое усмотрение|доверяю|полагаюсь|положусь|люб*|*нибудь|твою любимую|$dontCare) *
            q: * (~интересный|~хороший|~добрый|~веселый|~смешной|~поучительный) *
            random:
                a: Давай послушаем мою любимую сказку Пушкина.
                a: Вот самая интересная сказка.
                a: Слушай.
            script: 
                pushkinsFairytales.randomTale();
            go!: /PushkinsFairytales/PlayTale


        state: Nothing
            q: * ($notNow/$disagree/никак*/$stopGame) *
            q: * $stopGame * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * $stopGame * || fromState = "/PushkinsFairytales/PlayTale/CatchAll/IfContinue", onlyThisState = true
            q: * ($notNow/$disagree/никак*/$stopGame) * || fromState = "/PushkinsFairytales/PlayTale"
            q: * ($notNow/$disagree/никак*/$stopGame) * || fromState = "/PushkinsFairytales/PlayTale/NextPart"
            q: * ($no/$disagree/$stopGame) * || fromState = "/PushkinsFairytales/PlayTale/NextPart/No"
            q: * ($no/$disagree/не (буду/будем/хочу)/$stopGame) * || fromState = "/PushkinsFairytales/Enter/CatchAll/IfContinue", onlyThisState = true
            q: * ($no/$disagree/не (буду/будем/хочу)/$stopGame) * || fromState = "/PushkinsFairytales/PlayTale/NextPart/No", onlyThisState = true
            a: Мы обязательно вернемся в сказочную страну сказок Пушкина.
            script:
                $session.insideSkill = false;
            go!: /


        state: ChosenTale
            q!: * [*скажи/скажи*/~читать/~почитать/~зачитать/~прочитать] * [про/о] * $fairytalesName *
            q!: * {[~сказка|сказочк*] * $fairytalesName} *
            q: * {[хочу/давай/включи/поиграем/поиграй/играть/поговори/расскажи/скажи/создай/открой/запусти/поставь] * [~сказка|сказочк*] * $fairytalesName} *
            q: * [давай/включи/переключи/хочу/лучше/теперь/расскаж*/*скажи*/про/о] * $fairytalesName * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * [давай/включи/переключи/хочу/лучше/теперь/расскаж*/*скажи*/про/о] * $fairytalesName * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            q: * [давай/включи/переключи/хочу/лучше/теперь/расскаж*/*скажи*/про/о] * $fairytalesName * || fromState = "/PushkinsFairytales/Enter/CatchAll/IfContinue", onlyThisState = true
            script:
                if (!$session.pushkinsFairytales) {
                    $session.insideSkill = true;
                    pushkinsFairytales.init();
                    $reactions.answer(selectRandomArg("Слушай сказку. Если захочешь остановиться, просто скажи \"Хватит\".", "Если захочешь остановиться, просто скажи \"Хватит\". А сейчас слушай."))
                }
                else {
                    $reactions.answer("Слушай. Если захочешь остановиться, просто скажи \"Хватит\".")
                }
                $session.pushkinsFairytales.prevTale = $session.pushkinsFairytales.currentTale ? $session.pushkinsFairytales.currentTale : null;
                $session.pushkinsFairytales.currentTale = $parseTree.fairytalesName[0].value;
                $session.pushkinsFairytales.currentAudio = $session.pushkinsFairytales.currentTale.audio[0];
                $session.TalesTold.push($parseTree.fairytalesName[0].pushkinsFairytales[0].value);
                $session.pushkinsFairytales.parts = $pushkinsFairytales[$parseTree.fairytalesName[0].pushkinsFairytales[0].value].value.audio.length;
                $session.pushkinsFairytales.currentPart = 0;
            go!: /PushkinsFairytales/PlayTale


        state: CatchAll
            q: * [$changeThemeWords] * || fromState = "/PushkinsFairytales/Enter", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/PushkinsFairytales/Enter/CatchAll/IfContinue", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/PushkinsFairytales/PlayTale/Previous/IfRepeat", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/PushkinsFairytales/PlayTale/NextPart/No", onlyThisState = true
            script:
                if (!pushkinsFairytales.catchChangeTheme($parseTree)) {
                    if ($session.flagForNonsense) {
                        $reactions.answer('Что-то скучновато стало.');
                        $session.flagForNonsense = false;
                        $session.insideSkill = false;
                        $reactions.transition("/");
                    } else {
                        $session.flagForNonsense = true;
                        $reactions.answer('Кажется, мы собирались послушать сказку Пушкина? Будем слушать?');
                        $reactions.transition("/PushkinsFairytales/Enter/CatchAll/IfContinue")
                    }
                }

            state: IfContinue || modal = true
                state: Yes
                    q: * ($yes|продолж*|буду|будем|$agree) * || fromState = "/PushkinsFairytales/Enter/CatchAll/IfContinue", onlyThisState = true
                    q: давай || fromState = "/PushkinsFairytales/Enter/CatchAll/IfContinue", onlyThisState = true
                    script:
                        $session.flagForNonsense = false;
                        if ($session.pushkinsFairytales.currentTale) {
                            if (($session.pushkinsFairytales.currentTale.audio.length - 1) == $session.pushkinsFairytales.currentPart) {
                            $reactions.transition("/PushkinsFairytales/PlayTale/Yes") }
                            else {
                                $reactions.transition("/PushkinsFairytales/PlayTale/NextPart/Yes") 
                            }
                        }
                        else {
                            $reactions.transition("/PushkinsFairytales/Enter") }


    state: GoOn
        q!: * (продолж*/дальше) * (~пушкин/$fairytalesName) *
        q: * (продолж*/дальше) * $fairytalesName * || fromState = "/PushkinsFairytales/Enter", onlyThisState = true
        q: * ({(сейчас|только что| * назад) * (слуша*|включ*|*игр*)}|прошл*|предыдущ*) * || fromState = "/PushkinsFairytales/Enter", onlyThisState = true
        q: * (продолж*/дальше) * $fairytalesName * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
        q: * (продолж*/дальше) * $fairytalesName * || fromState = "/PushkinsFairytales/PlayTale/NextPart/No", onlyThisState = true
        script:
            if ($session.insideSkill == false) {
                $reactions.answer("Кажется, мы остановились здесь.")
            }
            else {
                $session.pushkinsFairytales = $session.lastPushkinFairytale
            }
            // else { Если устройство с экраном, то $reactions.answer("На чем мы остановились? Сейчас ты увидишь на экране плеер. Выбери пальчиком нужный фрагмент или попроси взрослых помочь тебе.") + выводится плеер.  
        go!: /PushkinsFairytales/PlayTale/NextPart/Yes


    state: PlayTale || modal = true
        q: * [повтор*/*скажи/скажи*/~читать/~почитать/~зачитать/~прочитать] * (еще раз/занов*/с начал*/повтор*) * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
        q: * [повтор*/*скажи/скажи*/~читать/~почитать/~зачитать/~прочитать] * (еще раз/занов*/с начал*/повтор*) * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
        script:
            $temp.rep = true;
            $session.lastPushkinFairytale = $session.pushkinsFairytales
            pushkinsFairytales.playTale();

        state: NextPart || modal = true
            state: Yes
                q: * [давай] * (вперед/следующ*/дальше/далее/еще) *
                q: (~другой|следующ*) * (часть|отрывок|кусо*) *
                script:
                    $session.pushkinsFairytales.currentPart += 1;
                go!: /PushkinsFairytales/PlayTale

            state: No
                q: * {(не (хочу|надо|~рассказывать)) * (дальше|больше)} *
                a: Может быть, тогда послушаем другую сказку?
    
        state: Next
            q: * [давай/включи/переключи/хочу] * (друг*/следующ*/еще од*/дальше/далее/еще) * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * [давай/включи/переключи/хочу] * (друг*/следующ*/еще од*/дальше/далее/еще) * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            q: * (не хочу|только не|не) * эту * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * (не хочу|только не|не) * эту * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            q: * какую-нибудь (еще|другую) * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * какую-нибудь (еще|другую) * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            q: * ($yes|$agree|послуша*|слуша*|включ*|друг*|еще) * || fromState = "/PushkinsFairytales/PlayTale/NextPart/No", onlyThisState = true
            script:
                if ($session.TalesTold.length === Object.keys($pushkinsFairytales).length) {
                    $reactions.transition("/PushkinsFairytales/PlayTale/AllTalesTold")
                }
                else {
                    pushkinsFairytales.randomTale();
                }
            go!: /PushkinsFairytales/PlayTale

        state: PreviousPart
            q: * (предыдущ*|прошл*) * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * (предыдущ*|прошл*) * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            script:
                if ($session.pushkinsFairytales.currentPart > 0) {
                    $session.pushkinsFairytales.currentPart -= 1;
                    $reactions.transition("../../PlayTale");
                } else {
                    $reactions.transition("../Previous");
                }

        state: Previous
            q: * {(предыдущ*|прошл*|~который * (был*|включ*|игра*|*слуш*) * (перед|до)) * (сказк*|сказочк*) } * || fromState = "/PushkinsFairytales/PlayTale", onlyThisState = true
            q: * {(предыдущ*|прошл*|~который * (был*|включ*|игра*|*слуш*) * (перед|до)) * (сказк*|сказочк*) } * || fromState = "/PushkinsFairytales/PlayTale/NextPart", onlyThisState = true
            script: 
                if (!$session.pushkinsFairytales.prevTale) {
                    $reactions.transition("/PushkinsFairytales/PlayTale/Previous/IfRepeat");
                } else {
                    $temp.replaceTale = $session.pushkinsFairytales.currentTale;
                    $session.pushkinsFairytales.currentTale = $session.pushkinsFairytales.prevTale;
                    $session.pushkinsFairytales.prevTale = $temp.replaceTale;
                    $session.pushkinsFairytales.currentAudio = $session.pushkinsFairytales.currentTale.audio[0];
                    $session.pushkinsFairytales.parts = $session.pushkinsFairytales.currentTale.audio.length;
                    $session.pushkinsFairytales.currentPart = 0;
                    $reactions.transition("/PushkinsFairytales/PlayTale");
                }

            state: IfRepeat
                a: Кажется, это самая первая сказка. Хочешь послушать ее заново?

                state: Yes
                    q: * ($yes|продолж*|буду|хоч*|$agree) *
                    script:
                        $session.pushkinsFairytales.currentPart = 0;
                        $reactions.transition("/PushkinsFairytales/PlayTale");

                state: No
                    q: * ($no/$disagree/не (буду/хочу)) * 
                    a: Слушай тогда такую сказку.
                    script:
                        pushkinsFairytales.randomTale();
                    go!: /PushkinsFairytales/PlayTale

        
        state: AllTalesTold
            script:
                $session.TalesTold = [];
                $session.insideSkill = false;
            a: Это были все сказки Александра Сергеевича Пушкина из моей коллекции. У меня есть много других интересных игр и развлечений.
            go!: /
