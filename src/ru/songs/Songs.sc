require: ../fairyTales/fairy-tales-patterns.sc
require: songs-patterns.sc
require: Songs.js

require: ../../main.js
require: ../../patterns.sc

require: common/common.sc
  module = zenbox_common

init:
    $global.themes = $global.themes || [];
    $global.themes.push("Media");

theme: /Media

    state: Play
        q!: * ($kidsSong/$lullabySong) * [$mashaSong] *
        q: * ($kidsSong/$lullabySong) * [$mashaSong] * || fromState = .
        q!: * {({[поставь*|*слушать|услышать|спой|споем|споешь|включи|включай|ой]  [пожалуйста] (~песня|~песенка|песенки)}|$sing|сыграй) * [$smesharikiSong|$mashaSong|$lullaby] * [о|про] ($kidsSong|$lullabySong)} *
        q!: * {[поставь*|*слушать|услышать|спой|споем|споешь|включи|включай|ой] * (~песня|~песенка|песенки) * ($smesharikiSong|$mashaSong)} *        
        q!: * ((спой|споешь|споёшь|сыграй) [$me]|ты спел*|[давай] споем|{(давай* [лучше]|лучше) * $sing}|{[$you] * (умеешь|можешь) * петь}) [(песн*|песен*)] *
        q!: [$pls|давай] {($sing|давай|сыграй|порадуй|ключи|пожалуйста) [$me] * [хорош*|добрую|доброй|классн*|крутую|крутой|какую-нибудь] (песен*|песню|песней)}
        q!: {[$pls|давай] (поставь*|включи*|включай|сыграй|хочу|порадуй|сбой) [$you] [любим*] (песню|песня|песенка|песенку|песни|песенки) [* $you $like] }
        q!: [$pls|давай|хочу] (*слушать|услышать) [$you] [любим*] (песню|песня|песенка|песенку) * [$you] [$like] 
        q!: [$pls|давай] ($sing|сыграй) [$me] [(что нибудь|что-нибудь| [$you] [любим*] (песен*|песню) * [$you] [$like])]
        q!: ({[давай] (~песня|песенка|песенку)}|{(песенки|песни) давай})
        q!: * (спой|споёшь|споешь) [$me]
        q!: * {(можешь|мог бы|может|~приказывать) * $sing [$me]} *
        q!: * (давай/можешь/может/есть/какую-нибудь/хочу/пожалуйста) * [$you] * (~петь/~спеть/~песенка/~песня) * [$kidsSong|$lullabySong] * [$mashaSong]
        q!: * [включи/включай/ключи/поставь/спой|споёшь|споешь] * {(~музыка|песн*|песенк*|спой|споёшь|споешь|включи*) * [из] ($smesharikiSong|$mashaSong|$lullaby|$mashaBear::mashaSong)} *
        q: * {[$sing/давай] (что-нибудь|~любая|давай|пой)} * || fromState = "../Songs list", onlyThisState = true
        q: * {[$sing/давай] (что-нибудь|~любая|давай|пой)} * || fromState = "../Unknown song", onlyThisState = true
        q: * [$sing] * (что|~которая) {тебе [больше (всего|всех)] нрав*} * || fromState = "../Songs list", onlyThisState = true
        q!: * $sing * (что|~которая) {тебе [больше (всего|всех)] нрав*} *        
        q: * [$sing] * (что|~которая) {тебе [больше (всего|всех)] нрав*} * || fromState = "../Unknown song", onlyThisState = true        
        q: * [тогда|хочу] [о|про] $kidsSong * [$mashaSong] * || fromState = "../Songs list", onlyThisState = true
        q: * [тогда|хочу] [о|про] $kidsSong * [$mashaSong] * || fromState = "../Unknown song", onlyThisState = true
        q: [a] * *петь * || fromState = /Music, onlyThisState = true
        q!: [a] [ну] {[спой/ой/спай/свой/спорт/спою/под] (песенк*/песн*) [тогда] [$botName]}
        q!: * (песенк*/песн*) мне
        q!: {[$botName] [включ*] ($smesharikiSong|$mashaBear::mashaSong)}
        q: * [из мульт*] * ($smesharikiSong/$mashaSong) *
        q: давай || fromState = "../Play", onlyThisState = true
        q: (давай/ой) || fromState = "../Unknown song", onlyThisState = true
        q: * {[что-нибудь/что нибудь/что нидь] * ($turnOn/$continue/играй/сыграй)} * || fromState = "../TurnOffSong", onlyThisState = true
        script:
            $response.intent = "songs_on"; 
            processSongsDict();
            if ($parseTree.kidsSong) {
                $session.currentSong = $parseTree.kidsSong[0].value;           
            } else if ($parseTree.lullabySong) {
                $session.currentSong = $parseTree.lullabySong[0].value;       
            } else if ($parseTree.lullaby) {
                var songN = selectRandomArg.apply(this, Object.keys($Lullaby));
                $session.currentSong = $Lullaby[songN].value;
            } else if ($parseTree.smesharikiSong || $temp.smesharikiFromMusic) {
                $session.currentSong = selectRandomArg.apply(this, $client.smesharikiSongs);
            } else if ($parseTree.mashaSong || $temp.mashaFromMusic) {
                $session.currentSong = selectRandomArg.apply(this, $client.mashaSongs);
            } else {
                var songN = selectRandomArg.apply(this, Object.keys($KidsSongs));
                $session.currentSong = $KidsSongs[songN].value;
            }
            $response.audio = $session.currentSong.stream;
            $response.name = $session.currentSong.title;
        if: ($response.audio != '')
            random:
                a: Ну, слушай!
                a: Сейчас спою!
            a: Это песня '{{$session.currentSong.title}}'.
        else:
            a: Что-то я забыла все песни.
      
    state: AboutLullaby
        q: * {(включи/поставь/спой/~любимый/включай/пой/сыграй) * [одну/какую-нибудь/любимую]} * || fromState="/Preferences/Ask user about music", modal=true
        q: * {(включи/поставь/спой/~любимый/включай/пой/сыграй) * [одну/какую-нибудь/любимую]} * || fromState="/Preferences/Favorite music", modal=true
        q!: * {(включи/поставь/спой/~любимый/включай/пой/сыграй) * [одну/какую-нибудь/любимую] * $lullaby} *
        q!: {$lullaby [поешь/споешь/включи/пожал*/сыграй]}
        q: * $lullaby *
        script:
            var songN = selectRandomArg.apply(this, Object.keys($Lullaby));
            $session.currentSong = $Lullaby[songN].value;
            $response.audio = $session.currentSong.stream;
            $response.name = $session.currentSong.title;
        if: ($response.audio != '')
            random:
                a: Ну, слушай!
                a: Сейчас спою!
            a: Это песня '{{$session.currentSong.title}}'.
        else:
            a: Что-то я забыла все песни.

    state: Switch Song
        q: * {[давай/$singSong] (еще|другую|другое|продолжай|не эту)} *
        q: * [о|про] $kidsSong *
        q: * {[$turnOn] * ((следующую|следующий|следующее|дальше*|вперед|~другой)|(пред*|назад|верни*)) * (трек|дорожк*|песн*|музык*|мелод*)} *        
        go!: ../Play

    state: Don't like Music
        q: * (не нравится/не хочу/не люблю/бесит/скучно/хочу ~другой) * [(запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем] *слушать/хочу/давай) ([$any] ~другой/$Text)]
        random:
            a: Извини, думала, тебе понравится.
            a: А мне нравится. Вкусы у всех разные.
        go!: ../Switch Song

    state: Song name
        q: * {(это|сейчас|только что|играет|называется) * как* (пес*)} * 
        q: * (как*|что [это] за) (пес*)
        q: * {что * (сейчас|ты) * (играет|пел|поешь)} *
        a: Эта песенка называется '{{$session.currentSong.title}}'         

    state: Unknown song
        q!: * ($sing|знаешь) (~песня|~песенка) * [о|про] ($Text/$kidsFairyTale/$smesharikiTale) *
        q!: (~песня|~песенка) * [о|про] ($Text/$kidsFairyTale/$smesharikiTale) *
        q!: * [$pls|давай] $sing * [о|про] $Text *
        q: * [тогда] [о|про] $Text * || fromState = "../Unknown song", onlyThisState = true
        random:
            a: Что-то не помню такой песни.
            a: Что-то не могу найти такую песню.
            a: Что-то не нахожу такой песни.
            a: Что-то я забыла такую песню.
            a: Я не на нашла у себя такой песни.
        random:
            a: Пока я знаю совсем немного земных песен, но надеюсь, что в ближайшем будущем буду пополнять   свою коллекцию!
            a: Я ещё не все песенки выучила.
        a: А пока могу проиграть {{getSongs()}}

    state: TurnOffSong
        q!: * {($turnOff|[поставь на] пауз*|запаузи*) * (пес*|музык*)} *
        q!: * поставь на паузу *
        q: * ($turnOff|$shutUp|стоп|хватит|надоело|достаточно|довольно|спасибо|тихо|заткни*|заткнись|заглохни*|умолкни*|стой|молчи*|*молчать|*молкни|тишин*|[поставь на] пауз*|запаузи*|*молчи|закончи|заканчивай|не пой|(хватит|прекрати|перестань) [петь]) * || fromState = "/Media/Play"
        script:
            $response.intent = "songs_off";
        random:
            a: Песенка окончена, спасибо за внимание!
            a: Спасибо за внимание, песня окончена!
        
    state: Songs list
        q!: * {(как*|что|про (что|кого)) * (~песня|песенки|*петь|~музыка) * [$you] (знаешь|помнишь|можешь|умеешь|есть|имеешь|поешь)} *
        q!: * {$you (знаешь|помнишь) * (~песня|песенки)} *
        q: * {(еще|ещё) [[о|про] (кого|что|чем)|как*]} * || fromState = "/Media/Songs list", onlyThisState = true
        a: Я могу проиграть песенки {{getSongs()}}. И, кстати, выучила кое-что ещё из земных песен.

    state: Play From List
        q: {[ну] (*играй/сыграй/давай) [$botName]} || fromState = "../Unknown song", onlyThisState = true
        q: {[ну] (*играй/сыграй/давай) [$botName]} || fromState = "../Songs list", onlyThisState = true
        if: $session.listOfSongs
            script:
                $session.currentSong = $session.listOfSongs[2];
                $response.audio = $session.currentSong.stream;
                $response.name = $session.currentSong.title;
            if: ($response.audio != '')
                random:
                    a: Ну, слушай!
                    a: Сейчас спою!
                a: Это песня '{{$session.currentSong.title}}'.
            else:
                a: Что-то я забыла все песни.              
        else:
            go!: ../Play

    state: Songs i dont want
        q!: * {(не (хочу|хочется|надо|нужно|пой)) * $singSong} * [$ftale] *
        q!: * [мне] [сейчас] не до (~песня|песенк*) *
        q: * (не (хочу|хочется|надо|нужно|пой)) * [$ftale] * || fromState = "/Media/Play", onlyThisState = true
        q: * (не (хочу|хочется|надо|нужно|пой)) * [$ftale] * || fromState = "/Media/Unknown song", onlyThisState = true
        q: $disagree || fromState = "/Media/Unknown song", onlyThisState = true
        if: $parseTree.ftale
            #go!: /Play FairyTale
            script:
                $reactions.answer('Сказок пока нет')
        else:
            random:
                a: Хорошо, тогда в другой раз! Я знаю много замечательных песенок.
                a: Ну ладно, тогда спою в другой раз!
                a: Жаль... А я уже успела выучить несколько земных песен...