require: lullabies.csv
    name = Lullaby
    var = $Lullaby
require: smeshariki-songs-ru.csv
    name = SmesharikiSongs
    var = $SmesharikiSongs
require: songs-ru.csv
    name = KidsSongs
    var = $KidsSongs

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters.KidsSongsTagConverter = function(parseTree) {
            var id = parseTree.KidsSongs[0].value;
            return $KidsSongs[id].value;
        };
    $global.$converters.SmesharikiSongsTagConverter = function(parseTree) {
            var id = parseTree.SmesharikiSongs[0].value;
            return $SmesharikiSongs[id].value;
        };
    $global.$converters.LullabyTagConverter = function(parseTree) {
            var id = parseTree.Lullaby[0].value;
            return $Lullaby[id].value;
        }; 

patterns:
    $kidsSong = $entity<KidsSongs> || converter = $converters.KidsSongsTagConverter
    $lullabySong = $entity<Lullaby> || converter = $converters.LullabyTagConverter

    $sing = (пой|спой|споешь|споёшь|споем|~петь|попеть|спеть|пропеть|пропой|пропоем|споем)
    $singSong = ($sing|~песня|~песенка/песенку) [~песня|~песенка]

    $smesharikiSong = (смешарик*/мульт*)
    $mashaSong = (маша/маши/машины/от маши/из маши/не [из/хочу [про]] смешарик*) [и медвед*]
    $mashaBear = (маша/машу/мажу) [и] медведь
    $lullaby = (колыбельн*/на ночь/чтобы уснуть|убаюкивающ*|успокаивающ*|успокоительн*|релакс*)