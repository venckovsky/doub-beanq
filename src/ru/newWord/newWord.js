var newWord = (function() {
    
    function init() {
        var $session = $jsapi.context().session;
        var $client = $jsapi.context().client;

        switch ($client.age) {
            case 4:
                $session.newWordDict = selectRandomArg($newWord34, $newWord45);
                break;
            case 5:
                $session.newWordDict = selectRandomArg($newWord45, $newWord56);
                break;
            case 6:
                $session.newWordDict = selectRandomArg($newWord56, $newWord67);
                break;
            default:
                if ($client.age < 4) {
                    $session.newWordDict = $newWord34;
                } else {
                    $session.newWordDict = $newWord67;
                }
        }
    }

    function getWord() {
        var $session = $jsapi.context().session;
        var $client = $jsapi.context().client;

        var data = getRandomElement($session.newWordDict);

        $client.newWord = {
            lastDate: currentDate().format("YYYY-MM-DD"),
            word: data["word"],
            definition: data["definition"]
        };        
    }

    function changeTheme() {
        var $request = $jsapi.context().request;
        var res = $nlp.match($request.query, "/");

        if (res.targetState) {
            $reactions.transition(res.targetState);
        }
    }

    return {
        init: init,
        getWord: getWord,
        changeTheme: changeTheme
    };
})();
