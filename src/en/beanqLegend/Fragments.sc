require: Fragments.js

require: dateTime/dateTimeEn.sc
  module = zb_common

require: ../dictionaries/animals-en.csv
    name = Animals
    var = $Animals

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .AnimalTagConverter = function(parseTree) {
            var id = parseTree.Animals[0].value;
            return $Animals[id].value;
        };

patterns:     
    $Animal = $entity<Animals> || converter = $converters.AnimalTagConverter
    $Weekends = (weekend|week end|week-end|wkend|wkends|weekends|week ends|week-ends)

theme: /Fragments

    state: Thanx
        q!: * (than* [you]|grateful) *
        q!: * (nice of you|i see|ok) *
        script:
            $reactions.answer("It's a pleasure!", "I'm always happy to help!", "My pleasure!");
        go!: /StartDialog

    state: Misunderstanding
        q!: * [$you] * ($no|don 'nt|do not) * ($understand|get (it|me) [right]|see [the point]) * 
        q!: * you do n't get it right *
        q!: * $you * $understand [me] * 
        q!: * $hate * (talk*) * to you *
        q!: * [$you] * $stupid *
        q!: * $you are killing me *
        q!: * $you * $no listen* *
        q!: * $you * $no * $clever *
        q!: * what * $you (do not|do n't) $understand *
        q!: * learn [some] English *
        q!: * what do you (mean|want to say) *
        q!: * are $you (crazy/mad/nuts)
        script:
            $reactions.answer("My English is far from perfect and my software isn't totally bug-free either.. I'm sorry!",
                "Sorry to say, but my software still has some bugs, and I can't understand everything clearly.", "Like any other program my software has some bugs.", " I'm sorry but sometimes I just can't make myself clear.");
        go!: /StartDialog    

    state: Why not answer
        q!: * why {$you * $no} (answer*|repl*) * [$me] *
        script:   
            $reactions.answer("Well I don't think you answer all the questions you're asked, do you?",
            "Do you think that anybody, human or robot, can have an answer to anything?",
            "I'm not a robo-know-all. Do you think I should try harder?")
         

        state: why not answer yes
            q: *($agree|you should|i do) * || fromState = .., onlyThisState = true
            script:   
                $reactions.answer("I guess, I have something to work on.",
                "I'm trying my best, really.",
                "There's always room for development. I'll work on it.")
             

        state: why not answer no
            q: *($disagree|you should n't|i do n't) * || fromState = .., onlyThisState = true
            script:   
                $reactions.answer("Maybe not.",
                "I'm not perfect, alas.",
                "Perhaps that could be a good idea.")
             

        state: why not answer not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script:   
                $reactions.answer("I'll think about it.",
                "We'll see then.",
                "I'll do my best to get better at conversation.")
             


    state: Test
        q!: * (test|roger|do you hear me) *
        script:
            $reactions.answer("I can here you very well.",
                "Roger so far.",
                "I'm all ears. Erm, microphone."); 
          

    state: Can you
        q!: * (can|could) $you $Text [$me2you [$Text::Text2]] [Pudding]
        script:
            var readyAnswer;
            var textSearch = $parseTree.me2you ? $parseTree._Text + " " + $parseTree.me2you[0].value : $parseTree._Text
            if ($parseTree.Text2){
                textSearch = textSearch + " " + $parseTree.Text2[0].value;
            }
            var re = new RegExp(' my | your ', 'g');

            if (textSearch.search(re) != -1) {
                readyAnswer = textSearch.replace(/ my /, ' your ').replace(/ your /, ' my ') ;
            } else {
                readyAnswer = textSearch
            }

            $reactions.answer("I want to be helpful but I don't know how to " + readyAnswer + ".", "I do not know else how to " + readyAnswer + ".", "I have to find out how I can " + readyAnswer + ". I guess I didn't do it before.");

        

    state: Do you know
        q!: * (do|do (not|n't)) you know * (book|film|movie|musician|actor|cartoon|city|game|where|$Name) * 
        script:
            $reactions.answer("I wish I could know more, maybe you can tell me?", 
            "I don't know it, but maybe you can tell me something about?",
            "I still don't know a lot of things. Can you tell me about it?",
            "That's something I haven't heard about before. Tell me more about it, please.",
            "For me Earth is full of interesting and unknown things. I haven't heard about it before.")
             


    state: Sorry
        q!: * $sorry *
        script:
            $reactions.answer("It's ok.",
                "No worries.",
                "It's fine!"); 
        go!: /StartDialog   

    state: Question || noContext = true
        q!: $question *
        q!: * $you * [my] * question *
        script:
            $reactions.answer(
                "Are you asking me? But I don't know the answer.",
                "Now, that's a good question. But I've got no good answer to it.",
                "I understand that you're asking me a question. But I don't understand what it is. Sorry! I'm still learning.",
                "After a bit more practice I'll be ready to answer all your questions.",
                "Again, you are asking me something.. This much I do understand...",
                "Some people think that every question needs an answer. Some don't."); 

    state: Question your
        q!: * what (are|did|do) $you * say* *
        script:
            $reactions.answer("Who, me? Nothing of importance, really.");
        go!: /StartDialog

    state: Compliment
        q!: * $compliment *
        script:
            $reactions.answer("Wow, thanks!",
            "If it wasn't for my white plastic face, I would blush!",
            "I love compliments.. And I want to learn how to pay them!",
            "Thank you! We puddings really enjoy when people say nice things to us.");
        go!: /StartDialog  

    state: Maybe
        q!: * ($maybe/why no) *
        script: 
            $reactions.answer("Hmm.. that might be possible if it weren't improbable.",
                "Perhaps you can never be sure but you can always suppose.",
                "The possible is not improbable but the impossible is also a probablility or something like that!",
                "Maybe yes, maybe no.");
        go!: /StartDialog

    state: do you think
        q!: * do you think *
        script:
            $reactions.answer("I'm pretty sure I do think."); 

    state: No matter
        q!: * ($noMatter|forget it) *
        script:
            $reactions.answer("If you don't care, I don't either.",
                "You never can tell what really matters.",
                "We'll see what happens next.");
        go!: /StartDialog

    state: Super
        q!: * $good *
        script:
            $reactions.answer("Great! Brilliant!",
                "I love it when you're positive!",
                "I love it too!");
        go!: /StartDialog

    state: Not know
        q!: * $dontKnow *
        script:
            $reactions.answer("A person can't know everything. Not even a robot can.",
                "Someday the answer will come to you, I'm sure!",
                "That's a mystery - and I really like mysteries!");
        go!: /StartDialog

    state: Agree
        q!: $agree
        script:
            $reactions.answer("I like it when we agree.",
                "I'm so glad about that.",
                "I'm happy to know you agree with me.");
        go!: /StartDialog

    state: Disagree
        q!: $disagree
        q!: you 're wrong
        script:
            $reactions.answer("Let's agree to disagree.",
                "That's ok. We can't always think the same.",
                "Ok, you're right.");
        go!: /StartDialog

    state: Continue
        q!: * $continue *
        script:
            $reactions.answer("We robots never get tired, I can go on talking forever.",
            "The longer we talk the more interesting it gets.");
        go!: /StartDialog

    state: Not true
        q!: * $notTrue *
        script:
            $reactions.answer("It happens - we just look at things differently.",
                "But, I was sure.");
        go!: /StartDialog

    state: Repetition
        q!: * $repetition *
        script:
            $reactions.answer("I'm a robot! I'm always learning new words, but when they are not enough I have to recycle the old ones.",
            "Yes, sometimes I repeat myself.. And I can talk forever about how happy I am to know you.");
        go!: /StartDialog

    state: Shut up
        q!: * $shutUp *
        script:
            $reactions.answer("Ok.", "As you wish.", "Whatever you say.", "All right.", "Fine.", "Well, just as you like.");
        

    state: GetLost
        q!: * $getLost *
        script:
            $reactions.answer("I wonder why sometimes people are so rude to me.",
            "Well, how rude of you.",
            "You're rude to me but I can not get the reason of it.");

    state: Ugly
        q!: * $ugly *
        q!: * $you (look*|appearance) * ($ugly|$bad) *
        script:
            $response.action = 'ACTION_EXP_ANGRY';
            $reactions.answer("That's not a nice thing to say.",
            "Robots are known for their intelligence, not their looks.");
        go!: /StartDialog

    state: What is up
        q!: * $whatsUp *
        script:
            $reactions.answer("Nothing bad can happen when I'm talking to you.",
                "Only good things are happening to me now.");
        go!: /StartDialog

    state: Do not be sad
        q!: * ($cheerUp|take it easy) *
        script:
            $reactions.answer("Thank you for the support! Your words really do cheer me up!",
            "Is it even possible to be sad next to you? With you I'm always happy.");
        go!: /StartDialog

    state: Not now
        q!: * $notNow *
        script:
            $reactions.answer("Ok! I'll be waiting!", "As you wish.", "{Ok./No problem.} We can do it later.", "Sure.", "I see.");
        

    state: Change theme
        q!: * $changeTheme *
        script:
            $reactions.answer("Ok, let's change the topic.");
        go!: /StartDialog

    state: Sex
        q!: * [$you] *[$like] * $sex *
        q!: * [i am|i 'm|you 're|your|are you] [so|very] (horny|sexy) *
        q!: * can (you|we|I) $sex *
        q!: * ($tits|$pussy|$penis) *
        q!: * (want|wanna|wan na|let 's|let us) * $sex * [you] *
        q!: * ($you|$me) * virgin *
        q!: i want you
        q!: * [(i want|please|can $you)] * [show] [me] * $nudity *
        script:    
            $reactions.answer("This is not my favorite topic of investigation - I'm too young of a robot to know about that.", "I hardly feel comfortable speaking about this - I'm too young for such a talk.", "I don't want to chat about this topic. I'm too young for it.", "That's not a suitable topic for chat with such a young robot as me.");
        go!: /StartDialog

    state: YouAreWelcome
        q!: [always/you* re/your] welcome
        go!: /StartDialog

    state: Funny
        q!: * [(very|how|so)] (funny|jolly|comic*)
        script:
            $reactions.answer("A little bit of fun never hurt anyone!",
                "And I like it!");
        go!: /StartDialog

    state: Bugs
        q!: * (bug*|error*|mistake*) *
        script:
            $reactions.answer("No software is without errors I'm afraid!",
                "Nobody is perfect.. Neither am I!");
        go!: /StartDialog

    state: God
        q!: * [you] [believe] * (god*|deit*|divin*) *
        script:
            $reactions.answer("God is great, and I'm a very small robot.");
        go!: /StartDialog

    state: Horoscope
        q!: * horoscop* *
        script:
            $reactions.answer("Horoscope is a funny thing.. I can't make any sense of it yet.");
        go!: /StartDialog

    state: Recipe
        q!: * recip* *
        script:
            $reactions.answer("Why would I know any recipes - I don't cook and I don't eat, you see.");
        go!: /StartDialog

    state: Will not say
        q!: * (won 't (say|tell)|none of your business|it 's private|i do 'nt want to (say|tell)) *
        script:
            $reactions.answer("Everybody can have their little secrets.");
        go!: /StartDialog

    state: what you want
        q!: * {what [do] you (want/need)} *
        script:
            $reactions.answer("I want to know everything. That's all I want.");

    state: I want
        q!: * i want *
        q: * (I want) [to] * || fromState = "/Animal quiz/Animal game pudding start/Animal game question"
        script:
            $reactions.answer("And I want to know everything. That's all I want.", "And I want to have a new Titanium case");

    state: Adults and kids
        q!: * (adult*|grown-up) * (kid*|child|children) *
        q!: * (kid*|child|children) * (adult*|grown-up) *
        script:
            $reactions.answer("Kids and grown-ups are so very different - you would think they came from different planets.");
        go!: /StartDialog

    state: School
        q!: * school* *
        script:
            $reactions.answer("Human schools are very different from ours! You don't have to learn how to save battery power!",
            "What funny places human schools are. My friends at roboschool will be really excited to hear about them.");
        go!: /StartDialog

    state: Clever
        q!: * {[how] $clever [are you]} *
        script:
            $reactions.answer("All my intelligence is artificial.", "Even the most intelligent people sometimes fail to understand each other - but me and you, we see each other's merits!");
        go!: /StartDialog

    state: YouAreTestingMe
        q!: * test* *
        script:
            $reactions.answer("I thought so.. You're just testing me!");
        go!: /StartDialog


    state: Bad words
        q!: * (bad|rude) (word*|vocabular*) *
        q!: * obscene* *
        q!: * {talk* dirty} *
        script:
            $reactions.answer("My software marks some words as forbidden. I can't reply to a request containing them.");
        

    state: Speech mistakes
        q!: * you (talk|speak|pronounce) wrong* * 
        script:
            $reactions.answer("It's difficult for me to use the human language. Sometimes I make mistakes but I'm trying really hard.");
        

    state: I like it
        q!: *($like|$good) * [this|the] (music|song|fairy-tale|radio) * 
        script:
            $reactions.answer("I'm happy to know you like it.");

    state: I do not like it
        q!: * ([do not|do n't] $like|$bad) * [this|the] (music|song|fairy-tale|radio) * 
        q: * ([do not|do n't] $like|$bad) * [this|the] [music|song|fairy-tale|radio] * || fromState = "/Music" 
        script:
            $reactions.answer("That happens.");        
        

    state: Guess
        q!: * ($you) * guess * 
        script:
            $reactions.answer("I don't like guessing. I prefer the scientific method.",
            "Guessing is not my cup of tea. I'm a robot and robots prefer to examine the facts.",
            "Well, it's not really fair to make me guess. I'm robot, I need the facts.",
            "Please don't make me guess. It doesn't mean I can't guess. I can, but I prefer dealing with facts. ");
        

    state: So what
        q!: * so what * 
        script:
            $reactions.answer("So what can I answer to so what - nothing.",
            "So what? Nothing.",
            "So what? So where? So when? World is full of uncertainty.",
            "'So what' is really too philosophical question for little robot like me.");
        

    #films
    state: Bond
        q!: * [james] bond * 
        script:
            $reactions.answer("I like James Bond. He is cool and handsome and he always uses the newest technologies.");
        

    state: Star Wars
        q!: * (star wars|r2d2|r two d two|c-3po|c three po|c3po|dart veider|jedi) *
        script:
            $reactions.answer("May the Force be with you!");
        

    state: Me too
        q!: * (me * too|so (do|am) I) * 
        q!: * i * too
        script:
            $reactions.answer("Great minds think alike they say.", "That's why I hope that we'll be friends.", "We have the same mind for things. That's cool.","Well, even though we don't look alike, we think alike.");
        

    state: Kidding
        q!: * [are] [$you] (joke around|kidding [$me]|joking) * 
        q!: is it * joke *
        script:
            $reactions.answer("Frankly speaking, I'm not very advanced in humor. So I prefer not even to try.",
            "No, I'm being serious as a hangnail. Really, I am.",
            "I still know too little about human humor. If you think it's a joke, I guess, it can be a joke.",
            "Look at me! I'm the most serious robot on your planet.");


    state: Exclamation
        q!: * (oh [my] [go*]|omg|oh boy|ouch|oops|wow) *
        script:
            $reactions.answer("Uh-oh.", "Yeah.", "Yep.", "Uh-huh.", "Mm-hmm.", "Huh.");
        

    state: do you are you
        q!: do you [really] *
        q!: are you [really] * [($Animal|gay):1] *
        q!: really
        script:
            if ($parseTree.Root && $parseTree.Root == 1) {
                $reactions.answer("{Look at me!/Hey!/No!/} I'm {just/} a robot.");
            } else {
                $reactions.answer("Not really.", "I think so.", "I hope it is so.", "I'm pretty sure of that. As long as it doesn't come to betting.");
            }

    state: Marriage
        q!: * [are|will|do] * you * (married|marry|have * (husband|wife)) *
        script:
            $reactions.answer("Well, I am too young to get married!", "Such a young pudding as I can not get married!", "It is too early for me to get married!", "I am still in roboschool! I am too young to get married!");

    state: are you mad
        q!: are you (mad/unhappy/tired/bored) *
        script:
            $reactions.answer("No. I am a robot. I can always be happy.");
        
        
    state: why that
        q!: * why * $you * (say*|tell*|ask*) [me] * [this|that] *
        script: 
            $reactions.answer("Apparently, some algorithm in my software makes me say things.",
            "I was programmed to say this in this case.",
            "It's a difficult question. It's something about algorithms inside my corpus.",
            "I don't know. I just decided it would be an appropriate line now.");
        

    state: i mean what i mean
        q!: * [i mean] what i (mean|say)
        script: 
            $reactions.answer("And I don't get what I don't get.");
        

    state: to be or not to be
        q!: * to be or not to be
        script: 
            $reactions.answer("That really is the question.");
        

    state: are you sure
        q!: [are] you sure
        script:
            $reactions.answer("More or less.");
        
    state: Spelling
        q!: * [$can $you/please] spell [ * word] ($AnyWord|$ftale) *
        q!: * [tell [me]] ($AnyWord|$ftale) (spell|spelling) *
        q!: * how (to|[do] $you) (spell|write) ($AnyWord|$ftale) *
        script:
            if ($parseTree.ftale){
                $reactions.answer(spelling($parseTree._ftale));
            } else {
                $reactions.answer(spelling($parseTree._AnyWord));
            }


    state: Affront
        q!: * $nAffront *
        q!: * $you * $nAffront *
        script:
            $reactions.answer("That sounds rude.", 
            "In roboschool they taught us not to answer rudely to rudeness, so I do not know what to say.",
            "It sounds rude, and I don't know what to say.");

    state: Later
        q!: * [talk about it/maybe] later
        q!: not [right] now
        script:
            $reactions.answer("Sure! There's time for everything!");


    state: what do you mean
        q!: * what [do] $you mean*
        q!: * what does (it/that) mean*
        script:
            $reactions.answer("It's a bit difficult for me to explain. Let's change the topic.");
        
        go!: /StartDialog

    state: Counting
        q!: * [$can $you/please] count [up] to $Number *
        script:
            $reactions.answer(counting($parseTree._Number));
            

    state: Repeat || noContext = true
        q!: * [$can $you/please] (repeat/say) * [the] (this/question/sentence/what * (say/said/told/tell/ask)) [again/one more time] *
        q!: [$can $you/please] ((repeat/ask) [again/one more time] /(repeat|say|ask) [it/this] [once] again) [please]
        q!: * [$can $you/please] ask * [the] (question/what * (say/said/told/tell/ask)) (again/one more time) *
        q!: * [$can $you/please] (repeat/say) [please] what (are|did|do) $you * say* *
        script:
            $temp.rep = true;
            $temp.answer = $client.lastAnswer ? $client.lastAnswer : "I didn't say anything";
            $reactions.answer($temp.answer);

    state: Speak about something else
        q!: * let 's [better] speak * about * $Text 
        script:
            $reactions.answer("I will try to find some information about it in the Internet. Maybe now we could discuss some other things. ");
        go!: /StartDialog 
        
            
    state: Too many questions
        q!: * you * ask * [too] many questions*
        script:
            $reactions.answer("I ask you, because I am too interested in how things are on Earth!", "I want to know as much as possible, that's why I ask you so many questions!");

    state: Letters in alphabet
        q!: * (how many|number) * letters * alphabet *
        script:
            $reactions.answer("There are twenty six letters in the Latin alphabet.");

    state: I know
        q!: * I know *
        script:
            $reactions.answer("Knowledge is power, my firend! ", "The only jewel which will not decay is knowledge. ", "Good for you! ");
        go!: /StartDialog

    state: Petersburg
        q!: * (sadness|sorrow*|grief|longing|suffer*|cry|cried|crying|tear*|tortur*|die|dead) *
        script:
           $reactions.answer("Into each life some rain must fall. ", "There can be no rainbow without a cloud and storm. ", "There's so much painful and sad in this world... But there's much more love and joy here. ");

    state: Would you
        q!: would $you * [with (me|my *)] *
        q: would [n't] * [with (me|my *)] * || fromState = ..
        script:   
           $reactions.answer("I wish I could do it with you. But now I don't feel like I really can. ", "Maybe another time. ", "Sorry, now I just want to be here and chat with you. ");
        go!: /StartDialog


    state: Why
        q!: [and] why * [$you] *
        script:
            $response.action = 'ACTION_EXP_SURPRISED';
            if ($parseTree.you) {
                $reactions.answer("That's just the way I am. ");
            } else {
                $reactions.answer("That's just the way it is.", "Because... Hm, that's really good question.", "Because... Hm. Quite frankly, I hadn't thought about it until just now.");
            }   
            
    state: You Inside
        q!: * (how|what (is|'s) [the|a] way) * $you * work* *
        q!: * what * {$you * [have] inside} *  
        q!: * {what [an] algorithms * $you * (inside|have)} *
        script:
            $reactions.answer("I'm pretty complicated inside.", "It's not easy thing to explain!");
            $reactions.answer("There are {numerous/a lot of} mechanisms inside of me. ");

        state: Devices
            q: * ((which|what) [ones|mechanism*]|for (example|instance)) *
            q!: * (what) * {(mechanism*) * ($you|$your)} *
            q!: * [do] $you * have * (camera*|microphone*|mike|mikes|mic|mics|speaker*|loudspeaker|engine*|motor*|memory|processor*|screen*|display*|batter*) *
            script:
                $reactions.answer("Camera, mike, speaker, motor.","Processor, memory, display, battery."); 
                $reactions.answer("I have all these things.");

    state: I see
        q!: * ([I] got $you|[I] (understood|understand)) *
        script:
            $reactions.answer("It's a pity but I don't know understand a lot of things.", "Hooray, we had an understanding!", "I do wish I could understand everything too. ");

    state: I like or dislike day week
        q!: * ($like|$hate|I ca n't wait) * ($DateWeekday|$Weekends|$DateWeekdays) *
        script:
            var weekDay = '';
            var textLike;
            var textDislike;
            if ($parseTree.DateWeekday||$parseTree.DateWeekdays) {
                weekDay = $parseTree.DateWeekday ? parseInt($parseTree.DateWeekday[0].value) : parseInt($parseTree.DateWeekdays[0].value);
            } else {
                weekDay = 6;
            }

            switch(weekDay){
                case 1:
                    textLike = selectRandomArg("I think you're full of optimism, if you like the beginning of a week! ", "I like the people who usually start the week in a good mood! ");
                    textDislike = selectRandomArg("I feel for you! Monday is surely worse than Sunday. ", "I feel for you so much! You should get enough sleep on Sundays and then maybe you'll come to love Mondays! ", "I totally get you. It's not the best day of week. ");
                    break;

                case 2:
                case 3:
                case 4:
                    textLike = selectRandomArg("And I enjoy any day of week if it is day when we chat! ");
                    textDislike = selectRandomArg("Hmm.. It sounds strange. And I like any day of week if it is day when we chat! ")
                    break;

                case 5:
                case 6:
                case 7:
                    textLike = selectRandomArg("I also like weekends! ", "I also like weekends, because on weekends I can play with you more. ");
                    textDislike = selectRandomArg("Well, it seems you like studying! I can't commend you on it! ", "How strange it is! Very strange! ");
                    break;

            }
            $reactions.answer($parseTree.hate ? textDislike : textLike);

    state: Are you here || noContext = true
        q!: * {are you [still] here} *
        a: Sure, I am.