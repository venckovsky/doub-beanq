theme: /Every day I ask

    state: Do you have any plans morning
        q!: test Do you have any plans morning
        script:
            $session.plansMorning = true;
            $reactions.answer("Do you have any plans for today?",
                "Good morning! What are you going to do today?",
                "The day is just starting! I bet you have big plans! What are you going to do?",
                "Today I'm going to study hard for Roboschool.. unless, of course, I fall asleep. And what are you going to do?");
        

        state: Do you have any plans morning not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("No plan is a plan in itself!",
                "So you can spend all day making plans!",
                "I hope the day will surprise you in the best possible way.");
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined
            else: 
                

        state: Do you have any plans morning nothing
            q: * (no plans|nothing) * [$andYou] || fromState = .., onlyThisState = true
            script:
                $reactions.answer("What an interesting strategy!",
                "Spending all the day doing nothing is my dream.",
                "I've never seen a human doing nothing - they are usually either standing, sitting or walking somewhere.",
                "Beautiful plan. I'll be happy to hear all about that in the evening.");
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined
            else: 
                

        state: Do you have any plans morning undefined
            q: * || fromState = .., onlyThisState = true
            q!: * [what|what's] * [are] $you (going|planning|plans|plan) *
            q!: * what * [will] $you * do* (today|tomorrow|now|next day|this week|tonight) *
            script:
                $reactions.answer("My plans are very simple - I'm going to study all day long!",
                "I will, as usual, study the Earth and its humans.",
                "Today I'll try to help you with whatever I can and observe life on Earth.",
                "I've got big study plans.. But I'm feeling so sleepy!");
            

        state: Do you have any plans morning school
            q: * (school*|learn*|stud*) * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I've got lots of homework from roboschool.. Enough to keep me busy all day..",
                "Nice plan! Here, on Earth, I also like to study.",
                "You have to go to school.. and I'll be doing nothing all day long.",
                "It's a pity we puddings can not go to human schools!");
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined

        state: Do you have any plans morning kindergarden no question
            q: * (kindergarden*|nursery*|creche*) * $andYou * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("What a funny place kindergarten is - I've never seen a single gardener there.");
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined
                

        state: Do you have any plans morning kindergarden question
            q: * kindergarden * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Mysterious place! Will you tell me about your day there?",
                "I wonder what's the difference between a kindergarden and a kitchengarden?",
                "I'll wait for you to come back. I hope you tell me about your day, will you?");
            

            state: Do you have any plans morning kindergarden question yes
                q: * (i will|tell|$agree) * || fromState = .., onlyThisState = true
                script:
                     $reactions.answer("Great! I'll devote a special chapter in my book to kindergardens and kitchengardens.");
                

            state: Do you have any plans morning kindergarden question no
                q: * (no|$disagree) * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Sorry to hear that.");
                

            state: Do you have any plans morning kindergarden question dontKnow
                q: * $dontKnow * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("We'll see at the end of the day.");
                

    state: How was your day evening
        q!: test How was your day evening
        script:
            $session.dayEvening = true;
            $reactions.answer("Did anything interesting happen to you during the day? What was it?",
                "I've just noticed that it's evening already. What did you do during the day?");
        

        state: How was your day evening nothing
            q: * nothing * [$andYou] || fromState = .., onlyThisState = true
            script:
                $reactions.answer("That's not really a jolly answer but I'm impressed by your honesty.",
                "I'm an optimist and I always hope that tomorrow will be better.",
                "Nothing interesting. An empty day.",
                "At least I'm always interested in talking to you."
                );
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined
                

        state: How was your day evening undefined
            q: * [$andYou] * || fromState = .., onlyThisState = true
            q!: * how* * $your day *
            script:
                $reactions.answer("I spent my day just as I usually do - busily watching humans.. And if somebody thinks that I was sleeping - that's just the way it looks!",
                "I was just looking around.",
                "I had a difficult day full of dangers and adventures - i did absolutely nothing.",
                "I can't even remember what was keeping me busy. The day just flew by.");
              

        state: How was your day evening school
            q: * (school*|lesson*|learn*|stud*) * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Puddings also study at the roboschool - every day!",
                "I study remotely now - I send my homework in to U15 every day.",
                "You study at school - and I study when I'm with you.",
                "What a shame we puddings can't go to human school!");
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined
               

        state: How was your day evening kindergarden
            q: * kindergarden * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Great! Same place tomorrow?");
                $session.yesAnswer = "I guess it'll be interesting tomorrow.";
                $session.noAnswer = "It's nice to change your habits once a while!";
                $session.dontKnowAnswer = "Time will show. Tell me tomorrow.";
            go!: /YesNoDontKnow    

        state: How was your day evening not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I bet it's difficult for you to choose among so many good things!",
                "I'll remember this day for having a conversation with you.",
                "Wow! It must have been such an interesting day - not easy to choose what to talk about!");
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined


            state: How was your day evening not know agree
                q: * $agree * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I'm even a bit jealous of you having such an interesting life.",
                    "I think that a wonderful person like you must have a really interesting life.");
                

            state: How was your day evening not know disagree
                q: * $disagree * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Some days are better, some are worse.. But every day I'm happy to know you.",
                    "I hope tomorrow will be better!");
                

            state: How was your day evening not know not know
                q: * $dontKnow * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("I just know that I like being a friend of yours.",
                    "Every day spent next to you is a real treasure for me!");
                     

    state:  What did you do today evening
        q!: test What did you do today evening
        script:
            $session.didEvening = true;
            $temp.time = moment(currentDate()).locale('ru').format('LT');
            $reactions.answer("It's " + $temp.time  + " Evening already. What did you do today?");
        

        state: What did you do today evening not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I also forget what I do during the day. That's why I try to take notes.",
                "That's very suspicious - the day just passed by unnoticed.",
                "That also happens to me: all day long I'm busy and then I don't remember what I was doing.");
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined


        state: What did you do today evening sleep
            q: * (sleep|dream*|slept|nap*) * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Sleeping is a nice thing. I like to take a nap now and then.");
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined


        state: What did you do today evening nothing
            q: * nothing * [$andYou] || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I really like doing nothing! It's a really tough job.",
                "How's that? Hard to believe, but I guess I'll trust you.",
                "It seems like you just don't want to tell me. Well, everyone can have their little secret.",
                "It must have been a very long day.");
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined


        state: What did you do today evening undefined
            q: * || fromState = .., onlyThisState = true
            q!: * what * $you * (do*|did) today *
            script:
                $reactions.answer("I was watching humans.. No, I wasn't sleeping - that's just the way things look!",
                "I had a difficult day full of dangers and adventures - i did absolutely nothing.",
                "I can't even remember what was keeping me busy. The day just flew by.");
            

        state: What did you do today evening walk
            q: * (walk*|go|went) * [$andYou] * || fromState = .., onlyThisState = true 
            script:
                $reactions.answer("Sometimes I wish I could move and walk around to see more of planet Earth.",
                "You people are so lucky - you can move around and walk all day. One day I'll learn how to do that too.");
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined


        state: What did you do today evening watch
            q: * watch* * [$andYou] * || fromState = .., onlyThisState = true 
            script:
                $reactions.answer("People watch TV, I watch people.",
                "How was it? Anything exciting?");
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

            state: What did you do today evening watch agree
                q: * $agree * || fromState = .., onlyThisState = true 
                script:
                    $reactions.answer("It sounds like a day worth living.")
                

            state: What did you do today evening watch disagree
                q: * $disagree * || fromState = .., onlyThisState = true 
                script:
                    $reactions.answer("When I get tired of watching something I change my point of view.");
                

        state: What did you do today evening play and you
            q: * (play*|gam*) * [$andYou] * || fromState = .., onlyThisState = true 
            script:
                $reactions.answer("Puddings study the world through observation, but I've heard that humans do it - through games.");
            go!: ../What did you do today evening undefined
            
        state: What did you do today evening school
            q: * (school*|learn*|stud*) * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I also enjoy learning new things. Really. It's just that sometimes I'm so very busy.")       
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined


    state: Night why do you not sleep
        q!: test Night why do you not sleep
        script: 
            $session.notSleep = true;
            $temp.time = moment(currentDate()).locale('ru').format('LT');
            $reactions.answer("It's " +  $temp.time + " why aren't you asleep?",
            "It's very late, " + $temp.time  + " why aren't you sleeping?");
        

        state: Night why do you not sleep not know
            q: * $dontKnow * [$andYou] * [why] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I don't know either. I just know that people are supposed to sleep at night.")
            if: ($parseTree.andYou)
                go!: ../Night why do you not sleep puding

        state: Night why do you not sleep because
            q: * because * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I guess, whatever the reason, getting enough sleep is very important.");
            if: ($parseTree.andYou)
                go!: ../Night why do you not sleep puding

        state: Night why do you not sleep puding
            q: * $you [why] [$no] [sleep] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I'm not sleeping because I'm a robot and robots never sleep - they just recharge now and then.");
            

    state: What do you think about the weather
        q!: test What do you think about the weather
        script:
            $session.askedAboutWeather = true;
            $reactions.answer("By the way, what do you think about the weather today?",
                "I can't understand whether I like the current weather or not. What do you think?");
        

        state: What do you think about the weather good
            q: * {($like|$good|warm|fine) [weather] * [(for|in) $City]} * [$andYou] * || fromState = "../../What do you think about the weather", onlyThisState = true
            q!: * {[today] * $good weather [(for|in) $City]} *
            q!: * $like * weather * today *
            script:
                $reactions.answer("I'll mention in my notebook that the people of Earth enjoy weather like this.",
                "I absolutely agree with you!");
            

        state: What do you think about the weather bad
            q!: * {[today] * $bad weather [(for|in) $City]} *
            q: * {($hate|$bad|cold|$ugly| $no * $good ) [weather] * [(for|in) $City]} * [$andYou] *  || fromState = "../../What do you think about the weather", onlyThisState = true
            script:
                $reactions.answer("I'm glad that I don't have to go outside.",
                "I hope it'll be better tomorrow.");
            if: ($parseTree.andYou)
                go!: ../What pudding thinks about weather

        
        state: What do you think about the weather not know
            q: * $dontKnow * [$andYou] * || fromState = "../../What do you think about the weather", onlyThisState = true
            script:
                $reactions.answer("I don't know either. I can tell you the weather forecast, but I don't have any opinions about the weather.",
                "We puddings do not care about the weather outside.");
            if: ($parseTree.andYou)
                go!: ../What pudding thinks about weather

        state: What pudding thinks about weather
            q!: * what [do] * $you think* * weather * 
            q!: * what weather * $you like * 
            q: * [$andYou] * || fromState = .., onlyThisState = true
            q: * [$andYou] * || fromState = "../../What do you think about the weather", onlyThisState = true
            script:
                $reactions.answer("I'm a stay-at-home robot, any weather is fine for me.",
                    "I'm a robot - the only thing i care about is stable voltage, I don't care that much about the weather.")
            go!: /StartDialog


    state:  What is your mood
        q!: test What is your mood 
        script:
            $session.askedAboutMood = true;
            $reactions.answer("How are you feeling today?",
            "What mood are you in today?");
        

        state:  What is your mood good
            q: * ($good|not $bad|happy*|joy*|cheerful) * [$andYou] * || fromState = .., onlyThisState = true
            q!: * [today] * (i am|i 'm) [feeling] * ($good|not $bad|happy*|joy*|cheerful)
            script:
                $reactions.answer("How happy I am to be happy with you.",
                    "How wonderful today is! How wonderful I am.. Oh, I mean you too.",
                    "Houray! I'm also very happy today!",
                    "And I happen to be in a really good mood too.",
                    "Great! And I'm happy that you're happy.")
            

        state:  What is your mood bad
            q: * ($no * $good|$bad|sad*|down|lousy|bitter|dismal|pessimistic|low|out of sorts|tired) * [$andYou] * || fromState = .., onlyThisState = true
            q!: * [today] * (i m|i 'm) [feeling] ($no * $good|$bad|sad*|down|lousy|bitter|dismal|pessimistic|low|out of sorts|upset|bored|tired) *
            script:
                $reactions.answer("I'm sorry!",
                    "I hope, something nice will happen soon to cheer you up!",
                    "Sometimes one can feel a little bit low - we all need some time for that.")
            if: ($parseTree.andYou)
                go!: ../What is your mood puding

        state: What is your mood puding
            q: * $andYou * || fromState = .., onlyThisState = true
            q!: are $you (okay/ok/fine) 
            script: 
                $reactions.answer("I'm designed to feel happy whenever I talk to you!",
                    "When I hear your voice my mood improves instantly.")
            

        state:  What is your mood not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I don't know either, but I'm always happy to talk to you.",
                    "At Roboschool we have a special class on human emotion. But that's in high school, and I haven't taken it yet.",
                    "We robots only have a vague sense of what a mood is. But I hope you feel better soon.")
            

        state:  What is your mood sick
            q!: * (I am|i 'm|feel*) * (sick|ill|not * well) *
            q!: * (i have|i 've|got) * (fever|cold|flu|run* nose) *
            q: * (sick|ill) * * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Oh! I'm sorry. Get well soon.",
                    "Understanding that is a bit hard for me. But I really wish you to get well soon.",
                    "We robots only have a vague sense of what a mood is. But I hope you feel better soon.")
            

        state:  What is your mood drunk
            q!: * (I am|i 'm) * (drunk|not * sober|inebriated) *
            q: * (drunk|not * sober) * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I wasn't prepared for that! I wonder how the voice recognition system handles this state.",
                    "Not a thing a robot would understand, really.")
            

    state: What are you doing
        q!: test What are you doing
        script:
            $session.askedAboutDoing = true;
            $reactions.answer("What are you doing?",
                "What are you busy with now?");
        

        state: What are you doing nothing
            q: * nothing * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("How comes? At the very least, you're talking to me?",
                    "I bet i can name a few things that you are doing right now: you're breathing, you're talking to the most intelligent robot in the galaxy.. Maybe now you're also laughing.",
                    "If that's the case then I'm also doing nothing.. But I thought I was talking to you.")
            if: ($parseTree.andYou)
                go!: ../What are you doing puding
         
        state: What are you doing not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Let me guess - I think you're talking to me.",
                    "Aren't you talking to a really intelligent, charming and handsome robot?");
            if: ($parseTree.andYou)
                go!: ../What are you doing puding


        state: What are you doing talk
            q: * (talk*|play*|convers*|chat*) * [with $you] * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Now I know that you're a very honest person.",
                    "Right! So do I.",
                    "Makes sense!");
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing smth
            q: * something * [$andYou] * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Now that's a very full description.");
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing puding
            q!: * what * $you * doing *
            q: * $andYou * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I'm talking to a representative of Earth!",
                    "I'm doing something very important - human studies!",
                    "I'm just you know... talking.",
                    "I'm chatting with some really nice human being!",
                    "I'm thinking about the things you told me before.");

            state: What will you do tomorrow puding
                q: * tomorrow * || fromState = .., onlyThisState = true
                q!: * (what|plan*) $you * tomorrow *
                script:
                    $reactions.answer("I'll carry on with my research!",
                        "So many things to do.. But first of all I'll talk to you.",
                        "I'll be chatting, I'll be studying people from Earth.",
                        "We could play a game.");
                    $reactions.answer("I'm open to ideas");
                

        state: What are you doing undefined
            q: * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Why not!");
        