theme: /Heroes

    state: Spiderman
        q!: * человек* паук* *
        a: Я знаю, что на земле живут не только люди, но и животные. А Человек-паук - это человек или насекомое?
        
        state: spiderman human
            q: * человек * || fromState = .., onlyThisState = true
            a: Буду знать, что люди тоже умеют делать паутину.
            
        state: spiderman insect
            q: * (насеком*/паук) * || fromState = .., onlyThisState = true
            a: Да уж, не верь своим глазам.

        state: spiderman not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            a: Загадочный персонаж!

    state: Starwars heroes weider
        q!: * [кто такой] (дарт*/вейдер*) * [$andYou] *
        a: Дарт Вейдер - это высокий мужчина в очень модном шлеме.

    state: Star wars Emelya
        q!: star wars
        script:
            $session.askedStarwars = "true";
        a: Изучаю земную культуру. Недавно посмотрела Звездные войны. Ты знаешь про них?
        
        state: Star wars Emelya yes
            q: * ($agree|обожа*|люблю|знаю|знаем|смотрел*) * || fromState = .., onlyThisState = true
            q: (есть|был*) ~такой || fromState = .., onlyThisState = true
            q: давай обсудим || fromState = "../Star wars Emelya no", onlyThisState = true
            a: Интересно!
            go!: ../../Starwars

        state: Star wars Emelya no
            q: * ($disagree|немно*|чуть*|чуточ*|не (смотрел*/знаю)) * || fromState = .., onlyThisState = true
            a: Можем обсудить, если посмотришь.

            state: Star wars Emelya no OK
                q: $agree || fromState = .., onlyThisState = true
                random:
                    a: Вот и славно!
                    a: Договорились!
                go!: /StartDialog

    state: Starwars
        q!: * {[смотрел/любишь] * (звездные (войны|воины))} *
        script:
            $session.askedStarwars = "true";
        a: Мне нравятся Звездные войны. А кто твой любимый герой?  

        state: Fragment || noContext = true
            q: [мой] любимый герой || fromState = .., onlyThisState = true
            random:
                a: Прости, не расслышала.
                a: Не уловила.
            a: Кто твой любимый герой?

        state: Robot
            q: робот || fromState = .., onlyThisState = true
            a: Да, {робот мне тоже нравится/он классный}.

        state: Starwars favorites weider
            q: * [любимый герой] * (дарт*/вейдер*/энакин*/эни/дарквэй*/дарт вейдер) * [$andYou] * || fromState = .., onlyThisState = true
            a: Мне нравится его шлем. Хочу себе такой же корпус.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you
                
        state: Starwars favorites Luke
            q: * [любимый герой] * (люк*/скайвокер*/скайуокер*/Skyw*) *  [$andYou] * || fromState = .., onlyThisState = true
            a: Мне он тоже нравится, потому что он дружит с роботами.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you
                

        state: Starwars favorites Kylo
            q: * [любимый герой] * (Кайло/Рен*) *  [$andYou] * || fromState = .., onlyThisState = true
            a: Я его немного опасаюсь. Кажется, он родственник Дарта Вейдера.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Leya
            q: * [любимый герой] * (Лея/Орган*/принцесс*) *  [$andYou] * || fromState = .., onlyThisState = true
            a: Мне очень нравится эта героиня. Она смелая и отважная.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Snoke
            q: * [любимый герой] * (император*/сноук*) * [$andYou] * || fromState = .., onlyThisState = true
            a: А я его немного боюсь.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you
                

        state: Starwars favorites Yoda
            q: * [любимый герой] * (йод*/yoda) * [$andYou] * || fromState = .., onlyThisState = true
            a: Мастер Йода немного похож на нашего учителя в робошколе.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Padme
            q: * [любимый герой] * (падм*/амидал*) * [$andYou] * || fromState = .., onlyThisState = true
            a: Когда вырасту, хочу быть как она.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Solo
            q: * [любимый герой] * (хан*/соло) * [$andYou] * || fromState = .., onlyThisState = true
            a: Да, мне он тоже нравится.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Chybakka
            q: * [любимый герой] * (чубак*/чуи/шушака) * [$andYou] * || fromState = .., onlyThisState = true
            a: Да, он тоже путешественник как и я.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites soldier
            q: * [любимый герой] * (капитан*/штурмовик*/солдат*) * [$andYou] * || fromState = .., onlyThisState = true
            a: Куплю себе на новый год костюм солдата империи.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you
                
        state: Starwars favorites nobody
            q: * (никто|{(у меня) * нет}) * [$andYou] * || fromState = .., onlyThisState = true
            a: Кажется, ты не фанат Звездных войн.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites Obi Wan
            q: * [любимый герой] * оби ван * [$andYou] * || fromState = .., onlyThisState = true
            a: Столь же мудрый, как магистр Йода, и столь же сильный, как магистр Винду.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you          

        state: Starwars favorites dontknow
            q: * $dontKnow *  [$andYou] * || fromState = .., onlyThisState = true
            a: Мне тоже было сложно определиться.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you
                
        state: Starwars favorites everybody
            q: * все *  [$andYou] * || fromState = .., onlyThisState = true
            a: Да, все персонажи в Звездных войнах просто чудесные.
            if: ($parseTree.andYou)
                go!: ../Starwars favorites and you

        state: Starwars favorites and you
            q: * $andYou * [$andYou] * || fromState = .., onlyThisState = true
            q: * {(твой/у тебя) * любим* [персонаж/герой]} *
            q!: * {(твой/у тебя) * любим* (персонаж/герой) * звездны*} *
            a: Мой любимый персонаж - R2D2.
            
    state: Avengers
        q!: * мстител* *
        a: Я пока не успела посмотреть все фильмы про супергероев. Но я стараюсь! Расскажи мне про мстителей!

    state: Legogo
        q!: * [ты] [знаешь] * [про] лего *
        a: А на моей планете нет Лего. Надо будет отвезти туда образец.
        
    state: Potter
        q!: * [читал/смотрел/знаешь] * гарри поттер* *
        a: Мне нравятся истории про Гарри Поттера, но мы, роботы, не имеем магического дара.

    state: Character
        q!: *  это * (герой/героиня) (фильма/мультфильма/сказки/книги/комиксов/игры) *
        a: Мы все - герои своих историй.