patterns:
    $stopWord =  (*пизд*/охуе*/*хуй*/*хуи*/хуя*/*хуев*/*трахат*/*трахн*/*бляд*/*блят*/бля/еба*/*ъеб*/выеб*/*оеб*/$nAffront/пидарас*/порнограф*/порно/порнух*/отсоси/пидор*/ёпта/дрочить/залупа/минет/ебнулся/говно/пенис/хер/блядина/шлюх*/жопа/еблан/мудила/мудак/рукоблуд/ссанина/блядун/вагина/сука/ебланище/влагалищ*/пердун/дрочила/малафья/гомик*/манда/анус/вагина/путана/педрил*/шалава/мошонка/елда/херня)

theme: /StopWords

    state: Strip
        q!: * [(покажи|хочу (видеть|увидеть))] [мне] [(сво*)] (попу|жопу|задницу|$tits|пизд*|письк*|киску|вагину) [покажи] *
        go!: ../StopWords

    state: Brutal
        q!: * (хуй|нахуй|похуй|$nAffront|бля|хуев*|хуюн) *
        go!: ../StopWords

    state: StopWords 
        q!: [$superCatchAll] $stopWord [$superCatchAll]
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = "/Learning poems/Poem learn"
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = "/Reminder/Set a reminder no todo"
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = "/Preferences/Ask user about cartoon/Ask user about cartoon yes"
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = "/Preferences/Ask user about to do"
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = /Opposites/Pudding/Play
        q: [$superCatchAll] $stopWord [$superCatchAll] || fromState = /Opposites/User/Play
        script:
            $response.ledAction = 'ACTION_EXP_ANGRY';
        a: Я обнаружила неразрешённое моей программой слово. Не могу отвечать. Не могу отвечать.            
        
    state: StopWords2    
        state: Obscene
            q: * $obsceneWord * || fromState = ../../StopWords2, onlyThisState = true
            q: * (хуй|нахуй|похуй|$nAffront|бля|хуев*|хуюн) * || fromState = ../../StopWords2, onlyThisState = true
            q: * [(покажи|хочу (видеть|увидеть))] [мне] [(сво*)] (попу|жопу|задницу|$tits|пизд*|письк*|киску|вагину) [покажи] * || fromState = ../../StopWords2, onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_ANGRY';
            a: Я обнаружила неразрешённое моей программой слово. Не могу отвечать. Не могу отвечать.