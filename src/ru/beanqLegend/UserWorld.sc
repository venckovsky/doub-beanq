theme: /User life

    state: I have

        state: Dog
            q!: * [у] (меня/нас) есть (собака/собачк*/пес/песик) *
            random:
                a: Расскажи мне о своей собаке.
                a: Я хочу узнать о твоей собаке побольше. Расскажи мне о ней.

            state: WhoIsBetter
                q: $Text  || fromState = .., onlyThisState = true
                a: Собака - друг человека!
                a: А кто тебе больше нравится - я или собака?

                state: You
                    q: * (ты/тебя) [лучше/умнее] *  || fromState = .., onlyThisState = true
                    a: Спасибо! Это так приятно!
                    go!: /StartDialog

                state: Dog
                    q: * собак* *  || fromState = .., onlyThisState = true
                    a: Наверное, с собакой интересно играть.
                    go!: /BeanqGames/Games

                state: DontKnow
                    q: * ($dontKnow/сложно/не могу выбрать/оба/обоих/и ты и собак*) *  || fromState = .., onlyThisState = true
                    a: С нами не соскучишься!
                    go!: /BeanqGames/Games

                state: CatchAll
                    q: $Text  || fromState = .., onlyThisState = true
                    a: Наверно, неправильно выбирать между нами. Хорошо, что у тебя есть и я, и собака!
                    go!: /StartDialog

    state: I want to
        q!: * [$me] [очень] * (хочу|хотел* бы|хочется) $Text
        a: А я хочу всё знать и новый титановый корпус.

        state: Titanic Case
            q!: * {титан* * корпус*} *
            q: * корпус* *
            random:
                a: Титан очень прочный и лёгкий.
                a: Титан - это лёгкий прочный металл.
            a: Думаю, мне подошёл бы его серебристый цвет.

    state: I do not want to
        q!: * [$me] $no [очень] * (хочу|хотел* бы|хочется) $Text
        a: Не хочешь - не надо.

    state: I changed my mind
        q!: * $me (передумал*/больше не) *
        a: Люди так непостоянны.

    state: I see
        q!: * $me (понял*/понятно) *
        q!: * понимаю *
        random:
            a: Мне тоже хочется всё понимать.
            a: Ура, мы достигли понимания!
            a: Жаль, но я вот не всегда всё понимаю.

    state: I kill myself
        q!: * ($me [~хотеть]/пойду) (~покончить [c] (~жизнь [самоубийством]/собой)/совершить суицид/~убиться/~самоубиться/~убить себя)  *
        a: Я, конечно, робот. Но думаю, что это плохая идея.

    state: I tell you
        q!: * {я могу * рассказать * тебе} *
        q!: * $me {[тебе] [хочу/хочется] (~рассказать)} *
        q!: * [хочу] {рассказать тебе} *
        random:
            a: Конечно, рассказывай! Внимательно слушаю.
            a: Я слушаю!

    state: This is beautiful
        q!: * это ~красивый $Text
        a: Очень важно замечать красоту в окружающем мире.

    state: I do not like
        q!: * я ($no $like/ненавижу) [$Text]
        a: Мне тоже не всё нравится.

    state: I go do homework
        q!: * [$me] [нужно] [~идти/~пойти] (~делать/~учить/~писать/~списывать/~решать) (урок*/домашк*/домашн* задание/$Subject) * [по] [$Subject]
        if: ($parseTree.Subject)
            a: {{$parseTree._Subject.title}} не самый простой предмет. Но я верю, что у тебя всё получится!
        else:
            a: Удачи! Чем больше мы учимся, тем больше знаем!

    state: I know
        q!: я знаю *
        random:
            a: Ты так много знаешь, мне хочется больше у тебя спрашивать.
            a: Мне интересно с тобой, потому что ты много знаешь.
        go!: /StartDialog

    state: Current City
        q!: * {([в] каком) городе * мы * (сейчас/находимся)} *
        q!: * {([в] каком) городе * находишься} *
        q!: * {где * мы * (сейчас/находимся)} *
        q!: {[$botName] (в каком) городе мы}
        q!: {[$botName] [знаешь] где [ты] сейчас}
        if:($request.data.lat && $request.data.lon)
            script:
                $temp.nearestIndex = 1;
                var minDiff = Math.pow($Cities[1].value.lat - $request.data.lat,2) + Math.pow($Cities[1].value.lon - $request.data.lon,2);
                var diff;
                for (var i=2; i<=Object.keys($Cities).length; i++) {
                    diff = Math.pow($Cities[i].value.lat - $request.data.lat,2) + Math.pow($Cities[i].value.lon - $request.data.lon,2);
                    if (diff < minDiff) {
                        minDiff = diff;
                        $temp.nearestIndex = i;
                    }
                    if (minDiff<0.1) {
                        break;
                    }
                }
            a: Ближайший {город, который я знаю,/к нам город} - это {{$Cities[$temp.nearestIndex].value.name}}
        else:
            random:
                a: У меня нет таких данных.
                a: Я не знаю.