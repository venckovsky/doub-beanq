// запоминаем настоящую функцию обработки ответа, т.к. мы будем вызывать её после обработки шаблона
var realAnswerFunction = $reactions.answer;
$reactions.answer = function (params) {
    // Приоритет выбора языка
    // 1. Параметр ответ a: Answer || lang = en
    // 2. $request.language
    // 3. языка проекта chatbot.yml language: ru
    var lang = params.lang;
    // Есть разница в том, как функция вызывается из JS-кода и из сценария, в js-файлах мы передаём строковый аргумент, а движок сценариев передаём объект с полем value. Нужно обработать этот случай
    if (typeof (params) === "object") {
        if (params.tts) {
            var $response = $jsapi.context().response;
            if ($response.replies) {
                $response.replies[$response.replies.length - 1].tts = params.tts;
            }
        }
        if (params.go) {
            $reactions.transition(params.go);
        }
        params = Array.isArray(params) ? selectRandomArg(params) : params.value;
    }

    // Выполняем обработку подстановок
    var tt = resolveInlineDictionary(resolveVariables(resolveInlineDictionary(params)));
    // Вызываем системную функцию обработки ответа
    realAnswerFunction({value: tt, lang: lang});
};

function checkConverterErrors() {
    var $parseTree = $jsapi.context().parseTree;
    var $temp = $jsapi.context().temp;    
    if ($parseTree && $parseTree.Where && $parseTree.Where[0].value.error && $parseTree.Where[0].value.error == "unknown location") {
        $temp.targetState =  "/Errors/Unknown location";
    }
}

bind("preProcess", checkConverterErrors);

function getRandomQuestionType() {
    var $session = $jsapi.context().session;  
    var types = [];
    if ($session.lastQuiz) {
        for (var j = 0; j < arguments.length; j++) {
            if ($session.lastQuiz.type != arguments[j]) {
                types.push(arguments[j]);
            }
        }
    } else {
        types = arguments;
    } 
    var index = randomIndex(types);
    return types[index];
}

function saveLastAnswer() {
    var session = $jsapi.context().session; 
    var response = $jsapi.context().response; 
    session.lastAnswer = response.answer;
}


bind("postProcess", saveLastAnswer);


function checkTag(tag){
    var $parseTree = $jsapi.context().parseTree;
    var parseTreeString = JSON.stringify($parseTree);
    var regexp = new RegExp("\"tag\":\"" + tag + "\"", "i");
    return (parseTreeString.search(regexp) != -1);
}