var bogatyris = (function () {

    function init() {
        var $session = $jsapi.context().session;
        $session.flagForNonsense = false;
        $session.bTalesTold = [];
        $session.bogatyris = {
            currentTale: null,
            moreParts: 0,
            currentPart: 0
        };
        $session.bTalesNumber = Object.keys($bogatyris).length;
        $session.bTalesIds = shuffle(_.range($session.bTalesNumber));
    }

    function randomBTale() {
        var $session = $jsapi.context().session;

        if ($session.bTalesIds.length > 0) {
            var id = $session.bTalesIds.pop();
            while ($session.bTalesTold.indexOf(id) > -1 && $session.bTalesIds.length != 0) {
                id = $session.bTalesIds.pop();
            }
            $session.bogatyris.prevTale = $session.bogatyris.currentTale ? $session.bogatyris.currentTale : null;
            $session.bogatyris.currentTale = $bogatyris[id].value;
            $session.bTalesTold.push(id);
            $session.bogatyris.parts = $bogatyris[id].value.audio.length;
            $session.bogatyris.currentPart = 0;
        } else {
            $reactions.transition("/Bogatyris/PlayTale/AllTalesTold");
        }
    }

    function playBTale() {
        var $session = $jsapi.context().session;
        var $response = $jsapi.context().response;
        $response.stream = $session.bogatyris.currentTale.audio[$session.bogatyris.currentPart];
        $response.intent = "fairytales_on";
        $response.action = "musicOn";
        if ($session.bogatyris.currentPart < $session.bogatyris.parts - 1) {
            $reactions.transition({value: "/Bogatyris/PlayTale/NextPart", deferred: true});
        }
        //$reactions.answer("Проигрывается аудио " + $session.bogatyris.currentTale.audio[$session.bogatyris.currentPart]);
    }



    return {
        init:init,
        randomBTale:randomBTale,
        playBTale:playBTale
    }
})();