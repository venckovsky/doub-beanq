var global_locale = "ru"; 

function processStreamContent(){
    var $response = $jsapi.context().response;
    var $client = $jsapi.context().client;
    var $temp = $jsapi.context().temp;
    
    if ($response.audio) {
        $client.audioHint = ($client.audioHint) ? ($client.audioHint) : 0;
        $client.audioHintMoment = ($client.audioHintMoment) ? ($client.audioHintMoment) : 0;
        var interval = moment(currentDate()) - $client.audioHintMoment;
        if ($client.audioHint < 6 && interval >= 1200000) {
            $temp.contentType = ($response.intent == "fairytales_on") ? "the fairy-tale" : "the music";
            $reactions.answer("To stop " + $temp.contentType + ", say Pudding or stroke my head two times.");
            $client.audioHint += 1;
            $client.audioHintMoment = moment(currentDate());
        }
    }
}

bind("postProcess", processStreamContent);

function getFullReplyText(){
    var $response = $jsapi.context().response;
    var res = '';
    
    if ($response.replies && $response.replies.length > 0) {
        for (var i=0; i<$response.replies.length; i++){
            if ($response.replies[i].text) {
                if (res != '') {
                    res += ' ';
                }
                res += $response.replies[i].text;
            }
        }        
    }

    return res;
}

function checkBotRepetition(){
    var $client = $jsapi.context().client;
    var $temp = $jsapi.context().temp;
    if ($client.lastAnswer) {
        if ($client.lastAnswer == getFullReplyText() && $client.lastAnswer != '' && typeof $temp.rep == 'undefined') {
            $reactions.answer("Something is wrong. " + selectRandomArg("Again and again.","I think I said that before.","I've already said that.","I'm repeating myself.","I'm repeating myself again."));
        } else {
            $client.lastAnswer = getFullReplyText();
        }               
    } else {
        $client.lastAnswer = getFullReplyText();
    }
}

bind("preMatch", function() {
    var $response = $jsapi.context().response;

    if ($response.beanq && $response.beanq.event && $response.beanq.event.type && $response.beanq.event.packageName) {
        if ($response.beanq.event.type === "START_APPLICATION") {
            if ($response.beanq.event.packageName === "com.justai.beanq.skill.animalquiz") {
                $jsapi.context().temp.targetState = '/animalsAndBirds/Enter';
            }
        }
    }
});

function fillResponse(actionType, packageName, payload) {
    var $response = $jsapi.context().response;

    $response.beanq = $response.beanq || {};
    $response.beanq.actions = $response.beanq.actions || [];

    $response.beanq.actions.push({
        "actionType": actionType , 
        "packageName": packageName,
        "payload": payload
    });
}


bind("postProcess", checkBotRepetition);

function setNextContext(){
    var $temp = $jsapi.context().temp;
    if ($temp.nextContext) {
        $reactions.transition({value: $temp.nextContext, deferred: true});
    }
}

bind("postProcess", setNextContext);


bind("postProcess", function($context) {
    $context.session.lastActiveTime = $jsapi.currentTime();
});

bind("preProcess", function($context) {
    if ($context.session.lastActiveTime) {
        var interval = $jsapi.currentTime() - $context.session.lastActiveTime;
        if (interval > 30*60*1000) {
            $reactions.newSession( {message: $context.parseTree.text, data: $context.request.data } );
        }
    }    
});   