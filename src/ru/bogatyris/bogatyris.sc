require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: ../../dictionaries/bogatyris.csv
    name = bogatyris
    var = $bogatyris

require: bogatyris.js

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .bogatyris = function(parseTree) {
            var id = parseTree.bogatyris[0].value;
            return $bogatyris[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("bogatyris");

patterns:
    $bogatyrisName = $entity<bogatyris> || converter = $converters.bogatyris

theme: /Bogatyris

    state: Enter || modal=true
        q!: * (*скажи/скажи*/*читай/*чти/~слушать/~послушать) * [про/о] * (~богатырь/былин*) *
        script:
            bogatyris.init();
        a: Давным-давно на Руси были свои супергерои - богатыри. Это были храбрые воины и защитники русского народа. У меня в памяти много рассказов о них. Какой рассказ ты хочешь послушать?

        state: WhatYouHave
            q: * (что/какие/какой/че) * (есть/знаешь/умеешь/можешь/хочешь) *
            q: * (назови/перечисли/предложи/выбери/выбирай/на твой выбор/на твое усмотрение/доверяю/полагаюсь/положусь/люб*/*нибудь/богатыр*/~былина) *
            random:
                a: Давай послушаем мою любимую историю
                a: Предлагаю послушать самую интересную историю из всех, что у меня есть
                a: Слушай 
            script:
                bogatyris.randomBTale();
            go!: /Bogatyris/PlayTale


        state: Nothing
            q: * ($notNow/$disagree/никак*/хватит/прекрати/прекращай/остановись/стоп/надоело) *
            a: Обязательно послушай все мои истории о богатырях. Они очень древние и очень героические. Во что хочешь поиграть сейчас? 
            go!: / 

        state: ChosenTale
            q: * $bogatyrisName *
            script:
                $session.bogatyris.prevTale = $session.bogatyris.currentTale ? $session.bogatyris.currentTale : null;
                $session.bogatyris.currentTale = $parseTree.bogatyrisName[0].value;
                $session.bogatyris.currentAudio = $session.bogatyris.currentTale.audio[0];
                $session.bTalesTold.push($parseTree.bogatyrisName[0].bogatyris[0].value);
                $session.bogatyris.parts = $bogatyris[$parseTree.bogatyrisName[0].bogatyris[0].value].value.audio.length;
                $session.bogatyris.currentPart = 0;
            a: Слушай. Если захочешь остановиться, просто скажи "Хватит"
            go!: /Bogatyris/PlayTale        
            state: No
                q: * ($no/$disagree) *
                go!: /Bogatyris/Enter/Nothing

        state: CatchAll
            q: * [$changeThemeWords] *
            script:
                var res = $nlp.match($request.query, "/");
                if ($parseTree.changeThemeWords && res.targetState) {
                    $reactions.transition(res.targetState);
                } else if($session.flagForNonsense) {
                    $reactions.answer('Кажется, становится скучновато. Во что тебе хочется поиграть сейчас?');
                    $reactions.transition("/");
                } else {
                    $session.flagForNonsense = true;
                    $reactions.answer('Кажется, мы собирались послушать рассказ о героях-богатырях? Будем слушать?');
                }

            state: Yes
                q: * ($yes/вперед/$agree/буд*) *
                a: Какой рассказ ты хочешь послушать?
                go: /Bogatyris/Enter
    
            state: No
                q: * ($no/$disagree) *
                go!: /Bogatyris/Enter/Nothing
        

    state: EnterParticularTale
        q!: * [*скажи/скажи*/~читать/~почитать/~зачитать/~прочитать/~слушать/~послушать] * [про/о] * $bogatyrisName *
        random:
            a: Слушай рассказ про этого славного героя. Если захочешь остановиться, просто скажи "Хватит".
            a: Если захочешь остановиться, просто скажи "Хватит". А сейчас слушай.
        script:
            bogatyris.init();
            $session.bogatyris.prevTale = $session.bogatyris.currentTale ? $session.bogatyris.currentTale : null;
            $session.bogatyris.currentTale = $parseTree.bogatyrisName[0].value;
            $session.bogatyris.currentAudio = $session.bogatyris.currentTale.audio[0];
            $session.bTalesTold.push($parseTree.bogatyrisName[0].bogatyris[0].value);
            $session.bogatyris.parts = $bogatyris[$parseTree.bogatyrisName[0].bogatyris[0].value].value.audio.length;
            $session.bogatyris.currentPart = 0;
        go!: /Bogatyris/PlayTale

    state: GoOn
        q!: * (продолж*/дальше) * (~богатырь/$bogatyrisName) *
        a: На чем мы остановились? Слушай
        go!: /Bogatyris/PlayTale

    state: PlayTale || modal=true
        script:
            $temp.rep = true;
            bogatyris.playBTale();

        state: NextPart || modal=true
            state: Yes
                q: * ($yes/вперед/$agree/следующ*/дальше/далее/еще) *
                script:
                    $session.bogatyris.currentPart += 1;
                go!: /Bogatyris/PlayTale
    
            state: No
                q: * ($no/$disagree) *
                go!: /Bogatyris/Enter/Nothing


        state: Again
            q: * [*скажи/скажи*/~читать/~почитать/~зачитать/~прочитать] * (повтор*/еще раз*/занов*/с начал*) * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            go!: ../../PlayTale
    
        state: Next
            q: * [давай/включи/переключи] * (друг*/следующ*/еще од*/дальше/далее) [сказк*/былин*/богатыр*] * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            q: * (*скажи/скажи*/*читай/*чти) * [про/о] * (~богатырь/былин*) * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            script:
                bogatyris.randomBTale();
            go!: /Bogatyris/PlayTale

        state: PreviousPart
            q: * (предыдущ*/прошл*) * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            script:
                if ($session.bogatyris.currentPart > 0) {
                    $session.bogatyris.currentPart -= 1;
                    $reactions.transition("../../PlayTale");
                } else {
                    $reactions.transition("../Previous");
                }

        state: Previous
            q: * (предыдущ*/прошл*) * (сказк*/былин*) * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            script: 
                if ($session.bogatyris.prevTal == null) {
                    bogatyris.randomBTale();
                    $reactions.transition("../../PlayTale");
                } else {
                    $temp.replaceTale = $session.bogatyris.currentTale;
                    $session.bogatyris.currentTale = $session.bogatyris.prevTale;
                    $session.bogatyris.prevTal = $temp.replaceTale;
                    $session.bogatyris.currentAudio = $session.bogatyris.currentTale.audio[0];
                    $session.bogatyris.parts = $session.bogatyris.currentTale.audio.length;
                    $session.bogatyris.currentPart = 0;
                }
            go!: /Bogatyris/PlayTale
                
    
        state: AllTalesTold
            script:
                $session.bTalesTold = [];
            a: Мы послушали все рассказы о богатырях, которые я знаю. Послушаем рассказы о храбрых воинах еще раз?
    
        state: StopBTale
            q: * ($notNow/$disagree/никак*/хватит/прекрати/прекращай/остановись/стоп/надоело) *
            q: * ($notNow/$disagree/никак*/хватит/прекрати/прекращай/остановись/стоп/надоело) * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            a: Обязательно послушай все мои истории о богатырях. Они очень древние и очень героические. Во что хочешь поиграть сейчас?
            go!: /

        state: ChangeThemes
            q: * $changeThemeWords * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            go!: /Bogatyris/MoveFromSkill

        state: CatchAll
            q: *
            q: * || fromState = "/Bogatyris/PlayTale/NextPart", onlyThisState = true
            script:
                if($session.flagForNonsense) {
                    $reactions.answer('Кажется, становится скучновато. Во что тебе хочется поиграть сейчас?');
                    $reactions.transition("/");
                } else {
                    $session.flagForNonsense = true;
                    $reactions.answer('Кажется, мы собирались послушать рассказ о героях-богатырях? Будем слушать?');
                }

        state: Yes
            q: * ($yes/вперед/$agree/буд*) *
            a: Какой рассказ ты хочешь послушать?
            go: /Bogatyris/Enter
    
        state: No
            q: * ($no/$disagree) *
            go!: /Bogatyris/Enter/Nothing

    state: MoveFromSkill
        script:
            var res = $nlp.match($request.query, "/");
            if ($parseTree.changeThemeWords && res.targetState) {
                $reactions.transition(res.targetState);
            }
