require: LearnAnswer.js

theme: /LearnAnswer

    state: LearnAnswer
        q!: * (когда/если) * ([тебе/тебя] (скаж*/говор*/спр*)/[ты] *слышишь [вопрос]) $Text::Question [ты] (ответь/отвечай/(скажи/говори) [в ответ]) $Text::Answer
        q!: ([тебе/тебя] (скаж*/говор*/спр*)/[ты] *слышишь [вопрос]) $Text::Question [ты] (ответь/отвечай/(скажи/говори) [в ответ]) $Text::Answer    
        script:
            learnAnswer("answersLearned", $parseTree.Question[0].text, $parseTree.Answer[0].text);
            $reactions.answer("Хорошо!", "Запомнила!", "Записала себе!", "Выучила!", "Запомню!");
            $reactions.answer("Если мне скажут '" + parseTree.Question[0].text + "', я отвечу '" + parseTree.Answer[0].text + "'.");

    state: SayAnswer || noContext = true
        script:
            $reactions.answer(getAnswerLearned("answersLearned", parseTree.text));

    state: ForgetAnswer
        q!: * (забудь/сотри) * ответ* *
        q: * (забудь/сотри/$agree) *, fromState = ../HowToForget
        script:
            $client.answersLearned = []; 
            $reactions.answer("Я забыла все ответы.");

    state: HowToForget
        q!: * как * забы* * ответ* *
        q!: * не (отвечай/говори) * [так] больше *
        q: забудь [это]
        q: * ($bad/нельзя) так говорить *    
        script:
            $reactions.answer("Я могу стереть из памяти слова, которым ты меня учишь. Хочешь, чтобы я их забыла?");

        state: DontForget
            q: * ($disagree/не (заб*/стир*)) *
            script:
                $reactions.answer("Хорошо! Я ничего не забуду!", "Буду помнить.");