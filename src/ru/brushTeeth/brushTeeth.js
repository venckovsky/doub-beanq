var brushTeeth = (function() {
    
    function init() {
        var $client = $jsapi.context().client;
        var $session = $jsapi.context().session;

        $client.brushTime = currentDate().format("HH:mm");

        // Для последовательного тестирования микросценариев разных типов
        if (testMode()) {
            $session.testFables = $session.testFables || [7, 21, 30, 2, 18, 1];
            $session.fableData = $fablesEvening[$session.testFables.pop()].value;
        } else {
            $session.fableDict = (($client.brushTime >= "04:00") && ($client.brushTime < "16:00")) ? $fablesMorning : $fablesEvening;
            $session.fableData = getRandomElement($session.fableDict);
        }
    }

    function changeTheme() {
        var $request = $jsapi.context().request;
        var res = $nlp.match($request.query, "/");

        if (res.targetState) {
            $reactions.transition(res.targetState);
        }
    }

    return {
        init: init,
        changeTheme: changeTheme
    };
})();
