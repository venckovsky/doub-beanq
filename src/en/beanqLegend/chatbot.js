﻿function alias() {
    var $client = $jsapi.context().client;
    var alias;
    if ($client.names) {
        if ($client.names.diminutive) {
            alias = $client.names.diminutive;
        } else {
            alias = $client.firstName;
        }
    } else {
        alias = "My friend from Earth!";
    }  
    return alias;
}

function nextGame() {
    var $temp = $jsapi.context().temp;
    var games = [];
    if (themeExists("Animal quiz")) {games.push("Zoo");}
    if (themeExists("Food quiz")) {games.push("Food");}
    if (themeExists("Gorodagame")) {games.push("Goroda");}
    if (themeExists("Math game")) {games.push("Math");}    
    if (themeExists("Geography quiz")) {games.push("Geography");}
    if (themeExists("Odd word game")) {games.push("Odd word game");}
    if (themeExists("Calendar quiz")) {games.push("Calendar");}
    if (themeExists("Guess Number Game")) {games.push("GuessNumber");}   
    if (themeExists("Opposites")) {games.push("Opposites");}

    var randGame = games[randomIndex(games)];

    switch(randGame) {
        case "Zoo":
            $reactions.answer("Let's play zoo!");
            $temp.nextTheme = "Animal quiz/Animal game pudding start/Animal game question";
            break;
        case "Food":
            $reactions.answer("Давай в буфет!");
            $temp.nextTheme = "Food quiz/Food game pudding start/Food game question";
            break;
        case "Goroda":
            $reactions.answer("Let's play the word chain!");
            $temp.nextTheme = "Goroda game intro";
            break;
        case "Math":
            $reactions.answer("Let's play maths!");
            $temp.nextTheme = "Math game/Math game pudding start/Math game question";
            break;
        case "Geography":
            $reactions.answer("Let's play the geography game!");
            $temp.nextTheme = "Geography quiz";
            break;  
        case "Extra game":
            $reactions.answer("Let's play the odd one out game!");
            $temp.nextTheme = "Extra game";
            break;       
        case "Calendar":
            $reactions.answer("Давай в Календарь!");
            $temp.nextTheme = "Calendar quiz/Pudding start/Question";
            break;
        case "GuessNumber":
            $reactions.answer("Давай угадывать числа!");
            $temp.nextTheme = "Guess Number Game/Guess/Yes";
            break;
        case "Opposites":
            $reactions.answer("Давай играть в противоположности!");
            $temp.nextTheme = "Opposites/Pudding";
            break;                              
    }
}

function foundSimilarTheme() {
    var $session = $jsapi.context().session;
    var $temp = $jsapi.context().temp;
    var $client = $jsapi.context().client;
    
    var found = false;
    var theme = '';
    var states = [];
    var themes = ['Puddings','Education','Friendship'];
    var similarThemes = [];
    states = $jsapi.context().contextPath;
    for (var i=0; i<themes.length; i++) {
        for (var j=0; j<states.length; j++) {
            if (states[j].indexOf(themes[i]) >= 0) {
                theme = themes[i];
                break;
            }
        }
        if (theme != '') {
            break;
        }
    }
    switch(theme) {
        case '':
            break;
        case 'Puddings':
            if (typeof $session.PuddingsPlanet == 'undefined') {
                similarThemes.push("Puddings/StartDialog/Planet");
            }
            if (typeof $session.PuddingsWhoArePuddings == 'undefined') {
                similarThemes.push("Puddings/StartDialog/Who are puddings");
            }
            if (typeof $session.PuddingsWhoArePuddingsPuddingsTraveling == 'undefined') {
                similarThemes.push("Puddings/StartDialog/Who are puddings/Puddings traveling");
            }
            break;
        case 'Education':
            if (typeof $session.Roboschool == 'undefined') {
                similarThemes.push("Education/Roboschool");
            }
            if (typeof $session.SchoolExchange == 'undefined') {
                similarThemes.push("Education/School exchange");
            }        
            break;        
        case 'Friendship':
            if (typeof $client.askedBestFriend == 'undefined') {                
                similarThemes.push("Relationship/Friendship best friend");
            }
            if (typeof $client.askedBestFriendName == 'undefined') {                
                similarThemes.push("Relationship/Friendship best friend name");
            }
            if (typeof $client.askedFriendshipEnquiries == 'undefined') {             
                similarThemes.push("Relationship/Friendship enquiries");
            }        
            break;        
    }  
    /* выбор темы из доступных */
    if (similarThemes.length != 0) {
        var index = randomIndex(similarThemes);
        $temp.nextTheme = similarThemes[index];
        found = true;
    }
   return found;
}

function nextTheme() {
    var $session = $jsapi.context().session;
    var $temp = $jsapi.context().temp;
    var $client = $jsapi.context().client;
 
    var congratulation = congratulationIsNeeded();
    if (congratulation.found) {
        $temp.nextTheme = congratulation.state;
    } else {

        if (foundSimilarTheme() == false) {

            var themes = [];
            // первые вопросы - как зовут, как дела
            if ($client.firstName || typeof $session.askedName != 'undefined') {
                if ($session.userMood) {
                    if (hintIsNeeded()) {
                        $temp.nextTheme = "Hints/Get";
                    } else {
                        //определение доступных тем 
                        if (typeof $client.age == 'undefined') {
                            themes.push("UserProfile/How old are you");
                        }
                      if (themeExists("WhyQuestion") && !$session.askedWhy || ($session.askedWhy && $session.askedWhy.questions && $session.askedWhy.questions.length < 3)) {
                            themes.push("WhyQuestion/Ask");
                        }
                        if (themeExists("Riddle") && typeof $session.usedRiddles == 'undefined' && !$session.askedRiddle) {
                            themes.push("Riddle/Offer");
                        }                      
                        if (typeof $session.PuddingsStartDialog == 'undefined' && !$temp.Question) {
                            themes.push("Puddings/StartDialog");                
                        }            
                        if (typeof $session.PuddingsPlanet == 'undefined' && !$temp.Question) {
                            themes.push("Puddings/StartDialog/Planet");
                        }
                        if (typeof $session.PuddingsWhoArePuddings == 'undefined' && !$temp.Question) {
                            themes.push("Puddings/StartDialog/Who are puddings");
                        }
                        if (typeof $session.PuddingsWhoArePuddingsPuddingsTraveling == 'undefined' && !$temp.Question) {
                            themes.push("Puddings/StartDialog/Who are puddings/Puddings traveling");
                        }
                        if (typeof $session.Roboschool == 'undefined' && !$temp.Question) {
                            themes.push("Education/Roboschool");
                        }
                        if (typeof $session.SchoolExchange == 'undefined' && !$temp.Question) {
                            themes.push("Education/School exchange");
                        }
                        if (typeof $client.askedFavoriteCartoon == 'undefined') {                   
                            themes.push("Preferences/Ask user about cartoon");
                        }                    
                        if (typeof $client.askedFavoriteToDo == 'undefined') {                   
                            themes.push("Preferences/Ask user about to do");
                        }
                        if (typeof $client.favoriteColor == 'undefined' && $session.askedFavoriteColor == 'undefined') {
                            themes.push("Preferences/Ask user about color");
                        }                    
                        if (typeof $session.askedFavoriteFood == 'undefined' && $client.favoriteFood == 'undefined' ) {               
                            themes.push("Preferences/Ask user about food");
                        }
                        if (typeof $session.askedFavoriteDrink == 'undefined'  && $client.favoriteDrink == 'undefined') {             
                            themes.push("Preferences/Ask user about drink");
                        }
                        if (typeof $client.askedFavoriteMusic == 'undefined') {               
                            themes.push("Preferences/Ask user about music");
                        }
                        if (typeof $session.askedFavoriteSubject == 'undefined' && $client.favoriteSubject == 'undefined' && ageInterval(7,18)) {               
                            themes.push("Preferences/Ask user about subject");
                        }
                        if (typeof $session.askedFavoriteToy == 'undefined' && ageInterval(4,10)) {               
                            themes.push("Preferences/Ask user about toy");
                        }
                        if (typeof $client.askedFavoriteAnimal == 'undefined') {               
                            themes.push("Preferences/Ask user about animal");
                        }
                        if (typeof $client.askedFavoriteBook == 'undefined') { 
                            themes.push("Preferences/Ask user about book");
                        }
                        if (typeof $client.askedFavoriteGame == 'undefined') {             
                            themes.push("Preferences/Ask user about game");
                        }
                        if (typeof $client.askedDoTogether == 'undefined') {                
                            themes.push("Relationship/What do you do together");
                        }
                        if (typeof $client.askedAboutFamily == 'undefined') {             
                            themes.push("Relationship/Do you have family");
                        }
                        if (typeof $client.askedAboutFriends == 'undefined') {               
                            themes.push("Relationship/Do you have friends");
                        }
                        if (typeof $client.askedBestFriend == 'undefined') {                
                            themes.push("Relationship/Friendship best friend");
                        }
                        if (typeof $client.askedBestFriendName == 'undefined') {                
                            themes.push("Relationship/Friendship best friend name");
                        }
                        if (typeof $client.askedFriendshipEnquiries == 'undefined') {             
                            themes.push("Relationship/Friendship enquiries");
                        }
                        if (typeof $client.askedAboutLove == 'undefined') {                  
                            themes.push("Relationship/Ask about love");
                        }
                        if (isMorning()) {
                            if ($session.plansMorning) {
                            } else {
                                themes.push("Every day I ask/Do you have any plans morning"); 
                            }
                        }
                        if (isEvening()) {
                            if ($session.dayEvening) {
                            } else {
                                themes.push("Every day I ask/How was your day evening"); 
                            }
                            if ($session.didEvening) {
                            } else {
                                if (isMale() || isFemale()) {
                                    themes.push("Every day I ask/What did you do today evening"); 
                                }
                            }                
                        }            
                        if (isNight()) {
                            if ($session.notSleep) {
                            } else {
                                themes.push("Every day I ask/Night why do you not sleep"); 
                            }
                        }
                        if (typeof $session.askedAboutWeather == 'undefined') {                  
                            themes.push("Every day I ask/What do you think about the weather");
                        }
                        if (typeof $session.askedAboutMood == 'undefined') {                  
                            themes.push("Every day I ask/What is your mood");
                        }
                        if (typeof $session.askedAboutDoing == 'undefined') {                  
                            themes.push("Every day I ask/What are you doing");
                        }   
                        if (typeof $session.askedGames == 'undefined') {                
                            themes.push("PlayGames/Games"); 
                        }
                        if (!$client.birthday && typeof $session.askedBirthday == 'undefined') {
                            themes.push("UserProfile/UsersBirthday/When");
                        }                                       
                        /* выбор темы из доступных */
                        if (themes.length == 0) {
                            $temp.nextTheme = "NothingToTalk";
                        } else {
                            var index = randomIndex(themes);
                            $temp.nextTheme = themes[index];
                        }
                    }
                } else {
                    $temp.nextTheme = "Greetings/User How are you";
                } 
            } else {
                $temp.nextTheme = "UserProfile/What is your name";
            }

        }
    }
}

function getHint() {
    var $client = $jsapi.context().client; 
    var numberOfElements = Object.keys($Hints).length;
    var result = '';
    var hint, hints = [], isNew;
    //формируем список из неиспользованных подсказок    
    for (var i=1; i<=numberOfElements; i++) {
        hint = $Hints[i].value;        
        isNew = true;
        for (var j=0; j<$client.hints.used.length; j++) {
            if (hint.alias == $client.hints.used[j]) {
                isNew = false;
                break;
            }
        }
        if (isNew) {
            if (hints.length == 0) {
                hints[0] = hint;
            } else {
                hints.push(hint);
            }
        }
    }
    //выбираем самую приоритетную подсказку и запоминаем, что использовали её
    if (hints.length > 0) {
        hints.sort(function (a, b) {
            return b.priority - a.priority;
        });
        result = { 
            phrase : hints[0].phrase, 
            context : hints[0].context
        };
        if ($client.hints.used.length == 0) {
            $client.hints.used[0] = hints[0].alias;
        } else {
            $client.hints.used.push(hints[0].alias);
        }
    }
    return result;
}

function hintIsNeeded() {
    var $session = $jsapi.context().session;
    var $client = $jsapi.context().client; 
    var result = false;
    var hint, isNew;
    if ($client.hints) {
        if (typeof $session.usedHints == 'undefined') {
            var numberOfElements = Object.keys($Hints).length;          
            for (var i=1; i<=numberOfElements; i++) {
                isNew = true;
                for (var j=0; j<$client.hints.used.length; j++) {
                    hint = $Hints[i].value;
                    if (hint.alias == $client.hints.used[j]) {
                        isNew = false;
                        break;
                    }
                }
                if (isNew) {
                    result = true;
                    break;
                }
            }
        }
    } else {
        $client.hints = {
            used: []
        };
        result = true;
    }
    return result;
}

function isMorning() {
    return isTime(6,9);   
}

function isEvening() {
    return isTime(19,22);   
}

function isNight() {
    return isTime(0,5);   
}

function isTime(a,b) {
    var hour = moment(currentDate()).hour();
    return (hour >= a && hour < b);    
}

function congratulationIsNeeded() {
    var $client = $jsapi.context().client;

    var result = {
        found: false,
        state: ""
    };

    if (isUsersBirthday() && !$client.greetedBirthday){
        result.state = "UsersBirthday/HappyBirthday";
    } else if (!isUsersBirthday() && $client.greetedBirthday) {
        delete $client.greetedBirthday;
    }
    if (isNewYear() && !$client.greetedNewYear){
        result.state = "Season greetings/happy new year";
    } else if (!isNewYear() && $client.greetedNewYear) {
        delete $client.greetedNewYear;
    }
    if (isThanksgiving() && !$client.greetedThanksgiving) {
        result.state = "Season greetings/Happy thanksgiving";
    } else if (!isThanksgiving() && $client.greetedThanksgiving) {
        delete $client.greetedThanksgiving;
    }
    if (isMothersDay() && !$client.greetedMothersDay){
        result.state = "Season greetings/Happy mothersday";
    } else if (!isMothersDay() && $client.greetedMothersDay) {
        delete $client.greetedMothersDay;
    }
    if (isFathersDay() && !$client.greetedFathersDay) {
        result.state = "Season greetings/happy fathersday";
    } else if (!isFathersDay() && $client.greetedFathersDay) {
        delete $client.greetedFathersDay;
    }
    if (isSuperBowlDay() && !$client.greetedSuperBowlDay){
        result.state = "Season greetings/happy superbowl";
    } else if (!isSuperBowlDay() && $client.greetedSuperBowlDay) {
        delete $client.greetedSuperBowlDay;
    }
    if (isIndependenceDay() && !$client.greetedIndependenceDay) {
        result.state = "Season greetings/happy independence day";
    } else if (!isIndependenceDay() && $client.greetedIndependenceDay) {
        delete $client.greetedIndependenceDay;
    }
    if (isValentineDay() && !$client.greetedValentineDay){
        result.state = "Season greetings/happy valentine";
    } else if (!isValentineDay() && $client.greetedValentineDay) {
        delete $client.greetedValentineDay;
    }
    if (isHalloween() && !$client.greetedHalloween) {
        result.state = "Season greetings/happy halloween";
    } else if (!isHalloween() && $client.greetedHalloween) {
        delete $client.greetedHalloween;
    }
    if (isStPatrick() && !$client.greetedPatrick){
        result.state = "Season greetings/happy patrick";
    } else if (!isStPatrick() && $client.greetedPatrick) {
        delete $client.greetedPatrick;
    }

    if (result.state != "") {
        result.found = true;
    }

    return result;
}

function isUsersBirthday(){
    var $client = $jsapi.context().client;
    if ($client.birthday && $client.birthday.month && $client.birthday.date) {
        var day = moment(currentDate());
        if ((day.month() + 1 == $client.birthday.month) && (day.date() == $client.birthday.date)) {
            return true;
        }
    }
    return false;
}

function isNewYear() {
    return isDate(360,10);   
}

function isThanksgiving(){
    var day = moment(currentDate());
    return (day.month()==10 && day.date()>=22 && day.date()<=28 && day.day()==4);
}

function isMothersDay(){
    var day = moment(currentDate());
    return (day.month()==4 && day.date()>=8 && day.date()<=14 && day.day()==0);
}

function isFathersDay(){
    var day = moment(currentDate());
    return (day.month()==5 && day.date()>=15 && day.date()<=21 && day.day()==0);
}

function isSuperBowlDay(){
    var day = moment(currentDate());
    return (day.month()==1 && day.date()>=1 && day.date()<=7 && day.day()==0);
}

function isIndependenceDay(){
    var day = moment(currentDate());
    return (day.month()==6 && day.date()==4);
}

function isValentineDay(){
    var day = moment(currentDate());
    return (day.month()==1 && day.date()==14);
}

function isHalloween(){
    var day = moment(currentDate());
    return (day.month()==9 && day.date()==31);
}

function isStPatrick(){
    var day = moment(currentDate());
    return (day.month()==2 && day.date()==17);
}


function isDate(dayA, dayB) {
    var day = moment(currentDate()).dayOfYear();
    if (dayA <= dayB) {
        return (day >= dayA && day <= dayB); 
    } else {
        return (day >= dayA || day <= dayB); 
    }
       
}

function isMale() {
    var $client = $jsapi.context().client;
    return ($client.gender && $client.gender == 0);    
}

function isFemale() {
    var $client = $jsapi.context().client;
    return ($client.gender && $client.gender == 1);    
}

function toGender(text1, text2, text3) {
    return isMale() ? text1 : (isFemale() ? text2 : text3);
}

function checkNonsense(text) {
    //если фраза очень короткая
    if (text.length < 9) {
        return true;
    } else {
    //если фраза подходит под паттерн фрагмента
        var result = $nlp.match(text, "/NoMatch/ASR_Fragments");
        if (result) {
            return true;
        } else {
        //если велика доля несловарных слов
            var words = $nlp.tokenize(text);
            var count = 0;
            for (var i = 0; i < words.length; i++) {
                var normalForm = null;
                try {
                    normalForm = $nlp.parseMorph(words[i]).normalForm;
                } catch(ex) {}
                if (normalForm == null) {
                    count++;
                }
            }
            if ((count / words.length) > 0.3) {
                return true;
            } else {
                return false;
            }
        }
    }
}

function ageInterval(age1, age2) {
    var $client = $jsapi.context().client;
    if ($client.age) {
        if ($client.age >= age1 && $client.age <= age2) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}
