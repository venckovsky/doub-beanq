require: ../../main.js
require: ../../patterns.sc
require: common/common.sc
  module = zenbox_common

theme: /Callback

    state: Callback
        q!: [$AnyWord] [$you] [$pls] [можешь] [$pls] (позвонить/набрать/связаться/набери/позвони/звони/свяжись/сдела* звонок) [с/номер] [$pls] [$me] $AnyWord [$AnyWord] [$pls]
        q!: [$pls] [$me] $AnyWord [$AnyWord] [$pls]  (позвонить/набрать/связаться/набери/позвони/звони/свяжись/сдела* звонок) [с/номер] [$pls] 
        q!: * (позвонил/набрал/сделал звонок) $AnyWord [$AnyWord] [$pls]
        script:
            $response.intent = "callback";
        random:
            a: Я передала твою просьбу.
            a: Да, хорошо, мой друг.
            a: Команда ясна, уже делаю звонок!
            a: Я тоже люблю звонить моим близким. Уже набираю!

    
    state: Where is mum
        q!: * где [мо*] (мама/папа/баба/бабушк*/деда/дедушк*/брат*/сестр*/дяд*/тет*) [$AnyWord] [$pls] [$me]
        q!: {[$botName] где [мо*] (мама/папа)}
        script:
            $response.intent = "callback";
        random:
            a: Может быть, на работе. Я передам, что ты скучаешь.
            a: Все мы скучаем. Я уже передала твоё сообщение.
            a: Наверное, снова на работе. Я попросила позвонить тебе. 