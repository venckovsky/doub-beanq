theme: /Cannot

    state: Video
        q!: * $turnOn * video* *
        script:
            $reactions.answer("I'm not yet ready to work with video.");
        

    state: Cartoon
        q!: * $turnOn * cartoon*  *
        script:
            $reactions.answer("Unfortunately, I can't play cartoons.");

    state: Housekeeper
        q!: * (make/order/buy/cook/(wanna/want to) have/give) * $food *
        script:
            $reactions.answer("I am a robot friend, not a housekeeper. I can play and sing songs.");
    