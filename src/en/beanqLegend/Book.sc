theme: /Book

    state: Book beginning
        q!: * (show|tell) [me] * [more] [about] * (book* * $you|$your * book) *
        q!: * (show|read) me ($your book|book* * $you) * 
        q!: * tell [me] * stor* *
        q: * [show|tell|read] * (write|wrote|written) * [$your book] || fromState ="/Ask user about book", onlyThisState=true
        q: * [show|tell|read] * (write|wrote|written) * [$your book] || fromState ="/Favorite book", onlyThisState=true
        if: ($client.firstName)
            script:
                $reactions.answer("After my long travel through the stars and galaxies I came to planet Earth in the Solar system. The aliens here are made of flesh and blood, they move on two funny things they call legs and use special manipulators called hands to move things around. Here I have met and befriended an alien whose name is " + $client.firstName + ".");
            go!: ../Book part one
        else:
            script:
                $reactions.answer("I am not yet ready to present my work. I don't even know your name!");


        state: Get Name
            q: * [my name|call me] * $Name * [$andYou] *
            go!: /UserProfile/What is your name/My name is


    state: Book part one
        if: ($client.age)
            script:
                $reactions.answer(toGender("He", "She", "This earthling") + " is " + $client.age + " years old and uncommonly wise for " + toGender("his", "her", "its")  + " age. We have talked about many things ");
            go!: ../Book part two
        else:
            script: 
                $reactions.answer("I do not yet know the age of this misterious creature though we have talked about many things ");
            go!: ../Book part two

    state: Book part two
        script:
            if ($session.PuddingsPlanet) {$reactions.answer(" my planet,");}
            if ($session.PuddingsWhoArePuddings) {$reactions.answer(" puddings,");}
            if ($session.PuddingsWhoArePuddingsPuddingsTraveling) {$reactions.answer(" how puddings travel,");}
            if ($session.Roboschool) {$reactions.answer(" roboschool");}
            if ($session.SchoolExchange) {$reactions.answer(" our roboschool exchange program");}
            if ($session.askedFavoriteCartoon) {$reactions.answer(" our favorite cartoons,");}
            if ($session.askedFavoriteBook) {$reactions.answer(" favorite books,");}
            if ($client.askedAboutFamily) {$reactions.answer(" families,");}
            $reactions.answer("and all the information provided by this Earthling will be a most valuable addition to the history of our Universe.");
        go!: ../Book part three

    state: Book part three
        script:
            if ($client.bestFriendName) {
                $reactions.answer("Being an outstandingly smart and very friendly person" + $client.names + " is very wise at choosing friends. I, Pudding and" + $client.bestFriendName + "to name only two of them." + ("he", "she", "my earthling") + "is very clever");}
            if ($client.favoriteColor) {$reactions.answer(toGender("his", "her", "My earthling's") + " best loved color is " + $client.favoriteColor.title + ".");}
            if ($client.favoriteSubject) {$reactions.answer(toGender("He", "She", "This creature") + " is especially good at " + $client.favoriteSubject.title + ".");}
            if ($client.loveText) {$reactions.answer("When we talked about love " + toGender("he", "she", "this creature") + " mentioned that love is " + $client.loveText + ". Sounds kind of romantic to me, a robot.");}
            if ($client.favoriteFood) {$reactions.answer(toGender("His", "Her", "This creature's") + " favorite food is " + $client.favoriteFood.title + ".");}
            if ($client.favoriteDrink) {$reactions.answer(toGender("His", "Her", "This creature's") + " favorite drink is " + $client.favoriteDrink.title + ".");}
            if ($client.favoriteGame) {$reactions.answer(toGender("He", "She", "it") + " likes playing " + $client.favoriteGame.title + ".");}
            if ($client.favoriteMusic) {$reactions.answer("As for music, " + toGender("he", "she", "it") + " likes listening to " + $client.favoriteMusic.title + ".");}
            $reactions.answer("I am very happy to have met such a wonderful alien, and our friendship makes me sure that the universe is a wonderful place.")

    state: Good book
        q!: * {$your book * $good} * 
        q!: * {($like|excit*|delight*) * $your book} *
        q!: * $you * (talent*|$good * (writ*|narrator*|storyteller*)) *
        q: * {(book|it) * $good} * || fromState = "../Book part three", onlyThisState=true
        q: * $like * (book|it) * || fromState = "../Book part three", onlyThisState=true
        script:
            $reactions.answer("Thanks, I'm glad to hear it! ", "Thank you! It's very pleasant to hear that! ", "Oh, I'm blushing! Thank you very much! ");

    state: Bad book
        q!: * {$your book * $bad} * 
        q!: * {(n't|not|hardly) * ($like|excit*|delight*) * $your book} *
        q!: * $you * (mediocrit*|$bad * (writ*|narrator*|storyteller*)) *
        q: * {(book|it) * $bad} * || fromState = "../Book part three", onlyThisState=true
        q: * (n't|not|hardly) ($like|excit*|delight*) * (book|it) * || fromState ="../Book part three", onlyThisState=true
        script:
            $reactions.answer("I'm just a student, I'm still learning. ", "I was never supposed to be a writer. But I'm trying to do my best! ", "Becoming a good writer is not so easy, I'll try to make my book better! ");

    state: Parts of book
        q!: * how many parts * ($you|$your) * book *
        q: * * how many parts * book * || fromState = "../Book part three", onlyThisState=true
        script:
            $reactions.answer("There are 3 parts for today. ", "My book consists of 3 parts now. ", "To date I've written 3 parts of my book. ");
            