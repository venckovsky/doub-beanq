function setAlarm(action, locale){
    fillDict();
    checkOffset();
    var $response = $jsapi.context().response;
    var $session = $jsapi.context().session;
    var $client = $jsapi.context().client;
    $response.action = action;
    $response.id = $session.id;
    $response.timer = $client.id_to_info[$session.id][0];
    $response.text = $client.id_to_info[$session.id][1];
    var sessionTimerMoment = moment.unix($session.timer).utcOffset($session.offset);
    var curDate = currentDate().utcOffset($session.offset);
    if (sessionTimerMoment.date() === curDate.date() && sessionTimerMoment.month() === curDate.month()) {
        return sessionTimerMoment.locale(locale).format('LT');
    } else {
        return sessionTimerMoment.locale(locale).format('LT, D MMMM');
    }
}

function fillDict(){
    var $session = $jsapi.context().session;
    var $client = $jsapi.context().client;    
    if ($client.id_to_info == undefined){
        $client.id_to_info = {};
        $session.id = 0;
    }
    else{
        if (Object.keys($client.id_to_info).length > 0){
            $session.id = Math.max.apply(null, Object.keys($client.id_to_info)) + 1;
        } else{
            $session.id = 0;    
        }    
    } 

    $client.id_to_info[$session.id] = [$session.timer, $session.reminderSummary];
}

function removeAlarm(action, type, value, text1, text2){
    var $response = $jsapi.context().response;    
    var $client = $jsapi.context().client;    
    var $temp = $jsapi.context().temp;   
    var id = -1;
    var j = (type == 'summary') ? 1:0;
    if ($client.id_to_info) {
        for (var i = 0; i < Object.keys($client.id_to_info).length; i++) {
            var k = Object.keys($client.id_to_info)[i];
            if (value == $client.id_to_info[k][j]){
                id = parseInt(k);
                break;
            }
        }
    }
    if (id > -1) {
        $response.action = action;
        $response.id = id;
        $temp.summary = $client.id_to_info[id][1];
        $reactions.answer(text1);
        delete $client.id_to_info[id];
    } else {
        $reactions.answer(text2);
    }
    
}