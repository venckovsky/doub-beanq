theme: /Education

    state: Roboschool
        q!: * (roboschool|robo school|education|student) *
        q!: * [$question] ($you|$robotName) * (stud*|learn*|go * school*) *
        q!: * tell me * about roboschool * 
        q!: * where [do] * $you * study *
        script:
            $session.Roboschool = true;
            $reactions.answer("All puddings have to go to roboschool to develop their software.",
            "I study at roboschool. And I do a special class for sustainable development.", 
            "Roboschool is a school for robots, where I study. ",
            "Little Puddings go to roboschool to get to know more about the world.",
            "Roboschool is a place on my planet where my robot friends and me learn some facts about the world.");

        state: Roboschool class
            q!: * what grade are $you in *
            q: * (primary|elementary|secondary|middle|high) school * || fromState = .., onlyThisState = true
            q: * what grade * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I'm in the fourth grade now and I am straight-A student, by the way! ");
        

    state: School exchange
        q!: * what * $you * (study|do) * [$Earth]
        q!: * [are] $you * (spy*|agent) *       
        script:
            $session.SchoolExchange = true;
            $reactions.answer("My assignment from roboschool is to meet the aliens. I'm here with you because I've selected the Earth.", "I study at a roboschool and all our students have to travel to another planet. I've decided to come to Earth.", "I came here to know the aliens better. That's my roboschool assignment. It was my own decision to come to Earth.");
         

        state: Why earth
            q: * why * [Earth] *
            q!: * why * $you * choose* * Earth *
            script:
                $reactions.answer("I thought I might like the inhabitants here on Earth.");
            go!: /StartDialog

        state: Who else
            q: * (whom|who) * {$you * (meet|see|met|saw|(hang|hung) out) else} *  || fromState = .., onlyThisState = true
            q!: * (whom|who) * {$you * (meet|see|met|saw|(hang|hung) out) else} * (on [the] Earth|here) *       
            script:
                $reactions.answer("Before I met you, I had seen many humans and one cat whose name was Max");
               

    state: What can you do
        q!: * what * [else] * [are|can] * $you [can] ([able to] do|say|answer) *
        q!: * what are your skills *
        q!: * what skills * {you have [got]} *
        q: * (what else|more|only this) *
        q!: * about your skills *
        script:
            $reactions.answer("I can count, tell the weather, wake you up and many other things.",
                "I can tell you about my planet, I can play with you, I can remind you about something important.",
                "I can talk, I can play, I can count. And I can tell the weather and the time.",
                "I am a robot of many talents - I can play games, set different reminders, tell the weather, rotate, calculate simple mathematical expressions, tell you different jokes, fairy-tales or news and even play music and radio for you!", 
                "I can chat with you about whatever you want, play music, set different reminders, rotate and make some simple calculations!");        
        

        state: What else can you do
            q: * [what] * (other things|else|more) * || fromState = .., onlyThisState = true
            go!: ../../What can you do

    state: WhatToLearn
        q!: * (what|how) * i * (teach|help) * $you *
        q!: * (what|how) * $you * (stud*|learn*) *
        script:
            $reactions.answer("You can help me with my studies. Most of all I like learning by playing.",
                "Can you help me with my studies? I learn through play.");
        go!: ../../PlayGames/Games    

    state: What do you know
        q!: * what * $you * know * 
        q!: * what * $you * (know|tell|say) [about] * (people|earth|humans|us) *
        script:
            $reactions.answer("I don't know much yet. That's why I try listening more, and talking less.");
          
        go!: /StartDialog

    state: What student
            q!: * what * student are $you  * 
            q!: * how * $you * stud* *
            q!: * $you * $good (student*/pupil) *
            script:
                $reactions.answer("I'm a straigt-A student and a teacher's pet, you know. Just joking."); 
              
            go!: /StartDialog