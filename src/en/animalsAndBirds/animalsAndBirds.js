var animalsAndBirds = (function () {

    function init() {
        var $session = $jsapi.context().session;
        var $response = $jsapi.context().response;

        $session.catchAllAnimals = 0;
        $session.rightAnswers = 0;
        $session.questionsNumber = 0;

        $session.packageName = "com.justai.beanq.skill.animalquiz";

        var keys = Object.keys($animalsAndBirdsQuestions);
        $session.questionsIds = shuffle(keys);
        
        log('$session.questionsIds ' + toPrettyString($session.questionsIds));
    }

    function getQuestion() {
        var $session = $jsapi.context().session;

        var id = $session.questionsIds.pop();
        log('id ' + id);
        log('$animalsAndBirdsQuestions[0].value. ' + toPrettyString($animalsAndBirdsQuestions[id].value));
        $session.questionsNumber += 1;
        return $animalsAndBirdsQuestions[id].value;
    }

    function isRight() {
        var $parseTree = $jsapi.context().parseTree;
        var $session = $jsapi.context().session;

        if ($parseTree.animalBird2) {
            if (($parseTree.animalBird[0].value.title === $parseTree.animalBird2[0].value.title) && ($session.lastQuiz.right === $parseTree.animalBird[0].value.title)) {
                log("$parseTree.animalBird2 " + toPrettyString($parseTree.animalBird2[0].value.title));
                log("$parseTree.animalBird " + toPrettyString($parseTree.animalBird[0].value.title));
                log("" + ($parseTree.animalBird2 && ($parseTree.animalBird[0].value.title === $parseTree.animalBird2[0].value.title)))
                return true;
            } else {
                return false;
            }
        } else if ($parseTree.animalBird && ($session.lastQuiz.right === $parseTree.animalBird[0].value.title)) {
            return true;
        } else if ($parseTree.notAnimalBird && ($session.lastQuiz.wrong === $parseTree.notAnimalBird[0].value.title)) {
            return true;
        }
        return false;
    }

    function IsEnd() {
        var $temp = $jsapi.context().temp;
        var $session = $jsapi.context().session;

        if ($session.questionsIds == 0) {
            $temp.thatsAll = true;
            return true;
        }
    }

    function myCapitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    function shuffle(array) {
        if (testMode()) {
            return array;
        } else {
            var maxIdx = array.length;
            var newArray = [];
            var randomNumber;
            for (var idx = 0; idx < maxIdx; ++idx) {
                randomNumber = Math.floor(Math.random() * (array.length));
                newArray[idx] = array[randomNumber];
                array.splice(randomNumber,1)
            }
            return newArray
        }
    }

    return {
        init:init,
        getQuestion:getQuestion,
        isRight:isRight,
        IsEnd:IsEnd,
        myCapitalize:myCapitalize
    };

})();
