function getRandomPlanet(obj) {
    var arr = [];

    for (var idx in obj) {
        if (obj[idx]) {
            arr.push(obj[idx]);
        }
    }

    if (arr.length === 0) {
        return false;
    }
    if (testMode()) {
        // В тестмоде смотрим на дни недели, т. к. их мало и можно проверить,
        // как будет себя вести скилл, если закончились все слова из темы.
        return arr[8].value;
    }
    return arr[Math.floor(Math.random() * arr.length)].value;
}

function getRandomOppositeCreature(obj, theme) {
    var arr = [];

    for (var idx in obj) {
        if (obj[idx] && obj[idx].value.theme.indexOf(theme) === -1) {
            arr.push(obj[idx]);
        }
    }

    if (arr.length === 0) {
        return false;
    }
    if (testMode()) {
        return arr[0];
    }
    return arr[Math.floor(Math.random() * arr.length)];
}

function getRandomCreature(obj, theme) {
    var arr = [];

    for (var idx in obj) {
        if (obj[idx] && obj[idx].value.theme.indexOf(theme) !== -1) {
            arr.push(obj[idx]);
        }
    }

    if (arr.length === 0) {
        return false;
    }
    if (testMode()) {
        return arr[0];
    }
    return arr[Math.floor(Math.random() * arr.length)];
}

function arrayUnseal(arr) {
    var unsealedArr = [];

    for (var item in arr){
        unsealedArr.push(arr[item]);
    }

    return unsealedArr;
}
