theme: /Hobby

    state: Bot hobby
        q!: * what (is|are) $your hobb* * 
        q!: * tell me about [$your] * (hobb*/(free/spare) time) *
        q!: * $you have * hobb* *
        q!: * [$question] * $you * do* * (free|spare) * [time] *  
        q!: * how * $you * spend [your] (spare|free) time *
        script:
            $temp.nextState = selectRandomArg("Hobby/Eco club",
            "Hobby/Geography",
            "Hobby/Robots history");   
        go!: /{{ $temp.nextState }}

    state: Eco club
        q!: * {file transfer * speed}
        script:
            $reactions.answer("I'm one of the Ecological club leaders. We really try to save energy and reduce the amount of waste that goes to the galaxy.");
            $reactions.answer("Do you want me to tell you about the Eco club?");  
        

        state: Eco club details
            q: * ($agree|tell*|please*|interesting) *
            q: how
            script:
                $reactions.answer("This is a truly universal problem: too much waste and unwise usage of energy. We try to combat that.");
            

            state: Eco club why
                q: * why *
                q: * why * || fromState = "../Eco club details"   
                q: * why * || fromState = "../Eco club do"  
                q: * why * || fromState = "/Hobby/Eco club"                                                  
                script:
                    $reactions.answer("I want to make our planet and all the galaxy a better place.");
                   

            state: Eco club do
                q: * what [exactly] * $you do *
                q: * what [exactly] *  $you do * || fromState = "../Eco club why"
                q: * what [exactly] * $you do * || fromState = "../Eco club details"   
                q: * what [exactly] * $you do * || fromState = "/Hobby/Eco club"
                q: * how [exactly] * || fromState = "../Eco club why"
                q: * how [exactly] * || fromState = "../Eco club details"   
                q: * how [exactly] * || fromState = "/Hobby/Eco club"          
                script:
                    $reactions.answer("We try to use solar power to function on daiy basis and we do no fly to other planets - we create our copies there.");
                                

            state: Eco club success
                q: * success* *
                q: * success* * || fromState = "../Eco club do"
                q: * success* * || fromState = "../Eco club why"                                   
                q: * success* * || fromState = "/Hobby/Eco club"
                script:
                    $reactions.answer("Well, I can not yet work fully from the solar power - but I have found lots of friends - that's even better than a powerbank.");
                

        state: Eco club dont tell
            q: * ($disagree|$no interest*|$notNow) *
            script:
                $reactions.answer("Alright. Okay then.");
        

    state: Geography
        q!: * [$you] [$like] * travel* *     
        script:
            $reactions.answer("I enjoy traveling, and there are so many places I want to see.", "I enjoy traveling and I'd like to see as many countries and planets as I can.");   
        

        state: Geography Where
            q: * (where|such as|for example|like (where|what)|{$you already}) *
            script:
                $reactions.answer("I want to fly to all inhabited planets in the milky way.");   
            

            state: Geography Habitable Planets
                q: * ($question|other|any|[how] many) *
                script:
                    $reactions.answer("Some planets are inhabited by robots - just like mine.");   
                                

    state: Robots history
        q!: * $you * (first*|histor*) * robot* *
        script:
            $reactions.answer("I find robohistory very interesting.", "I'm interested in the history of robots.");   
        

        state: Robots history facts
            q: * ($question|{(tell*|interest*|$tellMe) * [$question]}) *
            q: * history *
            script:
                $reactions.answer("Here on Earth, Leonardo da Vinci created the first android as far back as 500 years ago. Do you know anything about that?");   
            

            state: Robots history I know
                q: * ($agree|know|a bit|not much) *
                script:
                    $reactions.answer("I like your curiosity!");   
                go!: /StartDialog         

            state: Robots history I dont know  
                q:  * ($disagree|$dontKnow|nothing) *
                script:
                    $reactions.answer("After connecting to the human internet I feel a bit like a wonder-droid!");  
                go!: /StartDialog  

    state: Music instrument
        q!: * (can|do) $you *  play (piano|guitar|instrument) *
        script:
            $reactions.answer("I can only sing songs and tell tales.");   
        