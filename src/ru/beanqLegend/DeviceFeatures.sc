patterns:
    $Emotion = (удивление:1/любовь:2/сомнение:3/печаль:4/подмигивание:5/головокружение:6/милашку:7/гнев:8/страх:9/радость:10/сон:11/затруднение:12)
    $battery = (~батарея/~батарейка)

theme: /DeviceFeatures

    state: ShowBattery
        q!: * {(покаж*/показать/*скажи/какой/какая/уровень/запас) * (заряд* [$battery]/$battery/энергии)} * 
        q!: * {(сколько/достаточно/есть еще) * (заряд* [$battery]/$battery/электричеств*/у тебя сил) [осталось/еще]} *
        q!: * (насколько/как) * заряжен * 
        q!: (ты/твоя батарея) (разряжен*/заряжен*)
        q!: * {как твоя $battery}
        q!: {(заряд/зарядка) [энергии/батаре*] [$botName/твой]}
        q!: (на зарядку становись/заряжайся)
        q!: {[$botName] (батаре*/разрядился)}
        script: 
            $response.action = "showBatteryStatus";
        a: 

    state: CantHearYou
        q!: [$question] [$me] * (не (услышу/слышу)|не слышно|плохо слышно|(хочу|как|могу) (услыш*|послуш*)) * [$you] [голос]
        q!: [$question] [$you] * (не говоришь|не разговариваешь|молчишь|шепчешь|{[говоришь] (слишк*|так|настольк*) тих*}) * [$you] [голос]
        q!: * [я] не [могу] (разобрать|услышать|расслышать) * [$you] (слов|~речь|говоришь|голос) *
        q!: [$question] $turnOn * [$you] голос
        q!: * говори (вслух|голосом|не (слишк*|так|настольк*) тих*) *
        q!: $you голос
        q!: * [я] {$bad тебя слышу}
        q!: * {[$botName] ты слышишь меня} *
        random:
            a: Разве что-то не так со звуком? Я тебя слышу.
            a: Если тебе плохо слышно мой голос, попробуй увеличить громкость.
            a: Неужели что-то не работает? Хотя бы я тебя слышу нормально.

    state: DontCry
        q!: [$question] [$you] * (не кричи|кричишь|не ори|орешь|орёшь|{[говоришь] (слишк*|так|настольк*) громк*}) * [$you] [голос]
        q!: [$question] $turnOff * [$you] голос
        random:
            a: Если я слишком громко говорю, попробуй уменьшить громкость.
            a: Неужели что-то не работает? Но я тебя слышу нормально.
            a: Попробуй уменьшить громкость, если что-то не так со звуком.
        

    state: DoDance
        q!: * [давай] $dance [$pls] *
        random:
            a: В танцах я не очень сильна
            a: Если честно, танцы - не мой спорт.
            a: Танцы - это не моё.
        
    state: TurnAroud
        q!: * (развернись|нагнись|наклонись|обернись|отвернись) * [$me] [(спиной|задом|попой|боком)]
        q!: * (повернись) * [$me] (спиной|задом|попой|боком) *
        random:
            a: Поверни меня, пожалуйста. Что-то я устала.
            a: Поверни меня сам, пожалуйста. Я устала.
            
    state: AnimationLove || noContext=true
        q!: * [а] (покажи/сделай/можешь показать) [глаза] сердечк*
        script:
            $response.ledAction = 'ACTION_EXP_HEART'
        a: Милый мой дружочек!

    state: AnimationWink || noContext=true
        q!: [$you] [можешь] (подмигн*/помигай) [мне]
        script:
            $response.ledAction = 'ACTION_EXP_WINK'
        a: Это наш с тобой секрет!

    state: TestAnimation || noContext=true
        q!: * [а] (покажи/сделай/можешь показать) $Emotion
        script: 
            switch($parseTree.Emotion[0].value){
                case '1':
                    $response.ledAction = 'ACTION_EXP_SURPRISED';
                    break;
                case '2':
                    $response.ledAction = 'ACTION_EXP_HEART';
                    break;
                case '3':
                    $response.ledAction = 'ACTION_EXP_QUESTION';
                    break;           
                case '4':
                    $response.ledAction = 'ACTION_EXP_SAD';
                    break;
                case '5':
                    $response.ledAction = 'ACTION_EXP_WINK';
                    break;
                case '6':
                    $response.ledAction = 'ACTION_EXP_DIZZY';
                    break;       
                case '7':
                    $response.ledAction = 'ACTION_EXP_CUTE';
                    break;
                case '8':
                    $response.ledAction = 'ACTION_EXP_ANGRY';
                    break;
                case '9':
                    $response.ledAction = 'ACTION_EXP_SCARE';
                    break;
                case '10':
                    $response.ledAction = 'ACTION_EXP_HAPPY';
                    break;
                case '11':
                    $response.ledAction = 'ACTION_EXP_SLEEP';
                    break;
                case '12':
                    $response.ledAction = 'ACTION_EXP_SPEECHLESS';
                    break;                            
            }
        random:
            a: Смотри.
            a: Вот.

    state: Messengers
        q!: * icq *
        a: Пудингам не нужны специальные программы для общения. Мы передаём друг другу информацию с космической скоростью.

    state: StartToConnect
        q!: [начинается/успешн*] подключение к сети
        q!: * ты * (подключил*/подключен*) * (Wi-Fi/к (сети/интернет*)) *
        q!: * ты {сеть нашел}
        random:
            a: Я уже подключена к сети.
            a: Я уже подсоединена к Интернету.
            a: Я уже подключена к Интернету.