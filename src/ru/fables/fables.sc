require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: patterns.sc
    module = zb_common

require: common.js
    module = zb_common

require: number/number.sc
   module = zb_common

require: fables.js

require: answers.yaml
    var = $fablesAnswers

require: ../../dictionaries/fables.csv
    name = fables
    var = $fables

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .fables = function(parseTree) {
            var id = parseTree.fables[0].value;
            return $fables[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("fables");

    bind("postProcess", function($context) {
        var $session = $context.session, $parseTree = $context.parseTree
        $session.lastState = $context.currentState

    })

patterns:
    $fableName = $entity<fables> || converter = $converters.fables
    $localAgree = (люблю|знаю|легко|доводилось|приходилось|поступаю|буду|понимаю|готов*|бывало|считаю|попробую|помогу|лучше|много|достаточно|хватает|(не (брошу|убегу|хулиган*)|останусь) [$sure])
    $localYes = (здорово|хочу|хочется|ношу|бывает|понравилось|поверю|честно|ссорюсь|ссоримся|соглашусь|соглас*|договорились|всегда|отвечу|может быть|наверное|скорее всего|постоянно|постараюсь)
    $localDisagree = (не (приходилось|доводилось|помогу|буду|очень|много|люблю|легко|доводилось|поступаю|понимаю|готов*|бывало|попробую)|хуже|так себе|(брошу|оставлю|убегу) [$sure]|мало|немного|нечестно|хулиган*|что [же] [такое|это]|не правильно)
    $localNo = (не (здорово|хочу|хочется|ношу|понравилась|поверю|честно|ссорюсь|ссоримся|соглашусь|соглас*|договорились|считаю|всегда|отвечу))
    $modal = (($agree|$localAgree|$localYes|$Number):y|($disagree|$localDisagree|$localNo):n|($dontKnow|$localDontKnow):d)
    $continue = (будем|буду|продолж*|конечно|конешно|канешна|а то [нет]|обязательно|непременно|а как же|подтверждаю|пожалуй|(почему/что/че) [бы] [и] нет|хочу|было [бы] (неплохо|не плохо)|[($yes|очень)] (хочу|хо чу|ладно|хорошо|хoрoшo|можно|валяй*|согла*|естественно|разумеется|(еще|ещё) как|не (против|возражаю|сомневаюсь)|я только за|безусловн*))
    $localDontKnow = (может [быть] [и] (да|знаю) * может [быть] [и] (нет|не знаю)|(фиг|хрен|бог|черт) знает|хз)
    $changeThemeWords = (хочу/давай [*игра*/*слуш*/*говор*]/включи/поиграем/поиграй/поговори/расскажи/скажи/создай/загадай/открой/запусти/поставь)
    $localPlay = (расска*|включ*|постав*|давай|активир*|*читай|*чти|~слушать|послуша*|хочу|хочется|проиграй|теперь)


theme: /Fables

    state: Enter || modal = true
        q!: * {$localPlay * (~басня|$fableName)} *
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/TellFable", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/Modal/CatchAll", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/Modal", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/CatchAll", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/RandomPath", onlyThisState = true
        q: * {[$localPlay] * $fableName} * || fromState = "/Fables/Enter/RepeatFable", onlyThisState = true
        script:
            if ($session.insideSkill === undefined || $session.insideSkill === false) {
                fables.init();
            };
        go!: /Fables/Enter/ChooseFable


        state: ChooseFable
            script:
                if (!fables.chooseFable()) {
                    $reactions.answer("Кажется, это все басни, которые есть у меня в памяти. Попрошу своих друзей-пуддингов прислать мне еще поучительных историй для тебя. Во что поиграем сейчас?");
                    $reactions.transition("/");
                }
                else {
                    fables.ifFableTold();
                }

        state: RandomPath
            script:
                if ($session.stepNumber != 0) {
                    $reactions.answer(selectRandomArg("Давай вместе послушаем небольшую поучительную историю про людей и зверей. Если захочешь остановиться и поиграть во что-то другое, просто скажи \"хватит\".", "Как говорил известный житель вашей планеты, мудрости никогда не бывает много. Поучительные басни - тому подтверждение. Если захочешь остановиться и поиграть во что-то другое, просто скажи \"хватит\"."));
                }
                if ($session.stepNumber === 2) {
                    $reactions.transition("/Fables/Enter/TellFable")
                }
                else {
                    $temp.rep = true;
                    $response.audio = $session.currentFable.audio;
                    // $reactions.answer("Проигрывается аудио: {{$session.currentFable.audio}}");
                    $client.fables.fablesTold.push($session.currentFable.id);
                    $session.lastAudio = $session.currentFable.audio;
                    fables.clear();
                }

             
        state: TellFable
            script:
                $temp.rep = true;
                if (typeof $session.currentFable.subAnswer === 'object') {
                    if ($session.currentFable.subIndex === $session.currentFable.subAnswer.length)
                    {  
                        $session.currentFable.subIndex = -1;
                        $session.currentFable.subType = null;
                        $session.currentFable.subAnswer = undefined;
                        $session.currentFable.index++;
                    }                        
                }

                if ($session.currentFable.subIndex == -1) {
                    $reactions.answer($session.currentFable.answers[$session.currentFable.index]["a"]);
                }
                else { 
                    $reactions.answer($session.currentFable.subAnswer[$session.currentFable.subIndex]["a"]);
                }
                if ($session.currentFable.index !== $session.currentFable.answers.length - 1) {
                    if (Object.keys($session.currentFable.answers[$session.currentFable.index]).length == 1 || ($session.currentFable.subType != null && Object.keys($session.currentFable.subAnswer[$session.currentFable.subIndex]).length == 1) ) {
                        $reactions.transition("/Fables/Enter/Any");
                    }
                    else
                    {
                        $reactions.transition("/Fables/Enter/Modal");
                    }
                }
                else {
                    $response.audio = $session.currentFable.audio;
                    // $reactions.answer("Проигрывается аудио: {{$session.currentFable.audio}}");
                    $client.fables.fablesTold.push($session.currentFable.id);
                    $session.lastAudio = $session.currentFable.audio;
                    fables.clear();
                }

        state: RepeatFable
            q: * (повтори*|еще раз*|снова|предыдущ*) * || fromState = " /Fables/Enter/TellFable", onlyThisState = true
            q: * (повтори*|еще раз*|снова|предыдущ*) * || fromState = "/Fables/Enter/RandomPath", onlyThisState = true
            q: * (повтори*|еще раз*|снова|предыдущ*) * || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
            script:
                $temp.rep = true;
                $response.audio = $session.lastAudio;
                // $reactions.answer("Проигрывается аудио: {{$session.lastAudio}}");


        state: NextFable
            q: * {[давай|включи|включай|хочу|расскажи*|рассказ*] * (еще [одну]|следующ*|дальше|*нибудь)} * || fromState = "/Fables/Enter/RandomPath", onlyThisState = true
            q: * {[давай|включи|включай|хочу|расскажи*|рассказ*] * (еще [одну]|следующ*|дальше|*нибудь)} * || fromState = " /Fables/Enter/TellFable", onlyThisState = true
            q: * {[давай|включи|включай|хочу|расскажи*|рассказ*] * (еще [одну]|следующ*|дальше|*нибудь)} * || fromState = " /Fables/Enter/RepeatFable", onlyThisState = true
            go!: /Fables/Enter/ChooseFable

        state: Exit 
            q: * $stopGame *
            q: * $stopGame * || fromState = "/Fables/Enter/RandomPath"
            script:
                $session.insideSkill = false;
            random:
                a: Я и все мои друзья-роботы советуем тебе всегда дослушивать басни до конца. В конце - самое главное, что хотел сказать тебе автор. Во что хочешь поиграть сейчас?
                a: Хорошо. Во что хочешь поиграть сейчас?
            go!: /

        state: CatchAll
            q: *
            q: * || fromState = "/Fables/Enter/TellFable", onlyThisState = true
            q: * || fromState = "/Fables/Enter/RandomPath", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/Fables/Enter/RandomPath", onlyThisState = true
            q: * [$changeThemeWords] * || fromState = "/Fables/Enter/TellFable", onlyThisState = true
            script:
                if (!fables.catchInsideDialogue($parseTree)) {
                    if (!$session.offtopic) {
                        $session.offtopic = true;
                        $reactions.answer(selectRandomArg("Что-то не то. Будем слушать басни?", "Не то, что я ожидал услышать. Будем слушать басни?"));
                        $reactions.transition("/Fables/Enter/CatchAll/IfContinue");
                    }
                    else {
                        $reactions.answer("Кажется, мы устали или на сегодня мудрости достаточно.");
                        $reactions.transition("/");
                    }
                }

            state: IfContinue || modal = true
                state: Yes
                    q: * ($yes|$continue|будем|хочу|буду) * || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
                    q: давай || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
                    script:
                        $session.offtopic = false;
                        $reactions.transition("/Fables/Enter/NextFable");

                state: No
                    q: * ($no/$disagree/не (будем/буду/хочу)) * || fromState = "/Fables/Enter/CatchAll/IfContinue", onlyThisState = true
                    script:
                        $session.offtopic = false;
                    a: Кажется, мы устали или на сегодня мудрости достаточно.
                    go!: /


        state: Modal || modal = true

            state: Modal
                q: * $modal *
                script:
                    var response = fables.answer($parseTree._modal);
                    if (response != undefined) $reactions.answer(response)
                    delete $session.offtopic;
                    if ($session.currentFable.subIndex == -1) $session.currentFable.index++;
                go!: /Fables/Enter/TellFable

            state: CatchAll
                q: * [$changeThemeWords] * [$stopGame] *
                script:
                    if (!fables.catchInsideDialogue($parseTree)) {
                        if (!$session.offtopic) {
                            $session.offtopic = true;
                            $reactions.answer(selectRandomArg("Что-то не то. Давай повторю вопрос.", "Не то, что я ожидал услышать. Давай повторю вопрос."));
                            $reactions.answer("{{fables.answer(\"a\")}}");
                        }
                        else {
                            $session.insideSkill = false;
                            fables.clear();
                            $reactions.answer("Кажется, мы устали или на сегодня мудрости достаточно.");
                            $reactions.transition("/");
                        }
                    }

        state: Any || modal = true

            state: CatchAll
                q: * [$changeThemeWords] * [$stopGame] *
                script:
                    if (!fables.catchInsideDialogue($parseTree)) {
                        if ($session.currentFable.subIndex == -1) $session.currentFable.index++
                        else $session.currentFable.subIndex++;
                        $reactions.transition("/Fables/Enter/TellFable");
                    }
            


