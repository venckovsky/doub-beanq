require: animalsAndBirds.js

require: ../../dictionaries/animalsAndBirds-En.csv
  name = animalsAndBirds
  var = $animalsAndBirds

require: ../../dictionaries/animalsAndBirdsQuestions-En.csv
  name = animalsAndBirdsQuestions
  var = $animalsAndBirdsQuestions

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .animalsAndBirds = function(parseTree) {
            var id = parseTree.animalsAndBirds[0].value;
            return $animalsAndBirds[id].value;
        };

    bind("preMatch", function() {
        var $response = $jsapi.context().response;
        if ($response.beanq && $response.beanq.event && $response.beanq.event.type && $response.beanq.event.packageName) {
            if ($response.beanq.event.type === "STOP_APPLICATION") {
                if ($response.beanq.event.packageName === "com.justai.beanq.skill.animalquiz") {
                    $jsapi.context().temp.targetState = "/";
                }
            }
        }
    });

    bind("postProcess", function($context) {
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;

        log('$session.catchAllAnimals ' + $session.catchAllAnimals)
        log('$temp.catchAll ' + $temp.catchAll)

        if ($temp.catchAll) {
            $session.catchAllAnimals += 1;
        } else {
            $session.catchAllAnimals = 0;
        }
   });
   
    $global.themes = $global.themes || [];
    $global.themes.push("animalsAndBirds");


patterns:
    $animalBird = $entity<animalsAndBirds> || converter = $converters.animalsAndBirds
    $notAnimalBird = (no/not/nay/nope/n't) [in/at/by/on] [a/an/the] $animalBird
    $wantAnotherGame = * {(game|play|act) * (some|something|differ|~different)} *

theme: /animalsAndBirds

    state: ForceEnter
        q!: * start animalsandbirds *
        script:
            $context.session = {};
            $context.temp = {};
        go!: /animalsAndBirds/Enter

    state: Enter
        q!: * (quiz/game) [about] (zoo*/animal*/bird*) *
        q!: * {[come on|may|let us|let 's] * (game|play*|act|quiz|ask) * (zoo*/animal*/bird*)} *
        q!: * {(come on|may|let us|let 's|want|I want to|wanna) * (zoo*/animal*/bird*)} *
        q!: * animals (and/with) birds *
        q: about (zoo*/animal*/bird*) || fromState = /BeanqGames/Games, onlyThisState = true
        script:
            log(">>>>>>>>>" + toPrettyString($session));
            animalsAndBirds.init();
            $response.action = "beanqAction";
            $response.replies = $response.replies || [];
                $response.replies
                 .push({
                    type:"launchApplication"
                 });
        a: Lets learn animals and birds together! Let's play the quiz! I will ask a question and offer two answers, and you choose the right one. Do not be afraid to answer wrong, I will tell you the correct answer. || question = true
        go!: ../GetQuestion

    state: GetQuestion || modal = true
        q: * ($yes/do not know/don 't know/$agree/$dontKnow/continue*) * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: (guess/make a riddle/more/ask/play/game/go) || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        script:
            $session.lastQuiz = animalsAndBirds.getQuestion();
            $session.answered = false;
            $response.action = "beanqAction";
            $response.replies = $response.replies || [];
            $temp.payload = "{\"actionType\": \"SHOW_PICTURES\", \"firstPicture\": \"" + $session.lastQuiz.firstPicture + "\", \"secondPicture\": \"" + $session.lastQuiz.secondPicture + "\", \"firstCaption\": \"" + $session.lastQuiz.firstCaption + "\", \"secondCaption\": \"" + $session.lastQuiz.secondCaption + "\", \"sound\": \"" + $session.lastQuiz.sound + "\"}";
            $response.replies
                 .push({
                    type: "sendMessage",
                    payload: $temp.payload
                });
            log("ololo " + toPrettyString($response.replies));
        if: $temp.cachAll
            a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true
        else:
            random:
                a: Guess - {{$session.lastQuiz.question}} || question = true
                a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true

        state: Repeat
            q:  * (repeat | what (was/is) [the/a] question | what question) *
            q: * {(repeat*/say/saing/ask) [it/[the/a] question/this/that] (one more time/[once] again/else) * [please/pls]} *
            q: * (one more time/once again) *
            q: * what [did/do/had/was] [you (say/say*/said)] *
            q: * (no/not/n 't) hear* *
            q: [and] what [was/is] the question
            script:
                $temp.rep = true;
            if: $session.lastQuiz
                script:
                    $response.action = "beanqAction";
                    $response.replies = $response.replies || [];
                    $temp.payload = "{\"actionType\": \"SHOW_PICTURES\", \"firstPicture\": \"" + $session.lastQuiz.firstPicture + "\", \"secondPicture\": \"" + $session.lastQuiz.secondPicture + "\", \"firstCaption\": \"" + $session.lastQuiz.firstCaption + "\", \"secondCaption\": \"" + $session.lastQuiz.secondCaption + "\", \"sound\": \"" + $session.lastQuiz.sound + "\"}";
                    $response.replies
                        .push({
                            type: "sendMessage",
                            payload: $temp.payload
                        });
                random:
                    a: OK, let me repeat the question.{{$session.lastQuiz.question}} || question = true
                    a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true              
            go: .. 

        state: GoNext
            q: * (tell me/give me a hint/help/already asked/repeating/(say/tell) [me] [the/a] answer) *
            q: [$agree] (forward|next*|ask|say|one more) [next|forvard*|one more|say|ask|another] [question*] 
            q: * {you know [the] [answer]} *
            q: * {(*tell/what/name*/give [me]) [$AnyWord] (answer/how correct/how to/what* * right/[right/a/the] answer/right variant)} *
            q: {(yourself/you) [$AnyWord] (answer*/say/tell*/which one/name*/give)}
            q: * [i] [really] (have n't/have no/no) [a/an/the] [faintest/any] (idea/clue) *
            q: * $dontKnow * 
            q: * $dontKnow  * $animalBird or $animalBird * 
            q: * $animalBird or $animalBird * $dontKnow * 
            a: {{animalsAndBirds.myCapitalize($session.lastQuiz.answer)}}. || question = true 
            random:
                a: One more question? || question = true 
                a: Will you guess another animal or bird? || question = true 
                a: Shall we continue the quiz? || question = true 

        state: LocalForceEnter
            q: * start animalsandbirds *
            script:
                $context.session = {};
                $context.temp = {};
            go!: /animalsAndBirds/Enter

    state: GetAnswer || modal = true
        q: * $animalBird * [$animalBird::animalBird2] * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * $notAnimalBird * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        script:
            $session.answered = true;
        if: animalsAndBirds.isRight()
            script:
                $session.rightAnswers += 1;
                log('$response ' + toPrettyString($response));
                $response.action = "beanqAction";
                $response.replies = $response.replies || [];
                $temp.payload = "{\"actionType\": \"SHOW_ANSWER\", \"isCorrect\": \"true\", \"rightAnswer\": \"" + $session.lastQuiz.rightNumber + "\"}";
                $response.replies.push({
                    type: "sendMessage",
                    payload: $temp.payload
                });
            random:
                a: You are doing great! But my questions are so difficult! Here is your star! || question = true
                a: Right! Here is your star! || question = true
                a: Absolutely right! The reward is yours! || question = true
                a: Computer says this is the right answer! And here is the reward! || question = true
                a: Great! Catch a star! || question = true
                a: You are clever, the star is yours. || question = true
                a: You are an expert! Catch a deserved star! || question = true
            if: animalsAndBirds.IsEnd()
                go!: /animalsAndBirds/Stop
            else:
                random:
                    a: One more question? || question = true 
                    a: Guess another animal or bird? || question = true 
                    a: Shall we continue the quiz? || question = true 
        else:
            script:
                log('$response ' + toPrettyString($response));
                $response.action = "beanqAction";
                $response.replies = $response.replies || [];
                $temp.payload = "{ \"actionType\": \"SHOW_ANSWER\", \"isCorrect\": \"false\", \"rightAnswer\": \"" + $session.lastQuiz.rightNumber + "\"}";
                $response.replies.push({
                    type: "sendMessage",
                    payload: $temp.payload
                });
            random:
                a: Listen the right answer is {{$session.lastQuiz.answer}}. || question = true
                a: Well, the right answer is {{$session.lastQuiz.answer}}. || question = true
            if: animalsAndBirds.IsEnd()
                go!: /animalsAndBirds/Stop
            else:
                random:
                    a: I am sure you will answer the next question correctly and get the star! Shall we play again? || question = true 
                    a: Let's try to answer the next question. I believe this time the reward will be yours! Lets try again? || question = true 

        state: SecondGo
            q: * [the] first [one/answer/variant/thing] * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
            q: * [the] (second/latter/last) [one/answer/variant/thing] * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
            a: Call the animals by their names: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true 
            go: /animalsAndBirds/GetQuestion


    state: HowManyStars || modal = true

        q: * {how many * ((right*/correct) * answer*/I [am/'m] [already] gues*/star*/I was right)} * || fromState = /animalsAndBirds
        q: * (what/how/tell [me]) ['s/is/about/was] [the] [my] score * || fromState = /animalsAndBirds

        #what is my score", "how many right answers?
        #q: * {how many * (right* * answer*/I [am/'m] [already] gues*/star*/I was right)} * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        #q: * {how many * (right* * answer*/I [am/'m] [already] gues*/star*/I was right)} * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        #q: * {how many * (right* * answer*/I [am/'m] [already] gues*/star*/I was right)} * || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        #q: * (what/how/tell [me]) ['s/is/about] [the] [my] score * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        #q: * (what/how/tell [me]) ['s/is/about] [the] [my] score *  || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        #q: * (what/how/tell [me]) ['s/is/about] [the] [my] score *  || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        script:          
            var right = $session.rightAnswers;
            $temp.stars = right % 10 === 1 && right % 100 !== 11 ? 'star' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ? 'stars' : 'stars');
        a: Your score is {{$session.rightAnswers}} {{$temp.stars}}. One more question? || question = true 

        state: Agree
            q: * ($agree/$dontKnow/keep going/continue*/go ahead) * 
            q: (guess/ask [a] riddle/[one] more/i ['ll/will] guess/play/game*/let 's do it)
            q: * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
            q: * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
            q: * || fromState = /animalsAndBirds/GetQuestion/GoNext, onlyThisState = true
            if: $session.answered 
                go!: /animalsAndBirds/GetQuestion
            else:
                a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true 
                go: /animalsAndBirds/GetQuestion

    state: HowManyFails || modal = true
        q: * {how many * (wrong * answer*/fail*/i['m/am] [already] ((did not/didn 't/do not/don 't/not) gues*/guessed wrong))} * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * {how many * (wrong * answer*/fail*/i['m/am] [already] ((did not/didn 't/do not/don 't/not) gues*/guessed wrong))} * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * {how many * (wrong * answer*/fail*/i['m/am] [already] ((did not/didn 't/do not/don 't/not) gues*/guessed wrong))} * || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        q: * {how many * (wrong * answer*/fail*/i['m/am] [already] ((did not/didn 't/do not/don 't/not) gues*/guessed wrong))} * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: [аnd] from how [many] || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        script:          
            var right = $session.rightAnswers;
            $temp.answ = right % 10 === 1 && right % 100 !== 11 ? 'answer' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ? 'answers' : 'answers');
        a:  You have {{$session.rightAnswers}} {{$temp.answ}} correct answers out of {{$session.questionsNumber}}. One more question? || question = true 

        state: Agree
            q: * ($yes/$dontKnow/next*) * 
            q: * (guess again|one more|i will guess|play*|forward|next*|ask*|say*|i 'll guess|let 's do it) *
            q: * || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
            if: $session.answered 
                go!: /animalsAndBirds/GetQuestion
            else:
                a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true 
                go: /animalsAndBirds/GetQuestion

        state: Yes
            q: * ($yes/$continue) * || fromState =.., onlyThisState = true
            go!: /StartDialog
            #go!: {{$session.randomGame[2]}}

        state: No
            q: ($no/$disagree) || fromState =.., onlyThisState = true
            q: * ($notNow/$stopGame) * || fromState =.., onlyThisState = true
            go!: /animalsAndBirds/Stop


    state: CatchAll
        q: $Text || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        script:
            $temp.rep = true;
            $temp.catchAll = true;
                if ($parseTree._Text.indexOf("want ")  > -1 || $parseTree._Text.indexOf("come on ")  > -1 || $parseTree._Text.indexOf("switch on ")  > -1 || $parseTree._Text.indexOf("let's play ")  > -1 || $parseTree._Text.indexOf("game ")  > -1 || $parseTree._Text.indexOf("speak ")  > -1 || $parseTree._Text.indexOf("tell me")  > -1 || $parseTree._Text.indexOf("say ")  > -1 || $parseTree._Text.indexOf("create ")  > -1 || $parseTree._Text.indexOf("guess ")  > -1 || $parseTree._Text.indexOf("open ")  > -1 || $parseTree._Text.indexOf("run ")  > -1 || $parseTree._Text.indexOf("play ")  > -1) {
                    var res = $nlp.match($parseTree._Text, "/");
                    $reactions.transition(res.targetState);
                }
        if: $session.catchAllAnimals >= 2
            a: It seems to be boring, maybe we have to choose another activity. Just say me what do you want to do? || question = true
            script:
                $response.action = "beanqAction";
                $response.replies = $response.replies || [];
                $response.replies
                    .push({
                       type:"stopApplication"
                    });
            go!: / 
        else:
            script:
                $temp.cachAll = true;
            a: As a robot, I believe that if you play, then play by the rules. I ask you - you answer. The question is 
            if: $session.lastQuiz
                if: $session.answered 
                    go!: /animalsAndBirds/GetQuestion
                else:
                    a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} || question = true 
                    go: /animalsAndBirds/GetQuestion
            else:
                go!: /animalsAndBirds/GetQuestion 
       
    state: Stop
        q: ($disagree/$no/[i] [had] enough) || fromState = ../GetAnswer, onlyThisState = true
        q: ($disagree/$no/[i] [had] enough) || fromState = ../HowManyStars, onlyThisState = true
        q: ($disagree/$no/[i] [had] enough) || fromState = ../HowManyFails, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        q: * [please/want/let us/let 's] * (pause/stop/rest) * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * [please/want/let us/let 's] * (pause/stop/rest) * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * [please/want/let us/let 's] * (pause/stop/rest) * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: * [please/want/let us/let 's] * (pause/stop/rest) * || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        q: * [i am/i 'm/i] (tired*/exhausted|fatugued|exerted) * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * [i am/i 'm/i] (tired*/exhausted|fatugued|exerted) * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * [i am/i 'm/i] (tired*/exhausted|fatugued|exerted) * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: * [i am/i 'm/i] (tired*/exhausted|fatugued|exerted) * || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        script:
            $temp.right = $session.rightAnswers;
            $temp.answers = $temp.right % 10 === 1 && $temp.right % 100 !== 11 ? 'question' : ($temp.right % 10 >= 2 && $temp.right % 10 <= 4 && ($temp.right % 100 < 10 || $temp.right % 100 >= 20) ? 'questions' : 'questions');
            $temp.stars = $temp.right % 10 === 1 && $temp.right % 100 !== 11 ? 'star' : ($temp.right % 10 >= 2 && $temp.right % 10 <= 4 && ($temp.right % 100 < 10 || $temp.right % 100 >= 20) ? 'stars' : 'stars');
            animalsAndBirds.init();
        if: $temp.thatsAll
            a: It seems I asked all the questions that I had in my memory! The number of stars earned: {{$temp.right}}. I will ask friends from my planet to send me more questions about earthly animals and birds. They will be very difficult, but interesting! || question = false
        elseif: $temp.right == 0
            script:
                $response.action = "beanqAction";
                $response.replies = $response.replies || [];
                $response.replies
                    .push({
                        type:"stopApplication"
                    });
            a: Bye-bye! || question = false 
        else:  
            a: We answered correctly to {{$temp.right}} {{$temp.answers}} and received {{$temp.right}} {{$temp.stars}}! || question = false 
            script:
                $response.action = "beanqAction";
                $response.replies = $response.replies || [];
                $response.replies
                     .push({
                        type:"stopApplication"
                     });
        go!: /StartDialog

        #/BeanqGames