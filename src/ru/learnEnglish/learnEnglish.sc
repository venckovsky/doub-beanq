require: learnEnglish.js

require: learnEnglish.csv
  name = learnEnglish
  var = learnEnglish

require: newSessionOnStart/newSession.sc
  module = zb_common

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .learnEnglishTagConverter = function(parseTree) {
            var id = parseTree.learnEnglish[0].value;
            return learnEnglish[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("Learn English");

patterns:
    $Translation = $entity<learnEnglish> || converter = $converters.learnEnglishTagConverter
    $or = или
    $not = не
    $order = (~первый:1/~второй:2)
    $LEdoubts = [по-русски/наверное/возможно/очевидно/легко] [это] [значит/переводится [как]/будет] [это] [скорее всего / возможно / наверное / может быть [это] / (пусть/пускай) будет / наугад]

theme: /Learn English 

    state: Start
        q!: * [давай/хочу] * [изучать/учить/изучать/поучи/поучим/научи/*учи*/сыгра*/играть/поиграем/повтор*] * [(разговаривать/говорить) * [на/по]] * (англ*/английский/english/инглиш/по-английски) [слов*] *
        q: * (~правило/как играть/(объясни/что) * делать/как * общаться) *
        script:
            if($session.learningGreeting == undefined){
                $session.learningGreeting = true;
                toAnswerRu("Английский язык всегда пригодится! Я буду называть слово на английском, а ты переводишь его на русский. Когда надоест, скажи 'хватит'.");
            } 

            LE.init();
            $temp.rememberThis = true;
            toAnswerRu("Как переводится ", "Как будет по-русски ", "Что значит ");
            toAnswerEn(LE.getWord() + "?");
            
        go!: ./GetAnswer   

        state: GetAnswer || modal = true
            state: ProbablyAnswer
                q: $AnyWord
                script:
                    $temp.probablyTranslation = $parseTree.AnyWord[0].text;
                    LE.checkAnswer();
                go!: {{ $temp.nextState }}

            state: Answer
                q: * [$LEdoubts] * $Translation * [$Translation] * [$LEdoubts] * 
                q: * $Translation * $or * 
                q: * [$not] $order *
                q: * $not $Translation *  
                q: * [$LEdoubts] * [$Translation] * [$LEdoubts] *
                script:
                    LE.checkAnswer();
                go!: {{ $temp.nextState }}

            state: Tip
                q: * ($dontKnow|хз/не помню/забыл*/не могу вспомнить/что-то знакомое/хрень/хуйня/не могу/не переведу/не понятно) *
                q: * (~подсказка/подскажи/подсказывай/намекни|помоги/помощь) *
                q: * (ещё/новую/другую) * || fromState=./, onlyThisState=true 
                script:
                    $temp.rememberThis = true;
                    LE.getTip();
                if: $temp.nextState
                    go!: {{ $temp.nextState }}  

            state: Repeat
                q: * (ещё раз/(повтори/напомни) * [~слово|~фраза]) *
                q: ((как/что) (~сказать/~говорить) | (шо/чего) [~сказать/~говорить])
                script:
                    if ($session.learnEnglishPreviousAnswer) {
                        toAnswerRu("Повторяю: ");
                        $session.learnEnglishPreviousAnswer.forEach(function(reply){$response.replies.push(reply);})
                    } else {
                        $reactions.answer("Что-то я уже забыл...");
                        $temp.failedToRepeat = true;
                    }
                if: $temp.failedToRepeat
                    go!: ../../../Start
                    
                    

            state: Ask for answer
                q: * (сдаюсь/(скажи/говори) ответ|другое/следующее/дальше/скажи [ты]) * [перевод/как переводится]*
                q: * не хочу говорить *     
                q: [ну и] как [переводится] 
                q: [а] как [переводится] 
                q: * [ну и] какой [[вариант] перевода] у тебя [(вариант перевода/перевод)] *
                q: * [а] (как/что) у тебя записано * || fromState = ../Text
                script:
                    $temp.rememberThis = true;
                    if ($session.phraseTranslation) {
                        $session.phraseTranslation = false;
                        toAnswerEn("'"+$session.currentPhrase + "'");
                        toAnswerRu("переводится как '" + $session.currentPhraseTranslation[0] + "'.");
                        $session.rightAnswer = undefined;
                    } else {
                        LE.noRightAnswer();
                        toAnswerEn("'" + $session.currentWord + "'");
                        toAnswerRu("переводится как '" + $session.currentTranslation[0]+ "'.");
                    }
                if: $temp.entryPoint === 'StopGame'   
                    go!: ../../../Stop
                else:
                    go!: ../../

            state: Question again
                q: * уже [же] (было/спрашивал*/отвечал*/переводил*) * 
                q: * [уже] * слово [уже] (было/спрашивал*/отвечал*) * 
                q: * (повторяешься/[(хватит/достал)] повтор*/ не повторяйся) *  
                script:
                    toAnswerRu("Хочу удостовериться, что ты знаешь перевод. Скажи, как переводится слово ");
                    toAnswerEn($session.currentWord + "?");
                go: ../   

            state: EndTheGame
                q: * (стой|стоп|хватит|$stopGame) * 
                q: не хочу [больше] играть
                q: * (выйди/выходи/перестань/закончи) * 
                q: * [мне] надоело [играть] *
                q: * [я] устал [играть] *
                q: * давай (продолжим потом / на сегодня все * / отдохнем/ потом *)
                script:
                    $temp.entryPoint = 'StopGame';
                go!: ../Ask for answer

            state: Text
                q: $Text
                script:
                    if ($session.phraseTranslation) {
                        $reactions.answer('Это не совпадает с переводом, записанным у меня.');
                    } else {
                        $reactions.answer('Подумай лучше над переводом, а если надоест, скажи "хватит".');
                    }
            state: RestratSession
                q: * *start
                go!: /Start

            state: StopWords
                q: [$superCatchAll] $stopWord [$superCatchAll]
                a: Я обнаружила неразрешённое моей программой слово. Не могу отвечать. Не могу отвечать. 
                go!: ../Tip

    state: Stop
        script:
            $session.learningGreeting = undefined;
            LE.formAnswer();

    #state: test user data
    #    q!: test user data
    #    script:
    #        $client.wordsToLearn = {0:2, 2:2, 3:0, 4:0};
    #        $client.wellKnownWords = [];
    #        toAnswerRu("practically all words are used");


            