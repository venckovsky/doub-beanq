function getRiddle(){
    var $session = $jsapi.context().session;
    $session.usedRiddles = $session.usedRiddles || {
        questions: [],
        id: null,        
        question: '',
        answer: '',
        answerPatterns: []
    };

    var i, question, randNumber, numberOfQuestions;
    var questions = [];
    numberOfQuestions = Object.keys($Riddles).length;
    for (i=1; i<=numberOfQuestions; i++) {
        question = $Riddles[i].value;
        if (!_.contains($session.usedRiddles.questions, i) && question.question != "") {
            questions.push([i, question]);
        }
    }
    if (questions.length == 0) {
        $reactions.answer("Кажется, я загадал все свои загадки! Попрошу своих друзей с планеты Сверхновая прислать мне еще загадок для тебя. Они будут очень трудные, но интересные! Во что ты хочешь поиграть сейчас?");
        $reactions.transition("/");
    } else {
        question = questions[randomIndex(questions)];
        $session.usedRiddles.questions.push(question[0]);
        $session.usedRiddles.id = question[0];        
        $session.usedRiddles.question = question[1].riddleText; 
        $session.usedRiddles.answer = question[1].answerText;
        $session.usedRiddles.answerPatterns = question[1].answerPatterns; 
    }     
}
