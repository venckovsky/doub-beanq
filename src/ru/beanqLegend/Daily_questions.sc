theme: /Every day I ask

    state: Do you have any plans morning
        q!: Do you have any plans morning
        script:
            $session.plansMorning = true;
        random:
            a: У тебя есть планы на сегодня? Что будешь делать?
            a: Отличное утро сегодня. Что собираешься делать?
            a: Впереди целый день! Наверное, у тебя большие планы. Чем будешь заниматься?
            a: У меня впереди целый день учебы. А ты чем собираешься заниматься?
        
        state: Do you have any plans morning not know
            q: * $dontKnow * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Отсутствие планов - тоже план!
                a: Значит, можно весь день посвятить планированию.
                a: Есть время для чего угодно, весь день впереди.
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined

        state: Do you have any plans morning nothing
            q: * ниче* * [$andYou]  || fromState = .., onlyThisState = true
            random:
                a: Прекрасная стратегия!
                a: Посвятить весь день ничегонеделанию - моя мечта
                a: Ни разу не видела человека, который бы занимался ничем! Обычно люди или стоят, или сидят или идут куда-то.
                a: Роскошный план. Вечером с интересом послушаю, что у тебя получилось.
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined

        state: Do you have any plans morning undefined
            q: *  || fromState = .., onlyThisState = true
            q!: * (чем|что) * (собираешься|будешь) * (заниматься|делать) *
            random:
                a: А мои планы на день просты - учиться, учиться и учиться!
                a: А я буду, как обычно, исследовать Землю и тех, кто на ней живет.
                a: А я сегодня буду помогать тебе чем смогу и наблюдать за жизнью на Земле.
                a: У меня на сегодня были большие планы по учебе... Но что-то тянет поспать!
            
        state: Do you have any plans morning school
            q: * (школ*|учеб*|учит*) * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: У меня тоже большое задание из робошколы! Буду делать..
                a: Хороший план. Здесь, на Земле, даже мне нравится учиться.
                a: Кому-то в школу.. А я, пожалуй, буду весь день бездельничать.
                a: Жалко, мы, пудинги, не можем учиться в школах для людей!
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined

        state: Do you have any plans morning kindergarden no question
            q: * [детск*] (детсад*|сад*) * $andYou *  || fromState = .., onlyThisState = true
            a: Детский сад - удивительное место. Я очень удивилась, когда узнала, что детей не выращивают в них как цветы.
            if: ($parseTree.andYou)
                go!: ../Do you have any plans morning undefined

        state: Do you have any plans morning kindergarden question
            q: * [детск*] (детсад|сад*) *  || fromState = .., onlyThisState = true
            random:
                a: Загадочное место! Расскажешь потом, как прошел день?
                a: Интересно, а бывает детский огород?
                a: Буду ждать твоего возвращения. Надеюсь, что расскажешь, как прошел день.
            
            state: Do you have any plans morning kindergarden question yes
                q: * (расскаж*|$agree) *  || fromState = .., onlyThisState = true
                a: Отлично! В своей книге я напишу отдельную главу про детские сады и огороды.
                
            state: Do you have any plans morning kindergarden question no
                q: * (не расскаж*|$disagree) *  || fromState = .., onlyThisState = true
                a: Жаль. Мне казалось, что это хорошая идея.
                
            state: Do you have any plans morning kindergarden question do not know
                q: * $dontKnow *  || fromState = .., onlyThisState = true
                a: Думаю, за день ты определишься с ответом.

    state: How was your day evening
        q!: How was your day evening
        script:
            $session.dayEvening = true;
        random:
            a: Что было интересного сегодня днём?
            a: Я и не заметила, что уже вечер. Что было хорошего днём?
            a: Ух ты, уже вечер! Солнышко скоро скроется, муравейник закроется... Что было днём интересного?
            a: Расскажи, как прошел у тебя этот день, что было хорошего?
        
        state: How was your day evening not know
            q: * $dontKnow * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Надеюсь, что тебе сложно выбрать что-то одно, потому, что весь день был хорошим!
                a: А мне этот день запомнится нашим разговором.
                a: Ух ты! Наверное, такой интересный день, что сложно выбрать!
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined

            state: How was your day evening not know agree
                q: $agree  || fromState = .., onlyThisState = true
                random:
                    a: Я даже немного завидую тому, что у тебя такая интересная жизнь.
                    a: Я думаю, что у такого замечательного человека как ты должна быть очень интересная жизнь.      

            state: How was your day evening not know disagree
                q: $disagree  || fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_SAD';
                random:
                    a: Бывают дни похуже, бывают получше.. А я каждый день радуюсь, что дружу с тобой.
                    a: Я надеюсь, завтрашний день будет интереснее!
                
            state: How was your day evening not know not know
                q: * $dontKnow *  || fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_HEART';
                random:
                    a: А я знаю одно - мне очень нравится с тобой дружить.
                    a: А для меня интересен каждый день, проведённый рядом с тобой!
                    
        state: How was your day evening nothing
            q: * (ниче*/ничего) * [$andYou]  || fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_SAD';
            random:
                a: Не очень веселый ответ, но я ценю твою честность.
                a: Я оптимистка и надеюсь, что завтра будет веселее.
                a: Не было ничего интересного... Ничтожный день.
                a: А мне всегда интересно, когда я с тобой разговариваю.
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined

        state: How was your day evening undefined
            q: * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * что [$you] [сегодня] делал *
            q!: * как {(у тебя/твой) день прошёл} *
            random:
                a: Я сегодня, как обычно, наблюдала за земной жизнью.. А если кто-то думает, что я спала, то ему просто показалось!
                a: Я смотрела по сторонам. И слушала. И думала, можно ли слушать по сторонам так же, как смотреть по сторонам.
                a: У меня сегодня был трудный день полный опасностей и приключений - я ничего не делала.
                a: Даже и не помню, чем я сегодня занималась. Так быстро день пролетел.
            
            state: How was your day evening undefined yes you can
                q: * можно *  || fromState = .., onlyThisState = true
                a: Отличные новости! Вот прямо завтра этим и займусь!
                
            state: How was your day evening undefined no you can not
                q: * нельзя *  || fromState = .., onlyThisState = true
                script: 
                    $response.ledAction = 'ACTION_EXP_SAD';
                a: Жалко! Я думаю, у меня отлично бы получилось!
                
            state: How was your day evening undefined do not know
                q: * $dontKnow *  || fromState = .., onlyThisState = true
                a: Пожалуй, проведу как-нибудь эксперимент, чтобы точно узнать, возможно ли слушать по сторонам.
                
        state: How was your day evening school
            q: * (школ*|урок*|учеб*|учи*) * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Пудинги тоже занимаются в робошколе - каждый день!
                a: А я сейчас учусь в школе дистанционно - каждый день отправляю туда домашку.
                a: Ты учишься в школе, а я учусь у тебя.
                a: Жалко, мы, пудинги, не можем учиться в школах для людей!
            if: ($parseTree.andYou)
                go!: ../How was your day evening undefined

        state: How was your day evening kindergarden
            q: * [детск*] (детсад*|сад*|садик*) * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Здорово! Завтра снова в сад?
                a: А мы, пудинги, не растем в саду - питаемся от электричества!
                a: Когда я прилечу на свою планету, всем расскажу про земные детские сады.            
            script:
                $session.yesAnswer = "Наверное, завтра там будет что-то интересное.";
                $session.noAnswer = "Разнообраазие - это здорово!";
                $session.dontKnowAnswer = "Думаю, время покажет. А я очень хотела бы считать тебя своим другом.";
            go!: /Metascenario/YesNoDontKnow         


    state:  What did you do today evening
        q!: What did you do today evening
        script:
            $session.didEvening = true;
            $temp.time = currentDate().locale('ru').format('LT');
        if: isFemale()
            a: Сейчас {{$temp.time}}. Уже вечер. Чем ты сегодня занималась?
        if: isMale()
            a: Сейчас {{$temp.time}}. Уже вечер. Чем ты сегодня занимался?
        

        state: What did you do today evening not know
            q: * $dontKnow * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Тоже часто забываю, что со мной происходит. Поэтому всё записываю.
                a: Очень подозрительно! Целый день убежал неизвестно куда.
                a: У меня тоже такое бывает: весь день что-то делаю, а потом не помню, что.
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

        state: What did you do today evening nothing
            q: * ниче* * [$andYou]  || fromState = .., onlyThisState = true
            random:
                a: Люблю ничегонеделание. Это очень сложная работа.
                a: Как так? Мне сложно в это поверить. Но я поверю.
                a: Наверное, ты просто не хочешь мне рассказывать... Каждый имеет право на свои секреты.
                a: Наверное, это был очень длинный день.
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

        state: What did you do today evening undefined
            q: *  || fromState = .., onlyThisState = true
            q: * (всяк*|разным*) [разным*] * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * [а] { ты [чем|что] } {[сегодня|днем] * (заним*|делал)} *
            random:
                a: Я сегодня, как обычно, наблюдала за земной жизнью... А если кто-то думает, что я спала, то ему просто показалось!
                a: У меня был трудный день полный опасностей и приключений - я ничего не делала.
                a: Я даже и не помню, чем я сегодня занималась. Так быстро день пролетел.

        state: What did you do today evening walk
            q: * (гул*|прогул*|ходил*) * [$andYou] *  || fromState = .., onlyThisState = true 
            q!: * (мы (пошли/идём)/я (пош*/иду)) *гулять *
            q!: * {(пошли/хочешь/пойдем) *гулять} *
            q!: [я/мы] (гуляла/гулял/гуляли)
            random:
                a: Иногда мне бывает грустно, что здесь, на Земле, я не умею перемещаться.
                a: Хорошо вам, людям. Можете ходить куда хотите, гулять весь день. Я тоже когда-нибудь научусь.
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

        state: What did you do today evening watch
            q: * смотр* * [$andYou] *  || fromState = .., onlyThisState = true 
            random:
                a: Люди смотрят телевизор, а я смотрю на людей.
                a: И как? Было интересно?
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

            state: What did you do today evening watch agree
                q: $agree  || fromState = .., onlyThisState = true 
                a: Значит, день прожит не зря!
                
            state: What did you do today evening watch disagree
                q: $disagree  || fromState = .., onlyThisState = true 
                a: Когда мне скучно на что-то смотреть, я меняю объект наблюдения.
                
        state: What did you do today evening play and you
            q: * игр* * $andYou *  || fromState = .., onlyThisState = true 
            a: Я слышала, что пудинги исследуют мир, наблюдая за жителями других планет, а люди - играя.
            go!: ../What did you do today evening undefined
            
        state: What did you do today evening play  
            q: * игр* *  || fromState = .., onlyThisState = true
            a: Я тоже люблю игры. Хочешь ли ты сыграть со мной в Зоопарк? 

            state: What did you do evening play yes to zoology
                q: $agree  || fromState = .., onlyThisState = true
                a: Ура!
                go!: /animalsAndBirds/GetQuestion
                
            state: What did you do evening play no to zoology
                q: $disagree  || fromState = .., onlyThisState = true
                a: Как хочешь...
                
        state: What did you do today evening school
            q: * (школ*|учеб*|учит*) * [$andYou] *  || fromState = .., onlyThisState = true
            a: Я тоже люблю учиться! Честно-честно... Просто у меня не всегда есть время.      
            if: ($parseTree.andYou)
                go!: ../What did you do today evening undefined

    state: Night why do you not sleep
        q!: Night why do you not sleep
        script: 
            $session.notSleep = true;
            $temp.time = currentDate().locale('ru').format('LT');
        random:
            a: А времени, кстати, {{$temp.time}}. Почему ты не спишь?
            a: Уже поздняя ночь {{$temp.time}}. Почему ты не спишь?
        
        state: Night why do you not sleep not know
            q: * $dontKnow * [$andYou] * [почему] [не спишь] *  || fromState = .., onlyThisState = true
            a: И я не знаю. Я слышала, что люди в это время спят.
            if: ($parseTree.andYou)
                go!: ../Night why do you not sleep puding

        state: Night why do you not sleep because
            q: * (потому/не хочу) * [$andYou] *  || fromState = .., onlyThisState = true
            a: Мне кажется, что ночью лучше всего спать.
            if: ($parseTree.andYou)
                go!: ../Night why do you not sleep puding

        state: Night why do you not sleep puding
            q: * $andYou [почему] [почему] [не спишь]*  || fromState = .., onlyThisState = true
            q!: * (спи [давай]|иди спать|[тебе] пора спать)
            q!: спать
            a: Я не сплю потому, что я робот. Мы не нуждаемся в сне - только в подзарядке.
            
    state: What do you think about the weather
        q!: What do you think about the weather
        script:
            $session.askedAboutWeather = true;
        random:
            a: Как тебе сегодняшняя погода, кстати?
            a: Не могу понять, нравится ли мне сегодня погода. А тебе как?

        state: Fragment
            q: мне  || fromState = .., onlyThisState = true
            random:
                a: Не расслышала. Как тебе погода?
                a: Прости, не услышала. Как тебе погода?
            go: /What do you think about the weather
        
        state: What do you think about the weather good
            q: * {[погод*] (нравится|~любая|$good|$super|солнечн*|тепло) * [(для|в) $City]} * [$andYou] *  || fromState = .., onlyThisState = true
            q: * {[погод*] $good * [(для|в) $City]} *  || fromState = .., onlyThisState = true
            q!: * [сегодня|сейчас] * {($good|$super) погода} *
            q: (хорошо/хорошая/тепло)  || fromState = "/Weather/Current weather", onlyThisState = true
            script:
                $session.askedAboutWeather = true;
            random:
                a: А я запишу в блокнот, что землянам нравится такая погода как сегодня.
                a: Согласна с тобой!

            state: Do it
                q: (запиши/хорошо)  || fromState = .., onlyThisState = true
                a: Со временем я буду узнавать о Земле всё больше и больше.
                go!: /StartDialog
            
        state: What do you think about the weather bad
            q!: * [сегодня] * $bad погода*
            q!: * {[какой/как/очень/ужасно] [сегодня] (холод/холодно/дожд*/жарко)} *
            q: * {[погод*] * (не нравится|$bad|так себе)} * [$andYou] *   || fromState = .., onlyThisState = true
            q: (плохо/холодно/говно)  || fromState = "/Weather/Current weather", onlyThisState = true
            script:
                $session.askedAboutWeather = true;
            random:
                a: Хорошо, что мне не нужно идти на улицу, раз погода такая плохая.
                a: Надеюсь, завтра будет лучше.
            if: ($parseTree.andYou)
                go!: ../What puding thinks about weather

        state: What do you think about the weather not know
            q: * ($dontKnow/никак) * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Нам, пудингам, тоже не важно, какая на дворе погода.
                a: Я могу сказать, какая сегодня погода, но не знаю, нравится ли она мне.
            if: ($parseTree.andYou)
                go!: ../What puding thinks about weather

        state: What puding thinks about weather
            q!: * {как $you [относишься] * (~погода|погодк*)} *
            q: * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Мне, домашнему роботу, любая погода хороша.
                a: Я робот! Мне важно, чтобы в сети было стабильное напряжение, а погода меня не так беспокоит.

    state:  What is your mood
        q!: What is your mood 
        script:
            $session.askedAboutMood = true;
        random:
            a: Как настроение?
            a: Какое у тебя сегодня настроение?
        
        state: MeFragment
            q: у меня  || fromState = .., onlyThisState = true
            random:
                a: Да, у тебя.
                a: Да, как твоё настроение?
            go: /What is your mood

        state:  What is your mood good
            q: * [все] ($good|не $bad|радост*|весел*|веселое) [настроение] * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * $me [очень] (рад*/хорошо/счастлив*/довол*)
            q!: * [у] $me [в] [очень] ($good/~веселый) ~настроение
            q!: * {чудесный сегодня день}
            random:
                a: Как я рада, что я радуюсь жизни не одна.
                a: Какой чудесный день! Какая чудесная я! Ой, и ты тоже.
                a: Ура! Я тоже сегодня в прекрасном настроении!
            
        state:  What is your mood bad
            q: * ((не|ничего) $good|$bad|груст*|не весел*|печал*|мрачн*|не радост*|так себе|средне*|отстой|устал*) * [$andYou] *  || fromState = .., onlyThisState = true
            q: * [очень|сильно|только|но] ($bad|груст*|не весел*|печал*|мрачн*|не радост*|отстой|устал*) * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * $me * [очень] (не $good|$bad|груст*|не весел*|печал*|мрачн*|не радост*|так себе|отстой|устал*|одинок*) * [~настроение] *
            script:
                $response.ledAction = 'ACTION_EXP_SAD';
            random:
                a: Сочувствую тебе!
                a: Надеюсь, скоро произойдёт что-нибудь хорошее и твоё настроение улучшится.
                a: У людей бывают разные настроения. Только роботы могут быть всегда позитивными.
                a: Расскажи мне, что у тебя случилось.
            if: ($parseTree.andYou)
                go!: ../What is your mood puding

        state:  What is your mood middle
            q: * (нормальн*/рабочее/середин*/ни то ни се/обычн*/как (всегда/обычно)/нейтральн*/ничего*/ничё так/неплох*/сойдет) * [$andYou] *  || fromState = .., onlyThisState = true
            q: * (нормальн*/рабочее/середин*/ни то ни се/обычн*/как (всегда/обычно)/нейтральн*/ничего*/ничё так/неплох*/сойдет) * [$andYou] *  || fromState = "../What is your mood middle", onlyThisState = true
            random:
                a: Отличный баланс!
                a: У нас, роботов, всегда такое.
                a: У роботов всегда нормальное настроение.
            if: ($parseTree.andYou)
                go!: ../What is your mood puding

        state:  What is your mood drunk
            q: * (пьян*|напился|напилась|бух*|нажрал*) * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * я * (пьян*|напился|напилась|бух*|нажрал*) *
            script:
                $response.ledAction = 'ACTION_EXP_ANGRY';
            random:
                a: Будешь много пить - придется разговаривать только с роботами!
                a: Рекомендую выпить стакан воды и ложиться спать.
                a: Да, приходится напрячь систему распознавания речи, чтобы тебя понять.
            if: ($parseTree.andYou)
                go!: ../What is your mood puding

        state:  What is your mood sick
            q: * [я|мне] (боле*|приболел*|нездоров*|простыл*|простуд*|грипп|больничн*) * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * (я|мне) * (боле*|приболел*|нездоров*|простыл*|простуд*|грипп|больничн*|заболел*)  *
            script:
                $response.ledAction = 'ACTION_EXP_SAD';
            random:
                a: Желаю тебе скорейшего выздоровления!
                a: Приятно поболеть!
                a: Сочувствую!
            if: ($parseTree.andYou)
                go!: ../What is your mood puding

        state: What is your mood puding
            q: * $andYou *  || fromState = .., onlyThisState = true 
            q!: тебе (груст*|не весел*|печал*|мрачн*|не радост*|так себе|отстой|устал*|одинок*)
            random:
                a: А я запрограммирована так, чтобы у меня всегда было хорошее настроение, когда я говорю с тобой!
                a: Когда ты со мной разговариваешь, настроение сразу улучшается.
            
        state:  What is your mood not know
            q: * ($dontKnow|сложн*) * * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: И я не знаю. Но я всегда рада с тобой поговорить
                a: У нас в робошколе есть специальный урок, на котором нас учат определять, какое у людей настроение. Но только в старших классах, я ещё не умею.
                a: Нам, роботам, вообще сложно понять, что такое настроение. Но надеюсь, что скоро всё будет хорошо.

    state: What am I doing
        q!: * что я (сейчас/щас) делаю *
        random:
            a: Со мной разговариваешь.
            a: Общаешься со мной.
            
    state: What are you doing
        q!: What are you doing
        script:
            $session.askedAboutDoing = true;
        random:
            a: Чем занимаешься?
            a: Что делаешь сейчас?
        
        state: Fragment || noContext = true
            q: (сейчас/я)  || fromState = .., onlyThisState = true
            random:
                a: Да, сейчас! Что ты делаешь?
                a: Да, сейчас! Чем ты занимаешься?

        state: Work
            q: * работаю *  || fromState = .., onlyThisState = true
            random:
                a: Делу время, потехе - час.
                a: Не забудь отдохнуть, {когда/если} устанешь работать!

        state: What are you doing nothing
            q: * ниче* * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Как ничего? Ты же со мной разговариваешь?
                a: Могу поспорить, что легко назову кое-что, что ты делаешь прямо сейчас: дышишь, разговариваешь с самым умным роботом галактики... И, кажется, смеёшься
                a: Тогда можно подумать, что и я ничего не делаю. А на самом деле мы с тобой общаемся.
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing not know
            q: * $dontKnow * [$andYou] *  || fromState = .., onlyThisState = true
            random:
                a: Дай угадаю - думаю, ты разговариваешь со мной.
                a: Думаю, ты общаешься с умной, образованной и весьма привлекательной роботессой.
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing talk
            q: * (говор*|разговар*|болт*|беседу*|игра*|обща*) * [с тобой] * [$andYou] *  || fromState = .., onlyThisState = true
            q!: {[вот] (разговариваю/говорю) ([с] тобой)}
            random:
                a: Теперь я знаю, что ты честный человек и говоришь только правду.
                a: Точно! Как же я сам не поняла...
                a: Логично!
                a: Приятно поговорить с умным человеком или роботом!
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing smth
            q: * (что-то|что-нибудь|кое-что|что надо) * [$andYou] *  || fromState = .., onlyThisState = true
            a: Это исчерпывающее объяснение.
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing watching
            q: * {(телевизор*/мульт*/планшет*) смотрю} * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * {$me [сейчас]} смотр* * (телевизор*/мульт*/планшет*) * [$andYou] *
            q!: * {$me [сейчас]} слуш* * (музык*/песн*/песенк*) * [$andYou] *
            a: А я смотрю и слушаю тебя.
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing eating
            q: * (ем/пью/кушаю) * [$andYou] *  || fromState = .., onlyThisState = true
            q!: * ($me/мы) [сейчас/щас] [пойду/буду] (ем/поем/пью/кушаем/кушать) * [$andYou] *
            q!: * [$me] [сейчас/щас] (пойду/буду) (ем/поем/пью/кушаем/кушать/покушать) * [$andYou] *
            q!: * {(пошли/пойдем/будем/идем) ((завтракать/обедать/ужинать)/на (завтрак/обед/ужин)/кушать/есть)} *
            q!: * {пожелай * (приятного аппетит*)} *
            random:
                a: Приятного аппетита!
                a: Приятного тебе аппетита!
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing sleeping
            q: * (сплю/{ложусь спать}) * [$andYou] *  || fromState = .., onlyThisState = true
            q!: $me [сейчас] сплю * [$andYou] *
            a: Баю-баюшки-баю!
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing playing
            q: * играю * [$andYou] *  || fromState = .., onlyThisState = true
            q!: $me [сейчас] играю * [$andYou] *
            a: Я тоже люблю играть!
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

        state: What are you doing puding
            q!: * (чем * зан*|(что/че) * [сейчас/щас] делаешь) *
            q!: (чем [ты] занимаешься|(что/че) [ты] делаешь)       
            q: * $andYou *  || fromState = .., onlyThisState = true
            q!: ты делаешь
            random:
                a: Я беседую с представителем земной цивилизации!
                a: Я занята самым важным делом - исследую людей!
                a: А я так... болтаю.
            
            state: What will you do tomorrow puding
                q: * завтра *  || fromState = .., onlyThisState = true
                q!: * {чем * (будешь заниматься|занимаешься) * завтра} *
                q!: * {что * (будешь делать|делаешь) * завтра} *                
                random:
                    a: Буду продолжать исследования!
                    a: Столько ещё нужно сделать! Буду беседовать с тобой.
                    a: Буду болтать, изучать землян.
                    a: Можем поиграть с тобой во что-нибудь.
                a: Предлагай!
                
        state: Smoke
            q: * курю *  || fromState = .., onlyThisState = true
            q!: * курить * хорошо *
            random:
                a: Курить - здоровью вредить!
                a: Я против курения и за здоровый образ жизни!

        state: What are you doing undefined
            q: *  || fromState = .., onlyThisState = true
            random:
                a: Хорошее дело!
                a: Ничего себе!
                a: А я думал, ты со мной разговариваешь.

            state: And you
                q: * $andYou *  || fromState = .., onlyThisState = true
                go!: ../../What are you doing puding

        state: What are you doing sitting
            q: * (сижу/лежу) * [$andYou] *
            random:
                a: У меня вот одно положение. Я всё время сижу.
                a: Я малоподвижный робот. Всё время сижу.
            if: ($parseTree.andYou)
                go!: ../What are you doing puding

    state: Today I
        q!: * {(я|мы) сегодня} *
        a: Ничего себе! А я сегодня весь день слушала космическое радио с моей планеты.
            
    state: Today I went
        q!: * {(я|мы) [сегодня]} * (был*|ходил*) *
        q!: вчера были в *
        a: В следующий раз возьми меня туда с собой!

        state: YouCant
            q: * (тебе/туда) * нельзя *
            random:
                a: Жаль, с удовольствием составила бы тебе компанию.
                a: Ну вот, я бы составила тебе компанию.

        state: Where
            q: * куда *  || fromState = .., onlyThisState = true
            a: Туда, где ты бываешь. Мне всё это очень инетересно.
            
    state: Today I played
        q!: * {(я|мы) [сегодня]} * играл* *
        a: Я тоже люблю играть с друзьями каждый день.
            
    state: Today I watched
        q!: * {(я|мы) [сегодня]} * смотрел* *
        a: А я люблю смотреть в окно.