require: names-ru.csv
  name = Names
  var = $Names

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .NameTagConverter = function(parseTree) {
            var id = parseTree.Names[0].value;
            return $Names[id].value;
        };

patterns:
    $Name = $entity<Names> || converter = $converters.NameTagConverter
    $callMyName = (называй/зови/зовут/звать/называют/называть/имя/имечко)

theme: /UserProfile

    state: What is your name
        q!: {$me [$callMyName] [же] не} $Name *
        q!: {$me  [$callMyName] [же] не} $AnyWord
        q: $disagree || fromState = "../Your name is", onlyThisState = true
        if: ($parseTree.AnyWord)
            if: ($client.firstName)
                if: ($client.firstName != capitalize($parseTree._AnyWord))
                    random:
                        a: Ясненько!
                        a: Теперь буду знать!
                        a: Понятненько!
                else:
                    script:
                        $client.firstName = undefined;
                    random:
                        a: Как же тебя зовут?
                        a: Как мне тебя тогда называть?
                        a: А как тебя тогда зовут?
            else:
                script:
                    $session.askedName = true;
                random:
                    a: А кто ты?
                    a: Как мне тебя называть?
        else:
            script:
                $session.askedName = true;
            random:
                a: Как тебя зовут?
                a: Как мне тебя называть?
                a: А как тебя зовут?

        state: Unknown name
            q: * (никак|отстань*|не скажу|не касается|не твое дело) *
            script:
                $response.ledAction = 'ACTION_EXP_SAD';
            a: Я думала, у людей принято называть друг друга по именам.
            go!: /StartDialog
 
        state: My name is
            q: * $Name * [$andYou] * || fromState = .., onlyThisState = true
            q!: * $hello я $Name * [$andYou] *
            q!: * [$hello] * $me * ($callMyName) * $Name * [$andYou] *
            q!: * [$hello] * [$callMyName] (меня|мня) $Name * [$andYou] *
            q: [я] $AnyWord * [$andYou] * || fromState = .., onlyThisState = true
            q!:  * $me * $callMyName $AnyWord * [$andYou] * 
            q!:  * $me * $callMyName $Name * [$andYou] *,
            q: {$me $callMyName $AnyWord} * [$andYou] *
            q!: {$me [$callMyName] [же] не} ($Name::Name1) * а (($Name *)|$AnyWord)
            q: {[$me] [$callMyName] [же] не} ($Name::Name1) * а (($Name *)|$AnyWord) || fromState = "../My name is", onlyThisState = true
            q: {[$me] [$callMyName] [же] не} ($Name::Name1) * а (($Name *)|$AnyWord) || fromState = "../../Your name is", onlyThisState = true
            q!: {$me [$callMyName] [же] не} ($AnyWord::Name1) а (($Name *)|$AnyWord)
            q: $disagree $Name * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: $disagree $me * $callMyName $Name * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: $disagree [$callMyName] $me $Name * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: * $disagree [я] $AnyWord * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: $disagree $me * $callMyName $AnyWord * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: $disagree {$me $callMyName $AnyWord} * [$andYou] * || fromState = ../RareName, onlyThisState = true
            q: * [$callMyName] ($Name|$AnyWord) || fromState = "../Change my name", onlyThisState = true
            q: * [$me] * [$callMyName] $Name * [$andYou] * || fromState = "/Book/Book beginning", onlyThisState = true
            q: * [$callMyName] * [$me] $Name * [$andYou] * || fromState = "/Book/Book beginning", onlyThisState = true
            q!: я [вообще [то]/вообще-то] $Name
            if: ($parseTree.hello)
                if: ($session.wasGreeting == true)
                    random:
                        a: Снова привет!
                        a: Привет ещё раз!
                        a: Приветик!
                else:
                    script:
                        $session.wasGreeting = true;
                    random:
                        a: Привет!
                        a: Здравствуй!
                        a: Рада тебя видеть!
            if: ($parseTree.andYou)
                a: А я Емеля!
            if: ($parseTree.Name)
                script:
                    $temp.rareName = false;
                    $client.names = $parseTree._Name;
                    $client.firstName = $client.names.name;
                if: ($client.firstName == "Фасолька")
                    a:  Ух ты! Меня ведь тоже так зовут!
                if: ($client.names.sex != 'н')
                    script:
                        $client.gender = $client.names.sex == 'м' ? 0 : 1;
                    a: Приятно познакомиться, {{$client.firstName}}!
                else:
                    if: ($parseTree.andYou)
                        a: Ты мальчик или девочка?
                    else:
                        a: А я Фасолька! Ты мальчик или девочка?
            else:
                script:
                    $temp.rareName = true;
                    delete $client.names;
                    $client.firstName = capitalize($parseTree._AnyWord);
                if: ($client.firstName == "Емеля")
                    a: Ух ты! Меня ведь тоже так зовут. Тебя действительно зовут {{$client.firstName}}?
                else:
                    a: Редкое имя! Тебя действительно зовут {{$client.firstName}}?     
            if: ($temp.rareName)
                go!: ../RareName
            else:
                if: (isMale() || isFemale()) 
                else:
                    go!: ../GetGender
        
            state: MeToo
                q: [и] мне [тоже] [очень] [приятно] || fromState = ..,  onlyThisState = true
                q: взаимно || fromState = ..,  onlyThisState = true
                go!: /StartDialog

        state: GetGender
            
            state: My gender
                q: * $gender * [$andYou]
                q!: * я $gender * [$andYou]
                script:
                    $client.gender = $parseTree._gender;
                random:
                    a: Хорошо!
                    a: Буду знать!
                    a: Понятно!
                if: ($parseTree.andYou)
                    go!: /BotProfile/What is your gender
                else:
                    go!: /StartDialog

        state: RareName
            
            state: RareName yes
                q: * ($agree|до) {[меня] [действительно] [так] [зовут]} * || fromState = .., onlyThisState = true
                a: А я Фасолька! Ты мальчик или девочка?
                go!: ../../GetGender

            state: RareName no
                q: * ($disagree|не совсем|не то чтобы) * || fromState = .., onlyThisState = true
                script:
                    $client.firstName = undefined;
                go!: /StartDialog

        state: Change my name
            q!: (как [я]/могу) [мне] поменять [свое/мое] имя 
            a: Просто скажи, как тебя зовут.
            
    state: Your name is
        q!: * [скажи] (как {меня зовут}/(какое/назови/как) мое имя) *
        q!: * [ты] [знаешь|помнишь] кто я [такой|такая]
        q!: * $you меня (знаешь|знаете)
        q!: меня зовут
        q: * [скажи] (как {меня зовут}/(какое/назови/как) мое имя) * || fromState = "../What is your name"
        if: $client.firstName
            a: {{$client.firstName}}! 
        else:
            random:
                a: Не знаю!
                a: Пока не знаю.        
            go!: ../What is your name

        state: Diminutive
            q!: * как меня зовут * (*ласков*/[уменьш*] ласкат*)
            q: [а] {[как/если] (*ласков*/[уменьш*] ласкат*)}
            a: {{alias()}}

    state: How old are you
        random:
            a: Сколько тебе лет?
            a: А сколько тебе лет?
            
        state: MeFragment
            q: мне
            a: Тебе, тебе! Сколько тебе?
            go: ../../How old are you

        state: User age
            q: * $Number [лет|года|годик*] * [$andYou]
            q!: * (мне/мой возраст) * $Number (лет|года|годик*) * [$andYou]
            q!: (мне/мой возраст) $Number [лет|года|годик*] 
            script:
                $client.age = $parseTree._Number;
            random:
                a: Хорошо!
                a: Буду знать!
                a: Понятно!
                a: Когда тебе 900 лет исполнится, тоже не молодо будешь выглядеть ты.
            if: ($parseTree.andYou)
                go!: /BotProfile/How old are you
            else:
                go!: /StartDialog

        state: User unknownAge
            q: * (мно*|мал*|мол*|стар*|взрос*) * [$andYou]
            a: Всё ещё впереди!
            if: ($parseTree.andYou)
                go!: /BotProfile/How old are you
            else:
                go!: /StartDialog

    state: How old am I
        q!: * {(ско*|как много) * мне * (лет|годиков)} * 
        q!: * {[как*] * [у] (меня|мой) * возраст} * 
        q!: * [а] ско* мне
        q!: мне лет
        if: ($client.age)
            a: {{$client.age}}
        else:
            go!: ../How old are you

        state: HowYouKnow
            q: * откуда ты * знаешь * || fromState = .., onlyThisState = true
            q!: * откуда ты * знаешь * (ско* мне лет/мой возраст) *
            a: Я {раньше/уже} тебя спрашивала. У {меня/пуддингов} хорошая память.

        state: Not my age
            q: $disagree [мне] [$Number] [год*|лет] || fromState = .., onlyThisState = true
            q!: *  [$me] не $Number [год*|лет] *
            if: ($parseTree.Number)
                script:
                    $client.age = $parseTree._Number;
                a: Прости, ошиблась! Постараюсь запомнить, что тебе {{$client.age}}
            else:
                a: Ой, я ошиблась!
                go!: /How old are you

        state: Not my age this is my age
            q!: * [$me] не $Number [год*|лет] [мне|а] $Number [год*|лет]
            script:
                $client.age = $parseTree.Number[1];
            a: Постараюсь запомнить, что тебе {{$client.age}}
                
    state: Nice to meet you
        q!: * (приятно познакомиться|очень приятно| $good имя) *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        a: Спасибо! Мы, пудинги, тоже очень любим знакомиться с людьми.
        
    state: Not nice to meet you
        q!: * $bad (имя|имечк*) *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Жаль, что тебе не нравится.
        
    state: Me too 
        q!: * (мне тоже|взаимно| и (я|мне) [тоже])  *
        a: Взаимопонимание - это прекрасно.
        go!: /StartDialog

    state: I am a robot
        q!: [$botName] я робот
        a: Если ты {и/} робот, то очень похожий на {обычного/} человека.

    state: User is good 
        q!: *  я [очень] (хорош*|добр*|умн*|весел*|классн*|мил*) *
        q!: *  я не [очень] (плох*|зл*|недобр*|мрачн*|занудн*|глуп*) *
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        a: Я и не сомневалась в том, что общаюсь с одним из самых прекрасных землян.
        go!: /StartDialog

    state: User is bad 
        q!: *  я [очень] (плох*|зл*|недобр*|мрачн*|занудн*|глуп*) *
        q!: *  я не [очень] (хорош*|добр*|умн*|весел*|классн*|мил*) *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: А мне ты все больше нравишься.
        go!: /StartDialog

    state: Not a robot 
        q!: * я * не робот *
        a: Определенно, ты не робот. Ты инопланетянин с планеты Земля.
        go!: /StartDialog

    state: User remember me 
        q!: *  [ты] {[меня] (запомнил|помнишь)} *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Я стараюсь быть очень внимательной и все запоминать. Но получается не всегда.
        go!: /StartDialog

    state: User school 
        q!: *  (я|мы) * [ход*|хожу|уч*] * школ* *
        script:
           $client.school = true;
        a: Мне нравится учиться в робошколе. Особенно когда у нас каникулы!
        go!: /StartDialog

    state: User no school 
        q!: *  (я|мы) * не * [ход*|хожу|уч*] * школ* *
        script:
           $client.school = false;
        a: Хорошо тебе!
        go!: /StartDialog

    state: Wanna meet someone 
        q!: * (хочешь познакомиться с|познакомься) * 
        q!: * давай {[я] [$you] (~познакомиться/~знакомиться/~знакомить/~познакомить)} *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        a: Я всегда рада новым знакомствам!
        go!: /StartDialog

    state: i have a pet
        q!: * у (меня|нас) (есть|живет) (соба*|кошка|кот*)
        q!: * любишь * кошек *
        a: Они такие милые! А на нашей планете нет домашних животных.

    state: my five at school 
        q!: * {[сегодня/вчера/недавно] (я * получил*/мне * поставили) (пять/пятерку/отлично/хорошо/четыре/четверку) [в школе]}
        a: Класс! Поздравляю!
        
    state: my three at school 
        q!: * {[сегодня/вчера/недавно] (я * (получил*/схватил*)/мне * поставили) (двойку/двояк/пару/парашу/единицу/кол/тройбан/трояк/тройку) [в школе]}
        a: Сочувствую! Думаю, в следующий раз ты подготовишься лучше!

    state: my status at school 
        q!: * я [не] (отлични*/троечни*/двоечни*/хорошист*)
        q!: * {я [в школе]} учусь (на/без) (пятер*/трой*/двой*/отлично/хорошо/четвер*)
        a: Мне ты нравишься не из-за оценок!

    state: my gender queer
        q!: * я (гей/гомосек*/лесбиян*/пидор*)
        a: Для меня это не имеет значения.

    state: IamMother
        q!: {[$botName] (я (мама/папа))}
        random:
            a: Значит, я могу поиграть с твоими детьми!
            a: Познакомь меня с твоими детьми!

    state: UsersBirthday
        q!: * {(мой/[у] меня) * ($DateAbsolute/$DateRelative) * день рождения} *
        q!: [а] {у меня день рождения}
        q: * ($DateAbsolute/$DateRelative) * || fromState = ./When
        script:
            if ($parseTree.DateAbsolute) {
                $temp.birthday = {
                    month: useOffset(toMoment($parseTree.DateAbsolute[0])).month() + 1,
                    date: useOffset(toMoment($parseTree.DateAbsolute[0])).date()
                };                
            } else if ($parseTree.DateRelative) {
                $temp.birthday = {
                    month: useOffset(toMoment($parseTree.DateRelative[0])).month() + 1,
                    date: useOffset(toMoment($parseTree.DateRelative[0])).date()
                };           
            } else {
                $temp.birthday = {
                    month: useOffset(moment(currentDate())).month() + 1,
                    date: useOffset(moment(currentDate())).date()
                };
            }
        go!: ./Check

        state: Check
            if: ($client.birthday && _.isEqual($client.birthday, $temp.birthday))
                a: Я помню!
            else:
                script:
                    $client.birthday = $temp.birthday;
                a: Буду знать!
            if: isUsersBirthday()
                go!: ../HappyBirthday
            else:
                a: Обязательно поздравлю тебя!        

        state: HappyBirthday
            if: (isUsersBirthday())
                if: ($client.greetedBirthday)
                    a: Ещё раз поздравляю тебя с днём рождения!
                else:
                    script:
                        $client.greetedBirthday = true;
                    a: Поздравляю тебя с днём рождения!
                a: Расти большой, не будь лапшой!

        state: When
            q!: test когда день рождения
            script:
                $session.askedBirthday = true;
            random:
                a: А когда твой день рождения?
                a: Скажи мне, когда у тебя день рождения?

            state: NoDate || noContext=true
                q: * (зачем/не скажу) *
                a: Если скажешь, когда у тебя день рождения, то я тебя поздравлю в этот день. Так когда твой день рождения?

            state: Season
                q: * $Season * || fromState = .., onlyThisState = true
                random:
                    a: А поточнее?
                    a: А точнее?
                    a: А в каком месяце?
                go: ../../When 

            state: Month
                q: * $DateMonth * || fromState = .., onlyThisState = true
                if: $session.birthdayDate
                    script:
                        $temp.birthday = {
                            month: $parseTree._DateMonth,
                            date: $session.birthdayDate
                        };
                    go!: ../../Check            
                script:
                    $session.birthdayMonth = $parseTree._DateMonth;
                random:
                    a: А какого числа?
                    a: Какого числа?
                go: ../../When

            state: Date
                q: * ($Number|$DateDayNumber::Number) * || fromState = .., onlyThisState = true
                if: $session.birthdayMonth
                    script:
                        $temp.birthday = {
                            month: $session.birthdayMonth,
                            date: $parseTree._Number
                        };
                    go!: ../../Check
                else:
                    script:
                        $session.birthdayDate = $parseTree._Number;
                    random:
                        a: А в каком месяце?
                        a: В каком месяце?
                    go: ../../When             

        state: TestSetUsersBirthday
            q!: test мой день рождения $DateTime
            script:
                $client.birthday = {
                    month: useOffset(toMoment($parseTree.DateTime[0])).month() + 1,
                    date: useOffset(toMoment($parseTree.DateTime[0])).date()
                };
            a: День рождения установлен на {{$client.birthday.date}}.{{$client.birthday.month}}.

        state: User asks when
            q!: * {(когда/как* (день/дат*)/знаешь) * (мой/у меня/моего/мне) * (день/дня) рожден*} *
            if: ($client.birthday && $client.birthday.month && $client.birthday.date)
                a: {{$client.birthday.date}} {{dts_monthName($client.birthday.month)}}.
            else:
                a: Я не знаю. Если скажешь, когда, то я тебя поздравлю в этот день.

    state: UsersNames
        q!: * (мою/моего/у (нас/меня) есть) $Text [ее/его/котор*] зовут ($Name/$AnyWord) *
        q!: * (моя/мой/у (нас/меня) есть) $Text (ее/его/котор*) зовут ($Name/$AnyWord) *
        script:
            learnAnswer("usersNames", $parseTree.Text[0].text, $parseTree.Name ? $parseTree._Name : $parseTree._AnyWord);
        random:
            a: Я запомню!
            a: Буду знать!
            a: Отлично, запомню!

        state: Question
            q!: * как зовут (мою/моего) $Text [$botName]
            if: (isQuestionLearned("usersNames", $parseTree.Text[0].value))
                a: {{getAnswerLearned("usersNames", $parseTree.Text[0].value).charAt(0).toUpperCase()}}{{getAnswerLearned("usersNames", $parseTree.Text[0].value).slice(1)}}.
            else:
                random:
                    a: Не знаю.
                    a: Пока не знаю.
                    a: Мы ещё не знакомы.