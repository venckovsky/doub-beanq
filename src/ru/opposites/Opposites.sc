require: ../games/BeanqGames.sc

require: Opposites.js

require: opposites-ru.csv
    name = Opposites
    var = $Opposites

require: common/common.sc
  module = zenbox_common

require: answers.yaml
    var = OppositesCommonAnswers

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .OppositeTagConverter = function(parseTree) {
            var id = parseTree.Opposites[0].value;
            return $Opposites[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("Opposites");
    $global.$OppositesAnswers = (typeof OppositesCustomAnswers != 'undefined') ? applyCustomAnswers(OppositesCommonAnswers, OppositesCustomAnswers) : OppositesCommonAnswers;


patterns:
    $Opposite = $entity<Opposites> || converter = $converters.OppositeTagConverter
    $adjective = (*ой/*ий/*ый)

theme: /Opposites

    state: Fasolik
        q!: * [давай] * {[играть/сыграем/сыгрыть/играем/поиграем/поиграть/игра] * (противоположн*/перевертыш*/антоним*/онтоним*/наоборот)} *    
        q!: { (противоположно*/перевертыш*/антоним*/онтоним*/наоборот)}
        q!: * [давай] * [играть/сыграем/сыгрыть/играем/поиграем/поиграть/игра] * {[загадай/загадывай/загадывать/[ты] загадаешь/разгадаю/загадать/загадаем/загадаешь/загадывает/дай/задай/буду (отгадывать/угадывать/разгадывать)/{(можешь/умеешь/будешь) загад*}] * [мне] [еще] (противоп*/антоним*/перевертыш*/антоним*/онтоним*)} *
        q: * перевер* * || fromState = /PlayGames/Games, onlyThisState = true
        script:
            $session.oppositesNum = 0;
        a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["a1"]) }}
        go!: ./Play

        state: Play || modal = true
            q: * ($agree|начин*|начн*|помогу|поехали|вопрос*|задавай|называй|назови|говори) [*игра*] * || fromState = .., onlyThisState = true
            q: * ($agree|начин*|начн*|поехали|вопрос*|задавай|называй|назови|говори) * || fromState = "/Games/How to play Opposites", onlyThisState = true
            q: * [давай] * {[ты/сам] (загадай/загадывай/загадаешь/[я] (отгадаю/угадаю/разгадаю)/{(могу/умею/буду) (отгад*/разгад*/угад*)}/{(можешь/умеешь/будешь) загад*}/не (хочу/буду) загадывать) * [против*/перевертыш*]} * || fromState = ../../User/Play, onlyThisState = true  
            q: * [давай] * {[ты/сам] (загадай/загадывай/[я] (отгадаю/угадаю/разгадаю)/{(могу/умею/буду) (отгад*/разгад*/угад*)}/{(можешь/умеешь/будешь) загад*}) * [против*/перевертыш*]} * || fromState = /Opposites
            q: * [давай] * { ты [сам] говори} * || fromState = ../../User/Play, onlyThisState = true
            q: * {[давай] [лучше] (наоборот/*меняемся)} * || fromState = ../../User/Play, onlyThisState = true
            script:
                getOpposite();
                $session.repeatDontKnow = false;
                $session.noWishAgain = false;
                $session.wordWithNo1 = "не" + $session.opposites.last;
                $session.wordWithNo2 = "не " + $session.opposites.last;
                $session.wrongOpposite = undefined;
            a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["a1"]) }}

            state: Opposite
                q: * [$maybe|$sure] * $Opposite * || fromState = .., onlyThisState = true
                script:
                    if ($session.barrier) {
                        $session.barrier = undefined;
                    }
                if: $parseTree._Opposite.word == $session.opposites.last
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Opposite"]["if"]) }}
                    go: ../../Play
                else:
                    if: checkOpposite($parseTree._Opposite)
                        script:
                            $session.opposites.correct += 1;
                            $session.oppositesNum +=1;
                        a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Opposite"]["else"]["if"]["a1"]) }}
                        #a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Opposite"]["else"]["if"]["a2"]) }}
                             
                        if: $session.opposites.turn > 2
                            script:
                                $session.opposites.turn = 0;
                                $session.fasolikTurn = false;
                            go!: ../../../Next question enquire
                        else:
                            go!: ../../Next
                    else:
                        if: $session.wrongOpposite == "Undefined"
                            script:
                                $session.wrongOpposite = undefined;
                                $temp.answer = findOpposite($session.opposites.last);
                            a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Undefined"]["if"]) }}
                            go!: ../../Next
                        else:
                            script:
                                if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                                    var res = $nlp.match($request.query, "/");
                                    $reactions.transition(res.targetState);
                                }
                                $session.wrongOpposite = "Undefined";
                            a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Undefined"]["else"]) }} 
                            go!: /Opposites/Fasolik/Play/Repeat


            state: DontKnowFirst
                q: * ($dontKnow|не могу) * || fromState = .., onlyThisState = true
                q: * (подскажи/помоги) * || fromState = .., onlyThisState = true
                script:
                    $temp.answer = findOpposite($session.opposites.last);
                    $temp.dontKnow = true;
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["DontKnow"]["First"]) }}

                state: DontKnowSecond
                    q: * ($dontKnow|не могу) * || fromState = .., onlyThisState = true
                    q: * ($agree/$disagree/не хочу/не буду/не надо/подскажи/помоги/уже спрашивал/повторяешься/(скажи/какой/назови/давай) * (ответ/сам/ты)/(сам/ты) (ответь/отвечай)) * || fromState = .., onlyThisState = true
                    q: * [$agree] (дальше|след*|ещё|еще|игра*|задавай|другой) [дальше|след*|ещё|еще|игра*|задавай|другой] [вопрос*] * || fromState = .., onlyThisState = true
                    q: * $agree (дальше|след*|ещё|еще|игра*|задавай|другой) [дальше|след*|ещё|еще|игра*|задавай|другой] вопрос* * || fromState = .., onlyThisState = true
                    script:
                        $session.oppositesNum +=1;
                        $temp.answer = findOpposite($session.opposites.last);
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["DontKnow"]["DontKnowSecond"]["a1"]) }}
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["DontKnow"]["DontKnowSecond"]["a2"]) }}
                    go!: ../../../Next                    

            state: SayCorrectAnswer
                q: * (уже спрашивал/задавал*/повторяешься/(скажи/какой/назови/давай) * (ответ/сам/ты)/(сам/ты) (ответь/отвечай)) * || fromState = ..
                q: * [$agree] (дальше|след*|ещё|еще|игра*|задавай|другой|задай|(давай|спроси|назови) [~другой]) [дальше|след*|ещё|еще|игра*|задавай|другой] [вопрос*] * || fromState = ..
                q: * (уже спрашивал/повторяешься/(скажи/какой/назови/давай) * (ответ/сам/ты)/(сам/ты) (ответь/отвечай)) * || fromState = ../../Play
                q: * [$agree] (дальше|след*|ещё|еще|игра*|задавай|другой) [дальше|след*|ещё|еще|игра*|задавай|другой] [вопрос*] * || fromState = ../../Play
                script:
                    if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($request.query, "/");
                        $reactions.transition(res.targetState);
                    }
                    $session.oppositesNum +=1;
                    $temp.answer = findOpposite($session.opposites.last);
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["SayCorrectAnswer"]["a1"]) }}
                go!: ../../Next  

            state: OppositeWithNo
                q: * (не $Opposite) * || fromState = .., onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["OppositeWithNo"]["a1"]) }}
                go!: ../Repeat

            state: Obscene
                q: * ($stopWord|$obsceneWord) * || fromState = .., onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Obscene"]["a1"]) }}
                go!: ../Repeat

            state: Undefined
                q: * [$maybe|$sure] [$AnyWord] * || fromState = .., onlyThisState = true
                script:
                    if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($request.query, "/");
                        $reactions.transition(res.targetState);
                    }
                if: $session.barrier == "Undefined";
                    script:
                        $session.barrier = undefined;
                        $temp.answer = findOpposite($session.opposites.last);
                        $session.oppositesNum +=1;
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Undefined"]["if"]) }}
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Undefined"]["a1"]) }}
                    go!: ../../Next
                else:
                    script:
                        if ($parseTree._AnyWord == $session.wordWithNo1) {
                            $reactions.answer("Приставку НЕ использовать нельзя. Такое правило! Попробуй еще раз.");
                        }
                        else {
                            $session.barrier = "Undefined";
                            $reactions.answer(["Здесь больше подходит другое слово. Попробуй ещё раз!", "Очень близко. Но ответ другой. Попробуй снова."]);
                        }
                    go!: ../Repeat

            state: Repeat
                q: * ({повтори* * [слово]}/какое * слово) * || fromState = .., onlyThisState = true
                q: * {(как/какой) [вопрос]} * || fromState = .., onlyThisState = true
                q: * {(повтори/скажи) [вопрос|слово] [(еще|ещё) [раз]] } * || fromState = .., onlyThisState = true
                q: * (что) [ты] (~говорить|~спрашивать) * || fromState = .., onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Repeat"]) }}
                go: ../../Play

            state: LocalCatchAll
                q: $Text
                script:
                    if ($parseTree._Text.indexOf("хочу ")  > -1 || $parseTree._Text.indexOf("давай ")  > -1 || $parseTree._Text.indexOf("включи ")  > -1 || $parseTree._Text.indexOf("поиграем")  > -1 || $parseTree._Text.indexOf("поиграй ")  > -1 || $parseTree._Text.indexOf("поговори ")  > -1 || $parseTree._Text.indexOf("расскажи ")  > -1 || $parseTree._Text.indexOf("скажи ")  > -1 || $parseTree._Text.indexOf("создай ")  > -1 || $parseTree._Text.indexOf("загадай ")  > -1 || $parseTree._Text.indexOf("открой ")  > -1 || $parseTree._Text.indexOf("запусти ")  > -1 || $parseTree._Text.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($parseTree._Text, "/");
                        $reactions.transition(res.targetState);
                    }

        state: Next
            q: * ($agree/$continue/игра*) * || fromState = ../Next, onlyThisState = true
            a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Next"]["else"]) }}
            go!: ../Play

    state: Next question enquire
            a: {{ selectRandomArg($OppositesAnswers["Next"]["a1"]) }} 

            state: Next question enquire yes
                q: * ($agree|дальше|след*|ещё|еще|игра*|зада*|другой|продолж*|я готов*) [$agree|дальше|след*|ещё|еще|игра*|задавай*] [вопрос*]*
                q: * давай [следующий|другой|еще|ещё|свой|дальше|задавай*] *
                q: * (ещё|еще|продолжай*|не знаю) *
                q: * [можешь] (задать|задавай*) [след*|ещё|еще|другой] [вопрос*] *
                script:
                    $session.flagForNonsense = false;
                if: $session.fasolikTurn 
                    a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["Opposite"]) }}
                    go!: ../../Fasolik/Play
                else:
                    a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Opposite"]["else"]["if"]["a3"]) }}
                    go!: ../../User/Play

            state: Next question enquire no
                q: * ($disagree/хватит/не [хочу]/$notNow/ давай (потом/в другой/в следующий) *) * 
                go!: /Opposites/Stop

            state: LocalCatchAll
                q: *
                script:
                    if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($request.query, "/");
                        $reactions.transition(res.targetState);
                    }
                if: $session.flagForNonsense
                    script:
                        $reactions.answer('Кажется, стало скучновато, и мы уходим от темы. Во что другое хочешь поиграть?');
                        $session.flagForNonsense = false;
                    go: /
                else:
                    script:
                        $session.flagForNonsense = true;
                        $reactions.answer('Кажется, мы отвлеклись. Я как робот считаю, что если играть, то играть по правилам. Я назову слово, а ты назови другое слово, противоположное по смыслу. Сыграем?');
                    go: /Opposites/Next question enquire

    state: User

        state: Play || modal = true
            q!: * [давай] * [играть/сыграем/сыгрыть/играем/поиграем/поиграть/игра] * {[отгадай/разгадай/угадай/[я] загадаю/хочу загадать/буду загадывать/загадываю/(можешь/умеешь/будешь) (отгад*/разгад*/загадать/угад*)/попробую отгадать/отгадывать/угадывать] * [тебе] (противо*/антони*/переверт*)} *
            q: * [давай] * {[ты/сам] (моя очередь/(скажу/говорю/назову) слово/отгадай/отгадывай/угадывай/[я] загадаю/{(буду/хочу) загадывать}/{(можешь/умеешь/будешь) отгад*}) * [против*/переверт*]} * || fromState = ../../Fasolik/Play, onlyThisState = true  
            q: * [давай] * {[ты/сам] ((скажу/говорю/назову) слово/отгадай/отгадывай/угадывай/[я]загадаю/{(буду/хочу) загадывать}/{(можешь/умеешь/будешь) отгад*}) * [против*/переверт*]} * || fromState = /Opposites
            q: * {[давай] [лучше] (наоборот/*меняемся)} * || fromState = ../../Fasolik/Play, onlyThisState = true
            a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["a1"]) }}

            state: Correct
                q: (правильно/молодец/умница/умничка/да) || fromState = .., onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["Correct"]) }}
                go!: ../../Next

            #state: Incorrect
                #q: (врешь/неправильно/не правильно/нет) || fromState = .., onlyThisState = true
                #a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["Incorrect"]) }}
                #go!: ../../Next

            state: Opposite
                q: * $Opposite * || fromState = .., onlyThisState = true
                q: * $Opposite * || fromState = "/Games/How to play Opposites", onlyThisState = true
                q: * $Opposite * || fromState = /Opposites/Fasolik, onlyThisState = true
                script:
                    $session.oppositesNum += 1;
                    if ($session.barrier) {
                        $session.barrier = undefined;
                    }
                    prepareOpposite();
                    $session.opposites.used.push($parseTree._Opposite.word);
                    $session.opposites.turn += 1;
                    $session.opposites.correct += 1;
                    $temp.opposite = findOpposite($parseTree._Opposite.word);
                if: $temp.opposite != ""
                    a: {{capitalize($temp.opposite)}}!
                    if: $session.opposites.turn > 2
                        script:
                            $session.opposites.turn = 0;
                            $session.fasolikTurn = true;
                        go!: ../../../Next question enquire
                    else:
                        go!: ../../Next
                else:
                    script:
                        $session.opposites.last = $parseTree._Opposite.word;
                        $session.oppositesNum -=1;
                    go!: ../Unknown

            state: NoWish
                q: * (не хочу|не надо|нет|не буду|ты называй|называй|твоя очередь|(скажи|называй|говори|давай) слово) * || fromState = "/Opposites/User/Play", onlyThisState = true
                if: $session.noWishAgain
                    a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["NoWish"]["a1"]) }}
                    go!: /Opposites/Fasolik/Play
                else:
                    script:
                        $session.noWishAgain = true;
                    a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["NoWish"]["a2"]) }}

            state: Unknown 
                q: {[я (говорю/скажу)]  $adjective} || fromState = .., onlyThisState = true
                q: {[я (говорю/скажу)]  $adjective} || fromState = "/Games/How to play Opposites", onlyThisState = true
                q: {[я (говорю/скажу)]  $adjective} || fromState = /Opposites/Fasolik, onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["Unknown"]["a1"]) }}

                        
            state: Undefined
                q: * $AnyWord * || fromState = .., onlyThisState = true
                script:
                    if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($request.query, "/");
                        $reactions.transition(res.targetState);
                    }
                a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["Undefined"]["if"]) }}
                go: /Opposites/User/Play                

            state: DontKnow
                q: * ($dontKnow|не могу|какое) * || fromState = .., onlyThisState = true
                q: * (подскажи/помоги) * || fromState = .., onlyThisState = true
                if: $session.repeatDontKnow
                    script:
                        getOpposite();
                    a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["DontKnow"]["a2"]) }}
                    go: /Opposites/Fasolik/Play
                else:
                    script: 
                        $session.repeatDontKnow = true;
                    a: {{ selectRandomArg($OppositesAnswers["User"]["Play"]["DontKnow"]["a1"]) }}
                    go!: ../../Play

            state: Obscene
                q: * ($stopWord|$obsceneWord) * || fromState = .., onlyThisState = true
                a: {{ selectRandomArg($OppositesAnswers["Fasolik"]["Play"]["Obscene"]["a1"]) }}
                go!: ..

        state: Next
            q: * ($agree/$continue/игра*) * || fromState = ../Next, onlyThisState = true
            script:
            a: {{ selectRandomArg($OppositesAnswers["User"]["Next"]["else"]["a1"]) }}
            a: {{ selectRandomArg($OppositesAnswers["User"]["Next"]["else"]["a2"]) }}
            go: ../Play

    state: Stop
        q: * $stopGame *
        q: * ($stopGame|не надо|нет|не буду|не хочу) * || fromState = ../Fasolik/Play
        q: * $stopGame * || fromState = ../User/Play
        q: * ($stopGame|не надо|нет|не буду) * || fromState = /Opposites
        q: * ($disagree|$notNow|$stopGame|$dontKnow) * || fromState = ../Fasolik/Next
        q: * ($disagree|$notNow|$stopGame|$dontKnow) * || fromState = ../User/Next
        q: * {[давай] * [сделаем] (перерыв/прервемся)} * || fromState = ../Fasolik/Play
        q: * {[давай] * [сделаем] (перерыв/прервемся)} * || fromState = ../User/Play
        q: * {[давай] * [сделаем] (перерыв/прервемся)} *                
        q: [я] устал*
        q: * ($stopGame|не надо|нет|не буду|не хочу) * || fromState = "../Next question enquire"
        script:
            $session.opposites = undefined;
            $session.pair = $nlp.conform("пара", $session.oppositesNum);
        if: $session.oppositesNum == 0
            a: {{ selectRandomArg($OppositesAnswers["Stop"]["a1"]) }} 
        else:
            if: $session.oppositesNum == 1
                a: {{ selectRandomArg($OppositesAnswers["Stop"]["a2"]) }} 
            if: $session.oppositesNum > 1
                if: ($session.oppositesNum < 5)
                    a: {{ selectRandomArg($OppositesAnswers["Stop"]["a3"]) }} 
                else:  
                    a: {{ selectRandomArg($OppositesAnswers["Stop"]["a4"]) }}
        go: /BeanqGames