require: translatorRuEn.csv
    name = languageTable
    var = $languageTable

require: translatorRuEn.js

require: newSessionOnStart/newSession.sc
  module = zb_common

init:
    $global.themes = $global.themes || [];
    $global.themes.push("translatorRuEn");

    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .languageTableTagConverter = function(parseTree) {
            var id = parseTree.languageTable[0].value;
            return $languageTable[id].value;
        };

patterns:
    $languages = $entity<languageTable> || converter = $converters.languageTableTagConverter
    $WhoWhatIs = ((кто/что) [~такой] $Text)

theme: /TranslatorRuEn
    state: TranslatorRuEn || modal = true
        state: General
            q!: * [давай/ [я] хочу ] * (переведи*/перевод*/попереводи*/переводим/попереводим) * [слова] *
            q!: * [ты/вы] (умеешь/можешь/сможешь) (переводить/перевести) [мне] [пару слов] *
            q!: * [ты/вы] (можешь/сможешь) помочь [мне] [с] (переводом/переводами/перевести) *
            q!: (перевод/переводить/попереводи/переводчик)
            q!: * (хочу [узнать]/скажи/можешь сказать) [мне] перевод ~слово

            a: Я умею переводить с русского языка на английский! 
            go!: /TranslatorRuEn/TranslatorRuEn/GeneralTranslateCase

        state: GeneralTranslateCase 
            q!: * (переведи/перевод) [мне]  на $languages
            q!: * переведи (для меня/мне) [[пару] (~слово/~фраза)] *
            random:
                a: Скажи слово, которое хочешь перевести.
                a: Какое слово ты хочешь перевести?
                
            state: TranslateCase
                q!: * (переведи*/перевод) * [мне/сейчас/для меня/ для] * [пара/пары/пару] [текст*/~слово/~фраза] * [на $languages::languageTo] [язык*] [текст*/~слово/~фраза] $Text
                q!: * (переведи*/перевод) * [мне/сейчас/для меня/ для] * [пара/пары/пару] [текст*/~слово/~фраза] $Text [на $languages::languageTo] [язык*] *
                q!: * (переведи*/перевод) * с $languages::languageFrom [язык*] на $languages::languageTo [язык*] [слово/фраза] $Text
                q!: * (переведи*/перевод) * [слово/фраза] [$Text]  с $languages::languageFrom [язык*] на $languages::languageTo [язык*] *
                q!: * как [по/на] $languages::languageTo [язык*] (будет/есть/сказать) [$Text]
                q!: * как (сказать/будет/переводится/перевести) [по/на] $languages::languageTo [язык*] [~слово/~фраза] $Text
                q!: * как (сказать/будет/переводится/перевести) [(по/на/(~слово/~фраза) [$Text])] [по/на] $languages::languageTo [язык*] *
                q!: * какой перевод у [текст*/~слово/~фраза/~фраза] [$Text] 
                q!: * [$Text] как (будет/есть/сказать) [по/на] $languages::languageTo [язык*] *
                q!: * переведи [~слово/~фраза] [$Text] 
                
                q: * (переведи*/перевод) * [мне/сейчас/для меня/ для] * [пара/пары/пару] [текст*/~слово/~фраза] * [на $languages::languageTo] [язык*] [текст*/~слово/~фраза] $Text
                q: * (переведи*/перевод) * [мне/сейчас/для меня/ для] * [пара/пары/пару] [текст*/~слово/~фраза] $Text [на $languages::languageTo] [язык*] *
                q: * (переведи*/перевод) * с $languages::languageFrom [язык*] на $languages::languageTo [язык*] [слово/фраза] $Text
                q: * (переведи*/перевод) * [слово/фраза] [$Text]  с $languages::languageFrom [язык*] на $languages::languageTo [язык*] *
                q: * как [по/на] $languages::languageTo [язык*] (будет/есть/сказать) [$Text]
                q: * как (сказать/будет/переводится/перевести) [по/на] $languages::languageTo [язык*] [~слово/~фраза] $Text
                q: * как (сказать/будет/переводится/перевести) [(по/на/(~слово/~фраза) [$Text])] [по/на] $languages::languageTo [язык*] *
                q: * какой перевод у [текст*/~слово/~фраза] [$Text] 
                q: * [$Text] как (будет/есть/сказать) [по/на] $languages::languageTo [язык*] *
                q: * [переведи] [~слово/~фраза] [$Text] 
                q: * [переведи] [для меня/мне] [[пару]~слово/~фраза]
                q: ($Text|$WhoWhatIs)

                if: $parseTree.languageFrom
                    script:
                        $session.languageFrom = $parseTree.languageFrom[0].languageTable[0].value;
                else:
                    script:
                        $session.languageFrom = 8;
                if: $parseTree.languageTo 
                    script:
                        $session.languageTo = $parseTree.languageTo[0].languageTable[0].value;
                else:
                    script:
                        $session.languageTo =  4;    
                go!: /TranslatorRuEn/TranslatorRuEn/Translator
       
            state: WantMore?
                random:
                    a: Хочешь перевести ещё?
                    a: Будем переводить дальше?
                    a: Ещё перевод слова?

                state: More
                    q: $yes || onlyThisState=true
                    q: ([$yes] будем/хочу/[$yes] еще/естественно/ конечно) *
                    q: (почему бы и нет/можно/[ну] давай) *
                    q: [а] ты можешь *
                    go!: /TranslatorRuEn/TranslatorRuEn/GeneralTranslateCase

                state: Enough
                    q:  ($no/не) [спасибо] [не хочу] [не будем]*
                    q: [все] [больше] (хватит [на сегодня] */не хочу [больше] [перевод*]/я устал*/не будем */стоп */остановись/хорош) || onlyThisState=true
                    q: все [не надо] *
                    random:
                        a: Хорошо!
                        a: Все мы устаём.
                    go!: /StartDialog

            state: RestratSession
                q: * *start
                go!: /Start

        state: Translator
            if: translatorModule.isAvaliableTranslation()
                if: $parseTree.WhoWhatIs || $parseTree.Text || ($parseTree.text).toLowerCase() === 'слово'
                    script:
                        $session.text = $parseTree.Text ? $parseTree.Text[0].text : 'слово';
                        $session.text = $parseTree.WhoWhatIs ? $parseTree.WhoWhatIs[0].text : $session.text; 
                
                        translatorModule.callYandexTranslatorApi();
        
                        $session.text = undefined;
                        $session.languageTo = undefined;
                        $session.languageTo = undefined;
                else:
                    go!: /TranslatorRuEn/TranslatorRuEn/GeneralTranslateCase
                go!: /TranslatorRuEn/TranslatorRuEn/GeneralTranslateCase/WantMore?
            else:
                a: Я не умею переводить с {{$session.langFromGenTitle}} на {{$session.langToGenTitle}}.
                go!:/

        state: Cancel
            q: (хватит [на сегодня]/не хочу [больше] [перевод*]/я устал*)
            a: Хорошо! 
            go!: /StartDialog

        state: StopWords
            q: [$superCatchAll] $stopWord [$superCatchAll]
            a: Я обнаружила неразрешённое моей программой слово. Не могу отвечать. Не могу отвечать. 
            go!: /TranslatorRuEn/TranslatorRuEn/GeneralTranslateCase