require: fairy-tales-ru.csv
    name = FairyTales
    var = $FairyTales
require: smeshariki-ru.csv
    name = SmesharikiTales
    var = $SmesharikiTales
require: masha-i-medved-ru.csv
    name = MashaBearTales
    var = $MashaBearTales

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .FairyTalesTagConverter = function(parseTree) {
            var id = parseTree.FairyTales[0].value;
            return $FairyTales[id].value;
        };
    $global.$converters
        .SmesharikiTalesTagConverter = function(parseTree) {
            var id = parseTree.SmesharikiTales[0].value;
            return $SmesharikiTales[id].value;
        };
    $global.$converters
        .MashaBearTalesTagConverter = function(parseTree) {
            var id = parseTree.MashaBearTales[0].value;
            return $MashaBearTales[id].value;
        }; 
        
patterns:
    $kidsFairyTale = $entity<FairyTales> || converter = $converters.FairyTalesTagConverter
    $smesharikiTale = $entity<SmesharikiTales> || converter = $converters.SmesharikiTalesTagConverter

    $ftale = (~сказка/~сказочка/~история/кашку)

    $favouriteTale = {[~моя] ~любимая $ftale}

    $tellFtale = ((расскажи*|скажи*|рассказывай*/расскажешь/можешь рассказать/(можно/хочу/хочет/хочется/хотел* [бы]) * [услышать/послушать/слушать]|знаешь|есть у тебя|поставь|давай|почитай|почитаем|включи|порадуй|прочитай|почитай|прочти|слушать) [мне])

    $continueTale = (еще/ещё/продолжай/продолжи/продолжать/~следующий [часть])
    $moreTales = (~другой/другую/~другое/$continueTale)

    $smeshariki = (смешарики/смешариков);

    $masha = (маша/от маши/машина/машины/машину);

    $otherTale = (~другой/другую/~другое/не эту/следующую)