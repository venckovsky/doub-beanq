function getRandomSong(){
    var randomId = selectRandomArg.apply(this, Object.keys($KidsSongs));
    return $KidsSongs[randomId].value;
}


function getSongs(){
    var $session = $jsapi.context().session;
    $session.listOfSongs = [getRandomSong(), getRandomSong(), getRandomSong()];
    return "'" + $session.listOfSongs[0].title + "', '" + $session.listOfSongs[1].title + "' и '" + $session.listOfSongs[2].title + "'";
}

function processSongsDict(){
    var $client = $jsapi.context().client;
    if (!$client.smesharikiSongs && !$client.mashaSongs){
        $client.smesharikiSongs = [];
        $client.mashaSongs = [];
        for (var i=0;i<Object.keys($KidsSongs).length;i++){
            if ($KidsSongs[i].value.type == "smeshariki") {
                $client.smesharikiSongs.push($KidsSongs[i].value);
            } else if ($KidsSongs[i].value.type == "mashabear") {
                $client.mashaSongs.push($KidsSongs[i].value);
            }
        }
    }    
}