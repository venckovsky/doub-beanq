theme: /NoMatch

    state: TestNoMatchSetUserFirstName
        q!: TestNoMatchSetUserFirstName
        script:
            $client.firstName = "Pudding";
            $reactions.answer("Test data ready: username set.");

    state: CatchAll || noContext = true
        q!: $catchAll
        if: ($parseTree._catchAll == $session.lastCatchAll)
            go!: ./CatchAllRepetition
        else:
            script:
                $session.lastCatchAll = $parseTree.catchAll[0].text;
            if: checkNonsense($parseTree.text)
                if: ($client.firstName)
                    script:
                        $reactions.answer("{Sorry/Pardon me/I beg your pardon}, {{$client.firstName}}, I could not hear you.",
                            "I do not really {get/understand} you",
                            $client.firstName + ", I did not catch you.",
                            "{Sorry/Pardon me/I beg your pardon}, I don't fully understand you.",
                            "{It seems/I guess}, my ears are a little rusty, {say it again/repeat what you said}!",
                            "There was some static when you were saying. I did not get you, " + $client.firstName,
                            $client.firstName + ", I have not heard you.",
                            "{Sorry/Pardon me/I beg your pardon}, I do not think I got you.",
                            "I did not quite catch {you/your words}" + $client.firstName,
                            "{What?/What-what?} I do not think I've heard you correctly, " + $client.firstName,
                            "{Sorry/Pardon me}, I {couldn't make out/did not {catch you/get you}.",
                            "I did not {get/catch} you, {repeat what you said/say it again, please}!",
                            "I do not get you at all, sorry.",
                            "Something is wrong with my ears. I did not {get/catch} you.",
                            "My ears must be messed up. I can not {understand you/get you}.",
                            "These mikes! I did not {get/catch} you, " + $client.firstName,
                            "Something is wrong with my mikes. I do not {get/understand} what you are saying.",
                            "I'm {trying really hard/doing my best/trying hard}, but still do not {get/understand} some words in English. {Can/Could} you repeat it once again?",
                            "Humans speak so fast! {Sorry/Pardon me/I beg your pardon}, I can not keep up!");
                else:
                    script:
                        $reactions.answer("{Sorry/Pardon me/I beg your pardon}, I did not {get/catch/understand} you.",
                            "I have not got you, my dear friend!",
                            "I {did/do} not {/quite/fully} {get/catch/understand} you.",
                            "I did not {quite/fully} {get/catch/understand} you. {Repeat/Say} it {/once} again!",
                            "{Sorry/Pardon me/I beg your pardon}, I {do/did} not {/fully/quite} {get/catch/understand} you.",
                            "{Sorry/Pardon me/I beg your pardon}, {it seems/I guess} there was some static here. I did not {get/understand} {you/what you are saying}.",
                            "I {did/do} not {quite/fully} {get/catch/understand} that.",
                            "I did not understand {that/what you just said}.",
                            "{What?/What-what?} I did not catch that.",
                            "{Sorry/Pardon me/I beg your pardon}, I did not {understand/get}.",
                            "Do not understand, {say it once again/repeat, please}!",
                            "Understood nothing, sorry.",
                            "There's something wrong with my ears. I did not {understand/get} you.",
                            "My ears must be messed up again. Understood nothing.",
                            "These mikes! I did not {get/catch} you",
                            "Something is wrong with my mikes. I do not {get/understand} what you are saying.",
                            "I am {trying hard/doing my best/trying really hard}, but still do not {get/understand} some words in English. {Can/Could} you repeat it once again?",
                            "Humans speak so fast! {Sorry/Pardon me/I beg your pardon}, I can not keep up!");
            else:
                if: ($client.firstName)
                    script:
                        $reactions.answer("You mean?",      
                            "What do you mean, " + $client.firstName + "?",
                            "In what sense, " + $client.firstName + "?",
                            "That is to say?",
                            "Go on, " + $client.firstName + "!",
                            "Please continue!",
                            "What do you want to say, " + $client.firstName + "?",
                            "What can that mean?",              
                            "Clear.",
                            "Sounds intriguing...",                         
                            "I see.",
                            "I don't know what to add to that, " + $client.firstName + ".",
                            "What can I say...",           
                            "No comment.",
                            "I don't want to talk about it.",
                            "That's something to think about.",
                            "I'll think about it, " + $client.firstName + ".",
                            "I like our conversations more and more.",
                            "That sounds so nice, " + $client.firstName + "!",
                            "There's nothing else to mention.",
                            "Maybe, " + $client.firstName + "...",
                            "I don't think I understand.",
                            $client.firstName + ", i didn't quite get it.",           
                            "Sorry. Me no understand.",
                            "That wasn't really clear to me.",
                            "People of Earth speak so fast! I'm sorry, I can't quite follow you.",
                            "I'm trying really hard, but English doesn't come easy to me.",            
                            "Sorry, " + $client.firstName + ", i was thinking about something else. What were we talking about?",
                            "Sorry, I could not understand that.",
                            "Some questions are still difficult for me. But I'm trying.");
                
                else:
                    script:
                        $reactions.answer("You mean?",      
                            "What do you mean?",
                            "In what sense?",
                            "That is to say?",
                            "Go on!",
                            "Please continue!",
                            "What do you want to say?",
                            "What can that mean?",              
                            "Clear.",
                            "Sounds intriguing...",                         
                            "I see.",
                            "I don't know what to add to that.",
                            "What can I say...",           
                            "No comment.",
                            "I don't want to talk about it.",
                            "That's something to think about.",
                            "I'll think about it.",
                            "I like our conversations more and more.",
                            "That sounds so nice.",
                            "I have nothing else to say.",
                            "Maybe.",
                            "I don't think I understand.",
                            "I didn't quite get it.",           
                            "Sorry. Me no understand.",
                            "That wasn't really clear to me.",
                            "People of Earth speak so fast! I'm sorry, I can't quite follow you.",
                            "I'm trying really hard, but English doesn't come easy to me.",            
                            "Sorry, I was thinking about something else. What were we talking about?",
                            "Sorry, I could not understand that.",
                            "Some questions are still difficult for me. But I'm trying.");
            if: (typeof $client.lastAnswer == 'undefined' || ($client.lastAnswer.length > 0 && $client.lastAnswer.substr(-1) != '?'))
                go!: /StartDialog
        

        state: CatchAllRepetition || noContext = true
            script:
                $temp.answer = selectRandomArg('Again!',
                'Oh no not again!',
                'I think I have heard that before!',
                'I know this one!',
                'I can hear you quite well!',
                'Again and again!',
                'I have got no trouble hearing you!',
                'I have heard that before!',
                'You are repeating yourself!',
                'why again!') +
                ' ' + selectRandomArg('say it differently.',
                'Try saying it in a different way.',
                'Maybe you could use different words to say that.',
                'Say something else!',
                'It does not work. Say something new!',
                '', '', '', '', '');
            if: (typeof $client.lastAnswer == 'undefined' || ($client.lastAnswer.length > 0 && $client.lastAnswer.substr(-1) != '?'))
                go!: /StartDialog


    state: ASR_Fragments || modal = true
        state: Question
            q: * $question [you]
            q: * [a|the]