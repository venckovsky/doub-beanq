// инициализация необходимых переменных
function oddWordGameInit(){
    var $session = $jsapi.context().session;
    $session.oddWordGame = {
        rightAnswersCounter: 0,//количество правильных ответов
        answersCounter: 0,//количество всех ответов
        again: false, //указывает, повторная ли эта игра
        currentList: [],
        currentListText: "",
        rightAnswer: "",
        rightAnswerText: "",
        explanation: "",
        usedQuestions: []
    }
}

var OddWordGame = (function() {

    function chooseQuestion() {
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;

        var id = randomInteger(0, (Object.keys($oddWordGameQuestions).length - 1));
        while ($session.oddWordGame.usedQuestions.indexOf(id) > -1) {
            id = randomInteger(0, (Object.keys($oddWordGameQuestions).length - 1));
        }
        $session.oddWordGame.usedQuestions.push(id);
        if ($temp.start){
            $reactions.answer(selectRandomArg($OddWordGameAnswers['getWordChain']['if']));
        } else {
            $reactions.answer(selectRandomArg($OddWordGameAnswers['getWordChain']['else']));
        }
        $session.oddWordGame.currentList = $oddWordGameQuestions[id].value.question.split(', ');
        $session.oddWordGame.currentListText = $oddWordGameQuestions[id].value.question.replaceAll(',', ' –') + '.';
        $session.oddWordGame.rightAnswer = $oddWordGameQuestions[id].value.answer;
        $session.oddWordGame.rightAnswers = $oddWordGameQuestions[id].value.answerVariants;
        $session.oddWordGame.explanation = $oddWordGameQuestions[id].value.expl;
    }

    function processAnswerBeanq() {
        var $parseTree = $jsapi.context().parseTree;
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var order = $parseTree.oddWordGameOrder;
        var curList = $session.oddWordGame.currentList;
        var twoElmByOrder = order && order[1];
        if (!order) {
            var word = $parseTree.AnyWord[0].text.toLowerCase();
        }

        if (order) {
            if (order[0].value != 1 && order[0].value != 2 && order[0].value != 3 && order[0].value != 4) {
                $reactions.answer(selectRandomArg($OddWordGameAnswers['Odd Word game get answer']['answerOutOfRange']));
                $reactions.transition('../Odd Word game repeat list');
                return;
            } else if (order[0].value === 'lst') {
                $temp.userAnswer = curList[curList.length - 1];
            } else if (order[0].value === 'blst') {
                $temp.userAnswer = curList[curList.length - 2];
            } else {
                $temp.userAnswer = curList[order[0].value - 1];
            }
        }

        var i = 0;
        if ($temp.userAnswer) {
            for (i = 0; i < $session.oddWordGame.rightAnswers.length; i++) {
                if ($temp.userAnswer === $session.oddWordGame.rightAnswers[i]) {
                    $reactions.transition('../../RightAnswer');
                    break;
                }
            }
            if (i == $session.oddWordGame.rightAnswers.length) {
                if (curList.indexOf($temp.userAnswer) > -1 || $temp.beenHere) {
                    $reactions.transition('../../WrongAnswer');
                } else {
                    $reactions.answer(selectRandomArg($OddWordGameAnswers['Odd Word game get answer']['else']));
                    $temp.beenHere = true;
                    $temp.askNow = true;
                }
            }
        } else {
            for (i = 0; i < $session.oddWordGame.rightAnswers.length; i++) {
                if (word.equals($session.oddWordGame.rightAnswers[i])) {
                    $reactions.transition('../../RightAnswer');
                    break;
                } 
            }
            if (i == $session.oddWordGame.rightAnswers.length) {
                $reactions.transition('../../WrongAnswer');
            }
        }

    }

    return {
        chooseQuestion: chooseQuestion,
        processAnswerBeanq: processAnswerBeanq
    };
})();