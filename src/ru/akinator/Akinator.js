var Akinator = (function () {

    function init() {
        var $session = $jsapi.context().session;

        $session.akinator = {};
        $session.akinator.catchAll = 0;
        $session.akinator.characterPicked = false;
        $session.akinator.partnerID = '154';
        $session.akinator.apiKey = '187fd39dfaf3340aa327f88cc4b8c6bd';
        $session.akinator.base_number = 0;
        $session.akinator.isFirstQuestion = true;
        $session.akinator.challenge_auth = '';
        $session.akinator.signature = '';
        $session.akinator.session_number = '';
        $session.akinator.response_challenge = '';
        $session.akinator.progression = 0;
        $session.akinator.characterName = "";
        $session.akinator.characterID = "";
        $session.akinator.urlApi = "";
        $session.akinator.currentQuestion = "";
        $session.akinator.lastQuestions = [];
        $session.akinator.ageConstraint = 1;
        $session.akinator.isGuess = false;
        $session.akinator.step = 0;
        startSession();
        getFirstQuestion();
    }


    function startSession() {
        var $session = $jsapi.context().session;

        for(var i = 0; i < 3; i++) {
            var url = $http.get("http://global${num}.akinator.com/ws/instances_v2.php?media_id=${mediaID}&footprint=${footprint}&lang=ru&code_pays=RU", {
                timeout: 7000,
                query:{
                    num : i,
                    mediaID : $session.akinator.partnerID,
                    footprint: $session.akinator.apiKey
                }
            }).then(parseHttpResponse).catch(httpError);
            if (url["data"] && url["data"]["RESULT"] && url["data"]["RESULT"]["COMPLETION"] && url["data"]["RESULT"]["COMPLETION"] == "OK")  {
                $session.akinator.urlApi  = url["data"]["RESULT"]["PARAMETERS"]["INSTANCE"]["URL_BASE_WS"];
                break;
            } 
        }
    }

    function getFirstQuestion() {
        var $session = $jsapi.context().session;

        var firstQuestion = '';
        var data = $http.get($session.akinator.urlApi + "/new_session.php?${base}&partner=${partnerID}&question_filter=${question_filter}&soft_constraint=${soft_constraint}", {
                timeout: 7000,
                query:{
                    base: $session.akinator.base_number,
                    partnerID: $session.akinator.partnerID,
                    question_filter : "",
                    soft_constraint : "etat='EN'" 
                }
            }).then(parseHttpResponse).catch(httpError);
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] == "OK") {
            if (data["data"]["RESULT"]["PARAMETERS"]) {
                var parameters = data["data"]["RESULT"]["PARAMETERS"];
                $session.akinator.currentQuestion = replacePronoun(he.decode(parameters["STEP_INFORMATION"]["QUESTION"]))+'?';
                $session.akinator.step = parameters["STEP_INFORMATION"]["STEP"];
                $session.akinator.progression = parameters["STEP_INFORMATION"]["PROGRESSION"];
                $session.akinator.challenge_auth = parameters["IDENTIFICATION"]["CHALLENGE_AUTH"];
                $session.akinator.signature = parameters["IDENTIFICATION"]["SIGNATURE"];
                $session.akinator.session_number = parameters["IDENTIFICATION"]["SESSION"];
                $session.akinator.response_challenge = base64.encode(SHA256($session.akinator.apiKey + $session.akinator.challenge_auth));
            } else {
                $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
                $reactions.transition('/');
            }
        } else {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
    }

    function answerAndQuestion() {
        var $session = $jsapi.context().session;
        var $parseTree = $jsapi.context().parseTree;

        $session.akinator.lastQuestions.push($session.akinator.currentQuestion);

        if ($session.akinator.isFirstQuestion) {
            $session.akinator.isFirstQuestion = false;
            var data = $http.get($session.akinator.urlApi + "/answer.php?base=${base}&partner=${partnerID}&session=${session_number}&signature=${signature}&response_challenge=${response_challenge}&step=${step}&answer=${answer}&question_filter=${question_filter}", {
                timeout: 7000,
                query:{
                    base: $session.akinator.base_number,
                    partnerID: $session.akinator.partnerID,
                    session_number : $session.akinator.session_number,
                    signature : $session.akinator.signature,
                    response_challenge : $session.akinator.response_challenge,
                    step : $session.akinator.step,
                    answer : $parseTree._Root,
                    question_filter : ""
                }
            }).then(parseHttpResponse).catch(httpError);
        } else {
            var data = $http.get($session.akinator.urlApi + "/answer.php?base=${base}&partner=${partnerID}&session=${session_number}&signature=${signature}&step=${step}&answer=${answer}&question_filter=${question_filter}", {
                timeout: 7000,
                query: {
                    base: $session.akinator.base_number,
                    partnerID: $session.akinator.partnerID,
                    session_number : $session.akinator.session_number,
                    signature : $session.akinator.signature,
                    step : $session.akinator.step,
                    answer : $parseTree._Root,
                    question_filter : ""
                }
            }).then(parseHttpResponse).catch(httpError);
        }
        log("data2 " + toPrettyString(data));
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] == "OK") {
            if (data["data"]["RESULT"]["PARAMETERS"]) {
                var parameters = data["data"]["RESULT"]["PARAMETERS"];
                $session.akinator.step = parameters["STEP"];
                $session.akinator.progression = parameters["PROGRESSION"];
                $session.akinator.currentQuestion = replacePronoun(he.decode(parameters["QUESTION"]))+'?';
            } else {
                $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
                $reactions.transition('/');
            }
        } else {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
        if ($session.akinator.progression > 90) {
            $session.akinator.isGuess = true;
            makeGuess();
        }
    }

    function cancelAnswer() {
        var $session = $jsapi.context().session;

        $session.akinator.currentQuestion = $session.akinator.lastQuestions.pop();


        var data = $http.get($session.akinator.urlApi + "/cancel_answer.php?channel=0&session=${session_number}&signature=${signature}&step=${step}&question_filter=${question_filter}", {
            timeout: 7000,
            query: {
                session_number : $session.akinator.session_number,
                signature : $session.akinator.signature,
                step : $session.akinator.step,
                question_filter : ""
            }
        }).then(parseHttpResponse).catch(httpError);
        log("data3 " + toPrettyString(data));
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] == "OK") {
            if (data["data"]["RESULT"]["PARAMETERS"]) {
                var parameters = data["data"]["RESULT"]["PARAMETERS"];
                $session.akinator.step = parameters["STEP"];
                $session.akinator.progression = parameters["PROGRESSION"];
                if (isNotForKids($session.akinator.currentQuestion)) {
                    $reactions.transition('/Akinator/Game/AskQuestion/CancelAnswer');
                }
            } else {
                $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
                $reactions.transition('/');
            }
        } else {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
    }

    function makeGuess() {
        var $session = $jsapi.context().session;

        var data = $http.get($session.akinator.urlApi + "/list.php?channel=0&base=${base}&session=${session_number}&signature=${signature}&step=${step}&size=1", {
            timeout: 7000,
            query: {
                base: $session.akinator.base_number,
                session_number : $session.akinator.session_number,
                signature : $session.akinator.signature,
                step : $session.akinator.step
            }
        }).then(parseHttpResponse).catch(httpError);
        log("data3 " + toPrettyString(data));
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] == "OK") {
            if (data["data"]["RESULT"]["PARAMETERS"]) {
                var parameters = data["data"]["RESULT"]["PARAMETERS"];
                $session.akinator.characterName = replacePronoun(he.decode(parameters["ELEMENTS"]["ELEMENT"]["NAME"]));
                $session.akinator.characterID = parameters["ELEMENTS"]["ELEMENT"]["ID"];
                $session.akinator.ageConstraint = parameters["ELEMENTS"]["ELEMENT"]["VALIDE_CONTRAINTE"];
            } else {
                $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
                $reactions.transition('/');
            }
        }
    }

    function chooseCharacter() {
        var $session = $jsapi.context().session;

        var data = $http.get($session.akinator.urlApi + "/choice.php?channel=0&base=${base}&session=${session_number}&signature=${signature}&element=${element}", {
            timeout: 7000,
            query: {
                base: $session.akinator.base_number,
                session_number : $session.akinator.session_number,
                signature : $session.akinator.signature,
                element : $session.akinator.characterID
            }
        }).then(parseHttpResponse).catch(httpError);
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] != "OK") {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
    }

    function excludeCharacter() {
        var $session = $jsapi.context().session;

        var data = $http.get($session.akinator.urlApi + "/exclusion.php?channel=0&base=${base}&session=${session_number}&signature=${signature}&step=${step}", {
            timeout: 7000,
            query: {
                base: $session.akinator.base_number,
                session_number : $session.akinator.session_number,
                signature : $session.akinator.signature,
                step : $session.akinator.step
            }
        }).then(parseHttpResponse).catch(httpError);
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] != "OK") {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
    }

    function cancelSession() {
        var $session = $jsapi.context().session;

        var data = $http.get($session.akinator.urlApi + "/cancel_game.php?channel=0&base=${base}&session=${session_number}&signature=${signature}", {
            timeout: 7000,
            query: {
                base: $session.akinator.base_number,
                session_number : $session.akinator.session_number,
                signature : $session.akinator.signature
            }
        }).then(parseHttpResponse).catch(httpError);
        if (data["data"] && data["data"]["RESULT"] && data["data"]["RESULT"]["COMPLETION"] && data["data"]["RESULT"]["COMPLETION"] != "OK") {
            $reactions.answer("Кажется, что-то пошло не так и магия Джинна сейчас недоступна. Но я обязательно попытаюсь отгадать твоего персонажа чуть позже. Во что будем играть сейчас?");
            $reactions.transition('/');
        }
    }

    function secretAnswer() {
        var $parseTree = $jsapi.context().parseTree;

        $parseTree._Root = '2';
        $reactions.transition("/Akinator/Game/AskQuestion/Answer");

    }

    function isNotForKids(str) {
        var badWords = new RegExp("(дерьм.+|.+говно.+|гавн.+|говен.+|гавен.+|обосса.+|онанист.+|онанизм.+|мастурб.+|куни.+|дроч.+|подроч.+|отсос.+|анал.+|манда|член.+|клитор.+|вагин.+|пенис.+|прокладк.+|тампон.+|секс.+|интим.+|проститут.+|шлюх.+)");
        
        return str.search(badWords) !== -1
    }

    function replacePronoun(str) {
        var res = lowerFirstLetter(str);
        res =  res.replace('ваш ','твой ').replace('вашего ','твоего ').replace('вашим ','твоим ').replace('вашему ','твоему ').replace('вашем ','твоем ')
        res =  res.replace('ваша ','твоя ').replace('вашей ','твоей ').replace('вашу ','твою ').replace('?','');

     
        return myCapitalize(res.trim());
    }

    function myCapitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    function lowerFirstLetter(str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    }

    return {
        init:init,
        answerAndQuestion : answerAndQuestion,
        cancelAnswer : cancelAnswer,
        cancelSession : cancelSession,
        chooseCharacter : chooseCharacter,
        excludeCharacter : excludeCharacter,
        secretAnswer : secretAnswer,
        isNotForKids : isNotForKids
    };

})();
