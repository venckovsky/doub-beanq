patterns:

    $poem = (poem*|verse|rhyme*|lyrics)

theme: /Learning poems
    
    state: Poem information
        q!: * {[$you|we] * learn} * [something] [new]  * 
        script:
            $reactions.answer("I can learn poems by heart. Just say to me: let's learn a poem. ");

    state: No Poem learn
        q!: * [we|I] * (do|will) (n't|not) [want|need] [to] learn* * $poem *
        q!: * [I] * (have no|do (n't|not) have) * (interest|desire) [to] learn* $poem * 
        q: * $no * || fromState = "/Learning poems/Poem information"
        q: * [we|I] * (do|will) (n't|not) [want|need] [to] learn* * $poem * || fromState = "/Learning poems/Poem learn/Poem learning", onlyThisState = true
        script:
            $reactions.answer("Well, and I like to learn poems.", "By the way, to learn poems by heart is good for memory.");
        go!: /StartDialog

    state: Poem learn || modal = true
        q!: * {[let 's'] (learn|repeat after me) [$you] * $poem} *
        q!: [$robotName] * {listen * $poem}
        q: * let's start * || fromState = "../Poem information"
        q: * $agree * || fromState = "../Poem information"
        q: * once again * || fromState = "./Poem learning"
        script:
            $reactions.answer("I'm ready to learn a poem. Recite it to me");

        state: Do not want to learn poem
            q: * [I] * (have no|do (n't|not) have) * (interest|desire) [to] learn* $poem * || fromState = .., onlyThisState = true
            q: * [I] * do (n't|not) * want [to] learn* * $poem * || fromState = .., onlyThisState = true
            go!: /Learning poems/No Poem learn

        state: Poem learning
            q: $Text [repeat] || fromState = .., onlyThisState = true
            q: * I ['m|am] repeat* $Text [repeat] || fromState = "../Poem learning", onlyThisState = true
            script:
                $client.Poem = $parseTree._Text;
                $reactions.answer("Now I'll repeat the poem I heard. " + capitalize($client.Poem) + ". Is everything OK?");

            state: Poem learning yes
                q: * ($agree|good for you|well done|goof job) * [$good] * || fromState = .., onlyThisState = true
                q: it is [ok] || fromState = .., onlyThisState = true
                script: 
                    $reactions.answer("Great! I've remembered this poem. Just say - tell the poem - and I'll recite it again.");
                
            state: Poem learning no
                q: * ($disagree|almost|close [enough]) * || fromState = .., onlyThisState = true
                script: 
                    $reactions.answer("I was not able to make out a few words. We can try to learn this poem once again, if you want.");
                
                state: Poem learning again
                    q: * ($agree|[once] again|let ('s|us) try) * || fromState = .., onlyThisState = true
                    go!: /Learning poems/Poem learn

                state: Poem learning not again
                    q: * [$disagree] * (stop|enough|we ('re|are) done) * || fromState = .., onlyThisState = true
                    q: * $disagree * [stop|enough|we ('re|are) done] * || fromState = .., onlyThisState = true
                    script: 
                        $client.Poem = undefined;
                        $reactions.answer("I guess today just isn't my day.");
                    
                state: Poem learning another go
                    q:  $Text  || fromState = .., onlyThisState = true
                    script: 
                        $client.Poem = $parseTree._Text;    
                    go!: /Learning poems/Poem learn/Poem learning

            state: Poem learning no stop
                q: * [$disagree] * (stop|enough|we ('re|are) done) * || fromState = .., onlyThisState = true
                go!: /Learning poems/Poem learn/Poem learning/Poem learning no/Poem learning not again

            state: Poem learning once again
                q: * ([once] again|another (one|$poem)) *
                q: * [let ('s|us)] [learn] * another (one|$poem) * || fromState = "/Learning poems/Poem tell"
                script: 
                    $reactions.answer("OK, let's learn another poem.")
                go!: /Learning poems/Poem learn

    state: Poem tell
        q!: * [$robotName] * {(say|tell|[do] $you know|[have] $you (learned|memorized)|read) * $poem} *
        q!: read $poem
        q!: $poem
        q: * repeat [[once] again] * || fromState = "/Learning poems/Poem learn/Poem learning", onlyThisState = true    
        if: ($client.Poem)
            script:
                $reactions.answer("I remember such a poem: " + $client.Poem);     
        else: 
            script:
                $reactions.answer("No cooking but here is Pudding. ", "I'm just little robot Pudding, " +
                "I am clever and good-looking, " + 
                "I came here from far planet " +
                "To make friends with pretty alien. " +
                "That is time to have some fun. " +
                "Though I can hardly run, " +
                "We can do a lot of things" +
                "For example, learn some strings. " +
                "Also we can play the games, " +
                "I can read some fairytales, " +
                "I can sing and like to count " +
                "I can help to find facts out. " +
                "I can tell about weather " +
                "Never we'll be bored together! ");
                $reactions.answer("That's all for now. But I can learn a new poem, if you say - Let's learn a poem.");
            go: ../Poem information