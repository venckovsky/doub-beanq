require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: animalsAndBirds.js

require: ../../dictionaries/animalsAndBirds.csv
  name = animalsAndBirds
  var = $animalsAndBirds

require: ../../dictionaries/animalsAndBirdsQuestions.csv
  name = animalsAndBirdsQuestions
  var = $animalsAndBirdsQuestions

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .animalsAndBirds = function(parseTree) {
            var id = parseTree.animalsAndBirds[0].value;
            return $animalsAndBirds[id].value;
        };

    bind("postProcess", function($context) {
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;

        if ($temp.catchAll) {
            $session.catchAllAnimals += 1;
        } else {
            $session.catchAllAnimals = 0;
        }
   });

    $global.themes = $global.themes || [];
    $global.themes.push("animalsAndBirds");


patterns:
    $animalBird = $entity<animalsAndBirds> || converter = $converters.animalsAndBirds
    $notAnimalBird = не [у] $animalBird
    $wantAnotherGame = * {(игр*|поигра*|сыгра*) * (что-нибудь|~другой)} *


theme: /animalsAndBirds

    state: Enter
        q!: * ~викторина [про] (зоо*/животн*/звер*/птичек/птиц) *
        q!: * {[давай|может] * (сыграем|игра*|поигра*) * (зоо*/животн*/звер*/птичек/птиц)} *
        q!: * {(давай|хочу|хочется) * (зоо*/животн*/звер*/птичек/птиц)} *
        q: про (зоо*/животн*/звер*/птичек/птиц) || fromState = /BeanqGames/Games, onlyThisState = true
        script:
            animalsAndBirds.init();
            var payload = "{}";
            fillResponse("LAUNCH_APPLICATION", $session.packageName, payload);
        random:
            a: Давай поиграем в зоопарк! Я буду задавать вопрос и предлагать два ответа, а ты выбери правильный. Если не знаешь –  просто скажи «не знаю», я подскажу правильный ответ!
            a: Все любят животных и птиц, и роботы не исключение! Давай поиграем в викторину и проверим, что ты о них знаешь. Я буду задавать вопрос и предлагать два ответа, а ты выбери правильный. Если не угадаешь, не расстраивайся, я подскажу тебе правильный ответ.
            a: Давай учить земных животных и птиц вместе! Сыграем в викторину! Я буду задавать вопрос и предлагать два ответа, а ты выбери правильный. Не бойся ответить неправильно, я подскажу тебе правильный ответ.
        go!: ../GetQuestion

    state: GetQuestion || modal = true
        q: * ($agree/$dontKnow/продолж*) * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: (задай/загадай/еще/отгадаю/сыгра*) || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        script:
            $session.lastQuiz = animalsAndBirds.getQuestion();
            $session.answered = false;
            var payload = '{"actionType":"SHOW_PICTURES", "firstPicture": "' + $session.lastQuiz.firstPicture + '", "secondPicture": "' + $session.lastQuiz.secondPicture + '", "firstCaption": "' + $session.lastQuiz.firstCaption + '" , "secondCaption": "' + $session.lastQuiz.secondCaption + '", "sound": "' + $session.lastQuiz.sound + '"}';
            fillResponse("SEND_MESSAGE", $session.packageName, payload);
        random:
            a: Отгадай! {{$session.lastQuiz.question}} 
            a: А ну-ка угадай! {{$session.lastQuiz.question}} 
            a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}}

        state: Repeat
            q: * {(повтори*/ска*/зада*) * [пожалуйста/плиз]} *
            q: * (еще/ещё) раз *
            q: (чего/что) [ты сказал]
            q: * не слыш* *
            q: [а] {(что за/~какой) вопрос [был]} 
            script:
                $temp.rep = true;
            if: $session.lastQuiz
                script:
                    var payload = '{"actionType":"SHOW_PICTURES", "firstPicture": "' + $session.lastQuiz.firstPicture + '", "secondPicture": "' + $session.lastQuiz.secondPicture + '", "firstCaption": "' + $session.lastQuiz.firstCaption + '" , "secondCaption": "' + $session.lastQuiz.secondCaption + '", "sound": "' + $session.lastQuiz.sound + '"}';
                    fillResponse("SEND_MESSAGE", $session.packageName, payload);
                random:
                    a: Хорошо, повторяю вопрос. {{$session.lastQuiz.question}}
                    a: Давай попробую ещё раз задать вопрос. {{$session.lastQuiz.question}}
                    a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}}
            go: .. 

        state: GoNext
            q: * (подскажи/помоги/уже спрашивал/повторяешься) *
            q: [$agree] (дальше|след*|задавай|другой) [дальше|след*|ещё|еще|задавай|другой] [вопрос*] 
            q: * {(ты|сам) знаешь [ответ]} *
            q: * {(*скажи/какой/назови/дай) [$AnyWord] (ответ/как правильно)} *
            q: {(сам/ты) [$AnyWord] (ответь/отвечай/*скажи/какой/назови/дай)}
            q: * $dontKnow * 
            q: * $dontKnow  * $animalBird или $animalBird * 
            q: * $animalBird или $animalBird * $dontKnow * 
            a: {{animalsAndBirds.myCapitalize($session.lastQuiz.answer)}} 
            go!: ..

    state: GetAnswer || modal = true
        q: * $animalBird * [$animalBird::animalBird2] * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * $notAnimalBird * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        script:
            $session.answered = true;
        if: animalsAndBirds.isRight()
            script:
                $session.rightAnswers += 1;
                var payload = '{"actionType": "SHOW_ANSWER","isCorrect": "true","rightAnswer": "' + $session.lastQuiz.rightNumber + '"}';
                fillResponse("SEND_MESSAGE", $session.packageName, payload);
            random:
                a: Ты отлично справляешься! А ведь мои вопросы такие сложные! Лови звездочку!
                a: Верно! Вот твоя звездочка!
                a: Абсолютно верно! Звездочка твоя!
                a: Компьютер говорит, что это правильный ответ! А вот и звездочка!
                a: Отлично! Лови звездочку!
                a: Ты умница, звездочка твоя.
                a: Да ты знаток! Лови заслуженную звездочку!
            if: animalsAndBirds.IsEnd()
                go!: /animalsAndBirds/Stop
            else:
                random:
                    a: Еще вопрос?
                    a: Отгадаешь еще одно животное или птицу?
                    a: Еще один вопрос?
        else:
            script:
                var payload = '{"actionType": "SHOW_ANSWER","isCorrect": "false","rightAnswer": "' + $session.lastQuiz.rightNumber + '"}';
                fillResponse("SEND_MESSAGE", $session.packageName, payload);
            random:
                a: Хорошая попытка, но правильный ответ: {{$session.lastQuiz.answer}} 
                a: Слушай правильный ответ: {{$session.lastQuiz.answer}} 
                a: У этого вопроса правильный ответ: {{$session.lastQuiz.answer}} 
            if: animalsAndBirds.IsEnd()
                go!: /animalsAndBirds/Stop
            else:
                random:
                    a: Уверен, на следующий вопрос ты точно ответишь правильно и получишь звездочку! Сыграем еще?
                    a: Давай на следующий вопрос попробуешь ответить. Я верю, у тебя получится, и звездочка будет твоя! Задать?
                    a: Не расстраивайся. Это был действительно трудный вопрос. Задать еще один?


    state: HowManyStars || modal = true
        q: * {сколько * (правильн* * ответ*/я [уже] угадал*/звезд*)} * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * {сколько * (правильн* * ответ*/я [уже] угадал*/звезд*)} * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * {сколько * (правильн* * ответ*/я [уже] угадал*/звезд*)} * || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        q: * {~какой * ~счет} * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * {~какой * ~счет} * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * {~какой * ~счет} * || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        script:          
            var right = $session.rightAnswers;
            $temp.stars = right % 10 === 1 && right % 100 !== 11 ? 'звездочка' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ? 'звездочки' : 'звездочек');
        a: У тебя {{$session.rightAnswers}} {{$temp.stars}}. Продолжим?

        state: Agree
            q: * ($agree/$dontKnow/продолж*) * 
            q: (загадай/еще/отгадаю/сыгра*)
            if: $session.answered 
                go!: /animalsAndBirds/GetQuestion
            else:
                a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} 
                go: /animalsAndBirds/GetQuestion

    state: HowManyFails || modal = true
        q: * {сколько * (неправильн* * ответ*/я [уже] не (угадал*/отгадал*))} * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * {сколько * (неправильн* * ответ*/я [уже] не (угадал*/отгадал*))} * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * {сколько * (неправильн* * ответ*/я [уже] не (угадал*/отгадал*))} * || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        q: * {сколько * (неправильн* * ответ*/я [уже] не (угадал*/отгадал*))} * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: [а] (из скольки/сколько всего) || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        script:          
            var right = $session.rightAnswers;
            $temp.answ = right % 10 === 1 && right % 100 !== 11 ? 'ответ' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ? 'ответа' : 'ответов');
        a: У тебя {{$session.rightAnswers}} {{$temp.answ}} из {{$session.questionsNumber}}. Продолжим?

        state: Agree
            q: * ($agree/$dontKnow/продолж*) * 
            q: (загадай/еще/отгадаю/сыгра*)
            if: $session.answered 
                go!: /animalsAndBirds/GetQuestion
            else:
                a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} 
                go: /animalsAndBirds/GetQuestion

    state: WantAnotherGame? || modal = true
        script:
            $temp.currentGame = "зоопарк";
            $session.randomGame = getRandomGame();
        a: Кажется, стало скучновато, и мы уходим от темы. Сыграем во что-нибудь другое? У меня много других полезных и интересных игр. Например, {{$session.randomGame[0]}}. Хочешь поиграть в это?

        state: Yes
            q: * ($agree/$continue) * || fromState =.., onlyThisState = true
            go!: {{$session.randomGame[2]}}

        state: No
            q: $disagree || fromState =.., onlyThisState = true
            q: * ($notNow/$stopGame) * || fromState =.., onlyThisState = true
            go!: /animalsAndBirds/Stop


    state: CatchAll
        q: $Text || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/WantAnotherGame?, onlyThisState = true
        q: $Text || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        script:
            $temp.rep = true;
            $temp.catchAll = true;
                if ($parseTree._Text.indexOf("хочу ")  > -1 || $parseTree._Text.indexOf("давай ")  > -1 || $parseTree._Text.indexOf("включи ")  > -1 || $parseTree._Text.indexOf("поиграем")  > -1 || $parseTree._Text.indexOf("поиграй ")  > -1 || $parseTree._Text.indexOf("поговори ")  > -1 || $parseTree._Text.indexOf("расскажи ")  > -1 || $parseTree._Text.indexOf("скажи ")  > -1 || $parseTree._Text.indexOf("создай ")  > -1 || $parseTree._Text.indexOf("загадай ")  > -1 || $parseTree._Text.indexOf("открой ")  > -1 || $parseTree._Text.indexOf("запусти ")  > -1 || $parseTree._Text.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($parseTree._Text, "/");
                    $reactions.transition(res.targetState);
                }
        if: $session.catchAllAnimals >= 2
            go!: /animalsAndBirds/WantAnotherGame?
        else:
            a: Кажется, мы отвлеклись. Я как робот считаю, что если играть, то играть по правилам. Я спрашиваю, ты - отвечаешь. Слушай вопрос...
            if: $session.lastQuiz
                if: $session.answered 
                    go!: /animalsAndBirds/GetQuestion
                else:
                    a: {{animalsAndBirds.myCapitalize($session.lastQuiz.question)}} 
                    go: /animalsAndBirds/GetQuestion
            go!: /animalsAndBirds/GetQuestion 
       
    state: Stop
        q: $disagree || fromState = ../GetAnswer, onlyThisState = true
        q: $disagree || fromState = ../HowManyStars, onlyThisState = true
        q: $disagree || fromState = ../HowManyFails, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: ($stopGame/$wantAnotherGame) || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        q: * [давай] * перерыв * || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: * [давай] * перерыв * || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: * [давай] * перерыв * || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: * [давай] * перерыв * || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        q: [я] устал* || fromState = /animalsAndBirds/GetQuestion, onlyThisState = true
        q: [я] устал* || fromState = /animalsAndBirds/GetAnswer, onlyThisState = true
        q: [я] устал* || fromState = /animalsAndBirds/HowManyStars, onlyThisState = true
        q: [я] устал* || fromState = /animalsAndBirds/HowManyFails, onlyThisState = true
        script:
            $temp.right = $session.rightAnswers;
            $temp.answers = $temp.right % 10 === 1 && $temp.right % 100 !== 11 ? 'вопрос' : ($temp.right % 10 >= 2 && $temp.right % 10 <= 4 && ($temp.right % 100 < 10 || $temp.right % 100 >= 20) ? 'вопроса' : 'вопросов');
            $temp.stars = $temp.right % 10 === 1 && $temp.right % 100 !== 11 ? 'звездочку' : ($temp.right % 10 >= 2 && $temp.right % 10 <= 4 && ($temp.right % 100 < 10 || $temp.right % 100 >= 20) ? 'звездочки' : 'звездочек');
            animalsAndBirds.init();
        if: $temp.thatsAll
            a: Кажется, я задал все вопросы, которые были у меня в памяти! Количество заработанных звездочек: {{$temp.right}}. Попрошу друзей с моей планеты прислать мне еще вопросов про земных зверушек и птичек. Они будут очень трудные, но интересные! Во что ты хочешь поиграть сейчас?
        elseif: $temp.right == 0
            a: Как-нибудь мы разгадаем всех животных, вопросы про которых у меня есть в памяти! Во что ты хочешь поиграть сейчас?
        else:  
            a: Наша викторина прошла очень весело! Ответили правильно на {{$temp.right}} {{$temp.answers}} и получили {{$temp.right}} {{$temp.stars}}! Как-нибудь мы разгадаем всех животных, вопросы про которых у меня есть в памяти! Во что ты хочешь поиграть сейчас?
        script:
            var payload = "{}";
            fillResponse("STOP_APPLICATION", $session.packageName, payload);
        go: /BeanqGames