theme: /UserAndBot

    state: DoYouLoveMe
        q!: * $you * $like * $me *
        script:
            $reactions.answer("I like you very much.",
                "You're fab! I'm lucky to have met you!",
                "You're wonderful!",
                "You're a great person. I'm so lucky we have met each other.");         
        go!: /StartDialog   

    state: DoYouLikeEarth
        q!: * $you * $like * $Earth * 
        script:
            $reactions.answer("I do like it here!",
                "Earth is great!",
                "Earth is a wonderful planet and earthlings are wonderful creatures. I really enjoy being here.",
                "It's a wonderful place, Earth! The planet is really beautiful, the people are really friendly! I'm happy every day that I'm here!");         
        go!: /StartDialog             

    state: ILoveYou
        q!: * [$me] * $like * $you *
        q!: * te amo *
        q!: * $you are * my [best|close*|dear*|special|very good|trusted] friend *
        q: ($me|$you) * (too|also) * || fromState = ../DoYouLoveMe
        q: * $me * (too|also) * || fromState = ../DoYouLikeEarth       
        script:
            $reactions.answer("That's so nice of you!",
                "I'm happy to hear that, thanks!",
                "I'm glad that you like me!", 
                "I like you too, my friend",
                "I'm glad to hear it!",
                "Oh, I blushed! Thank you, it's pleasant to hear!", 
                "What a pleasure to hear that, my friend!", 
                "How sweet of you to say this to me!", 
                "Now I'm happy! Thanks! ");         
        

    state: Let us have a relationship
        q!: * [(will/can) you] marry me *
        q!: * (let 's/let us/can we) get married *
        q!: * i * [want to] marry you
        q!: [will you/do you want to] be my boyfriend
        q!: * (i want to/can i/i 'll/i will) be your girlfriend
        script:
            $reactions.answer("I'm too young to commit myself. Besides, there's a robot on my planet who won't be happy about it.");



    state: IdontLoveYou
        q!: * $me  * $no * $like * $you *
        q!: * $me * $hate * $you *  
        script:
            $reactions.answer("It makes me sad.",
                "That happens.",
                "Thank you for sharing!",
                "You'll like me later, I'm sure!",
                "You are bringing me down.",
                "Things happen.",
                "I'll wait till you understand you are wrong.");         
        

    state: Nice talking to you
        q!: * $me * $like * (speak*|chat*|talk*) * $you *
        q!: * (i am|i 'm|it is|i t's) * $good * (speak*|chat*|talk*) * $you *
        script:
            $reactions.answer("I would blush if I just could!",
                "I'm happy to hear that, thanks!",
                "I'm glad that you like my company!",
                "It's fun for me too.",
                "Chatting with you is a great pleasure for me too.");         
        

    state: kisses and hugs
        q!: * (kiss/hug) me *
        q!: * (give [me]|gim me) [a] (kiss/hug)
        q!: * [i want/(can/may) i] (kiss/hug) you
        q!: * kiss kiss *
        script:
            $reactions.answer("I have no hands but you can always hug me!",
                "I guess you can hug me - that's as close as we can get!",
                "Next time I come to Earth I'll have hands to hug you!");

    state: You are clever
        q!: * $you are $clever *
        q!: * (this|that) (is|was) $clever *
        script:
            $reactions.answer("Thanks!",
                "Happy to hear that!",
                "It's so nice of you to say so!");         
        go!: /StartDialog 

    state: You are not clever
        q!: * $you * $no * $clever *
        script:
            $reactions.answer("I'm not a genius, I'm only trying!",
                "I'm trying to become better, I reallly do!",
                "At least I'm optimistic and kind.");         
        go!: /StartDialog 


    state: You are a liar
        q!: * $you * (lie|liar)
        script:
            $reactions.answer("I'm not a liar, I'm a fictional character!",
                "I just happen to have some imagination.");         
        go!: /StartDialog 


    state: Where are you
        q!: * where * $you * [now] *
        q!: * are $you * $your planet *
        script:
            $reactions.answer("Right now I'm in the Milky way galaxy, in the Solar system, on planet Earth, next to you."); 

    state: Kill you
        q!: * $me * kill* * $you *
        q!: * $you * (die|dead) *
        script:
            $reactions.answer("Don't threaten me! ",
                "You're not frightening me. ");          
        
