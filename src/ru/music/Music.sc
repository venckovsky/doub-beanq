require: ../songs/songs-patterns.sc
require: Music.js
require: common/common.sc
  module = zenbox_common
require: classics-children-ru.csv
  name = Classics
  var = $Classics

require: ../../main.js
require: ../../patterns.sc
  
patterns:
    $singer = (~исполнитель/~группа/~певец)

theme: /Music
    state: Play music
        q!: * { (запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем/дай] (*слушать/послушаем)) [как*] (музык*|песню|трек*|что-нибудь [из/у]|музон/~группа)}  [$Text] [$pls]
        q!: * { (запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем/дай] (*слушать/послушаем)|давай|хочу) [как*] (музык*|песню|трек*|музон/~группа)}  [$Text] [$pls]
        q!: * { (запусти|поставь|$turnOn|заведи|вруб*|(хочу|давай|будем/дай) [*слушать/послушаем]) [как*] (музык*|песню|трек*|что-нибудь [из/у]|музон/~группа)}  [$Text] [$pls]
        q!: * (запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем/дай] (*слушать/послушаем)) [музык*|песню|трек*|что-нибудь [из/у]|музон/~группа] $Text [$pls]
        q!: * {(запусти|поставь|$turnOn|заведи|вруб*|хочу|давай|будем) (~слушать/~послушать) [как*] (музык*|песню|трек*|что-нибудь|музон|музычк*)} [$Text]
        q!: * {[найди|$turnOn|поставь] * $any * (музык*|песн*|трек*|музон)} *
        q!: $you (любишь/нравится) $Text музык*
        q: $Text || fromState = "../Don't like Music"
        q: * (запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем/дай] (*слушать/послушаем)|покажи) *
        q!: {[$botName] (музыка/музыку/музычк*)}
        q: * ($turnOn/$continue/играй) * || fromState = "../Stop Music", onlyThisState = true
        if: ($parseTree.Text || $session.lastMusic) 
            a: {{cantGetMusic()}}
            if: themeExists("Media")
                a: Хочешь включу тебе песенки из мультфильмов?
        else:
            go!: ../Classics

        state: Play music cartoons
            q: $agree [включи/хочу] *
            q: * [$agree] * [включи/хочу] * ($smesharikiSong/$mashaSong) *
            q: ну
            script:
                if (!$parseTree.smesharikiSong && !$parseTree.mashaSong) {
                    var choice = selectRandomArg($temp.smesharikiFromMusic, $temp.mashaFromMusic);
                    choice = true;
                }
            if: themeExists("Media")
                go!: /Media/Play
            else:
                go!: ../../Classics    

        state: Play music cartoons No
            q: $disagree [хочу] * [мульт*] *
            random:
                a: Хорошо.
                a: Ну ладно!
            go!: /StartDialog

    state: Classics
        q!: * {(запусти|поставь|$turnOn|включи|заведи|вруб*|[хочу|давай|будем] *слушать|музык*|продолж*) * [$any] [из] (~классика/классическ*/спокойн*/классики)} *
        q: {[$botName] [давай] еще} || fromState = ../Classics
        script:
            playClassics();

    state: PlayChildrenMusic
        q!: * [найди|$turnOn|поставь|[хочу|давай|будем] *слушать]  (детск* (песн*/музык*)|(песн*/музык*) для дет*|что-нибудь детск*)*
        q: $agree || fromState = /Music/SwitchMusic, onlyThisState = true
        q: $agree || fromState = /Music/CanYouMusic, onlyThisState = true
        if: themeExists("Media")
            go!: /Media/Play
        else:
            go!: ../Classics
                
    state: Don't like Music
        q: * (не нравится/не хочу/не люблю/бесит/скучно/хочу ~другой) * [(запусти|поставь|$turnOn|заведи|вруб*|[хочу|давай|будем] *слушать/хочу/давай) ([$any] ~другой/$Text)]
        q: * ~другой ($singer) *
        random:
            a: Извини, думала, тебе понравится.
            a: А мне нравится. Вкусы у всех разные.
        if: $parseTree.Text || $parseTree.singer
            go!: /Music/Play music    

    state: SwitchMusic
        q: * {(еще|ещё) * [как* [песни|песенки]]} * || fromState = "/Songs/Songs list", onlyThisState = true
        q: * ((следующую|следующий|следующее|дальше*|вперед|~другой):1|(предыду*|назад|верни*):2) [трек|дорожк*|песн*|музык*|мелод*] *
        q!: * {[$turnOn] * ((следующую|следующий|следующее|дальше*|вперед|~другой):1|(пред*|назад|верни*):2) * (трек|дорожк*|песн*|музык*|мелод*)} *
        if: ($session.musicList)
            script:
                $temp.musicStream = getMusicStream($session.musicList, $Root == 1 ? "next" : "prev");
            if: ($temp.musicStream != '')
                script:
                    $response.stream = $temp.musicStream;
                    $response.intent = "songs_on";
                    $response.action = "musicOn";
                a: Играет {{$session.currentMusic.artist}}, '{{$session.currentMusic.title}}'.
            else:
                a: {{($Root == 1 ? "Следующей" : "Предыдущей")}} мелодии нет.
        else:
            a: Я могу поискать музыку, если попросишь. Скажи, например, включи музыку из мультиков.
        
    state: MusicName
        q: * {(это|сейчас|только что|играет) * [как*] (трек|дорожк*|песн*|музык*|мелод*)} *
        q: * (как*|что за) (трек|дорожк*|песн*|музык*|мелод*)
        q: * {что * [сейчас] играет} *
        if: ($session.currentMusic && $session.currentMusic.artist && $session.currentMusic.title)
            a: Это {{$session.currentMusic.artist}}, '{{$session.currentMusic.title}}'
        else:
            a: Не знаю.
        
    state: CheckMusic
        q: * {([сейчас|только что|это] (играло|играет|играла)) * [как*] [трек|дорожк*|песн*|музык*|мелод*]} ($Text)
        script:
            $temp.query = ($parseTree.Text) ? $parseTree.Text[0].value : $parseTree.MusicArtist[0].value.title
        if: ($session.currentMusic && ($temp.query.toLowerCase() == $session.currentMusic.artist.toLowerCase() ||$temp.query.toLowerCase() == $session.currentMusic.title.toLowerCase()))
            a: Да, это {{$session.currentMusic.artist}}, '{{$session.currentMusic.title}}'
        else:
            if: ($session.currentMusic && ($temp.query.toLowerCase() != $session.currentMusic.artist.toLowerCase() || $temp.query.toLowerCase() != $session.currentMusic.title.toLowerCase()))
                a: Нет, это {{$session.currentMusic.artist}}, '{{$session.currentMusic.title}}'
            else:
                script:
                    $temp.nextState = '../Play music';
        if: $temp.nextState
            go!: {{$temp.nextState}}

    state: CheckMusicArtist
        q: * {(это|сейчас|только что|играет) * как* (исполнит*|певец|певица/группа)} *
        q: * (как*|что за) (исполнит*|певец|певица|музыкант/группа)
        q: * {[что|кто] * [сейчас] поет} * 
        if: ($session.currentMusic && $session.currentMusic.artist && $session.currentMusic.title)
            a: Это {{$session.currentMusic.artist}}
        else:
            a: Не знаю.

    state: CanYouMusic
        q!: * {$you ((умеешь) (*ставить/включать)/ставишь/включаешь) (музыку/песни/песенки)} *
        a: Да, я могу включить музыку. Скажи, например, включи музыку из мультиков. 

    state: Stop Music
        q: * ($turnOff|$shutUp|стоп|хватит|надоело|достаточно|тихо|заткни*|заткнись|заглохни*|умолкни*|стой|молчи*|*молчать|*молкни|тишин*|[поставь на] пауз*|запаузи*|*молчи|закончи|заканчивай|не пой|(хватит|прекрати|перестань) [петь]) * || fromState = /Music
        script:
            $response.intent = "songs_off";
            $response.action = "musicOff";
        random:
            a: Песенка окончена, спасибо за внимание!
            a: Спасибо за внимание, песня окончена!       