theme: /Hints

    state: Get
        script:
            $session.usedHints = true;    
            $temp.hint = getHint();
            if ($temp.hint && $temp.hint.phrase && $temp.hint.phrase != "") {
                $reactions.answer($temp.hint.phrase);
            } else {
                $reactions.answer("Hm. I don't even know what else to tell you.");
            }
        
        go: {{ $temp.hint.context }}