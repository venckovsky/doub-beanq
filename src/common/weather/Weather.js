var Weather = (function(){
    var apiKey = WeatherParams["apiKey"];

    function openWeatherMapCurrent(units, lang, lat, lon){
        return $http.get("http://api.openweathermap.org/data/2.5/weather?APPID=${APPID}&units=${units}&lang=${lang}&lat=${lat}&lon=${lon}", {
            timeout: 10000,
            query:{
                APPID: apiKey,
                units: units,
                lang: lang,
                lat: lat,
                lon: lon
            }
        }).then(parseHttpResponse).catch(httpError);
    };

    function openWeatherMapForecast(ctn, units, lang, lat, lon){
        return $http.get("http://api.openweathermap.org/data/2.5/forecast/daily?cnt=${ctn}&APPID=${APPID}&units=${units}&lang=${lang}&lat=${lat}&lon=${lon}", {
            timeout: 10000,
            query:{
                APPID: apiKey,
                ctn: ctn,
                units: units,
                lang: lang,
                lat: lat,
                lon: lon
            }
        }).then(parseHttpResponse).catch(httpError);
    };

    return {
        openWeatherMapCurrent : openWeatherMapCurrent,
        openWeatherMapForecast : openWeatherMapForecast
    };

})();

function check16daysDateTime(dateTime) {
    var diff = dayStart(toMoment(dateTime)) - dayStart(currentDate());
    return (diff >= 0 && diff <= 1382400000);
}

function checkCurrentWeatherCondition(result){
    var currentCondition = result.weather[0].id;
    if (currentCondition == 800) {
        currentCondition = 1; //ясно
    } else {
        currentCondition = Math.floor(currentCondition/100);
        switch(currentCondition) {
            case 2:
            case 3:
            case 5:
                currentCondition = 2; //дождь
                break;
            case 6:
                currentCondition = 4; //снег
                break;
            case 7:
                currentCondition = 6; //туман
                break;
            case 8:
                currentCondition = 3; //облачно
                break;
            default:
                currentCondition = 5; //ветер
        }
    }
    return currentCondition
}

function checkLocation() {
    var $request = $jsapi.context().request;
    var $client = $jsapi.context().client;
    if ($request && $request.data) {
        if ($request.data.lat && $request.data.lon) {
            $client.lat = $request.data.lat;
            $client.lon = $request.data.lon;
            return true;
        }
    }
    if ($client) {
        if ($client.lat && $client.lon) {
            return true;
        }
    } 
    return false;
}


function weatherToResponse(result, dateTime) {
    var $response = $jsapi.context().response;
    $response.date = dateTime !== undefined ? toMoment(dateTime).locale('ru').format('YYYY-MM-DD') : '';
    $response.focus = "weather";
    $response.alter = "";
    $response.index = "";
    $response.pm25 = "";
    $response.windDay = "";
    $response.windDayLevel = "";
    $response.windDir = "";
    $response.windLevel = "";
    $response.windNight = "";
    $response.windNightLevel = "";
    if (result != "empty") {
        $response.city = result.name ? result.name : "";
        $response.maxTemp = result.main && result.main.temp_max ? result.main.temp_max.toString() : "";
        $response.minTemp = result.main && result.main.temp_min ? result.main.temp_min.toString() : "";
        $response.temperature = result.main && result.main.temp ? result.main.temp.toString() : "";
        $response.weather = result.weather && result.weather.main ? result.weather.main : "";
    } else {
        $response.city = "";
        $response.maxTemp = "";
        $response.minTemp = "";
        $response.temperature = "";
        $response.weather = "";
    }
}