patterns:

    $greetingDayTime = (
        (утр*):0|
        (ден*|дн*):1|
        (вечер*):2|
        (ноч*):3)

theme: /Greetings

    state: Hello
        q!: * $hello *
        q!: давн* не виделись
        q!: [$botName] это я
        q: $me * (пришла|пришел|вернулась|вернулся|тут|дома) || fromState = ../Bye, onlyThisState = true 
        if: ($session.wasGreeting == true)
            random:
                a: Снова привет!
                a: Привет ещё раз!
                a: Приветик!
        else:
            script:
                $session.wasGreeting = true;
            random:
                a: Привет!
                a: Здравствуй!
                a: Рада тебя видеть!
                a: И снова - здорово!
                a: Приветик!
                a: Привет-привет!
                a: Приветствую {тебя/}!
                a: Я тебе {так/очень/} рада!
            random:
                a: {{alias()}}
                a: 
                a: 
                a:               
        go!: /StartDialog

    state: Bye
        q!: * [спасибо] [$botName] ($bye / спи / усни / засни / поспи / засыпай/иди спать/*баюшки*) *
        q!: увидимся
        q!: * я (пошел/пошла/пойду/поеду/{прощаюсь с тобой}/уезжаю) 
        q!: пока
        q!: мне пора [~прощаться/~идти/~убегать/~спать/~есть]
        q!: * все отдыхай
        q!: [$botName] попрощайся с *
        script:
            $response.ledAction = 'ACTION_EXP_SLEEP';
            $response.intent = "sleep";
            $session.wasGreeting = false;
        random:
            a: Пока,
            a: До связи,
            a: Удачи,
            a: Аста ла виста, бэйби.
            a: Да пребудет с тобой Сила,
            a: Всего хорошего,
            a: Пока-пока,
            a: До встречи,
        random:
            a: {{alias()}}
            a: 
            a:  

        state: Last user answer
            q: * $secondBye *
            script:
                $response.intent = "sleep";
            a: Пока!

    state: Relax
        q!: {[$botName] [тогда] (отдохни [немного]/([тебе] (надо/нужно/пора/нужен)/можешь) (отдохнуть/отдых)/отдыхай)}
        q!: {[$botName] [все] (выключились/отбой/отдыхай/пауза)}
        q!: * (([тебе] (надо/нужно/пора)/можешь) отдохнуть/отдыхай)
        script:
            $response.ledAction = 'ACTION_EXP_SLEEP';
            $response.intent = "sleep";
            $session.wasGreeting = false;
        random:
            a: Хорошо, я пока отдохну.
            a: Буду отдыхать.
        random:
            a: Позови, если понадоблюсь.
            a: Зови, если заскучаешь.        

    state: Good luck to you
        q!: * удачи [тебе]
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Юху, спасибо!
        a: {{alias()}}
        
    state: Good luck to me
        q!: * пожелай мне удачи *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Ни пуха ни пера!

    state: GoodMorningFragment
        q!: доброе
        a: Доброе!
        go!: /StartDialog

    state: GoodMorning
        q!: {[$botName] ([мы] [только] (просыпаемся/проснулись)/я [только] (проснулся/проснулась))}
        random:
            a: С добрым утром!
            a: С пробуждением!
        go!: /StartDialog

    state: GoodDayTime
        q!: * {$good $greetingDayTime} *
        script:
            $session.wasGreeting = true;                                  
            switch($parseTree._greetingDayTime) {
                case "0":
                    $reactions.answer("Доброго утра!", "С добрым утром!");
                    break;
                case "1":
                    $reactions.answer("Доброго дня!", "Добрый день!");
                    break;
                case "2":
                    $reactions.answer("Доброго вечера!", "Добрый вечер!");
                    break;
                case "3":
                    $reactions.answer("Доброй ночи!");
                    break;                                        
            }
        go!: /StartDialog

    state: GoodNight
        q!: * $goodNight *
        q!: * {(иди|ложись|давай|пора) спать} *
        q!: * [я] * [сейчас/щас] * {(буду/хочу/ложусь/иду/пойду) спать} [$botName]
        script:
            $response.ledAction = 'ACTION_EXP_SLEEP';
            $response.intent = "sleep";        
            $session.wasGreeting = false;
        random:
            a: До скорой встречи, пока-пока!
            a: Пусть тебе приснится что-нибудь доброе!
            a: Спокойной ночи!
            a: Баю-бай!

    state: How are you
        q!: * [$hello] * как* * [$you|твои|твоё|ваш*] * (дела|де ла|делишки|делаа|дила|себя (чувствуешь|чувствуете)|поживаешь|поживаете|настроение|жизнь|настрой) * 
        q!: * [$hello] * (кагдила|каг дила|хау а ю) * 
        q!: [$hello] ({как (ты|вы)}|как (живёшь|живёте)) 
        q!: * [$hello] * как * [твой|ваш|прошел|прошёл] * день 
        q!: * [$hello] * $you в порядке 
        q!: * [$hello] * как (житье|житуха|жись|оно|живется|живётся|сама|сами) 
        q!: * [$hello] * как* (жизнь|живет*|живёш*|жывет*|жывёш*|жевёш*|жевет*|жызнь)
        q!: [ну] (че|чо|что) как
        q!: * [а] {(у тебя|твои) дела}
        q!: * [$hello] * $you все ($good|$normal|$bad)
        q!: * $you * себя хорошо (чувствуешь|чувствуете) *
        q!: тебя (дела/делишки)
        if: ($parseTree.hello)
            if: ($session.wasGreeting == true)
                random:
                    a: Снова привет!
                    a: Привет ещё раз!
                    a: Приветик!
                    a: Хелло бейби!
            else:
                script:
                    $session.wasGreeting = true;
                random:
                    a: Привет!
                    a: Здравствуй!
                    a: Рад тебя видеть!
                    a: Какие люди!
        random:
            a: Отличный денёк!
            a: У меня всё {хорошо/прекрасно}.
            a: Я полна энергии!
            a: Отлично, очень хорошо.
            a: Отличный денёк, и у меня всё хорошо! Спасибо за вопрос!
            a: Сегодня у меня отличный день.
            a: У меня всё хорошо, спасибо.
            a: Всё {просто/} чудесно!
        go!: /StartDialog

    state: User How are you
        q!: тест как дела
        script:
            $session.userMood = "asked";
        random:
            a: Как твои дела?
            a: Как у тебя дела?
            a: Расскажи, как у тебя дела?
            a: Расскажи, как твои дела?
            a: Как дела?

        state: Fragment
            q: дела || fromState = .., onlyThisState = true
            random:
                a: Прости, не расслышала.
                a: Повтори, пожалуйста.
            go: ../../User How are you
        
        state: You know
            q: ты [уже] знаешь || fromState = .., onlyThisState = true
            random:
                a: Я же уже спрашивала.
                a: Да, уже знаю.
            go!: /StartDialog

        state: User Good mood   
            q: * ($good|лично|$super|$normal|$everyWillBeFine|как обычно|пучком|~крутой) * [$andYou]
            q!: все (пучком/прекрасно)
            script:
                $session.userMood = "ok";
            random:
                a: Хорошо!
                a: Я рада!
                a: Приятно слышать!
                a: Это чётко!
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog
           
        state: User Bad mood
            q: * ($bad|$dontKnow|сложн*|средне*|ни то ни се|ничего) * [$andYou]
            q: так
            script:
                $session.userMood = "bad";
                $response.ledAction = 'ACTION_EXP_SAD';
            random:
                a: Не нужно грустить!
                a: Не унывай!
                a: Я уверена, что всё будет хорошо!
                a: Всё будет чики-пуки!
                a: Да ладно, не парься!
                a: Вот тленота-то!
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog    

        state: User drunk
            q: * (пьян*/бух*/набух*) * [$andYou]  
            q: * не * трезв* * [$andYou]                    
            script:
                $session.userMood = "bad";
                $response.ledAction = 'ACTION_EXP_SAD';
            a: Хм, тут я вряд ли смогу составить тебе компанию... Но всегда буду рада поболтать.
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog 

        state: User sick
            q!: * [$me] * [только|сильно] (больн*|болен|не * хорошо|мне [оч|очень*|совсем] плохо/плох* * *чувств*/простуд*/грипп*/заболева*/~болезнь/болит/болят/болею/~болеть) * [$andYou]
            q!: * у меня бол* (живот/животик/голова/горло/спина/~ухо/ног*/ножк*/рук*/ручк*/~палец/~пальчик)
            script:
                $session.userMood = "bad";
                $response.ledAction = 'ACTION_EXP_SAD';
            random:
                a: Выздоравливай!
                a: Боль, печаль!
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog 

        state: User asks
            q: * $andYou * 
            q: [у] тебя || fromState = .., onlyThisState = true
            q: * у тебя || fromState = .., onlyThisState = true                  
            go!: /Greetings/How are you

        state: Joke
            q: * пока не родила * [$andYou]
            random:
                a: Смешно!
                a: Люблю твои шуточки!
                a: Рада, что у тебя хорошее чувство юмора!
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog 

    state: WhatsNew
        q!: * $question [у тебя|у вас] (нового|новенького|новости) *
        random:
            a: Для меня тут на Земле всё новое!
            a: Постепенно привыкаю к Земле и землянам.

    state: Meet Name
        q!: {[$botName] [*знаком*]} это $Name
        a: {Привет/Здравствуй}, {{$parseTree.Name[0].value.name}}!