require: patterns.sc
    module = zb_common
require: common.js
    module = zb_common

require: brushTeeth.js

require: fablesMorning.csv
    name = fablesMorning
    var = $fablesMorning
require: fablesEvening.csv
    name = fablesEvening
    var = $fablesEvening

patterns:
    $changeThemeWords = (давай/включи/поиграем/поиграй/поговори/расскажи/скажи/создай/загадай/открой/запусти/поставь)

init:
    $global.themes = $global.themes || [];
    $global.themes.push("BrushTeeth");

# Пояснения про типы микросценариев и про структуру справочников

## Всего в скилле 60 сказок. Микросценарии предваряющих их диалогов распадаются на 7 групп:
### [1 утр., 1 веч.] просто текст, после которого сразу идёт сказка (ответ не запрашивается)
### [5 утр., 6 веч.] вступление, на которое пользователь даёт любой ответ, далее заключение и сказка
### [4 утр., 6 веч.] то же самое, но пользователь даёт любой ответ дважды
### [10 утр., 9 веч.] вступление, на которое пользователь отвечает «да» (тогда заключение и сказка) или «нет» (перед заключением дополнительная реплика)
### [9 утр., 5 веч.] вступление, любой ответ, продолжение, блок «да/нет» с дополнительной репликой в ответ на «нет», заключение, сказка
### [1 утр., 2 веч.] то же самое, но пользователь даёт любой ответ дважды
### [0 утр., 1 веч.] вступление, любой ответ, продолжение, разные реплики в ответ на «да» и «нет», заключение и сказка

## В соответствии с этим значения в справочниках переменны и могут содержать следующие атрибуты:
### intro — самая первая реплика микросценария (обязательный атрибут)
### any — список ответов бота на любые ответы пользователя
### no — ответ на «нет» в блоке «да/нет»
### yes — ответ на «да» в блоке «да/нет»
### outro — заключение после блока «да/нет»
### url — ссылка на файл со сказкой (обязательный атрибут)

theme: /BrushTeeth

    state: InitGame || modal = true
        q!: * [хочу|давай] {[*чист*] (зубы|зубки)} *
        q!: * [спой|развлеки|повесели|развесели] [$me] * {(*чист*|*чищ*) зуб*} *
        q!: * [спой|включи|запусти|поставь|расскажи] [$me] (стих*|стиш*|песн*|песен*|музык*|сказк*|рассказ*) * {(*чист*|*чищ*) зуб*} *
        if: !$client.brushTime
            a: Я слышал, что землянам, чтобы быть здоровыми и веселыми, нужно регулярно чистить зубки. Я с радостью помогу тебе в этом непростом деле. Вооружайся скорее щеточкой и пастой, наше зубное приключение начинается. Если захочешь поиграть во что-то другое, просто скажи «Хватит».
        script:
            brushTeeth.init();
        a: {{ $session.fableData["intro"] }}

        if: $session.fableData["any"]
            script:
                $session.anyReplyCount = 0;
        elseif: $session.fableData["no"]
            go: ../YesOrNo
        else:
            go!: ../TellFable

        state: AnyReply
            q: $catchAll
            script:
                $reactions.answer($session.fableData["any"][$session.anyReplyCount]);
                $session.anyReplyCount++;

            if: $session.anyReplyCount === $session.fableData["any"].length
                if: $session.fableData["no"]
                    go: ../../YesOrNo
                else:
                    go!: ../../TellFable

    state: YesOrNo || modal = true

        state: Yes
            q: * ($agree|готов*|начина*|принима*|приступа*) *
            script:
                delete $session.offtopic;
            if: $session.fableData["yes"]
                a: {{ $session.fableData["yes"] }} {{ $session.fableData["outro"] }}
            else:
                a: {{ $session.fableData["outro"] }}
            go!: ../../TellFable

        state: No
            q: * ($disagree|$notNow) *
            script:
                delete $session.offtopic;
            a: {{ $session.fableData["no"] }} {{ $session.fableData["outro"] }}
            go!: ../../TellFable

        state: CatchAll
            q: $catchAll
            if: !$session.offtopic
                script:
                    $session.offtopic = true;
                a: Кажется, мы отвлеклись немного. Готова ли щеточка и паста?
            else:
                script:
                    delete $session.offtopic;
                a: Кажется, мы устали или зубная фея улетела от нас. Во что хочешь поиграть сейчас?
                go: /

    state: TellFable
        script:
            $response.audio = $session.fableData.url;
        go: /

    state: ChangeTheme
        q: * $changeThemeWords * || fromState = /BrushTeeth/InitGame
        q: * $changeThemeWords * || fromState = /BrushTeeth/YesOrNo
        script:
            brushTeeth.changeTheme();

    state: StopGame
        q: * $stopGame * || fromState = /BrushTeeth/InitGame
        q: * $stopGame * || fromState = /BrushTeeth/YesOrNo
        a: Давай вместе чистить зубки каждое утро и каждый вечер, чтобы они были крепкие и белые. Чем хочешь заняться сейчас?
        go: /
