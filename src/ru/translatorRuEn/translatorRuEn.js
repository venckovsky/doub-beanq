var translatorModule = (function(){


    function callYandexTranslatorApi(){
        var $session = $jsapi.context().session;
        var key = "trnsl.1.1.20171219T125221Z.d22c8649ac75016d.462e906200e15cdb90bcc7fc879ffaf87920825e";
        var url = "https://translate.yandex.net/api/v1.5/tr.json/translate";
        log($session.text);
        var fullUrl = url + "?key=" + key + "&text=" + encodeURIComponent($session.text + ".") + "&lang="+ $session.langPair;

        $http.query(fullUrl).then(function (res){
            if ((res['code'] == 200) & (res['text'].length > 0)){
                $reactions.answer({value: res['text'][0].slice(0,-1), lang: "en"});
            } else {
                $reactions.answer(cantTranslateInLanguage());
            }
        });
    }



    function cantTranslateInLanguage() {
        return selectRandomArg("Прости, я не могу перевести.",
            "К сожалению, у меня не получается перевести это.",
            "Жаль, но я не знаю, как это будет в переводе.");    
    }


    function isAvaliableTranslation(){
        var $session = $jsapi.context().session;
        var langCodeTo = $languageTable[$session.languageTo].value.code;
        var langCodeFromInfo = $languageTable[$session.languageFrom].value;
        $session.langToGenTitle = $languageTable[$session.languageTo].value.title;
        $session.langFromGenTitle = $languageTable[$session.languageFrom].value.genTitle;
        $session.langPair = undefined;
        if (langCodeFromInfo.avaliableTo){
            if (langCodeFromInfo.avaliableTo.indexOf(langCodeTo) > -1){
                $session.langPair = langCodeFromInfo.code + "-" + langCodeTo;
                return true
            }
        }
    }

    return {
        callYandexTranslatorApi: callYandexTranslatorApi, 
        cantTranslateInLanguage: cantTranslateInLanguage,
        isAvaliableTranslation: isAvaliableTranslation
    }

})();