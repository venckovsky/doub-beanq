require: ../games/BeanqGames.sc

require: Akinator.js

require: ../../common/sha256.js
require: ../../common/base64.js
require: ../../common/he.js

init:
    $global.themes = $global.themes || [];
    $global.themes.push("Akinator");
    
    bind("postProcess", function($context) {
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;

        $session.akinator = $session.akinator || {};

        if ($temp.akinatorCatchAll) {
            $session.akinator.catchAll += 1;
        } else {
            $session.akinator.catchAll = 1;
        }
   });

patterns:

    $agreeAkinator = ([$AnyWord] $agreeStrong [$AnyWord]|$agreeWeak|$yes)

    $disagreeAkinator = ([$AnyWord] $disagreeStrong [$AnyWord]|$disagreeWeak|$no)

    $maybeAkinator = {(может [быть]|наверн*|возможн*|вероятн*|скорее всего|вроде) [тогда]}

theme: /Akinator

    state: Enter || modal = true
        q!: * {[давай|может] * (сыграем|игра*|поигра*) * (акинатор*/джин*)} *
        q!: * {(давай|хочу|хочется) * (акинатор*/джин*)} *
        q: [$botName] $agree [$botName] || fromState = "/BeanqGames/Games/How to play Akinator", onlyThisState = true
        q: * (сыграем|хочу|попробую|еще|готов*|начинай) * || fromState = "/BeanqGames/Games/How to play Akinator", onlyThisState = true
        a: Ты не поверишь! Я умею угадывать загаданных тобой персонажей. Прямо как известный Джинн Акинатор. Посмотрим, смогу ли я тебя удивить! Правила такие - ты загадываешь персонажа, а я отгадываю. Я буду задавать вопросы, а ты отвечай только «да», «нет», «я не знаю», «возможно, частично», «скорее нет, не совсем». Понятны ли правила?
        script:
            Akinator.init();
        
        
        state: DontUnderstandRules
            q: [$botName]  ($disagreeAkinator|$dontKnow|не совсем) [$botName]
            q: * не понятн* *
            q: * {(повтори/повторим/повторить/(скажи/сказать [пожалуйста] ещё раз)) [пожалуйста] [ещё раз] [правил*]} *
            q: * {(какие/что за) [были] правила} *
            a: Правила такие - ты загадываешь персонажа, а я отгадываю. Я буду задавать вопросы, а ты отвечай только «да», «нет», «не знаю», «возможно, частично», «скорее нет, не совсем». Понятны ли правила?
            go: ../

        state: Continue || modal = true
            q: [$botName] $agreeAkinator [$botName] 
            q: * понятн* *
            if: $session.akinator.characterPicked
                go!: ../../Game/AskQuestion
            else: 
                a: Тогда загадывай персонажа скорее. Я жду. Готово?

            state: NotReady
                q: [$botName] $disagreeAkinator [$botName]
                q: [$botName] [$AnyWord] [еще|пока] (не (готов*|загад*|совсем)|нет|почти) [еще|пока] [$AnyWord] [$botName]
                a: Загадай персонажа. Готово?
                go: ../

            state: Ready
                q: [$botName] $agreeAkinator [$botName]
                q: [$botName] [$AnyWord] [$AnyWord] (ясн*|понятн*) [$AnyWord] [$AnyWord] [$botName]
                q: * (*ехали|поехал*|едем|вперед|начина*|погнали|продолж*) *
                q: [$botName] [$AnyWord] (готов*|загад*) [$AnyWord] [$botName]
                script:
                    $session.akinator.characterPicked = true;
                go!: ../../../Game/AskQuestion

    state: Game 
        state: AskQuestion || modal = true
            script:
                $temp.rep = true;
            if: $session.akinator.isGuess
                go!: ../Guessed?
            else:
                if: Akinator.isNotForKids($session.akinator.currentQuestion)
                    script:
                        Akinator.secretAnswer();
                else:
                    a: {{$session.akinator.currentQuestion}}

            state: Answer
                q: [$botName] [$maybeAkinator] $yes:0 [$maybeAkinator] [$botName]
                q: [$botName] [$maybeAkinator] $no:1 [$maybeAkinator] [$botName]
                q: [$botName] ([я] не (знаю|в курсе|незнаю)|неизвестно|без понятия|без понятий|понятия не имею):2 [$botName]
                q: [$botName] (возможно|частично|возможно частично):3 [$botName]
                q: [$botName] (скорее нет|не совсем|скорее нет не совсем):4 [$botName]
                script:
                    Akinator.answerAndQuestion();
                go!: ../

            state: CancelAnswer
                q: * {(измен*|отмен*|помен*|~другой) [$AnyWord] ~ответ} *
                if: $session.akinator.lastQuestions.length > 0
                    script:
                        Akinator.cancelAnswer();
                else:
                    a: Это был мой первый вопрос!
                go!: ../

        state: Guessed? || modal = true
            script:
                $session.akinator.isGuess= false;
            if: $session.akinator.ageConstraint == 0
                a: Думаю, я угадал, но тебе не скажу. Возрастные ограничения. Сыграем ещё?
                script:
                    $session.akinator.characterPicked = false;
                    Akinator.cancelSession();
                go: ./NotGuessed/No
            else:
                a: Кажется, это {{$session.akinator.characterName}}? Я прав?


            state: NotGuessed || modal = true
                q: [$botName] $disagreeAkinator [$botName]
                q: * (не угадал*|не то) *
                script:
                    Akinator.excludeCharacter();
                a: Буду угадывать дальше?

                state: No || modal = true
                    q: [$botName] $disagreeAkinator [$botName]
                    q: * не угадыв* *
                    script:
                        $session.akinator.characterPicked = false;
                        Akinator.cancelSession();
                    a: Сегодня удача на твоей стороне! Когда-нибудь обязательно угадаю! Сыграем еще?

                    state: Yes
                        q: [$botName] $agreeAkinator [$botName]
                        q: * (сыграем|хочу|попробую|еще|готов*|начинай) *
                        script:
                            Akinator.init();
                        go!: /Akinator/Enter/Continue

                    state: LocalCatchAll
                        q: *
                        script:
                            if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                                var res = $nlp.match($request.query, "/");
                                $reactions.transition(res.targetState);
                            }
                            $temp.akinatorCatchAll = true;
                        if: $session.akinator.catchAll <= 2
                            a: Не будем отвлекаться. Сыграем еще?
                            go: ../
                        else:
                            a: Кажется, становится скучновато или просто неподходящее время для магии Джинна. Во что хочешь поиграть сейчас? 
                            go: /

                state: Yes
                    q: [$botName] $agreeAkinator [$botName]
                    q: * угадыв* *
                    go!: ../../../AskQuestion

                state: LocalCatchAll
                    q: *
                    script:
                        if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                            var res = $nlp.match($request.query, "/");
                            $reactions.transition(res.targetState);
                        }
                        $temp.akinatorCatchAll = true;
                    if: $session.akinator.catchAll <= 2
                        a: Кажется, мы отвлеклись. Угадывать этого персонажа дальше?
                        go: ../
                    else:
                        a: Кажется, становится скучновато или просто неподходящее время для магии Джинна. Во что хочешь поиграть сейчас? 
                        go: /

            state: Guessed || modal = true
                q: [$botName] $agreeAkinator [$botName]
                q: * угадал* *
                script:
                    $session.akinator.characterPicked = false;
                    Akinator.chooseCharacter();
                a: Да - я угадал! Сыграем еще?

                state: Yes
                    q: [$botName] $agreeAkinator [$botName]
                    q: * (сыграем|хочу|попробую|еще|готов*|начинай) *
                    script:
                        Akinator.init();
                    go!: /Akinator/Enter/Continue

                state: LocalCatchAll
                    q: *
                    script:
                        if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                            var res = $nlp.match($request.query, "/");
                            $reactions.transition(res.targetState);
                        }
                        $temp.akinatorCatchAll = true;
                    if: $session.akinator.catchAll <= 2
                        a: Не будем отвлекаться. Сыграем еще?
                        go: ../
                    else:
                        a: Кажется, становится скучновато или просто неподходящее время для магии Джинна. Во что хочешь поиграть сейчас? 
                        go: /

            state: LocalCatchAll
                q: *
                script:
                    if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($request.query, "/");
                        $reactions.transition(res.targetState);
                    }
                    $temp.akinatorCatchAll = true;
                if: $session.akinator.catchAll <= 2
                    a: Кажется, мы отвлеклись. Это {{$session.akinator.characterName}}?
                    go: ../
                else:
                    a: Кажется, становится скучновато или просто неподходящее время для магии Джинна. Во что хочешь поиграть сейчас? 
                    go: /

    state: CatchAll
        q: *
        q: * || fromState = ../Enter, onlyThisState = true
        q: * || fromState = ../Enter/Continue, onlyThisState = true
        q: * || fromState = ../Game/AskQuestion, onlyThisState = true
        q: * || fromState = ../Game/AskQuestion/Answer, onlyThisState = true
        q: * || fromState = ../Game/CancelAnswer, onlyThisState = true
        script:
            if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                var res = $nlp.match($request.query, "/");
                $reactions.transition(res.targetState);
            }
            $temp.akinatorCatchAll = true;
        if: $session.akinator.catchAll <= 2
            a: Кажется, мы отвлеклись. С Джинном можно играть только по правилам. Напоминаю: только «да», «нет», «не знаю», «возможно, частично», «скорее нет, не совсем». Понятны ли правила?
            go: /Akinator/Enter
        else:
            a: Кажется, становится скучновато или просто неподходящее время для магии Джинна. Во что хочешь поиграть сейчас? 
            go: /

    state: Exit
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * 
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Enter, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Enter/Continue, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Game/AskQuestion, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Game/Guessed?, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Game/Guessed?/NotGuessed, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Game/Guessed?/NotGuessed/No, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame|$dontKnow|[мне] скучно) * || fromState = ../Game/Guessed?/Guessed, onlyThisState = true
        q: [$botName] [$AnyWord] [$AnyWord] $disagreeAkinator [$AnyWord] [$AnyWord] [$botName] || fromState = ../Game/Guessed?/Guessed, onlyThisState = true
        q: [$botName] [$AnyWord] [$AnyWord] $disagreeAkinator [$AnyWord] [$AnyWord] [$botName] || fromState = ../Game/Guessed?/NotGuessed/No, onlyThisState = true
        a: Кажется, на сегодня время Джиннов подошло к концу. Во что хочешь поиграть сейчас?
        script:
            $session.akinator.characterPicked = false;
            Akinator.cancelSession();
        go: /