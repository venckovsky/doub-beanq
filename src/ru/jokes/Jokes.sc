require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: jokes.js

require: jokes.csv
    name = Jokes
    var = $Jokes

require: patterns.sc
    module = zb_common

init:
    $global.themes = $global.themes || [];
    $global.themes.push("BeanqJokes");

patterns:
    $jokes = (анекдот*|~байка|~шутка|шуток|шуточк*|шутеечк*|шутейк*|юмор*|прикол/[что-то/что-нибудь] (смешное/веселое/забавное/прикольное)|(смешн*/шуточн*/прикольн*/весел*) (~история|~случай|~кейс))

theme: /BeanqJokes

    state: Jokes Start 
        q!: * [фасолик|фасолька] [знаешь|умеешь|можешь] (рассказ*|расскаж*) * $jokes *
        q!: * {[ты можешь|можешь] (развлеки*|рассмеши*|развесел*/повесели*/насмеши*) * $me} *
        q!: * {[$tellMe/хочу/хочется] [*слышать/*слушать] [еще|ещё] * $jokes } *
        q!: * (подн*|повыс*) ($me|свое*) настро* *
        q!: * [я] (хочу/хочется) * (посмеяться/повеселиться/весель*/юмор*) *
        q!: * (развлеки*|рассмеши*|насмеши*|развесели*|пошути*|шути*|повесели*|рассмеши*) * {[еще|ещё] [меня/нас]} *
        a: Шутка — лучшее лекарство от плохого настроения! Я знаю много веселых шуток и смешных анекдотов для детей. Давай рассмешу тебя! Если захочешь остановиться, просто скажи «Фасолик, хватит». Рассказать анекдот?
        script:
            $session.jokeList = [];
            $session.offtopic = 0;

        state: Jokes Yes
            q: * ($agree|$continue|$dontKnow) *
            q: * [$yes] слушаю [$you] *
            q: * [$yes] (шути*|пошути*|говори*|начина*|рассказ*|расскаж*) *
            q: * {[только] [что-нибудь|какой-нибудь|как-нибудь] (хорош*|смешн*|весел*|прикол*|забавн*|интересн*)} *
            script:
                getRandomJoke();
            if: !$temp.catchAllJokes
                script:
                    $session.offtopic = 0;
            if: $session.jokeList.length !== 0
                random:
                    a: Рассказать ещё?
                    a: Пошутить ещё?
                go: ../../Jokes Start
            else:
                a: Кажется, это все анекдоты, которые были у меня в памяти. Мы отлично провели время! Попрошу друзей-роботов прислать мне ещё смешных анекдотов и шуток. Во что хочешь поиграть сейчас?
                go: /BeanqGames

        state: Jokes No
            q: * [фасолик|фасолька] ($disagree|$notNow|$stopGame) *
            a: Если захочешь, чтобы я рассказал все смешные и добрые анекдоты, которые знаю, просто скажи: «Фасолик, расскажи анекдот». Во что хочешь поиграть сейчас?
            go: /BeanqGames

        state: Jokes CatchAll
            q: $catchAll
            script:
                if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($request.query, "/");
                    $reactions.transition(res.targetState);
                }
                $session.offtopic++;

                if ($session.offtopic === 1) {
                    $reactions.answer("Кажется, мы отвлеклись. У меня в памяти много смешных и добрых шуток про людей и зверей. Слушай:");
                    $temp.catchAllJokes = true;
                    $reactions.transition("../Jokes Yes");
                } else {
                    $temp.currentGame = "шутки и анекдоты";
                    $session.randomGame = getRandomGame();
                    $reactions.answer("Кажется, стало скучновато. Сыграем во что-нибудь другое? У меня много других полезных и интересных игр. Например, " + $session.randomGame[0] + ". Хочешь поиграть в это?");
                }

            state: Jokes Another Game Yes
                q: * $agree *
                go!: {{ $session.randomGame[2] }}

            state: Jokes Another Game No
                q: * ($disagree|$dontKnow) *
                a: А во что ты хочешь поиграть? 
                go: /BeanqGames
