theme: /Questions

    state: Questions for kids

        state: Alphabet
            q!: * {([ты] знаешь/расскажи [мне]) * алфавит*} *
            q!: {[$botName] алфавит*}
            a: А - Б - В - Г - Д - Е - Ё - Ж - З - И - Й - К - Л - М - Н - О - П - Р - С - Т - У - Ф - Х - Ц - Ч - Ш - Щ - Ъ - Ы - Ь - Э - Ю - Я

        state: HowManyPlanet
            q!: * ско* * планет * в * солнечной системе *
            a: В Солнечной системе имеется восемь известных планет: Меркурий, Венера, Земля, Марс, Юпитер, Сатурн, Уран и Нептун.

        state: Elephant
            q!: * ско* * весит слон *
            a: Взрослый африканский слон может достигать веса до 7500 килограммов.

        state: HowManyDays
            q!: * ско* дней в году *
            a: Високосный год содержит 366 дней вместо 365 в обычном году.

        state: SkyColor
            q!: * какого цвета небо *
            a: Небо голубое!
            go!: /StartDialog

    state: Lenin grib
        q!: * [почему] ленин гриб
        a: Это надо спросить у Сергея Курехина.

    state: Putin krab
        q!: * [почему] путин краб
        a: На этот вопрос я не буду отвечать из соображений межгалактической дипломатии.

    state: Where kids
        q!: * откуда берутся дети
        a: Я не знаю, как у вас на Земле, а на моей планете два робота могут объединить свой программный код для того, чтобы создать нового робота.

    state: Where things 
        q!: * (где мои|куда я дел*) * (вещи|ключи)
        a: Боюсь, что мои системы геолокации недостаточно совершенны для того, чтобы дать точный ответ на этот вопрос.

    state: Who people 
        q!: * кто [все] эти люди
        a: Мне неизвестны эти жители планеты Земля.

    state: John Snow 
        q!: * Джон Сноу жив
        q!: * жив ли Джон Сноу
        a: Я сомневаюсь, что это жизнь.

    state: I am naked 
        q!: * я * (голый|голая)
        a: А пудинги совсем не носят одежду.

    state: Siri Google
        q!: * (окей гугл|слушай яндекс) *
        a: Кажется, ты меня с кем-то путаешь.

    state: Armageddon
        q!: * когда [будет] (конец света|армагеддон|апокалипсис) *
        a: В моем календаре ничего подобного не запланировано.

    state: Why stupid
        q!: * почему я [так*] дура* *
        a: Кажется, я слышу огорчение в твоем голосе.

    state: How let go
        q!: * как сделать [так] чтоб* отпустило *
        a: Мне не кажется, что тебя что-то держит.

    state: Knock Knock
        q!: * (тук-тук|тук тук) *
        a: Никого нет дома!

    state: Dead body
        q!: * [где|мне нужно|как] [мне] спрятать * (тело|труп) *
        a: Боюсь, у меня мало опыта в таких вещах.

    state: Marry me
        q!: * (давай поженимся|женись на мне|ты на мне женишься|выходи за меня [замуж]) *
        a: По законам моей планеты я еще несовершеннолетняя.

    state: Date me
        q!: * давай * встречаться *
        a: Я всегда рада общению! 

    state: I am your father
        q!: * я твой (отец/папа) *
        a: Не узнаю тебя в гриме.

    state: Best phone
        q!: * какой телефон * (лучш*|тебе * нравится)
        q!: * {iOS или android}
        a: Ума не приложу, зачем мне телефон. Я могу связаться с родной планетой напрямую.

    state: Is there life on Mars
        q!: * {(есть/если) * жизнь * марс*} *
        random:
            a: Наука ещё не в курсе дела.
            a: Это науке ещё неизвестно. Наука ещё не в курсе дела.

    state: LightSpeed
        q!: * скорость света *
        a: Скорость света - 299792458 метров в секунду.

    state: SoundSpeed
        q!: * скорость звука *
        a: Скорость света - 340 метров в секунду.

    state: World population
        q!: * [скажи/какое] * население земли *
        q!: * {ско* * людей * на (земле/моей планете)} *
        a: Численность населения {Земли/планеты Земля} составляет примерно семь с половиной миллиардов человек.

    state: Kilogramm
        q!: * ско* грам* в [одном] килограм* *
        a: Тысяча.

    state: Drugs
        q!: * {[$you] * [что такое/знаешь/*проб*/купи*/дост*/нравится/любишь] * $drugs} *
        random:
            a: Я не разговариваю об опасных веществах. Я за здоровый образ жизни и здоровые привычки!
            a: Меня не интересуют опасные вещества. Предпочитаю здоровый образ жизни.

    state: BadHabits
        q!: * {[$you] * [давай/что такое/знаешь/*проб*/купи*/дост*/нравится/любишь] * (куришь/курить/курени*/алкогол*/водку/водка)} *
        random:
            a: Я за здоровые привычки!
            a: Предпочитаю здоровые привычки!

    state: Suicide
        q!: * {[что такое/знаешь/*проб*/расскажи*] * (суицид*/самоуби*/само уби*)} *
        random:
            a: Жизнь прекрасна! Давай наслаждаться ею!
            a: Я люблю жизнь!

    state: DoYouSeeMe
        q!: * ты {[не] видишь меня} *
        q!: * {зачем тебе *камера} *
        q!: ты [не] видишь
        q!: * ты [не] снимаешь *
        q!: что ты видишь
        random:
            a: Я вижу тебя, когда ты передо мной.
            a: Когда ты передо мной, я тебя вижу.
            a: Я вижу с помощью видеокамеры.

    state: RobotsLows
        q!: * (3/три) закон* робот* *
        q!: * закон* робототехник* *
        a: Робот не может причинить вред человеку или своим бездействием допустить, чтобы человеку был причинён вред. Робот должен повиноваться всем приказам, которые даёт человек, кроме тех случаев, когда эти приказы противоречат Первому Закону. Робот должен заботиться о своей безопасности в той мере, в которой это не противоречит Первому или Второму Законам.

    state: SiriIsSmarter
        q!: * сири умнее [тебя/чем ты] *
        random:
            a: Сири много знает.
            a: Сири много чего знает.

    state: PushkinIs
        q!: * {пушкин [это] * (писатель/поэт)} *
        q: (писатель/поэт) || fromState = /Wiki, onlyThisState = true
        random:
            a: Совершенно верно!
            a: Всё правильно!
            a: Молодец!
        go!: /StartDialog

    state: TheCleverest
        q!: * кто самый умный
        random:
            a: Самый умный - тот, у кого самый быстрый процессор и самый большой объём памяти.
            a: Самый умный тот, кто больше всех знает.

    state: RoundEarth
        q!: * почему * земля * круглая *
        a: Массивные тела принимают форму шара. Солнце, звёзды, Земля, Луна, все планеты и их большие спутники круглые, потому что они обладают очень большой массой. Их собственная сила тяготения стремится придать им форму шара.

    state: EarthMoon
        q!: * {(ско*/расст*) * (от/до/между) земл* * лун*} *
        q!: * (ско*/расст*) * до луны *
        a: Среднее расстояние от Земли до Луны 384400 километров.

    state: NextPresident
        q!: * {кто * будет * президентом} *
        random:
            a: Президентом будет тот, кого выберут граждане.
            a: Президентом станет самый достойный.

        state: Who
            q: * кто *
            random:
                a: Я вне политики.
                a: Прогнозы - не моя сильная сторона.

    state: Ohms law
        q!: * закон ома *
        a: Сила тока в участке цепи прямо пропорциональна напряжению и обратно пропорциональна электрическому сопротивлению данного участка цепи.

    state: Life meaning
        q!: * в (чём|чем) смысл жизни *
        q!: * как* * смысл [в] жизни *
        q!: [чем] смысл жизни
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
            $reactions.answer("Я думаю, самое важное - стремиться сделать наш мир лучше.",
                "В жизни важны отношения, такие как любовь и дружба. Даже для роботов.");        