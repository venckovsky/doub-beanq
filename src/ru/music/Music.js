function playClassics(){
    var $response = $jsapi.context().response;
    var $temp = $jsapi.context().temp;
    var $session = $jsapi.context().session;
    var id = selectRandomArg(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    $temp.musicStream = $Classics[id].value.link;
    $temp.musicName = $Classics[id].value.title;
    $temp.musicArtist = $Classics[id].value.artist;
    $response.stream = $temp.musicStream;
    $response.intent = "songs_on";
    $response.action = "musicOn";
    $session.currentMusic = {"artist":$temp.musicArtist, "title":$temp.musicName};
    $reactions.answer("Играет " + $session.currentMusic.artist + " '" + $session.currentMusic.title + "'.");
}

function cantGetMusic() {
    return selectRandomArg("К сожалению, я не могу найти подходящей музыки.",
        "Не удаётся найти подходящей музыки.",
        "Хотела найти музыку, которую ты просишь, но не получилось.",
        "Я хотела бы узнать больше земной музыки, но пока знаю на всю.",
        "Не получается найти музыку, которую ты просишь.",
        "Жаль, но подходящей музыки мне не удалось найти.",
        "Я могу играть только музыку, которую знаю. А знаю я пока не всё.");
}