theme: /Device Features

    state: CantHearYou
        q!: * can * $you (speak/talk) *
        q!: [$botName] can you (speak/talk) [$botName]
        script:
            $reactions.answer("Is something wrong with the sound? Try turning my volume up.",
               "If you don't hear me, maybe you should turn the sound up.",
                "How come it's not working? I can hear you well.",
                "And I hear you well. Try turning my volume up.",
                "It's so weird that you don't hear me, I hear you pretty well.");
        

    state: DoDance
        q!: * [please/can you/will you] * dance * [for me] *
        script:
            $reactions.answer("Dancing is not my thing.",
                "Frankly speaking, I'm not much of a dancer.",
                "Dancing is not my thing.",
                "I'm not a dancing girl, really.");
        

    state: Can you hear me
        q!: * (can you/do you) * hear $me *
        script:
            $reactions.answer("I can hear you very well.",
                "Absolutely.",
                "Of course I hear you.");

    state: Can you something
        q!: * can you  * (count|calculate|see) *
        script:
            $reactions.answer("Sure! ", "Yes, I can");           

    state: change your voice
        q!: * [how (do/can) i/i want to] change $you voice *
        script:
            $reactions.answer("You can change my voice in the mobile app.");

    state: bot can not
        q!: * (sing * song*) *
        q!: * sing * for me
        q!: * you * like * sing* * 
        script:
            $reactions.answer("I'm no singer, but I can switch on some music.");
        
    state: call me
        q!: * (phone/call) me *
        script:
            $reactions.answer("You can use mobile app to make a video call through me.");
        

    
    
        