require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: findSyllable.js

require: findSyllable45.csv
  name = syllableEasy
  var = syllableEasy

require: findSyllable67.csv
  name = syllableHard
  var = syllableHard

require: common/common.js
  module = zenbox_common

init:
    $global.themes = $global.themes || [];
    $global.themes.push("Find Syllable");

patterns:
    $syllableEasy = $entity<syllableEasy> || converter = function(pt){
        var id = pt.syllableEasy[0].value;
        return syllableEasy[id].value;
        }

    $syllableHard = $entity<syllableHard> || converter = function(pt){
        var id = pt.syllableHard[0].value;
        return syllableHard[id].value;
        }

    $wantAnotherGame = * {(игр*|поигра*|сыгра*) * (что-нибудь|~другой)} *


theme: /FindSyllable 

    state: Enter
        q!: * [хочу/давай/может] (~играть|~сыграть|~поиграть|~игра) * [в/про] [найди/найти] (слог/слоги/слога) *
        q!: * {(давай|хочу|хочется) * [в/про] * [найди/найти] (слог/слоги/слога)} *
        q: * [найди/найти] (слог/слоги/слога) * || fromState = /BeanqGames/Games, onlyThisState = true
        a: Чтобы научиться читать, надо звуки различать, буквы изучать и слоги уметь составлять. Я умный робот и знаю много слов. Хочу с тобою поиграть в игру «Найди слог». Я назову тебе слог, ты его запомнишь и выберешь слово, где этот слог есть. Если захочешь остановиться и поиграть во что-то другое, просто скажи "Фасолик, хватит". Будешь со мной играть?
        script:
            findSyllable.init();
            $session.myPoints = 0;

        state: Yes
            q: * $agree * 
            q: * (готов|давай|буду|хочу|начина*|попробую) * [*играть] *
            random:
                a: Отлично! Вперед!
                a: Супер!
                a: Я рад. Тогда начнем.
            go!: ../../GetSyllable

    state: GetSyllable || modal = true
        script: 
            findSyllable.getSyllable();
            $session.dontKnow = 0;
            $session.rightAnswer = false;
            $session.questionRepeated = false;
            $session.questionInProgress = true;
        a: Сейчас слушай внимательно! В каком из этих слов есть слог "{{$session.lastGroup.syllable.toUpperCase()}}": {{$session.words}}?

        state: Answer
            q: * {[$maybe|$sure|не знаю|думаю|полагаю|[мне] кажется] * [в слове] $Text} * || fromState = ../../GetSyllable
            script:
                if ($parseTree._Text.indexOf("хочу ")  > -1 || $parseTree._Text.indexOf("давай ")  > -1 || $parseTree._Text.indexOf("включи ")  > -1 || $parseTree._Text.indexOf("поиграем")  > -1 || $parseTree._Text.indexOf("поиграй ")  > -1 || $parseTree._Text.indexOf("поговори ")  > -1 || $parseTree._Text.indexOf("расскажи ")  > -1 || $parseTree._Text.indexOf("скажи ")  > -1 || $parseTree._Text.indexOf("создай ")  > -1 || $parseTree._Text.indexOf("загадай ")  > -1 || $parseTree._Text.indexOf("открой ")  > -1 || $parseTree._Text.indexOf("запусти ")  > -1 || $parseTree._Text.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($parseTree._Text, "/");
                    $reactions.transition(res.targetState);
                }
            if: $parseTree.Text && (findSyllable.checkAnswer($parseTree._Text) || $nlp.matchPatterns($parseTree._Text, $session.lastGroup.answerPatterns))
                if: $nlp.matchPatterns($parseTree._Text, $session.lastGroup.answerPatterns)
                    random:
                        a: Правильный ответ!
                        a: Отлично!
                        a: Ответ правильный!
                    script:
                        $session.myPoints += 1;
                        $session.rightAnswer = true;
                        $session.questionInProgress = false;
                        $session.CatchAll = false;
                    go!: ../../WantMore

                elseif: $session.dontKnow < 1
                    random:
                        a: Это трудное задание, нужно постараться, попробуй еще раз, внимательно слушай слова: 
                        a: Нужно попробовать еще раз и отыскать нужное слово. Слушай внимательно: 
                    a: {{$session.words}}. В каком из этих слов есть слог "{{$session.lastGroup.syllable.toUpperCase()}}"?
                    script:
                        $session.rightAnswer = false;
                        $session.dontKnow += 1;
                        $session.CatchAll = false;
                else: 
                    random:
                        a: Это было действительно сложно, правильный ответ - {{$session.lastGroup.answerText}}.
                        a: Было сложно. Правильный ответ - {{$session.lastGroup.answerText}}. 
                    a: В этом слове есть слог "{{$session.lastGroup.syllable.toUpperCase()}}".
                    script:
                        $session.rightAnswer = false;
                        $session.questionInProgress = false;
                        $session.CatchAll = false;
                    go!: ../../WantMore
            else:
                random:
                    a: Это было действительно сложно, правильный ответ - {{$session.lastGroup.answerText}}.
                    a: Было сложно. Правильный ответ - {{$session.lastGroup.answerText}}. 
                a: В этом слове есть слог "{{$session.lastGroup.syllable.toUpperCase()}}".
                script:
                    $session.rightAnswer = false;
                    $session.questionInProgress = false;
                    $session.CatchAll = false;
                go!: ../../WantMore

        state: DontKnow
            q: * (не знаю|повтори [слова/слог]|{какие слова (были/сейчас/есть)}|{какой слог (был/сейчас)}) *
            if: $session.questionRepeated
                script:
                    $session.questionRepeated = false;
                go!: ../Answer
            else:
                a: Слушай внимательно! В каком из этих слов есть слог "{{$session.lastGroup.syllable.toUpperCase()}}": {{$session.words}}?
                script:
                    $session.dontKnow += 1;
                    $session.questionRepeated = true;

    state: HowManyPoints
        q: * {сколько * (правильн* * ответ*/я [уже] [угадал*/набрал*/навал*]/у меня очков)} * 
        q: * {~какой * ~счет} *
        q: * {сколько * (правильн* * ответ*/я [уже] [угадал*/набрал*/навал*]/у меня очков)} * || fromState = ../GetSyllable
        q: * {~какой * ~счет} * || fromState = ../GetSyllable
        if: $session.myPoints == 0
            a: Мы еще не угадали ни один слог.
        else:
            script:
                $session.points = $nlp.conform("правильный", $session.myPoints) + " " + $nlp.conform("ответ", $session.myPoints);
            a: У тебя {{$session.myPoints}} {{$session.points}}.
        if: $session.questionInProgress
            a: Слушай внимательно! В каком из этих слов есть слог "{{$session.lastGroup.syllable.toUpperCase()}}": {{$session.words}}?
            go: ../GetSyllable/Answer
        else:
            go!: ../WantMore

    state: WantMore
        if: $session.questionsIds.length == 0
            a: Кажется, мы отгадали все слоги, даже самые трудные. У нас с тобой отличный слух и хорошее внимание! Во что хочешь поиграть сейчас?
            go: /BeanqGames
        elseif: $session.rightAnswer
            random: 
                a: Будем с тобой еще играть?
                a: Сыграем еще?
        else: 
            random: 
                a: Я верю, у тебя получится! Будем с тобой еще играть?
                a: Сыграем еще?

        state: ContinueGame
            q: * $agree * 
            q: * (сыграем|давай|~быть|хочу|попробую|ещё) *
            script: 
                $session.CatchAll = false;
            go!: ../../GetSyllable

    state: StopGame
        q: * ($disagree|$stopGame|$wantAnotherGame|$dontKnow) * || fromState = ../WantMore, onlyThisState = true
        q: * ($disagree|$stopGame|$wantAnotherGame|$dontKnow) * || fromState = ../Enter, onlyThisState = true
        q: * ($stopGame|$wantAnotherGame) * || fromState = ../GetSyllable
        q: * $no * || fromState = ../WantMore, onlyThisState = true
        q: * $no * || fromState = ../Enter, onlyThisState = true
        q: * [давай] * перерыв * || fromState = ../WantMore, onlyThisState = true
        q: [я] устал* || fromState = ../WantMore, onlyThisState = true
        a: Когда-нибудь мы угадаем все слова, даже самые сложные! Во что хочешь поиграть сейчас?
        script:
            $session.CatchAll = false;             
        go: /BeanqGames

    state: LocalCatchAll
        q: * $Text * || fromState = ../WantMore, onlyThisState = true
        q: * $Text * || fromState = ., onlyThisState = true
        q: * || fromState = ../GetSyllable
        q: *
        script:
            if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                var res = $nlp.match($request.query, "/");
                $reactions.transition(res.targetState);
            }
        if: $session.CatchAll
            script:
                $temp.currentGame = "слоги";
                $session.randomGame = getRandomGame();
                $session.CatchAll = false;
            a: Кажется, стало скучновато или мы просто устали. Сыграем в что-нибудь другое? У меня много других полезных и интересных игр. Например, {{$session.randomGame[0]}}. Хочешь поиграть в это?
            go: ./NextGame
        else:
            script: 
                $session.CatchAll = true;
            a: Кажется, мы отвлеклись. Я как умный и воспитанный робот считаю, что надо играть по правилам. Я назову тебе слог, ты его запомнишь и выберешь слово, где этот слог есть. Будешь со мной играть? 
            go: ./Yes

        state: Yes
            q: * $agree *  || fromState =., onlyThisState = true
            q: * (готов|давай|буду|хочу|начина*|попробую) * [*играть] *
            random:
                a: Отлично! Вперед!
                a: Супер!
                a: Я рад. Тогда начнем.
            go!: ../../GetSyllable

        state: NextGame
            state: Yes
                q: * $agree * || fromState =.., onlyThisState = true
                q: * (готов|давай|буду|хочу|начина*|попробую) * [*играть] * || fromState =.., onlyThisState = true
                go!: {{$session.randomGame[2]}}

            state: No
                q: * ($disagree|$no|$notNow|$wantAnotherGame|$dontKnow| давай (потом/в другой/в следующий) *) * || fromState =.., onlyThisState = true
                q: * || fromState =.., onlyThisState = true
                a: А во что ты хочешь поиграть?
                go: /BeanqGames