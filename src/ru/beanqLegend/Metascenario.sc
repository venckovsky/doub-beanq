###############################################################################
###############          Да/Нет/Не знаю                          ##############
###############################################################################

theme: /Metascenario

    state: YesNoDontKnow
        if: ($session.catchAllAnswer)
            go!: ../YesNoDontKnowCatchAll
            

        state: YesNoDontKnow Yes
            q: $agree|| fromState = .., onlyThisState = true
            q: $agree|| fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true        
            script:
                $session.catchAllAnswer = undefined;
                if ($session.yesNextState) {
                    $session.nextState = $session.yesNextState;
                }
            if: ($session.yesAnswer)
                a: {{$session.yesAnswer}}
            else:
                a: Прости, у меня что-то с памятью. Я зыбыла, о чём мы говорили.
            go!: ../../GoToNextTheme

        state: YesNoDontKnow No
            q: $disagree|| fromState = .., onlyThisState = true
            q: $disagree|| fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true        
            script:
                $session.catchAllAnswer = undefined;
                if ($session.noNextState) {
                    $session.nextState = $session.noNextState;
                }                        
            if: ($session.noAnswer)
                a: {{$session.noAnswer}}
            else:
                a: Прости, у меня что-то с памятью. Я зыбыла, о чём мы говорили.
            go!: ../../GoToNextTheme

        state: YesNoDontKnow DontKnow
            q: * $dontKnow *|| fromState = .., onlyThisState = true
            q: * $dontKnow *|| fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true        
            script:
                $session.catchAllAnswer = undefined;   
                if ($session.dontKnowNextState) {
                    $session.nextState = $session.dontKnowNextState;
                }                     
            if: ($session.dontKnowAnswer)
                a: {{$session.dontKnowAnswer}}
            else:
                a: Прости, у меня что-то с памятью. Я зыбыла, о чём мы говорили.
            go!: ../../GoToNextTheme 

    state: YesNoDontKnowCatchAll
        

        state: YesNoQuestion CatchAll
            q: * || fromState = .., onlyThisState = true
            if: ($session.catchAllAnswer)
                script:
                    $session.catchAllAnswer = undefined;
                a: {{$session.catchAllAnswer}}
            else:
                a: Прости, у меня что-то с памятью. Я зыбыла, о чём мы говорили.
            go!: ../../GoToNextTheme

    ###############################################################################
    ###############          Переход к следующей теме                ##############
    ###############################################################################


    state: GoToNextTheme
        if: ($session.nextState)
            script:
                $temp.nextState = $session.nextState;
                $session.nextState = undefined;
            go!: /{{$temp.nextState}}