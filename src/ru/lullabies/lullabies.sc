require: lullabies.js

require: lullabies.csv
    name = Lullabies
    var = $Lullabies

init: 
    $global.themes = $global.themes || [];
    $global.themes.push("Lullabies");

    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters.LullabyCsvConverter = function(parseTree) {
        var id = parseTree.Lullabies[0].value;
        return $Lullabies[id].value;
    }; 

    bind("preMatch", function() {
        var $response = $jsapi.context().response;
        if ($response.beanq && $response.beanq.event && $response.beanq.event.type && $response.beanq.event.packageName) {
            if ($response.beanq.event.type === "STOP_APPLICATION") {
                if ($response.beanq.event.packageName === "com.justai.beanq.skill.lullabies") {
                    $jsapi.context().temp.targetState = "/";
                }
            }
        }
    });

    $global.$converters.LullabyCustomConverter = function(parseTree) {
        var id = parseTree.value;
        return $Lullabies[id].value;
    };

patterns:
    $lullabyCsv = $entity<Lullabies> || converter = $converters.LullabyCsvConverter
    $lullabyCustom = (({кош* колыбельн*} [~песня/песенк*]):0 |
                    ({(кошкин*/кошачь*/~кошка/~киса/кошечк*) * [~песня/песенк*/колыбельн*]}):0 |
                    ({звезд* * колыбельн*}|~звездный|[~песня/песенк*/колыбельн*] про звезд*):1 |
                    (колыбельн* [для] доч*):3 |
                    ([про] [~серый] ~кот):4 |
                    ([про] журавл* [~песня/песенк*/колыбельн*]):5 |
                    ([~песня/колыбельн*] [для] сын*):7 |
                    ({[~песня/песенк*/колыбельн*] * (осень/~осенний)}):10 |
                    ([колыбельн*] [для] (~взрослый/~большой)):14 |
                    ((ходит/бродит) * (дрёма/сон/дрёма сон)):15 |
                    (ходит бродит * (дрёма/сон)):15) || converter = $converters.LullabyCustomConverter
    $lullabyName = ($lullabyCsv|$lullabyCustom)

theme: /Lullabies

    state: Enter
        q!: * [спой/поставь*/включи/включай/*слушать/~послушать] * колыбельн* [~песня/~песенка] *
        q!: * убаюк* *
        random:
            a: Закрывай глазки и слушай красивую песенку.
            a: А теперь все детки и все роботы ложатся спать и слушают красивую песенку.
        script:
            Lullabies.init();
            Lullabies.randomSong();
            $response.replies = $response.replies || [];
            $response.replies
                .push({
                    type:"launchApplication"
                });
        go!: ../PlaySong

    state: NameLullaby
        q!: * [поставь*|*слушать|услышать|спой|споем|споешь|включи|включай|давай|хочу] * $lullabyName *
        q: * [поставь*|*слушать|услышать|спой|споем|споешь|включи|включай|давай|хочу] * $lullabyName * || fromState = "../PlaySong"
        script:
            if (!$session.started) {
                Lullabies.init();
            }
            $session.lullaby.prev = $session.lullaby.current;
            $session.lullaby.current = $parseTree.lullabyName[0].value;
            $session.lullaby.currentAudio = $session.lullaby.current.audio;
        go!: ../PlaySong

    state: PlaySong || modal = true
        script: Lullabies.playSong($session.lullaby.currentAudio);

        state: More
            q: * [давай/включи/переключи/хочу/можно] * (продолж*/дальше/еще/следующ*/далее/~другой) *
            script:
                if ($session.lullabiesIds == 0) {
                    Lullabies.init();
                }
                Lullabies.randomSong();
            go!: ../../PlaySong

        state: Repeat
            q: * (повтор*/снова/еще раз*/занов*/с начал*) *
            script:
                $temp.rep = true;
            go!: ../../PlaySong

        state: Previous
            q: * (~предыдущий/до ~этот/~прошлый) *
            script:
                $temp.rep = true; 
                var previous = $session.lullaby.prev ? $session.lullaby.prev : $session.lullaby.current;
                $session.lullaby.prev = $session.lullaby.current;
                $session.lullaby.current = previous;
                $session.lullaby.currentAudio = previous.audio;
            go!: ../../PlaySong

        state: CatchAll
            q: *
            if: $session.catchAll < 1
                script: $session.catchAll += 1;
                a: Поставить тебе колыбельную?
            else: 
                go!: ../Exit

            state: Yes
                q: * ($agree/$yes/поставь/ставь) *
                script: 
                    Lullabies.randomSong();
                    $session.catchAll = 0;
                go!: ../../../PlaySong

            state: No
                q: * ($disagree|$no) *
                go!: ../../Exit

        state: Exit
            q: * $stopGame *
            a: Чем хочешь сейчас заняться?
            script:
                $session.started = false;
                $response.replies = $response.replies || [];
                $response.replies
                    .push({
                        type:"stopApplication"
                    });
            #go: /ChoosePlayorTalk/Start - пока не в мастере
            go: /BeanqGames