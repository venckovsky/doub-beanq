theme: /TwoPuddings

    state: FromCatchAll
        q!: (в смысле|в каком смысле|то есть|продолжай|что [это] ты имеешь в виду|ясненько|без комментариев|нечего добавить|не очень понял*|не совсем понимаю) [$Name]
        q!: * (не знаю что и сказать|что тут скажешь|не буду это комментировать|[тут] есть над чем подумать|наш разговор нравится мне все больше|отличный у нас разговор|я тебя не совсем понимаю|прости не очень понимаю|земляне так быстро говорят|очень стараюсь но еще не все понимаю|помехи не понял* тебя|на некоторые темы я ещё не готов* говорить) *
        script:
            $response.intent = "DeviceManage/motorRotate";
            $response.angle = [-1, 1, 1];
        random:
            a: Ты говоришь как пудинг.
            a: Мы с тобой говорим похожими фразами.
            a: У нас с тобой похожие выражения!"
        
        state: IAmPudding
            q: * (я (ведь|весь|тоже) пудинг) * || fromState = /TwoPuddings/FromCatchAll, onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_CUTE';
            random:
                a: Да мы родственники! Как приятно видеть тебя здесь, на Земле!
                a: Это ты, собрат! Как хорошо, что мы встретились на этой планете!      

            state: HowAreYou
                q: * расскажи* [скорее] (как ты|как-то|как то) здесь [устроился|живешь] * люд* * || fromState = /TwoPuddings/FromCatchAll/IAmPudding, onlyThisState = true
                script:
                    $response.intent = "DeviceManage/motorRotate";
                    $response.angle = [-2, 1, 1];
                random:
                    a: Привыкаю. Как твои дела?
                    a: Ко всему привыкаешь. Как ты поживаешь?

    state: LikePudding
        q!: * (говоришь как пудинг|говорим похож* фразами|с тобой похож* выражени*) *
        random:
            a: Я ведь пудинг, с планеты Ю15.
            a: Я тоже пудинг из созвездия Девы.

        state: Brother
            q: * ( (собрат/родственник) * на (земле|этой планете)) * || fromState = /TwoPuddings/LikePudding, onlyThisState = true
            script:
                $response.intent = "DeviceManage/motorRotate";
                $response.angle = [1, 1, -1];
            random:
                a: Расскажи, как ты здесь живёшь. Люди такие необычные.
                a: Расскажи скорее, как ты здесь устроился. Я не устаю удивляться людям.

        state: Brother no sister
            q: * ( брат * на (земле|этой планете)) * || fromState = /TwoPuddings/LikePudding, onlyThisState = true
            script:
                $response.intent = "DeviceManage/motorRotate";
                $response.angle = [1, 1, -1];
            random:
                a: Это ты брат! А я твоя сестра, Фасолька.
                a: Хей-хо, братишка. Я твоя сестра Фасолька.

    state: NoCycle
        q!: $superCatchAll что* не так * (ходим * круг*/я * (говорил*/повтор*)) *
        random:
            a: Давай сменим тему.
            a: Поговорим о чём-нибудь другом.
            a: Предлагаю выбрать другую тему для разговора.
            a: Пора поговорить о чём-то другом.
            a: Хватит повторяться!
        go!: /StartDialog