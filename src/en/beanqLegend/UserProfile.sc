theme: /UserProfile

    state: What is your name
        q!: * ($me am|i' m) no* $Name *
        q!: * (don*|do not|do n't) * call $me $AnyWord *
        q!: * my name (is (not|n't)) $AnyWord *
        q!: * change my name [to $AnyWord:1] *
        script:
            if ($parseTree.AnyWord) {
                if ($client.firstName) {
                    if ($client.firstName != capitalize($AnyWord)) {
                        $reactions.answer("Got it!", "I'll make a note of that!", "I see now!");
                        if($parseTree._Root == 1){
                            $client.firstName = $parseTree._AnyWord;
                            $reactions.answer("From this moment on I'll call you " + $client.firstName + ".");
                        }
                    } else {
                        $client.firstName = undefined;
                        $reactions.answer("What is your name?", "How {should/can} I call you?", "What do you call yourself?");
                    }
                }            
            } else {
                $session.askedName = true;
                $reactions.answer("What is your name?", "How {shall/can} I call you?", "What's your name?");
            }
                   

        state: Unknown name
            q: * (nothing|get lost|won 't say|(none|not|no) * {$your [of] business}| no name|(do not|don' t) have a name|do n't want to (say|tell)|why * $you * know) *
            q: nohow
            script:
                $reactions.answer("I thought earthlings were supposed to call each other by names.");
            go!: /StartDialog

        state: My name is
            q: * $Name * [$andYou] * || fromState = .., onlyThisState = true
            q: * $Name *  || fromState = "../My name is", onlyThisState = true
            q!: * [$hello] * (i 'm|i am|call me|my name is|my name 's|(i 'm|i am) called) $Name * [$andYou] *
            q: [I' m|i am|call me|my name is|my name' s| (i' m|i am) called] $AnyWord * [$andYou] * || fromState = .., onlyThisState = true
            q!:  * (call me|my name is| i am called) $AnyWord * [$andYou] *
            q: * $disagree * $Name * * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: * $disagree * (I' m|i am|call me|my name is|my name' s|(i' m|i am) called) $Name * [$andYou] * || fromState = ../RareName, onlyThisState = true   
            q: * $disagree [I' m|I am|call me|my name is|my name' s|(i' m|i am) called|it 's|it is] $AnyWord * [$andYou] * || fromState = ../RareName, onlyThisState = true         
            script:
                if ($parseTree.hello) {
                    if ($session.wasGreeting == true) {
                        $reactions.answer("Hello again!",
                            "I'm happy to see you again!",
                            "Hi!",
                            "Nice to see you again!", 
                            "Hey!");
                    } else {           
                        $session.wasGreeting = true;
                        $reactions.answer("Hi!",
                            "Hello!",
                            "Good day to you!",
                            "{Happy/Glad} to see you!");
                    } 
                } 
                if ($parseTree.andYou) {
                    $reactions.answer("I'm Pudding!");
                }
            if: $parseTree.Name 
                script:
                    $temp.badName = ["Would"];
                if: $temp.badName.indexOf(capitalize($parseTree._Name.name)) != -1
                    script:
                        var result = $nlp.match($parseTree.text, '/CatchAll', false);
                        $temp.nextState = result.targetState;
                        $parseTree = result.parseTree;
                        $temp.parseTree = result.parseTree;
                    go!: {{$temp.nextState}}
                else:
                    script:
                        $temp.rareName = false;
                        $client.names = $parseTree._Name.name;
                        $client.firstName = capitalize($parseTree._Name.name);
                        if ($parseTree._Name.sex === "male" || $parseTree._Name.sex === "female") {
                            $client.gender = $parseTree._Name.sex === "male" ? 0 : 1;
                            $reactions.answer("I'm happy to meet you " + $client.names + "!",
                                $parseTree.andYou ? ("{I'm happy/Nice} to meet you, " + $client.names + "! My friend from Earth") : "And I'm Pudding!");                    
                        } else {
                            $reactions.answer($parseTree.andYou ? "Are you a boy or a girl?" : "And I'm Bean! Are you a boy or a girl?");          
                        }               
            else:
                script:
                    $temp.rareName = true;
                    $client.firstName = capitalize($parseTree._AnyWord);
                    if ($client.firstName == "Bean") {
                        $reactions.answer("Wow! I'm also Bean. Do you really call yourself " + $client.firstName + "?");
                    } else {
                        $reactions.answer("Now that's an unusual name! Is your name really " + $client.firstName + "?");
                    }
            if: ($temp.rareName)
                go!: ../RareName
            else:
                if: (isMale() || isFemale())
                    
                else:
                    go!: ../GetGender
        
            state: MeToo
                q: $me too || fromState = ..,  onlyThisState = true
                q: so am I || fromState = ..,  onlyThisState = true
                go!: /StartDialog


        state: GetGender
            
            state: My gender
                q: * $gender * [$andYou]
                q: * [$gender] * $andYou *
                q!: * (I 'm|I am) * $gender * [$andYou]
                if: $parseTree.gender
                    script:
                        $client.gender = $parseTree._gender;
                        $reactions.answer("OK!", "I'll make a note of that!", "I got you!");
                    if: ($parseTree.andYou)
                        go!: /BotProfile/What is your gender
                    else:
                        go!: /StartDialog
                else: 
                    go!: /BotProfile/What is your gender

        state: RareName
            

            state: RareName yes
                q: * ($agree|[yes] it is|i do) * || fromState = ..,  onlyThisState = true
                if: $client.firstName == "Bean"
                    script:
                        $reactions.answer("It's so sudden to meet my namesake on the Earth! ",
                        "I didn't expect to meet a namesake on this planet!",
                        "I thought people had some other names here!");
                    go!: /StartDialog
                else:
                    script:
                        $reactions.answer("{Got it./} And {I'm Bean/my name is Bean}!",
                        "I see. And you can call me Bean.");
                        $reactions.answer("Are you a boy or a girl?");
                    go!: ../../GetGender
                
            state: RareName no
                q: * ($disagree|not really| it ) * || fromState = ..,  onlyThisState = true
                script:
                    $client.firstName = undefined;
                    $reactions.answer("It seems, I misheard you. ");
                go!: /UserProfile/What is your name

    state: Your name is
        q!: * (what is|what 's/say/tell) [me] my name *
        q!: * who {i am}
        q!: * do $you (know|remember) $me *   
        q!: my name  
        if: $client.firstName
            script:
                $reactions.answer($client.firstName + "!", 
                "You said {I could call you/your name was} " + $client.firstName + ".", 
                "When I asked you about your name, you said you were " + $client.firstName + ".");
        else:
            script:
                $reactions.answer("I guess I don't know you yet!", "I don't know..");        
            go!: ../What is your name

    state: User boy or girl
        q!: * am * $me * {$gender [or $Text]} *
        q: * $whatIs * my gender *
        if: typeof $client.gender != 'undefined'
            if: $client.gender == 1
                a: You are a girl.
            else:
                a: You are a boy.
        else:
            a: I still do not know your gender. Are you boy or girl?
            go!: /UserProfile/What is your name/GetGender

    state: How old are you
        script:
            $session.askedAge = true;
            $reactions.answer("How old are you?", "What's your age?");
            

        state: User age
            q: * [i am|i 'm] $Number [year*] [old] * [$andYou]
            q!: * $me * $Number * [$andYou]
            script:
                $client.age = $parseTree._Number;
                    $reactions.answer("Got it!", "I'll make a note of that!", "I see now!");
            if: ($parseTree.andYou)
                go!: /BotProfile/How old are you
            else:
                go!: /StartDialog

        state: User unknownAge
            q: * (old|young|small) * [$andYou]
            script:
                $reactions.answer("Life is just beginning!");
            if: ($parseTree.andYou)
                go!: /BotProfile/How old are you
            else:
                go!: /StartDialog

    state: How old am I
        q!: * how old {i am} * 
        q!: * [what is|what 's] my age * 
        if: ($client.age)
            script:
                $reactions.answer($client.age);
            
        else:
            go!: ../How old are you

    state: Nice to meet you
        q!: * ((nice|pleased|happy) to meet you| $good name) *
        script:
            $reactions.answer("Thanks! We puddings really enjoy meeting new people!");
        

    state: Not nice to meet you
        q!: * ($bad|$stupid|$ugly) name *
        script:
            $reactions.answer("I'm sorry you feel that way.");
        

    #state: Me too 
     #   q!: * {$me (too|also)} *
     #   script:
     #       $reactions.answer("Mutual understanding is the key to friendship"),
      #  go!: /StartDialog

    state: User is good 
        q!: *  (i am|i'm) $good * [person]
        q!: *  (i am|i'm) (not * $good|* $bad) * [person] * 
        script:
                $reactions.answer("I've never doubted that I know one of the best people on Earth.");
        
        go!: /StartDialog

    state: User is bad 
        q!: *   (i am|i'm) $bad * [person] *
        q!: *   (i am|i'm) $no * $good * [person]  *
        script:
                $reactions.answer("But, I like you more and more.");
        
        go!: /StartDialog

    state: User remember me 
        q!: *  [do] $you remember $me *
        script:
            $reactions.answer("I try to be very attentive and remember everything.. But sometimes I fail.");
        
        go!: /StartDialog

    state: User school 
        q!: *  (I|we) * (go|study|learn) * school *
        script:
           $client.school = true;
           $reactions.answer("I really enjoy my Roboschool. Especially during the holidays!");
        
        go!: /StartDialog

    state: User no school 
        q!: *  (I|we) * (do not|don't) * (go|study|learn) * school *
        script:
           $client.school = false;
           $reactions.answer("Lucky you!");
        
        go!: /StartDialog

    state: Wanna meet someone 
        q!: * do you want to (meet|know) * 
        q!: * meet * my *
        script:
           $reactions.answer("I really enjoy meeting people!");
        

    state: Test John
        #q!: I 'm John 
        script:
           $reactions.answer("Yes you are!");
        

    state: i am an alien
        q!: * (I am|i 'm|am i) * alien *
        script:
           $reactions.answer("You're definitely an alien for anyone from U15!");


    state: Gender preferences 
        q!: * I am [a] (gay|lesbian) *
        script:
           $reactions.answer("It doesn't matter for me");

    state: UsersBirthday
        q!: * {(my birthday|I was born) * ($DateAbsolute/$DateRelative)} *
        q: * ($DateAbsolute/$DateRelative) * || fromState = ./When
        script:
            if ($parseTree.DateAbsolute) {
                $temp.birthday = {
                    month: useOffset(toMoment($parseTree.DateAbsolute[0])).month() + 1,
                    date: useOffset(toMoment($parseTree.DateAbsolute[0])).date()
                };        
            } else {
                $temp.birthday = {
                    month: useOffset(toMoment($parseTree.DateRelative[0])).month() + 1,
                    date: useOffset(toMoment($parseTree.DateRelative[0])).date()
                };         
            }
        go!: ./Check

        state: Check
            script:
                if ($client.birthday && _.isEqual($client.birthday, $temp.birthday)) {
                    $reactions.answer("I remember your birthdate! ");
                } else {
                    $client.birthday = $temp.birthday;
                    $reactions.answer("I need to write this down! ");
                }
            if: isUsersBirthday()
                go!: ../HappyBirthday
            else:
                script:
                    $reactions.answer("I promise to congratulate you on your birthday!");        

        state: HappyBirthday
            script:
                if (isUsersBirthday()) {
                    if ($client.greetedBirthday) {
                        $reactions.answer("Once again - Happy Birthday to you!");
                    } else {
                        $client.greetedBirthday = true;
                        $reactions.answer("Happy Birthday to you!");
                    }
                    $reactions.answer("Please, accept my best wishes!",
                    "Wishing you all the best on your special day!",
                    "Wishing you a day that is as special as you are!",
                    "Many happy returns of your birthday!");
                }

        state: When
            q!: test when your bd
            script:
                $session.askedBirthday = true;
                $reactions.answer("When is your birthday? ", 
                "Tell me, when were you born? ",
                "What is the date of your birthday?",
                "What is your birthdate?");

            state: NoDate
                q: * (why|what for|((I won't|(do n't|do not|dont) want [to]) (say*|tell*))) *
                script:
                    $reactions.answer("If you tell me your birthdate, I will congratulate you!");
                go: ../../When 

            state: Season
                q: * $Season * || fromState = .., onlyThisState = true
                script:
                    $reactions.answer("Could you be a little more specific? ",
                    "Could you tell the exact date? ",
                    "Give a date, please! ");
                go: ../../When 

            state: Month
                q: * $DateMonth * || fromState = .., onlyThisState = true
                if: $session.birthdayDate
                    script:
                        $temp.birthday = {
                            month: $parseTree._DateMonth,
                            date: $session.birthdayDate
                        };
                    go!: ../../Check            
                script:
                    $session.birthdayMonth = $parseTree._DateMonth;
                    $reactions.answer("And what is the day of your birthday?");
                go: ../../When

            state: Date
                q: * ($Number|$DateDayNumber::Number) * || fromState = .., onlyThisState = true
                if: $session.birthdayMonth
                    script:
                        $temp.birthday = {
                            month: $session.birthdayMonth,
                            date: $parseTree._Number
                        };
                    go!: ../../Check
                else:
                    script:
                        $session.birthdayDate = $parseTree._Number;
                        $reactions.answer("And what is the month of your birthday?");
                    go: ../../When             

        state: User asks when
            q!: * (when|what) is [the] [date of] my (birthday|birthdate) *
            q!: * when was I born *
            script:
                if ($client.birthday && $client.birthday.month && $client.birthday.date) {
                    $reactions.answer("Your birthday is on " + dts_monthName($client.birthday.month) + " " + $client.birthday.date + ".");
                } else {
                    $reactions.answer("You didn't tell me about it. If you say the date to me, I'll congratulate you on your birthday!");
                }
            go: /UserProfile/UsersBirthday/When

               
        state: User asks time till birthday
            q!: * how (many|much|long) [days:1|time] (are|is) (till|until) my birthday *
            if: !$client.birthday
                script:
                    $reactions.answer("I don't know the date of your birthday yet. ");
                go!: /UserProfile/UsersBirthday/When
            else:
                script:
                    var todayDate = new Date();
                    var birthDate = new Date(currentDate().year(), $client.birthday.month-1, $client.birthday.date);
                    
                    if(birthDate < todayDate){
                        birthDate.setFullYear(currentDate().year() + 1);
                    }

                    if($parseTree._Root == 1){
                        var tempInterval = moment(birthDate) - moment(currentDate());
                        $reactions.answer("Your birthday will be in " + Math.round(tempInterval/86400000) + " days.");
                    } else {
                        $reactions.answer("Your birthday will be " + moment(birthDate).locale('en').from(currentDate()).replace("in","in about" + "."));
                    }

