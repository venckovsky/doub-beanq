###############################################################################
###############          Да/Нет/Не знаю                          ##############
###############################################################################

theme: /Metascenario

    state: YesNoDontKnow
        if: ($session.catchAllAnswer)
            go!: ../YesNoDontKnowCatchAll
          

        state: YesNoDontKnow Yes
            q: * $agree * || fromState = .., onlyThisState = true
            q: * $agree * || fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true       
            script:
                $session.catchAllAnswer = undefined;
                if ($session.yesAnswer) {
                    $reactions.answer($session.yesAnswer);
                } else {
                    $reactions.answer("I'm sorry, I must have a short-term memory loss. I forgot what we were talking about.");
                }
                if ($session.yesNextState) {
                    $session.nextState = $session.yesNextState;
                }
            go!: ../../GoToNextTheme

        state: YesNoDontKnow No
            q: * $disagree * || fromState = .., onlyThisState = true
            q: * $disagree * || fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true       
            script:
                $session.catchAllAnswer = undefined;        
                if ($session.noAnswer) {
                    $reactions.answer($session.noAnswer);
                } else {
                    $reactions.answer("I'm sorry, I must have a short-term memory loss. I forgot what we were talking about.");
                }  
                if ($session.noNextState) {
                    $session.nextState = $session.noNextState;
                }            
            go!: ../../GoToNextTheme

        state: YesNoDontKnow DontKnow
            q: * $dontKnow * || fromState = .., onlyThisState = true
            q: * $dontKnow * || fromState = ../../YesNoDontKnowCatchAll, onlyThisState = true       
            script:
                $session.catchAllAnswer = undefined;        
                if ($session.dontKnowAnswer) {
                    $reactions.answer($session.dontKnowAnswer);
                } else {
                    $reactions.answer("I'm sorry, I must have a short-term memory loss. I forgot what we were talking about.");
                }
                if ($session.dontKnowNextState) {
                    $session.nextState = $session.dontKnowNextState;
                }              
            go!: ../../GoToNextTheme 

        state: YesNoDOntKnow Mutual
            q: * (mutual*| [the] same [feelings]) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("I am glad we agree upon it. ");         
            go!: /StartDialog


    state: YesNoDontKnowCatchAll
        
        state: YesNoQuestion CatchAll
            q: * || fromState = .., onlyThisState = true
            script:
                if ($session.catchAllAnswer) {
                    $reactions.answer($session.catchAllAnswer);
                    $session.catchAllAnswer = undefined;
                } else {
                    $reactions.answer("I'm sorry, I must have a short-term memory loss. I forgot what we were talking about.");
                }  
            go!: ../../GoToNextTheme

    ###############################################################################
    ###############          Переход к следующей теме                ##############
    ###############################################################################


    state: GoToNextTheme
        if: ($session.nextState)
            script:
                $temp.nextState = $session.nextState;
                $session.nextState = undefined;
            go!: /{{ $temp.nextState }}
            