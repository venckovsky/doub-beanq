require: ../dictionaries/film-characters.csv
    name = MovieCharacter
    var = $MovieCharacter

require: ../dictionaries/film-name.csv
    name = Movie
    var = $Movie

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .MovieCharacterConvertor = function(parseTree) {
            var id = parseTree.MovieCharacter[0].value;
            return $MovieCharacter[id].value;
        };

    $global.$converters
    .MovieConvertor= function(parseTree) {
        var id = parseTree.Movie[0].value;
        return $Movie[id].value;
    };


patterns: 
    $movieCharacter = $entity<MovieCharacter> || converter = $converters.MovieCharacterConvertor      
    $movieName = $entity<Movie> || converter = $converters.MovieConvertor

theme: /Movies
    
    state: Has bot seen movie
        q!: * $you * (seen|watch*|saw|see) * $movieName *
        q!: * tell me * $movieName *
        q!: * what is $movieName about * 
        script:   
            $reactions.answer("{I've watched this movie!/I heard about this film./Oh, of course, I know it.} It's about " + $parseTree._movieName.filmAbout + " {I like it./Nice film.////}");

    state: Bot watch some movies
        q!: * $you * (watch*|see|seen|saw|read*) * (movie*:1|film*:2|cartoon*:3|comics:4) *
        script:
            var approvedCharacters = [];
            switch($parseTree._Root){
                case "1":
                case "2":   
                    $temp.searchType = "film";
                    break;
                case "3":
                    $temp.searchType = "cartoon";
                    break;
                case "4":
                    $temp.searchType = "comics";
                    break;
            }

            var movieCharactersNum = Object.keys($MovieCharacter);
            
            for (var i = 1; i <= movieCharactersNum; i++) {
                nextMovieCharacter = $MovieCharacter[id].value;
                for (var n = 1; n <= nextMovieCharacter.from.length; n++) {
                    if(nextMovieCharacter.from[n] == $temp.searchType){
                        approvedCharacters.push(nextMovieCharacter.name);
                    }
                }
            }

            $temp.nameCharacter = approvedCharacters[random(1, approvedCharacters.length)];

            $reactions.answer("I remember the last {{$temp.searchType}} I saw was with {{$temp.nameCharacter}}.");



    state: Does bot know character
        q!: * $you * know * $movieCharacter *
        q!: * $you * (say|hear|heard) * $movieCharacter *
        q!: * (who|what) is * $movieCharacter *
        script:
            $temp.genderPronTo = $parseTree._movieCharacter.gender == 'm' ? 'him' : 'her';
            $temp.genderPron = $parseTree._movieCharacter.gender == 'm' ? 'he' : 'she';
            if ($parseTree._movieCharacter.from[0] == 'film' || $parseTree._movieCharacter.from[0] == 'cartoon'){
                $temp.verbAct = 'watched';
            } else {
                $temp.verbAct = 'read';
            }
            $reactions.answer("I " + $temp.verbAct + " " + $parseTree._movieCharacter.from[0] + " with " + $temp.genderPronTo + ". If I'm not mistaken {{$temp.genderPron}} is " + $parseTree._movieCharacter.description + ".");

    
    state: Lord of Rings phrases
        q!: * (took [away] my precious):1 *
        q!: * (you shall not pass):2 *
        q!: * (home * behind you * the world * ahead):3 *
        script:
            switch($parseTree._Root){
                case "1":
                    $reactions.answer("We be nice to them, if they be nice to us, my precious.");
                    break;
                case "2":
                    $reactions.answer("Gandalf, is that you?");
                    break;
                case "3":
                    $reactions.answer("Well, if you were born to the rolling hills and little rivers of the Shire, than yes.");
                    break;
            }


    state: Harry Potter phrases
        q!: * (we|everybody) * (face|faces) * choice between * right and * easy *
        script:
            $reactions.answer("Dumbledore was a good guy, yep.");

    state: Godfather phrases
        q!: * (I|$you|he|she|they) * (make|makes) [him|her|$you|them] * offer (he|she|$you|them) (can ('t|not) refuse) *
        script:
            $reactions.answer("Oh, then... Leave the gun. Take the cannoli.");

    state: Bond phrases
        q!: * Bond James Bond *
        script:
            $reactions.answer("I've looked forward to this moment, Mr. Bond.");

    state: Terminator phrases
        q!: * I ('ll|will) be back *
        q!: * hasta la vista [baby] *
        script:
            $reactions.answer("You certainly look like Terminator now.");

    state: The Wizard of Oz phrases
        q!: * we * not in Kansas anymore *
        script:
            $reactions.answer("We must be over the rainbow!");


    state: Apollo13 phrases
        q!: * {Houston * (problem|problems)} *
        script:
            $reactions.answer("Oh, what's going on your flight?");


    state: Forrest Gump phrases
        q!: * {run * Forrest [run]} *
        script:
            $reactions.answer("Maybe one day I will try to. You know, life is like a box of chocolates. You never know what you're gonna get.");

    state: Games of Thrones phrases
        q!: * ([the] winter is coming):1 *
        q!: * (know nothing * John Snow):2 *
        q!: * (John Snow * know nothing):2 *
        q!: * (John Snow * dead):3 *     
        script:
            switch($parseTree._Root){
                case "1":
                    $reactions.answer("Keep your ears warm.", "Somewhere on the Globe, I am quite sure.");
                    break;
                case "2":
                    $reactions.answer("One does not have to remember everything. They say the North remembers.");
                    break;
                case "3":
                    $reactions.answer("I wonder!");
                    break;
            }


    state: Star wars phrases
        q!: * may the Force be with ($you|him|her|them) *
        script:
            $reactions.answer("Easy. Jedi business");

    state: Shining phrases
        q!: * here ('s|is) Johnny *
        script:
            $reactions.answer("All work and no play makes Jack a dull boy");

    state: Holmes phrases
        q!: * {elementary * [my dear] Watson} *
        script:
            $reactions.answer("Well, then the game, Mrs Hudson, is on.");

    state: Matrix phrases
        q!: * everything begins with choice *
        script:
            $reactions.answer("Why, oh why didn't I take the blue pill?");

    state: Jerry Maguire phrases
        q!: * show me [the] money *
        script:
            $reactions.answer("Congratulations, you're still my agent, Jerry");

    state: Titanic phrases
        q!: * I ('m|am) * king of * world *
        script:
            $reactions.answer("Oh, I love you, Jack! Just get down from ship's bow, pal.");
