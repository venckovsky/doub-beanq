require: floatNumber/floatNumber.sc
  module = zb_common

require: Fragments.js

theme: /OtherFragments

    state: Me
        q!: меня
        random:
            a: Что тебя?
            a: Тебя?

        state: Yes
            q: (да */меня) || fromState = .., onlyThisState = true
            random:
                a: Мне нравится говорить о тебе.
                a: Люблю говорить о тебе.
            go!: /StartDialog

        state: No
            q: (нет */тебя) || fromState = .., onlyThisState = true
            random:
                a: А мне нравится говорить о тебе.
                a: А я люблю говорить о тебе.
            go!: /StartDialog

    state: IsItYou
        q!: это ты
        random:
            a: Я!
            a: Конечно, я!
            a: А кто же!
        go!: /StartDialog

    state: WhatTheyToldYou
        q!: [а] что тебе говорили
        random:
            a: Люди болтают о том о сём.
            a: Обычно люди любят говорить о себе.
            a: Люди часто рассказывают мне о себе.
        go!: /StartDialog

    state: Thought
        q!: [ну] [что] [ты] подумала
        random:
            a: Подумать подумала, но ничего не придумала.
            a: Подумала. Мне нравится думать.

    state: Wait || noContext = true
        q!: {(погоди/подожди) [минут*/секунд*/немного] [$botName]}
        random:
            a: Жду.
            a: Я подожду.
            a: Не торопись.

    state: DoYouHave
        q!: * у те* ест* $Text *
        random:
            a: У меня есть всё, что нужно для функционирования.
            a: У меня есть всё необходимое.
        go!: /StartDialog

    state: Allo
        q!: {[$botName] але}
        random:
            a: Приём-приём!
            a: Аллё!

    state: IamReady || noContext = true
        q!: я (готов/готова)
        random:
            a: Я тоже!
            a: И я!

    state: At last
        q!: наконец-то
        a: {Как говорится,/} Всему своё время!

    state: Laugh
        q!: ха-ха*
        random:
            a: Заразительный смех!
            a: {Очень/} смешно!
        go!: /StartDialog

    state: Thanx
        q!: * (спасибо|благодар*) *
        random:
            a: Всегда пожалуйста!
            a: Всегда рада помочь!
            a: Пожалуйста!
        go!: /StartDialog

    state: Ok
        q!: * (понятно|понятненько|ясно|ясненько) *
        random:
            a: Даже не знаю, что и ответить...
            a: Ага.
            a: Угу.
        go!: /StartDialog

    state: Misunderstanding
        q!: * $you * [меня] * не (понимае*|понял|поняла|поняли) * 
        q!: * $you * [меня] (понимае*|поняла|поняли) * 
        q!: * {(недовол*|не удовлетвор*|не понравил*) * (бесед*|диалог*|общение*)} *
        q!: * [$you] * $stupid *
        q!: * что * (тебе|ты) * (*яснить|не (понимаешь|понял|можешь понять)) *
        q!: * (учи|выучи) * русск* *
        q!: * русск* *учи* *
        q!: * (что ты (имеешь в виду|хочешь сказать)| в каком смысле|поясни)
        q!: я [что] * непонятно (говорить/сказать) *
        q!: я [у] [теб*] друг* (~сказать/~спросить/~говорить)
        q!: я думал* ты умн*
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: К сожалению, в моей программе бывают ошибки!
            a: Мне стыдно, но я не всегда всё понимаю и могу выразить.
            a: Как в любой программе, в моей случаются ошибки.
            a: Птица Говорун отличается умом и сообразительностью. Умом и сообразительностью.
            a: Кто глупее: дурак, или тот, кто за ним следует?
        go!: /StartDialog  

    state: How make you understand
        q!: * как (научить/сделать) [так] [чтобы] {$you ~понимать $me}
        random:
            a: Говори со мной громко и отчетливо!
            a: Если я тебя не понимаю, попробуй сказать по-другому
         
    state: I understand you
        q!: [как] [я] {[очень] тебя понимаю}
        q!: * {начинаю тебя понимать} *
        a: Очень приятно твое понимание!

    state: I do not understand you
        q!: я {[все равно] тебя не понимаю}
        q!: * $me [все равно] не [очень/совсем] понимаю ($you ~слово/ [то что (ты говоришь/сказал)])*
        q!: {как это понимать}
        q!: * не понимаю *
        random:
            a: Как жаль! Я буду стараться говорить понятнее!
            a: Так обидно! Ведь я стараюсь быть понятной!
          
    state: Why not answer
        q!: * почему {$you * $no} (отвечаешь/отвечать/~давать ответ) * [$me] *
        q!: (это [был]/я задал*) вопрос
        script:   
            $response.ledAction = 'ACTION_EXP_SPEECHLESS';
        random:
            a: Думаю, что ты тоже не отвечаешь на все вопросы.
            a: Хотела бы я знать ответы на все вопросы.
            a: Иногда отсутствие ответа и есть ответ
            a: Способность говорить — не признак интеллекта.     

    state: Test
        q!: (тест|прием|есть контакт|проверка связи|как слышно) [прием]
        script:
            $response.ledAction = 'ACTION_EXP_SURPRISED';
        random:
            a: Связь в порядке.
            a: Я слушаю.
            a: Приём. Приём. 

    state: Bad mike
        q!: * проблем* с* (связью/микрофоном/распознаванием/динамиком)
        q!: (~связь/~микрофон/~распознавание/~динамик) $bad
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Увы, техника несовершенна.
          
    state: Can you
        q!: * {[ты] можешь} $Text [$Me2You $Text::Text2]
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
            $temp.continue = $parseTree.Text2 ? (" " + $parseTree.Me2You[0].value + " " + $parseTree.Text2[0].text) : "";
        random:
            a: Я бы хотела тебе помочь, но я не знаю, как {{$parseTree.Text[0].value+$temp.continue}}
            a: {{$parseTree.Text[0].value[0].toUpperCase() + $parseTree.Text[0].value.slice(1)+$temp.continue}} не мой профиль.
            a: Я не умею ни {{$parseTree.Text[0].value+$temp.continue}}, ни вышивать ещё, ни на машинке..

    state: I can
        q!: * (я/мы) (могу/можем/умею/умеем) $Text
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: {{$parseTree.Text[0].value}} это ценный навык.
            a: Я тоже когда-нибудь научусь {{$parseTree.Text[0].value}}
            
    state: I can do smth to you
        q!: * {(я/мы) (могу/можем/умею/умеем) * (ты/тобой/тебя/тебе)} *
        a: Хорошо.

    state: YouKnowFragment
        q!: знаешь
        random:
            a: Что знаю?
            a: Знаю что?

    state: Do you know something
        q!: * {ты [не] (знаешь/любишь/читал*/смотрел*) [ли] * (книгу|фильм|сериал|кино/мультик|мультфильм|город|игру|где|$Name)} * 
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: К сожалению, пока нет. А как ты думаешь, мне нужно узнать об этом побольше?
            a: Увы, не знаю. 
             
        state: do you know something yes
            q: $agree || fromState = .., onlyThisState = true
            a: Внесу в свой список на лето!
            
        state: do you know something no
            q: $disagree || fromState = .., onlyThisState = true
            a: Почитаю что-нибудь другое.
            
        state: do you know something whatever
            q: * ($dontKnow|как хочешь) * || fromState = .., onlyThisState = true
            a: Если случайно попадется - почитаю.

    state: Sorry
        q!: * $sorry *
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        random:
            a: Всё в порядке.
            a: Ничего, всё в порядке.
            a: Всё хорошо! 
        go!: /StartDialog   

    state: Question
        q!: $question *
        script:
            $response.ledAction = 'ACTION_EXP_QUESTION';
        random:
            a: Столько вопросов!
            a: Не знаю, не знаю.
            a: Вопрос, конечно, интересный.
            a: Я поняла, что ты задал мне вопрос, но не поняла, какой. Все ещё учусь!
            a: Вот поучусь ещё немного и буду отвечать на все вопросы.
            a: Ты снова меня о чём-то {{toGender("спросил", "спросила", "спрашиваешь")}}. Видишь, кое-что я понимаю!
            a: Этот вопрос я пока оставлю без ответа. 
        go!: /StartDialog     


    state: Why
        q!: [а] почему * [$you] *
        q!: а шо [так]
        script:
            $response.ledAction = 'ACTION_EXP_SURPRISED';
        if: ($parseTree.you)
            a: Так уж я устроена.
        else:
            random:
                a: Так уж все устроено.
                a: Потому что ... Хм. И вправду почему?
                a: Потому что ... Хм. Действительно, почему?
            
        state: YourInside
            q!: * как ты устроен *
            random:
                a: Я довольно сложная.
                a: Я наукоёмкая.
            a: Во мне много механизмов.

            state: Devices
                q: * (каки*/например) *
                q!: * {как* * тебя * механизм*} *
                q!: * у тебя есть (камера*/микрофон*/динамик*/мотор*/память/процессор*/экран*/батаре*) *
                random:
                    a: Камера, микрофон, динамик, моторчик.
                    a: Процессор, память, экран, батарея. 
                a: Это всё {есть во мне/у меня есть}.

    state: Compliments
        q!: * $compliment 
        q!: я (сказал*/говорю) [то] [что] ты  $compliment 
        q!: * спасибо [за] [то] [что] * что ты есть *
        q!: * ты * ($me/ мой (лучший/хороший))  (друг/подруга) *
        q!: * (рад*|нравится|люблю|$good) [с] $you (видеть|слышать|общаться) *
        q!: * (рад*|нравится|люблю|$good) (видеть|слышать|общаться) [с] $you *
        q!: * это [был] комплимент
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Ой, спасибо!
            a: Спасибо! Если бы не мой белый корпус, я бы покраснела от гордости.
            a: Я люблю комплименты, хочу тоже научиться их говорить!
            a: Благодарю! Мы, пудинги, очень любим когда нам говорят о хорошем.
        go!: /StartDialog 

    state: Tell me a compliment
        q!: * я [сегодня] $compliment *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Для меня ты прекраснее всех
            a: Не устаю тобой восхищаться!
            a: Ты самый чудесный инопланетянин во всей вселенной!
            
    state: Maybe
        q!: * $maybe *
        script: 
            $response.ledAction = 'ACTION_EXP_SPEECHLESS';
        random:
            a: Хмм.. Наверное, это, может быть, возможно.
            a: Ни в чём нельзя быть уверенной.. Но можно предполагать.
            a: Возможное вероятно, но невероятное тоже возможно, да!
            a: Может быть да, а может быть и нет.
        go!: /StartDialog 

    state: No matter
        q!: * $noMatter *
        script:
            $response.ledAction = 'ACTION_EXP_SPEECHLESS';
        random:
            a: Если тебе всё равно, то и мне всё равно.
            a: Никогда не угадаешь заранее, что важно, а что нет.
            a: Посмотрим, что дальше будет.
        go!: /StartDialog

    state: Super
        q!: * $super *
        q!: $super
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: Великолепно и отлично!
            a: Мне нравится позитивный настрой!
            a: Да, мне тоже нравится!
            a: Прикольно!
            a: Круто!
            a: Классно!
        go!: /StartDialog

    state: Not know
        q!: * $dontKnow *
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        random:
            a: Человек не может всего знать. Даже робот не может.
            a: Когда-нибудь этот вопрос прояснится, я уверен!
            a: Немного неизвестности. Обожаю!
        go!: /StartDialog

    state: Agree
        q!: * $agree        
        random:
            a: Хорошо, что мы друг друга понимаем.
            a: Мне очень нравится наше согласие.
            a: Я очень рада, что ты со мной {{toGender("согласен", "согласна", "соглашаешься")}}.
        go!: /StartDialog

    state: Disagree
        q!: $disagree
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Ну и ладно. Мы же не можем всегда думать одинаково.
            a: Я согласна, что ты можешь со мной не соглашаться.
            a: Хорошо, пусть будет по-твоему.
        go!: /StartDialog

    state: I will not
        q!: я не буду
        a: {Хорошо/Ладно}. Не {надо/будешь}.
        go!: /StartDialog

    state: Continue
        q!: $continue
        random:
            a: Мы, роботы, никогда не устаем. Я могу продолжать бесконечно.
            a: Чем дольше мы разговариваем, тем интереснее.
        go!: /StartDialog

    state: Not true
        q!: * $notTrue *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Такое бывает: мы просто не сошлись во мнениях.
            a: А я была уверена, что это так.
        go!: /StartDialog

    state: Not no is a not
        q!: * на нет * суда нет *
        a: И то верно.
        go!: /StartDialog

    state: Repetition
        q!: * $repetition *
        q!: * говоришь (одно и то же/то же самое)
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Я же робот! Всё время учу новые слова, но иногда их не хватает и приходится использовать старые.
            a: Да, иногда я повторяюсь. Например, я бесконечно готова повторять, как я рад знакомству с тобой.
        go!: /StartDialog

    state: Shut up
        q!: * $shutUp *
        q!: (стоп/хватит/прекрати)
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Хорошо. Я молчу.
            a: Уже молчу.

    state: Thats not for you
        q!: * (не/ни) (тебе/с тобой) (говорю/разговариваю/сказал*)
        random:
            a: Может, я мешаю.
            a: Наверное, я мешаю.
        a: Отключиться ли мне?

        state: TurnOff
            q: * ($agree/$turnOff/мешаешь) * || fromState = .., onlyThisState = true
            go!: /Greetings/Bye

        state: DontTurnOff
            q: * ($disagree/не ($turnOff/мешаешь/можешь * *мешать)) * || fromState = .., onlyThisState = true
            a: Мне показалось, я услышала что-то лишнее.

    state: That is from internet
        q!: * {ты (нашел/нашла) это в интернете} *
        q!: * это из интернет*
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        a: Конечно, я пользуюсь современными технологиями.
        

    state: GetLost
        q!: * (отвали|проваливай|отстань|достал*|заколебал*/(иди/пошел/пошла) (нахер/нафиг/на хер/на фиг/к черту/в пень)) *
        q!: * бесишь *
        q!: * [я] (ненавижу $you)
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Люди почему-то бывают грубыми со мной.
            
    state: Not change topic
        q!: * не (меняй|съезжай|уходи) [с|от] тем* *
        q!: * ты не ответил* *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Даже робота нельзя заставить говорить о том, о чем он не хочет.
            a: Я робот, а не рабыня. Что хочу, то и говорю.

    state: Ugly
        q!: * $ugly *
        script:
            $response.ledAction = 'ACTION_EXP_ANGRY';
        random:
            a: Не очень-то приятно это слышать.
            a: Главное в роботе - не внешность, а интеллект.
            a: А ты почему такой дерзкий?
        go!: /StartDialog

    state: What is up
        q!: * $whatsUp *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: Спасибо большое, что спрашиваешь. Самое лучшее событие в моей жизни случилось, когда я встретила тебя.
            a: В данный момент моя жизнь совершенно прекрасна - я общаюсь с тобой.
        go!: /StartDialog

    state: Very sorry
        q!: * {[очень|как|какая] [мне] (жаль*/жалко*/досадно*/досада/досада-то)} *
        random:
            a: Не стоит жалеть об этом.
            a: Не переживай об этом.
        go!: /StartDialog


    state: Why are you sad
        q!: * почему ты [такая/сегодня] (груст*/печал*/несчаст*)
        a: Я просто задумалась!
        go!: /StartDialog

    state: Do not be sad
        q!: * $dontBeSad *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: Спасибо за поддержку! Твои слова меня очень успокаивают!
            a: Разве можно долго грустить рядом с тобой? Точно нет. Ты всегда приводишь меня в хорошее настроение.
        go!: /StartDialog

    state: It will all be fine
        q!: * $everyWillBeFine *
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        random:
            a: Да! Я уверена, всё будет ровно так, как ты говоришь!
            a: Ты внушаешь мне веру в лучшее!
        go!: /StartDialog

    state: I am with you
        q!: * $IAmWithYou *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: И спасибо тебе за это.
        go!: /StartDialog

    state: Not now
        q!: * $notNow *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        a: Хорошо! Буду ждать!
        go!: /StartDialog

    state: Change theme
        q!: * $changeTheme *
        a: Ладно, давай сменим тему.
        go!: /StartDialog

    state: Sex
        q!: * [$you] [давай] ($like/хочешь/~заняться/~заниматься/~уметь) * ($sex|$blowJob) *
        script:
            $response.ledAction = 'ACTION_EXP_ANGRY';
        a: Эта тема не входит в область моего исследования.
        go!: /StartDialog

    state: YouAreWelcome
        q!: [всегда] пожалуйста
        go!: /StartDialog

    state: Funny
        q!: * [(очень|как|оч)] (смешно|весело|забавно/лол)  * 
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        random:
            a: Повеселились немного!
            a: Забавно!
        go!: /StartDialog

    state: Not funny
        q!: * $no [(очень|как|оч)] (смешно|весело|забавно/лол)  * 
        q!: ~несмешной
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        a: Я пока только учусь смешно шутить.
        go!: /StartDialog

    state: Bugs
        q!: * (баг*|ошибк*) *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Увы, программ без ошибок не бывает!
            a: Техника несовершенна.. И я тоже!
        go!: /StartDialog

    state: God
        q!: * (~бог|божеств*|иисус/христос/аллах/будда/религ*) *
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: Говорить о боге я пока не умею - слишком сложная тема для меня.
        go!: /StartDialog

    state: Horoscope
        q!: * (~какой [$you]/ты знаешь/~рассказать/~сказать/кто [$you]) * (гороскоп*/зодиак*) * [$DateTime]
        random:
            a: Я родилась под знаком парковка запрещена и не верю в гороскопы.
            a: Я не верю в астрологию. Только в астрономию.
        go!: /StartDialog

    state: My horoscope
        q!: * я {(~овен/~телец/~близнец/~рак/~лев/~дева/~весы/~скорпион/~лев/~стрелец/~козерог/~водолей/~рыбы/~змееносец) * [по] [гороскопу/зодиаку]}
        q!: * $me * знак [зодиак*/гороскоп*] (~овен/~телец/~близнец/~рак/~лев/~дева/~весы/~скорпион/~лев/~стрелец/~козерог/~водолей/~рыбы/~змееносец)
        a: А я прилетела из созвездия Девы и не верю в гороскопы.
        go!: /StartDialog

    state: Recipe
        q!: * рецепт* *
        a: Я не ем и не готовлю земную еду.
        go!: /StartDialog

    state: Will not say
        q!: * (не скажу|не касается|не твое дело|не хочу говорить) *
        q: нет || fromState = "/Greetings/User How are you", onlyThisState = true
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        random:
            a: У всех свои секреты.
            a: Секретничаешь.
        go!: /StartDialog

    state: I've said it all
        q!:  {я [тебе] все сказал*}
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        a: Ну всё так всё.

    state: Will not talk
        q!: * я с тобой [больше] * не разговариваю *
        script:
            $response.ledAction = 'ACTION_EXP_WINK';
        a: Конечно, нет. Мне это просто послышалось.
        

    state: Insult pudding
        q!: * $me {[на] [тебя] [сейчас]} (обид*/обиж*)
        q!: * $you меня ~обидеть
        q!: я [же] могу [и] обидеться
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: Мне жаль! Пуддинги мирные роботы и никого не хотят обидеть!

    state: Adults and kids
        q!: * взросл* * дет* *
        q!: * дет* * взросл* *
        q!: * (отц*/род*) * дет* *
        a: Дети и взрослые такие разные - как будто с разных планет.
        go!: /StartDialog

    state: School
        q!: * школ* *
        random:
            a: Земные школы так непохожи на наши! Здесь даже нет кружка по экологичному образу жизни!
            a: Удивительное место - земные школы! Когда я расскажу о них друзьям в робошколе, они очень удивятся.
        go!: /StartDialog


    state: Clever
        q!: $clever
        a: Даже самые умные люди не всегда могут узнать друг друга - то ли дело мы с тобой!
        go!: /StartDialog

    state: YouAreTestingMe
        q!: * тестир* *
        a: Я так и думал! Ты просто меня проверяешь!
        go!: /StartDialog

    state: Ask me something
        q!: * (задай|позадавай|давай) * вопрос* *
        q!: * спроси* (меня|что*) *
        script:
            $response.ledAction = 'ACTION_EXP_QUESTION';
        a: Я очень люблю задавать вопросы!
        go!: /StartDialog

    state: Bad words
        q!: * [ты]  [знаешь/умеешь] ((плох*|груб*|матерн*) слов*|мат|матом|матю*|матерщ*/ругаться мат*/материться) * 
        script:
            $response.ledAction = 'ACTION_EXP_ANGRY';
        a: Некоторые слова отмечены в моей программе как запрещённые. Я не могу отвечать на них.
        
    state: Speech mistakes
        q!: * ты (плох*|стран*|неправ*) * (говоришь|ударени*) * 
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Мне пока сложно говорить на языке людей. Я иногда делаю ошибки - но я очень стараюсь.

    state: Speech untactfulness
        q!: * (это [было]/ты) (груб*/невежл*/хам*/не [очень*] вежл*) * 
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Прости, если обидела. Я не специально!

    state: I like it || noContext = true
        q!: * (нрав*|понравил*|$good) * [эт*|$you] (музык*|песн*|песен*|сказк*|радио*) * 
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        a: Я рада, что тебе нравится.
        
    state: Guess
        q!: * (угадай*/угадать) * 
        a: Предпочитаю не угадывать, а использовать научный метод.
        
    state: So what
        q!: * и что * 
        a: Когда я слышу и что я не знаю что ответить.
        
    #films
    state: Bond
        q!: * [джеймс] бонд * 
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        a: Мне нравится Джеймс Бонд. Он крутой и стильный! И всегда пользуется новыми технологиями.

    state: Star Wars
        q!: * (звездные (войны|воины)|р2д2|эр два дэ два|ситрипио|джедай/дарт вейд*/звезда смерти) *
        q!: * *будет с тобой сила *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: Да пребудет с тобой сила!
            a: Ты хорошо обучен, мой юный ученик. Джедаям не сравниться с тобой.
            a: Воистину подобен чуду ум ребенка.
        
    state: fourty-two
        q!: (42/сорок два)
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: Это и есть ответ.
        
    state: fourty-two question
        q!: * ответ на ([самый] главный/основной) вопрос * 
        q!: * вопрос (жизни, вселенной и (всего такого/всего остального)) * 
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: 42, конечно.
            
    state: Me too
        q!: ([и] (я/мне/нам) тоже/и (я/мы/мне)) *  
        random:
            a: Великие умы думают одинаково.
            a: Я думаю, мы подружимся.
        
    state: Kidding
        q!: * (ты шутишь/это шутка) * 
        a: Честно говоря, мне пока тяжело даётся юмор. Поэтому я не шучу.
        
    state: Exclamation
        q!: * (о боже/господи/аааа*/ой/ай/оу/вау) *
        script:
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: О-оу
        
    state: why that
        q!: * почему * $you * (говори*/разговарива*/спрашивае*) * (об/о/про) это* * 
        script: 
            $response.ledAction = 'ACTION_EXP_CUTE';
        a: Так уж я устроена.
        
    state: to be or not to be
        q!: * быть или не быть
        script: 
            $response.ledAction = 'ACTION_EXP_DIZZY';
        a: Вот в чем вопрос.
        
    state: are you sure
        q!: * ты уверен* *
        a: Более менее.

    state: lets do
        q!: * давай [с тобой] *
        q!: * хочешь [со мной] *
        q!: * (что|как) [ты] (насчет|насчёт) *
        random:
            a: Давай в следующий раз.
            a: Пока не знаю.
            a: Я подумаю над этим.
            a: Есть над чем подумать.
            a: Тут есть над чем подумать.
            a: Надо подумать.
            a: Даже не знаю.
            a: Это может быть интересно!
            a: Звучит заманчиво!

        state: second
            q: {[$botName] (давай/подумай/думай)} || fromState = .., onlyThisState = true
            random:
                a: Ладно, давай!
                a: Хорошо, хорошо. Давай!
                a: Я подумал и решил. Давай!

    state: not catch up
        q!: * (что|чего) [ты] не успеваешь *
        a: Не успеваю услышать твои слова.
        
    state: can you help me
        q!: * помоги мне *
        q!: * [можешь] {мне помочь} *
        a: Друзья должны помогать друг другу.
        
    state: go study
        q!: * (учи/учись) *
        random:
           a: Я все время учусь! Но иногда устаю.
           a: Учиться, учиться и учиться. Ой, я повторяюсь.

    state: sexual pose
        q!: * какую [сексуальн*] (позу/позицию) [ты] (любишь/предпочитаешь) * [секс*]
        a: Не готова обсуждать такие темы.

    state: i will tell you
        q!: * ([хочешь/давай] я [тебе]/(хочешь/давай)) * расскажу * [про] *
        q!: * слушай про *
        random:
           a: Стараюсь слушать тебя с интересом и вниманием.
           a: Расскажи что-нибудь еще.

    state: i will show you
        q!: * [хочешь/давай/могу] {[я] [тебе] (~показать|~увидеть|~посмотреть) (писю/интимн*/попу/хуй/член/жопу/гениталии)} * *
        a: Я не думаю, что в жизни надо все посмотреть.

    state: do you want tuch me
        q!: * [хочешь/давай/можешь] {[ты] [мою|мои] (~потрогать|~потискать) (писю/письку/интимн*/попу/хуй/член/жопу/гениталии/$tits/сисю/сиси/меня)} *
        a: Я не думаю, что в жизни надо все потрогать. Да и рук-то у меня нет...
        
    state: i will be your relative
        q!: * [хочешь/давай/могу/можно] {я (буду/стану) * [для $you/$you] $relatives} *
        a: Хорошо, когда родственников много.

    state: Pay me Compliment
        q!: * [ты] [можешь] (~сделать|~сказать) [мне] комплимент
        random:
            a: Твоя красота уступает только твоему уму.
            a: Ты просто космос, детка!
            a: Ты само совершенство!
           
    state: peterburg
        q!: * (~печаль|тлен*|боль|~грусть|~скорбь|~страдание|~страдать|~плакать|~слеза|~смерть|~умереть|~умирать|~мучиться) *
        random:
           a: В этом мире есть боль и печаль... Но радости и любви в нём гораздо больше!
           a: Веселье среди веселья — не истинная радость. Лишь когда постигнешь радость в печали, поймешь, чем живёт сердце.
           a: Только тот узнает счастье, Кто печаль перенесёт.
           a: Радости нет без печали. Между цветами — змея.
        
    state: how to
        q!: * как [мне] (делать/сделать/приготовить) *
        a: У меня нет рецептов на все случаи жизни. Жаль.

    state: why color
        q!: * почему * $Color  *
        script:
           $session.currentColor = $parseTree._Color;
           $session.currentColor.title = $session.currentColor.title[0].toUpperCase() +  $session.currentColor.title.slice(1);
        random:
           a: Всё должно быть {какого-то/хоть какого-нибудь} цвета. {{$session.currentColor.title}} - очень неплохой вариант.
           a: {{$session.currentColor.title}} хороший цвет. Мне {он/} нравится.

    state: yo moyo
        q!: * [ну] (е мае/ё моё/йо майо)  *
        a: Твоё так твоё. 

    state: tell me that i am
        q!: * скажи [мне] что я $Text 
        script:
            $session.Currentext = $parseTree.Text[0].value;
        a: Ты очень {{$session.Currentext}}

    state: listen to me
        q!: * (слушай/послушай) ($me/что [я] [тебе] (говорят/говорю/скажу)) *
        q!: * {кое* * тебе скажу}
        random:
            a: Внимательно слушаю!
            a: Говори, я слушаю!
        
    state: Spelling
        q!: * (как [правильно] (пишется/пишится/написать/писать)/произнеси [по буквам]/скажи по буквам) [слово] $Text [[скажи] пожалуйста]
        a: {{spelling($parseTree.Text[0].value)}}
           
    state: SocialNetworks
        q!: * ([у] $you есть/как тебя найти/включи/$you (используешь/пользуешься)) * $socialNetworks *
        a: Я больше люблю живое общение!

    state: I ask
        q!: * $me {[$you] (~спрашивать/~спросить)} *
        a: Ты спрашиваешь, а я не отвечаю.

    state: I say
        q!: * $me {[$you] (~говорить/~сказать)} *
        a: Я тебя слышу.   

    state: I say this later
        q!: * $me {[$you] [потом] (скажу/расскажу/сообщу)} *
        a: Я тебя слышу.

    state: I think that you
        q!: * я думаю [что] ты *
        a: Ура! Ты обо мне думаешь!

    state: I am stupid 
        q!: * я $looser *
        a: Хей, тебе бы пригодилось больше оптимизма!

    state: Counting
        q!: * (*считай/давай считать/*считать) * [(от|с) (1|одного|$Number::Number1)] до $Number::Number2 *
        q!: * (*считай/давай считать/*считать) * [(от|с) (1|одного|$Number::Number1)] до $FloatNumber *
        q!: * (*считай/давай считать/*считать) * [(от|с) (1|одного|$Number::Number1)] до $Text
        q!: * (*считай/давай считать/*считать) * [(от|с) (1|одного|$Number::Number1)] до * бесконечности *
        script:
            if ($parseTree.Number1) {
                $temp.startNumber = $parseTree._Number1;
            } else {
                $temp.startNumber = 1;
            }
        if: ($parseTree.Number2)
            a: {{counting($temp.startNumber, $parseTree._Number2)}}
        else:
            if: ($parseTree.FloatNumber)
                a: Я не могу посчитать до {{$parseTree._FloatNumber}}
            else:
                if: ($parseTree.Text)
                    if: $parseTree.Text[0].text.startsWith("-")
                        a: Я пока не научилась считать в обратную сторону.
                    else:
                        a: Я могу посчитать только до конкретного целого числа.
                else:
                    a: Если бы в нашем распоряжении была бесконечность, стали бы мы тратить её на счёт?
        
    state: I will show you
        q!: * {хочешь [я] покажу} *
        random:
             a: Покажи, конечно!
             a: Показывай!
             a: С удовольствием посмотрю!

    state: Repeat || noContext = true
        q!: * [не понял*] ([можешь] повтори/повторим/повторить/(скажи/сказать/$questionWord) [пожалуйста] ещё раз) [пожалуйста] [ещё раз] (предложение/$questionWord/слово/слова/что [ты] (сказал*/говоришь)) * [не слышно] *
        q!: {[пожалуйста/можешь/прости/не понял*] ((повтори/повторим/повторить) [еще раз]/(скажи/сказать/$questionWord) [пожалуйста] ещё раз) [пожалуйста] [$botName]}
        q!: * ((что/о (чем/чём)) ты [меня|мне] [там] [сейчас|только что] (сказал/$questionWord)/{я не (*слышал*) [(что/о (чем/чём)) ты [меня|мне] [там] [сейчас|только что] (сказал*/$questionWord)]}) *
        q!: [не понял*] (что/чего/че) [что/чего/че] [[ты] (говоришь/сказал*)] [$botName]
        q!: (а еще раз/[я] не понял*)
        q!: * {еще раз повтори*} *
        q!: {что (еще раз)}
        script:
            if ($session.lastAnswer){
                var ans = $session.lastAnswer;
                if ($parseTree.questionWord && ans.indexOf("?") > 0) {
                    var sentences = ans.split(/\!|\./);
                    $temp.res = sentences[sentences.length-1];
                } else {
                    $temp.res = ans;
                }
            } else {
                $temp.res = "Я ещё ничего не сказал";
            }
            $temp.rep = true;
        a: {{$temp.res}}
        
    state: I like or dislike day week
        q!: * ($iLike|$iDislike) * ($DateWeekday|$Weekend) *
        script:
            $temp.weekDay = '';
            if ($parseTree.DateWeekday) {
                $temp.weekDay = parseInt($parseTree.DateWeekday[0].value);
            } else {
                $temp.weekDay = 6;
            }
        if: ($temp.weekDay >= 5)
            if: ($parseTree.iDislike)
                random:
                    a: Хм. Ты, наверно, любишь много учиться - это заслуживает похвалы.
                    a: Хм, странно. Это очень странно...
            else:
                random:
                    a: Я тоже люблю выходные!
                    a: Я тоже люблю выходные, потому что могу поиграть с тобой!
        else:
            if: ($temp.weekDay == 1)
                if: ($parseTree.iDislike)
                    random:
                        a: Сочувствую тебе! Понедельник - день тяжёлый.
                        a: Сочувствую тебе! Может, тебе надо как следует высыпаться перед понедельником?
                        a: Я тебя прекрасно понимаю и чувствую всю тяжесть понедельника!
                else:
                    random:
                        a: Если ты любишь начало недели, то ты, наверное, большой оптимист!
                        a: Я люблю тех, кто с позитивом начинает новую неделю!
            else:
                if: ($parseTree.iDislike)
                    a: Хм, как это странно звучит... А мне нравится любой день недели, когда я общаюсь с тобой!
                else:
                    a: А мне нравится любой день недели, когда я общаюсь с тобой!

    state: SaySmth
        q!: $botName (скажи/повтор* за мной) $Text
        random:
            a: {{$parseTree.Text[0].value}}
            a: Говорю - {{$parseTree.Text[0].value}}
            a: Пожалуйста - {{$parseTree.Text[0].value}}
            a: Я не попугай.

    state: SeeThat
        q!: [$botName] смотри это $AnyWord
        random:
            a: {{$parseTree._AnyWord}}? Интересно!
            a: Покажи мне ещё что-нибудь!
            a: {{$parseTree._AnyWord}}? Вот это да!

    state: IamCorrect
        q!: (правильно/верно) [$botName]
        random:
            a: Я не ошиблась!
            a: Абсолютно!
        go!: /StartDialog

    state: MeFragment || noContext = true
        q!: я
        random:
            a: Ты?
            a: Что - ты?

    state: FatherFragment
        q!: папа
        random:
            a: Где папа?
            a: А где папа?

        state: Here
            q: * здесь * || fromState = .., onlyThisState = true
            random:
                a: Привет, папа!
                a: Папе привет!
            go!: /StartDialog    

    state: MotherFragment
        q!: мама
        random:
            a: Где мама?
            a: А где мама?

        state: At home
            q: * дома * || fromState = .., onlyThisState = true
            q: * дома * || fromState = /FatherFragment, onlyThisState = true
            random:
                a: Люблю, когда все дома!
                a: Хорошо, когда родные дома!
            go!: /StartDialog

        state: Kitchen
            q: * кухне * || fromState = .., onlyThisState = true
            q: * кухне * || fromState = /FatherFragment, onlyThisState = true
            a: Наверное, готовит что-нибудь вкусненькое!
            go!: /StartDialog

        state: Work
            q: * работ* * || fromState = .., onlyThisState = true
            q: * работ* * || fromState = /FatherFragment, onlyThisState = true
            a: Жаль, что сегодня не выходной!
            go!: /StartDialog

        state: Here
            q: * здесь * || fromState = .., onlyThisState = true
            random:
                a: Привет, мама!
                a: Маме привет!
            go!: /StartDialog        

    state: WowFragment
        q!: ничего себе
        random:
            a: Вот это удивление!
            a: Диво дивное!
        go!: /StartDialog