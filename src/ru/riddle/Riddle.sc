require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: riddle.js

require: riddles.csv
  name = Riddles
  var = $Riddles

require: common/common.sc
  module = zenbox_common

require: answers.yaml
  var = RiddleCommonAnswers

init:

    $global.themes = $global.themes || [];
    $global.themes.push("Riddles");

    $global.$RiddleAnsw = (typeof RiddleCustomAnswers != 'undefined') ? applyCustomAnswers(RiddleCommonAnswers, RiddleCustomAnswers) : RiddleCommonAnswers;


theme: /Riddles

    state: Get || modal=true
        q!: * {(загадай/загадывай/загадывать/разгадаю/расскажи/давай/загадать/загадаем/загадаешь/загадывает/играть/поиграем/поиграть/дай/задай/хочу/загадал/загадала/заграном/загадано) * [мне] [еще] (загадк*/загадку)} *
        q: * (еще/продолжай) * || fromState = ../Get
        q!: [тогда/в] загадк*
        q: * {[ты/сам] (загадай/отгадаю/угадаю/разгадаю/(могу/умею/буду) (отгад*/разгад*/угад*)) * [загадк*]} *
        script:
            getRiddle();  
        if: !$session.fromWantMore
            random:
                a: Нет лучше зарядки для ума, чем парочка интересных и трудных загадок! Сейчас загадаю тебе парочку!
                a: Я знаю много загадок обо всем на свете! Я буду загадывать, а ты отгадывай! 
                a: Сейчас загадаю тебе загадку. Если не знаешь ответ –  просто скажи «не знаю», я тебе подскажу! Только не расстраивайся! Ведь главное в загадках – это не отгадка, а веселье. Начали.
        else:
            script:
                $session.fromWantMore = false;
        a: {Отгадай,/А, ну-ка, угадай,/} {{$session.usedRiddles.question}}
 
        state: Answer
            q: * [думаю|полагаю|[мне] кажется] [это] $Text || fromState="/Riddles/Get"
            script:
                if ($parseTree._Text.indexOf("хочу ")  > -1 || $parseTree._Text.indexOf("давай ")  > -1 || $parseTree._Text.indexOf("включи ")  > -1 || $parseTree._Text.indexOf("поиграем")  > -1 || $parseTree._Text.indexOf("поиграй ")  > -1 || $parseTree._Text.indexOf("поговори ")  > -1 || $parseTree._Text.indexOf("расскажи ")  > -1 || $parseTree._Text.indexOf("скажи ")  > -1 ||
                $parseTree._Text.indexOf("создай ")  > -1 || $parseTree._Text.indexOf("загадай ")  > -1 ||
                $parseTree._Text.indexOf("открой ")  > -1 || $parseTree._Text.indexOf("запусти ")  > -1 || $parseTree._Text.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($parseTree._Text, "/");
                    $reactions.transition(res.targetState);
                }
            if: ($parseTree.Text && $nlp.matchPatterns($parseTree._Text, $session.usedRiddles.answerPatterns))
                a: {{ selectRandomArg($RiddleAnsw["Get"]["Answer"]["if"]["a1"]) }}
                a: {{ selectRandomArg($RiddleAnsw["Get"]["Answer"]["if"]["a2"]) }} 
                script:
                    $session.rightRiddleAnswer = true;

            else:
                a: {{ selectRandomArg($RiddleAnsw["Get"]["Answer"]["else"]["a1"]) }}
                a: {{ selectRandomArg($RiddleAnsw["Get"]["Answer"]["else"]["a2"]) }}
                a: {{$session.usedRiddles.answer}}
                script:
                    $session.rightRiddleAnswer = false;
            go!: ../../WantMore

        state: StopRiddles
            q: * (хватит/не [хочу] * (загад*)/$stopGame/прекрати/остановись) *
            go!: /Riddles/StopRiddles

        state: DontKnow
            q: * ($dontKnow/следу*/какая отгадка/интересно) *
            q: (нет/что это/[ты] знаешь)
            a: {{ selectRandomArg($RiddleAnsw["Get"]["DontKnow"]["a1"]) }}
            a: {{ selectRandomArg($RiddleAnsw["Get"]["DontKnow"]["a2"]) }}
            a: {{$session.usedRiddles.answer}}
            go!: ../../WantMore

        state: Repeat the question
            q: * {(повтори*|ска*|зада*) (вопрос/загадк*) [(еще|ещё) раз] [пожалуйста|плиз]}*
            q!: * {(повтори*|ска*|загада*) ~загадка} *
            q: * (чего|что|(еще|ещё) раз) *
            q: * повтори *
            q: хорошо
            script:
                $temp.rep = true;
            if: ($session.usedRiddles)
                a: {{ selectRandomArg($RiddleAnsw["Get"]["Repeat the question"]) }}
                a: {{$session.usedRiddles.question}}
                go: ../../Get
            else:
                go!: ../../Get

        state: CallFasolikFromRiddle || noContext=true
            q: (фасолик|фасолька)
            random:
                a: Я тебя слышу.
                a: Я тут.
                a: Что-то я отвлёкся.
            a: Мы играли в загадки. Повторю, пожалуй, свою загадку. Итак:
            a: {{$session.usedRiddles.question}}
            go: ../../Get


    state: WantMore
        if: $session.rightRiddleAnswer
            script:
               $session.rightRiddleAnswer = false 
            a: {{ selectRandomArg($RiddleAnsw["WantMore"]["RightAnswer"]) }} 
        else:
            a: {{ selectRandomArg($RiddleAnsw["WantMore"]["NotRightAnswer"]) }} 


        state: Yes
            q: * ($agree/загад*/попробуй/задать/говори/какая/пожалуйста/$continue/давай загадку/спроси/заказывать) * || fromState =.., onlyThisState = true
            script:
                $session.fromWantMore = true;
            go!: ../../Get

        state: No
            q: * ($disagree/хватит/не [хочу] (загад*)/$notNow/ давай (потом/в другой/в следующий) *) * || fromState =.., onlyThisState = true
            q: * ($disagree/не [хочу] (загад*)/$notNow/ давай (потом/в другой/в следующий) *) * || fromState =/Riddles, onlyThisState = true
            go!: /Riddles/StopRiddles

        state: DontKnow
            q: * $dontKnow *
            a: Давай попробуем! Уверен, у тебя получится!
            go!: ../../Get

        state: CallFasolik || noContext=true
            q: (фасолик|фасолька)
            a: Мы хотели поиграть с тобой в загадки. Ну что, играем?
            go: ../../WantMore

        state: StopRiddles
            q: * (хватит/не [хочу] * (загад*)/$stopGame/прекрати/остановись) *
            go!: /Riddles/StopRiddles

        state: LocalCatchAll
            q: $Text
            if: $session.flagForNonsense
                script:
                    if ($parseTree._Text.indexOf("хочу ")  > -1 || $parseTree._Text.indexOf("давай ")  > -1 || $parseTree._Text.indexOf("включи ")  > -1 || $parseTree._Text.indexOf("поиграем")  > -1 || $parseTree._Text.indexOf("поиграй ")  > -1 || $parseTree._Text.indexOf("поговори ")  > -1 || $parseTree._Text.indexOf("расскажи ")  > -1 || $parseTree._Text.indexOf("скажи ")  > -1 || $parseTree._Text.indexOf("создай ")  > -1 || $parseTree._Text.indexOf("загадай ")  > -1 || $parseTree._Text.indexOf("открой ")  > -1 || $parseTree._Text.indexOf("запусти ")  > -1 || $parseTree._Text.indexOf("поставь ")  > -1) {
                        var res = $nlp.match($parseTree._Text, "/");
                        $reactions.transition(res.targetState);
                    }
                    $temp.currentGame = 'загадки';
                    $session.randomGame = getRandomGame();
                    $reactions.answer('Кажется, стало скучновато, и мы уходим от темы. Сыграем в что-нибудь другое? У меня много других полезных и интересных игр. Например, ' + $session.randomGame[0] + '. Хочешь поиграть в это?');
                    $session.flagForNonsense = false;
            else:
                script:
                    $session.flagForNonsense = true;
                    $reactions.answer('Кажется, мы отвлеклись. Я как робот считаю, что если играть, то играть по правилам. Я загадываю, ты - отгадываешь. Загадать ещё одну загадку?');
                go: ../../WantMore

            state: Yes
                q: * ($agree/загад*/попробуй/задать/говори/какая/пожалуйста/$continue/давай загадку/спроси/заказывать/не знаю) * || fromState =.., onlyThisState = true
                go!: {{$session.randomGame[2]}}

            state: No
                q: * ($disagree/хватит/не [хочу] (загад*)/$notNow/ давай (потом/в другой/в следующий) *) * || fromState =.., onlyThisState = true
                q: * ($disagree/не [хочу] (загад*)/$notNow/ давай (потом/в другой/в следующий) *) * || fromState =/Riddles, onlyThisState = true
                go!: ../../../StopRiddles

    state: StopRiddles
        q: * (хватит/не [хочу] * (загад*)/$stopGame/прекрати/остановись) *
        a: {{ selectRandomArg($RiddleAnsw["StopRiddles"]) }} 
        go:/BeanqGames

