require: LearnEnglishFairytales.js
require: ../fairyTales/fairy-tales-patterns.sc

require: LearnEnglishFairytales.csv
    var = $learnEnglishFairytales
    name = learnEnglishFairytales

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .learnEnglishFairytalesConverter = function(parseTree) {
            var id = parseTree.learnEnglishFairytales[0].value;
            return $learnEnglishFairytales[id].value;
        };

    $global.themes = $global.themes || [];
    $global.themes.push("LearnEnglishFairytales");

patterns:
    $learnEnglishFairytale = $entity<learnEnglishFairytales> || converter = $converters.learnEnglishFairytalesConverter

theme: /LearnEnglishFairytales

    state: Enter || modal = true
        q!: * {[$tellFtale] * $ftale [[о/про] $learnEnglishFairytale] * {(англ*/инглиш*) [язык*/слов*]}} *
        q!: * {$tellFtale * [$ftale] [[о/про] $learnEnglishFairytale] * {(англ*/инглиш*) [язык*/слов*]}} *
        q!: * {[$tellFtale] * [$ftale] [о/про] $learnEnglishFairytale * {(англ*/инглиш*) [язык*/слов*]}} *
        script:
            if (!$session.englishFairytales) {
                $session.englishFairytales = {
                    left: _.range(1, Object.keys($learnEnglishFairytales).length + 1)
                };
            }
        a: Давай слушать сказки и учить английские слова! И весело, и познавательно. Если захочешь остановиться и поиграть во что-то другое, просто скажи «хватит». А сейчас слушай.
        script:
            learnEnglishFairytales.nextTale();
            $response.audio = $session.englishFairytales.next.url;

        state: Next
            q: * {[давай] $continueTale * [$ftale]} *
            q: * {[$tellFtale] * [$ftale] [о/про] $learnEnglishFairytale} *
            if: ($session.englishFairytales.left.length !== 0)
                script:
                    learnEnglishFairytales.nextTale();
                    $response.audio = $session.englishFairytales.next.url;
                go: ..
            else:
                a: Это все сказки с английскими словами. Будут еще, обещаю!
                script:
                    learnEnglishFairytales.cleanUp();
                go!: /BeanqGames/Games
                # a: go!: /ChoosePlayorTalk/Start  # FIXME

        state: Repeat
            q: * {[$tellFtale] (еще (раз*/~один)/повтори*/снова) * [[эт*] $ftale]} *
            script:
                $response.audio = $session.englishFairytales.next.url;
            go: ..

        state: Last
            q: * {[$tellFtale] (~последний/~предыдущий) [$ftale]} *
            q: * {[$tellFtale] [$ftale] * (ранее/раньше/до того/до этого/перед этим)} *
            if: $session.englishFairytales.last !== undefined
                script:
                    $response.audio = $session.englishFairytales.last.url;
                go: ..
            else:
                go!: ./NoLast

            state: NoLast
                a: Кажется, это самая первая сказка. Хочешь послушать ее заново?
                q: * ($agreeStrong/$yes) * || toState = ../../Repeat

                state: No
                    q: * $disagree *
                    a: Слушай тогда такую сказку.
                    go!: ../../../Next

        state: Stop
            q: * $stopGame *
            q: * [$me] (довольно/достаточно) *
            a: Когда-нибудь мы обязательно послушаем все сказки и выучим много новых английских слов!
            script:
                learnEnglishFairytales.cleanUp();
            go!: /BeanqGames/Games
            # a: go!: /ChoosePlayorTalk/Start  # FIXME

        state: CatchAll || modal = true
            q: *
            a: Не совсем понял тебя. Будем слушать сказки и английские слова учить?
            q: * ($agree/буд*/[буд*] (слушать/учить)/включ*/рассказ*/расскаж*/*ставь) * || toState = ../Next
            q: * ($disagree/не (буд*/включ*/рассказ*/расскаж*/*ставь)) * || toState = ../Stop

            state: CatchAllAgain
                q: *
                a: Ну, совсем мы с тобой отвлеклись.
                script:
                    learnEnglishFairytales.cleanUp();
                go!: /BeanqGames/Games
                # a: go!: /ChoosePlayorTalk/Start  # FIXME

        state: ChangeTheme
            q: * {$changeThemeWords $nonEmptyGarbage} *
            q: * {$changeThemeWords $nonEmptyGarbage} * || fromState = "../CatchAll"
            script:
                learnEnglishFairytales.changeTheme();
