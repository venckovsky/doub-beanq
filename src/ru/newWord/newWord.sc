require: ../games/BeanqGames.sc
require: ../games/BeanqGames.js

require: patterns.sc
    module = zb_common
require: common.js
    module = zb_common

require: newWord.js

require: newWord34.csv
    name = newWord34
    var = $newWord34
require: newWord45.csv
    name = newWord45
    var = $newWord45
require: newWord56.csv
    name = newWord56
    var = $newWord56
require: newWord67.csv
    name = newWord67
    var = $newWord67

init:
    $global.themes = $global.themes || [];
    $global.themes.push("newWord");

patterns:
    $changeThemeWordsNewWord = (включи/поиграем/поиграй/поговори/расскажи/скажи/создай/загадай/открой/запусти/поставь)

theme: /newWord

    state: Init || modal = true
        q!: * {птица слово} *
        q!: * [хочу|давай] (узна*|выучи*) [новое] слово [дня] *
        q!: * [хочу|давай|скажи|расскажи|научи] [$me] * ~слово дня *
        q!: * [хочу|давай|скажи|расскажи|научи] [$me] * (~новый|~неизвестный) [$me] ~слово *
        q!: * [скажи|расскажи|научи] [$me] ~слово * [$me] * (не знаю|~новый|~неизвестный) *
        script:
            newWord.init();
        if: !$client.newWord
            a: Привет! Хочу тебе кое-что рассказать. Я недавно нашёл ящик. Я его открыл, и из него вылетела одна птица. Это была птица-слово. Я увидел надпись на ящике: «Слово дня». Представляешь, каждый день из него вылетает одно слово. И все, к кому оно прилетает, могут с ним познакомиться. Хочешь узнать, какое слово было сегодня?
        else:
            script:
                var prevDate = $client.newWord.lastDate;
                $client.newWord.lastDate = currentDate().format("YYYY-MM-DD");

                if ($client.newWord.lastDate !== prevDate) {
                    $reactions.transition("/newWord/Word");
                } else {
                    $reactions.transition("/newWord/UserKnows");
                }

        state: Yes
            q: * ($agree|хочу|давай) *
            script:
                delete $session.saidNo;
                delete $session.offtopic;
            go!: /newWord/Word

        state: No
            q: * ($disagree|$notNow|$dontKnow) *
            if: (!$session.saidNo)
                script:
                    $session.saidNo = true;
                random:
                    a: Точно не хочешь открыть этот загадочный ящик?
                    a: Может, все-таки откроем ящик со словами-птицами?
                go: ..
            else:
                script:
                    delete $session.saidNo;
                    delete $session.offtopic;
                a: Возвращайся, и мы вместе откроем ящик со словами-птицами. Это будет очень интересно, обещаю. Во что сыграем сейчас?
                go: /BeanqGames

        state: CatchAll
            q: * $catchAll *
            if: (!$session.offtopic)
                script:
                    $session.offtopic = true;
                a: Чур не отвлекаться. Хочешь открыть волшебный ящик и познакомиться с новым словом?
                go: ..
            else:
                script:
                    delete $session.saidNo;
                    delete $session.offtopic;
                a: Что-то мы заскучали. Птица-слово улетела. Но я знаю много интересных историй и игр. Во что хочешь поиграть сейчас?
                go: /BeanqGames

    state: Word || modal = true
        script:
            newWord.getWord();
        random:
            a: Слушай птицу-слово.
            a: Вот какое слово вылетело сегодня.
            a: Вот такая птица слово.
        a: {{ $client.newWord.word }}.
        a: Ты знаешь, что это? Что значит {{ $client.newWord.word }}?

        state: Yes
            q: * ($agree|знаю) *
            random:
                a: Здорово!
                a: Молодец!
                a: Ты умница!
            go!: /newWord/UserKnows

        state: No
            q: * ($disagree|$notNow|$dontKnow) *
            q: * что [это] (такое|значит) *
            random:
                a: Не переживай, я тебе расскажу.
                a: А вот что значит наша птица-слово.
            go!: /newWord/UserKnowsNot

        state: CatchAll
            q: $catchAll
            a: Давай каждый день возвращаться к таинственному ящику и встречать новую птицу-слово. Во что поиграем сейчас?
            go: /BeanqGames

    state: UserKnows || modal = true
        a: Давай с тобой повторим нашу птицу-слово.
        a: {{ $client.newWord.word }} – {{ $client.newWord.definition }}
        a: В следующий раз, когда захочешь узнать, какая птица-слово вылетела сегодня, скажи «Слово дня», хорошо?

        state: Again
            q: * {повтори* [$pls]} *
            q: * [скажи*] (еще/ещё) раз [$pls] *
            go!: ..

        state: Yes
            q: * $agree *
            a: Запомни эту птичку-слово и ее значение. Во что будем играть сейчас?
            go: /BeanqGames

        state: CatchAll
            q: $catchAll
            a: Возвращайся, и мы вместе откроем ящик со словами-птицами. Во что сыграем сейчас?
            go: /BeanqGames

    state: UserKnowsNot || modal = true
        a: {{ $client.newWord.word }} – {{ $client.newWord.definition }}
        a: В следующий раз, когда захочешь узнать, какая птица-слово вылетела сегодня, скажи «Слово дня», хорошо?

        state: Again
            q: * {повтори* [$pls]} *
            q: * [скажи*] (еще/ещё) раз [$pls] *
            go!: ..

        state: CatchAll
            q: $catchAll
            a: Возвращайся к нашему волшебному ящику каждый день и знакомься с новой птицей-словом. Во что будем играть с тобой сейчас?
            go: /BeanqGames

    state: ChangeTheme
        q: * $changeThemeWordsNewWord * || fromState = /newWord/Init
        q: * $changeThemeWordsNewWord * || fromState = /newWord/Word
        q: * $changeThemeWordsNewWord * || fromState = /newWord/UserKnows
        q: * $changeThemeWordsNewWord * || fromState = /newWord/UserKnowsNot
        script:
            newWord.changeTheme();
