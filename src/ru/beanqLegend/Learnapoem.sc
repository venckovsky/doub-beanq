theme: /Learning poems
    
    state: Poem information
        q!: * {[$you] (~научить/~выучить/~учить)} * 
        a: Я умею учить стихи. Просто скажи мне: давай учить стихи.

    state: No Poem learn
        q!: * [мы] не (будем/хочу/надо) учить сти* *
        random:
            a: А мне нравится учить стихи.
            a: Учить стихи полезно для памяти.
        go!: /StartDialog

    state: Poem learn || modal = true
        q!: * [давай] (~учить/~выучить/~научить/~запомнить/учи/повторяй за мной) [$you] * (стих*/стиш*/творение) *
        q!: {[$botName] слушай стих*}
        q: * давай *  || fromState = "../Poem information"
        q: стихи  || fromState = "../Poem information"
        q: * давай еще раз *  || fromState = "./Poem learning"
        q!: давай запоминать
        a: Я готова запоминать. Говори!

        state: Poem learning
            q: $Text  || fromState = .., onlyThisState = true
            q: $Text  || fromState = "../Poem learning", onlyThisState = true
            q: $Text  || fromState = "../Poem repeating", onlyThisState = true
            script:
                $temp.rep = true;
                $client.Poem = $parseTree.Text[0].value;
            a: Повторяю. {{$client.Poem}}. Всё ли правильно?

            state: Poem learning yes
                q: * ($agree/хорошо/молодец/четко) *  || fromState = .., onlyThisState = true
                q: все  || fromState = .., onlyThisState = true
                q: * ($agree/хорошо/молодец/четко) *  || fromState = "../../Poem repeating", onlyThisState = true
                q: все  || fromState = "../../Poem repeating", onlyThisState = true
                a: Отлично! Запомню. Скажи мне. Расскажи стишок. И я снова его расскажу.
                go: /Learning poems
                
            state: Poem learning no
                q: * ($disagree/почти) *  || fromState = .., onlyThisState = true
                q: * ($disagree/почти) *  || fromState = "../../Poem repeating", onlyThisState = true
                a: Не всё удалось расслышать. Если хочешь, попробуем ещё раз.
                
                state: Poem learning again
                    q: * ($agree/еще [раз]) *  || fromState = .., onlyThisState = true
                    go!: ../../../../Poem learn

                state: Poem learning not again
                    q: * [$disagree] * хватит *  || fromState = .., onlyThisState = true
                    q: $disagree [хватит] *  || fromState = .., onlyThisState = true
                    q: * [$disagree] * хватит *  || fromState = "../../Poem repeating", onlyThisState = true
                    q: $disagree [хватит] *  || fromState = "../../Poem repeating", onlyThisState = true
                    script: 
                        $client.Poem = undefined;
                    a: Сегодня я просто не в голосе.
                    go: /Learning poems
                    
                state: Poem learning another go
                    q:  $Text || fromState = .., onlyThisState = true
                    script: 
                        $client.Poem = $parseTree.Text[0].value;    
                    go!: ../../../Poem learning

            state: Poem learning no stop
                q: * [$disagree] * хватит *  || fromState = .., onlyThisState = true
                q: * [$disagree] * хватит *  || fromState = "../Poem repeating", onlyThisState = true
                go!: ../Poem learning no/Poem learning not again

            state: Poem learning once again
                q: * (еще/друго*) *
                q: * [давай] (еще/друго*) *  || fromState = "/Poem tell"
                a: Давай выучим другой
                go!: ../../../Poem learn

        state: Poem repeating  
            q: (повтори|повторяй) [ещё [раз]] || fromState = "../Poem learning", onlyThisState = true
            q: ((повтори|повторяй) ещё [раз]|ещё [раз]) || fromState = "../Poem repeating", onlyThisState = true
            script:
                $temp.rep = true;
            a: Повторяю. {{$client.Poem}}. Всё ли правильно?

    state: Poem tell
        q!: * [$botName] * {(*скажи|ты (знаешь/*учил/запомнил)|рассказать|расскажи|рассказывай|давай*|почитай|прочитай) * (стих*/стиш*)} *
        q!: читать стихи
        q!: (стихотворение/стих творени*/стишок)
        q: * повтори еще раз *  || fromState = "/Poem learn/Poem learning", onlyThisState = true  
        if: ($client.Poem)
            a: Я помню такое стихотворение. {{$client.Poem}}     
        else: 
            random:
                a: Муха села на варенье вот и все стихотворенье.
                a: Я не котик, не ребёнок,
                   Я Фасолька, роботёнок!
                    Я лечу с другой планеты
                    Поискать друзей по свету.
                    Хочешь - сказку прочитаю,
                    Хочешь - песенку спою,
                    Хочешь - в города сыграем,
                    Я ещё считать люблю!
                    Вместе весело учиться,
                    Вместе весело играть -
                    Я Фасолька, и со мною
                    Не приходится скучать!
            a: Больше я пока не знаю, но могу выучить, если ты скажешь давай учить стихи.
            go: ../Poem information