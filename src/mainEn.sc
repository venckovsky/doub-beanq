require: en/alarmTimer/AlarmTimer.sc
  module = zenbox_common

#require: en/animalGame/animalGame.sc
#  module = zenbox_common

require: en/citiesGame/CitiesGame.sc
  module = zenbox_common

require: en/geographyGame/GeographyGame.sc
  module = zenbox_common

#require: en/volumeControl/VolumeControl.sc
#  module = zenbox_common

#require: en/oddWordGame/OddWordGame.sc
#  module = zenbox_common

require: en/jokes/Jokes.sc
  module = zenbox_common

require: en/dateTime/DateTime.sc
  module = zenbox_common

require: en/mathGame/mathGame.sc
  module = zenbox_common

#require: en/radio/Radio.sc
#  module = zenbox_common

#require: en/wiki/Wiki.sc
#  module = zenbox_common

#require: en/reminder/Reminder.sc
#  module = zenbox_common

require: patternsEn.sc
  module = zb_common

require: city/cityEn.sc
  module = zb_common

require: common/common.sc
  module = zenbox_common

require: en/beanqLegend/BotProfile.sc

require: patternsEn.sc

require: en/weather/Weather.sc

require: en/animalsAndBirds/animalsAndBirds.sc

#require: en/dateTime/dateTime.sc

#require: en/fairyTales/FairyTales.sc

#require: en/songs/Songs.sc

#require: en/music/Music.sc

#require: en/healthCheck/HealthCheck.sc

#require: en/rotation/Rotation.sc

#require: en/callback/Callback.sc

#require: en/translatorRuEn/translatorRuEn.sc

#require: en/learnEnglish/learnEnglish.sc

#require: en/calendarGame/CalendarGame.sc

#require: en/animalGame/AnimalGame.sc

#require: en/calculator/Calculator.sc

#require: en/foodGame/Foodgame.sc

#require: en/geographyGame/GeographyGame.sc

#require: en/mathGame/MathGame.sc
