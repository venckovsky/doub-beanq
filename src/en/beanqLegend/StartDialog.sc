require: chatbot.js
require: ../dictionaries/hints-en.csv
    name = Hints
    var = $Hints

init:
    $global.themes = $global.themes || [];
    $global.themes.push("StartDialog");

theme: /

    state: StartDialog || noContext = true
        q: start dialog test
        script:
            $temp.startDialog = true;
            nextTheme();
        go!: /{{ $temp.nextTheme }}


    state: TellSomething
        q!: * what * [do|would] $you  * (want|like) * (know/talk/speak (about/of))
        q!: * what * $me * tell *
        q!: * (say|tell) * [$me] something *
        q!: * talk to me * [about something] *
        q!: speak
        q!: * let 's (speak|discuss|talk) * [about] something *
        script:
            $temp.TellMe = true;
            nextTheme();
        go!: /{{ $temp.nextTheme }}


    state: AskQuestion
        q!: * more questions *
        q!: * ask * [$me] * (something|question*) *
        q!: * want * question *
        q: * (next|another) question * [please]*
        script:
            $temp.Question = true;
            nextTheme();
        go!: /{{ $temp.nextTheme }}

    #если исчерпаны все темы, инициируем сброс данных сессии (заходим на 2-й круг)
    state: NothingToTalk
        script:
            $reactions.answer("We have so many things to talk about!");
            $session = {};
            
        state: ManyThemes
            q: $agree * 
            script:
                $reactions.answer("I am learning so much from you!", "The more we talk - the more things we have to discuss");
            go!: /StartDialog