var Lullabies = (function () {

    function init() {
        var $session = $jsapi.context().session;
        $session.lullaby = {
            current: null,
            currentAudio: null,
            prev: null
        };
        $session.lullabiesNumber = Object.keys($Lullabies).length;
        $session.lullabiesIds = shuffle(_.range($session.lullabiesNumber));
        $session.catchAll = 0;
        $session.started = true;
    }

    function randomSong() {
        var $session = $jsapi.context().session;
        var id = $session.lullabiesIds.pop();
        if ($session.lullaby.currentAudio == $Lullabies[id].value.audio) {
            id = $session.lullabiesIds.pop();
        }
        $session.lullaby.prev = $session.lullaby.current;
        $session.lullaby.current = $Lullabies[id].value;
        $session.lullaby.currentAudio = $Lullabies[id].value.audio;
    }

    function playSong(audio) {
        var $session = $jsapi.context().session;
        var $response = $jsapi.context().response;
        $response.stream = audio;
        $response.intent = "songs_on";
        $response.action = "musicOn";
        
        $response.replies = $response.replies || [];
        $response.replies
            .push({
                type: "playAudio",
                sound: $session.lullaby.currentAudio
            });
        //$reactions.answer("Проигрывается аудио " + $session.lullaby.currentAudio)
    }

return {
        init:init,
        randomSong:randomSong,
        playSong:playSong
    };
})();