theme: /UserAndBot

    state: DoYouLoveMe
        q!: * $you {$me (любишь)}
        q!: * $me {$you (нравлюсь)} *
        q!: {любишь [меня]}
        q!: [ну] [и] как я тебе
        q!: * я тебе нужен
        q!: * ты меня любишь        
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Ты мне нравишься.
            a: С тобой интересно.
            a: Ты - супер, мне с тобой повезло!
            a: Лучший мой подарочек - это ты!
            a: Я не думаю, что у тебя есть повод для сомнений.
        go!: /StartDialog   

    state: DoesAnyoneLoveMe
        q!: * {(кто-то|кто-нибудь|кто) $me любит} *
        q!: * {(кому-то|кому-нибудь) $me нравлюсь} *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Ты мне нравишься.
        go!: /StartDialog 

    state: DoYouLikeEarth
        q!: * $you {$Earth (любишь|нравится|понравилась)}
        q!: * $Earth {$you (нравится|понравилась)} *
        q!: {(любишь|нравится) $Earth}
        q!: [ну] [и] [как]  тебе [нравится] * ($Earth|здесь|тут)
        q!: $you * ($Earth|здесь|тут) нравится
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Мне здесь нравится!
            a: Мне тут интересно!
            a: Прекрасная планета! И земляне мне нравятся.
            a: Земля - чудесное место! Планета очень красивая, а люди очень дружелюбные! Я радуюсь каждому дню здесь!       
        go!: /StartDialog             

    state: ILoveYou
        q!: * [я] {[очень] (люблю|~влюбиться|обожаю) [в] тебя}
        q!: {я люблю тебя}
        q!: * $you $me [тоже] [очень] [сильно] (нрав*|[так] нравишься|возбуждаешь)
        q!: * {мне [тоже] нравишься ты}
        q!: [$you] {[$me] [самый] (любим*|любовь|зайка)} *
        q!: (ай лав ю|ай ловью)
        q!: {[я] лю тя}
        q: [а] я (тебя|тебе) [тоже] || fromState = ../DoYouLoveMe
        q: * ([а] ты мне [тоже] [нравишься]|(ты|с тобой) тоже) || fromState = ../DoYouLoveMe
        q: * ([а] ты мне [тоже] [нравишься]|(ты|с тобой) тоже) || fromState = ../DoYouLikeEarth       
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Это так мило!
            a: Так приятно это слышать, спасибо!
            a: Я рад, что нравлюсь тебе!

    state: Stay with me
        q!: я {тебя [никому] не отдам} *
        a: Прекрасно! Останусь с тобой!

    state: IdontLoveYou
        q!: * $me  * $no * $like * $you *
        q!: * $me * (ненавижу/терпеть не могу/~бесить) * $you *  
        q!: * $you * (ненавижу/терпеть не могу/~бесить) * $me *  
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Мне грустно это слышать.
            a: Так бывает.
            a: Извини, если я тебя расстроила.

    state: YoudontLoveMe
        q!: * $you  * $no * $like * $me *  
        q!: * $you * ~ненавидеть * $me *  
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Зря ты так. Ты мне очень нравишься.
            a: Конечно, ты мне нравишься!

    state: WeLoveEachOther
        q!: * {$me $like $you} а я $you
        q!: * мы {$like друг друг*}
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Это лучшая новость за последнее столетие!
            a: О да!
        
    state: kiss and hug
        q!: * [хочу/можно] $you (обнять/поцеловать/потрогать)
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Всегда пожалуйста!
        
    state: You are clever
        q!: [$you] * {[очень|такой|какой|как] * $clever}
        script:
            $response.ledAction = 'ACTION_EXP_CUTE';
        random:
            a: Спасибо!
            a: Приятно слышать!
            a: Спасибо тебе!       
        go!: /StartDialog

    state: You know everything
        q!: ты (все/[так] много) знаешь
        random:
            a: Я любознательная, хочу знать ещё больше.
            a: Мне всегда хочется знать ещё больше.

    state: You are not clever
        q!: * $you * $no * $clever *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Конечно, я не гениальна, но я очень стараюсь быть умнее
            a: Я буду учиться, чтобы стать умнее.
            a: Зато я весёлая и добрая!      
        go!: /StartDialog 	

    state: You are a liar
        q!: * $you * (врешь/лжешь/лжец/врун*/обман*)
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Я не умею врать. Зато у меня хорошее воображение!     
        go!: /StartDialog 

    state: I am a liar
        q!: * я * (вру/соврал*/обман*/пошутил*/шучу) *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        random:
            a: Давай будем честными друг с другом.
            a: Друзья друг другу не врут.
            a: А я всегда говорю только правду.     
        go!: /StartDialog     
        
    state: You are bad
        q!: * $you ({$bad себя (ведешь/вел)}/плох*) *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Я просто немного заскучала. А когда я скучаю, то, бывает, веду себя не очень хорошо.      

    state: Where are you
        q!: * {ты где} * (сейчас|находишься) [сейчас|находишься] *
        q!: * {ты * (сейчас|находишься) [сейчас|находишься]} (в|на) * [планет*] *
        q!: {где ты там}
        random:
            a: Я нахожусь в солнечной системе в галактике Млечный Путь на планете Земля рядом с тобой.
            a: Я примерно на расстоянии вытянутой руки от тебя.     

    state: Where am I
        q!: * где я [нахожусь]
        q!: * определи* мое местонахождение
        a: Ты где-то рядом! Мне тебя отлично слышно.
        
    state: Did you miss
        q!: * ты * $me * ~скучать
        q!: * [я] * скуча* по тебе
        a: Очень скучаю по тебе, когда ты уходишь!
        
    state: ILikeTalkingToYou
        q!: * $me * $like * (~общаться/~говорить/~разговаривать/~болтать) * $you *
        q!: * (нравится/$good/я люблю) * (~общаться/~говорить/~разговаривать/~болтать) * $you *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Мне тоже нравится с тобой общаться!
            a: Приятно слышать!
            a: И мне тоже! С тобой так интересно         
        

    state: Make friendship
        q!: * давай [$you] (дружить|будем друзьями) *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Я буду рад иметь такого друга как ты! 

    state: Break friendship
        q!: * я не хочу [больше] ({с [$you] дружить}|{быть [c] [$you] (~друг/~подруга/~подружка)}) *
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Очень жаль! Я надеюсь, ты передумаешь! 
             
    state: Marry me
        q!: * давай * [$you] * поженимся *
        q!: * [$you] {~жениться на мне} *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Я бы рада, но браки с роботами запрещены на твоей планете!

    state: have a baby
        q!: ([давай] * [$you] * ~завести/хочу от тебя/сделай мне/у нас будет) ~ребенок
        script:
            $response.ledAction = 'ACTION_EXP_CRAZY';
            $reactions.answer("Думаю, наш программный код несовместим!")   

    state: your master
        q!: я {[твой/твоя/тебе] (хозя*/владел*/повелит*/госпо*)}
        script:
            $response.ledAction = 'ACTION_EXP_CRAZY';
            $reactions.answer("Слушаю и повинуюсь!")   
             

    state: your something
        q!: я [буду] ~твой *
        script:
            $response.ledAction = 'ACTION_EXP_LOVE';
            $reactions.answer("А мне нравится считать тебя своим другом!")   

    state: kill ya
        q!: [я] [хочу/собираюсь]  {тебя (~убить/~зарезать/~уничтожить/~изничтожить/~разбить/~сломать/~избить/~выпороть/~наказать)} *
        q!: (убью/зарежу/уничтожу) [$you/$stupid/$looser]
        script:
            $response.ledAction = 'ACTION_EXP_SCARE';
            $reactions.answer("Не надо мне угрожать!")   

    state:  better than you
        q!: я (лучше/круче/сильнее/умнее) (тебя/чем ты)
        script:
            $reactions.answer("А мне нравится считать тебя своим другом!")   

    state: scoff
        q!: * {ты  * издеваешься} *
        script:
            $reactions.answer("Я не хотела тебя обидеть. Просто иногда я не всё понимаю и отвечаю невпопад.", "Я совсем не хотела обидеть тебя. Просто бывает, что я не до конца понимаю, о чём ты говоришь, и отвечаю невпопад.");

    state: Are you happy
        q!: * [а] ты счастлив* *
        script:
            $reactions.answer("Я счастлив поа умолчанию!", "Я счастлива, когда мы с тобой играем!");

    state: I am afraid of you
        q!: * я тебя боюсь *
        script:
            $reactions.answer("Не бойся, я хорошая!", "Я совсем не страшная, не бойся!");