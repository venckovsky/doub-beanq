require: ru/alarmTimer/AlarmTimer.sc

require: ru/citiesGame/CitiesGame.sc
  module = zenbox_common

require: ru/volumeControl/VolumeControl.sc

require: ru/dateTime/DateTime.sc

require: ru/radio/Radio.sc

require: ru/wiki/Wiki.sc
  module = zenbox_common

require: ru/reminder/Reminder.sc

require: ru/guessNumberGame/GuessNumberGame.sc
  module = zenbox_common

require: ru/poets/Poets.sc
  module = zenbox_common

require: ru/unitConverter/UnitConverter.sc
  module = zenbox_common

require: ru/learnAnswer/LearnAnswer.sc
  module = zenbox_common

require: ru/why/Why.sc
  module = zenbox_common

require: patterns.sc
  
require: ru/beanqLegend/BotProfile.sc

require: ru/weather/Weather.sc

# require: ru/fairyTales/FairyTales.sc

#require: ru/songs/Songs.sc

#require: ru/music/Music.sc

require: ru/rotation/Rotation.sc

require: ru/callback/Callback.sc

require: ru/translatorRuEn/translatorRuEn.sc

require: ru/learnEnglish/learnEnglish.sc

require: ru/calendarGame/CalendarGame.sc

require: ru/calculator/Calculator.sc

require: ru/foodGame/Foodgame.sc

require: ru/geographyGame/GeographyGame.sc

require: ru/mathGame/MathGame.sc

require: ru/animalsAndBirds/animalsAndBirds.sc

require: ru/opposites/Opposites.sc

require: ru/riddle/Riddle.sc

require: ru/games/BeanqGames.sc

require: ru/spaceTravel/SpaceTravel.sc

require: ru/jokes/Jokes.sc

require: ru/oddWordGame/OddWordGame.sc

require: ru/findSyllable/findSyllable.sc

require: ru/newWord/newWord.sc

require: ru/akinator/Akinator.sc

require: ru/pushkinsFairytales/pushkinsFairytales.sc

require: ru/brushTeeth/brushTeeth.sc

require: ru/bogatyris/bogatyris.sc

require: ru/fables/fables.sc

require: ru/lullabies/lullabies.sc

require: ru/LearnEnglishFairytales/LearnEnglishFairytales.sc

theme: /

    init:
        $global.routing = function($context){
        }

        $jsapi.bind({
            type: "preMatch",
            handler: function($context)
            {
                if (!$context.request.query) {
                    routing($context);
                }
           },
           path: "/",
           name: "avoidNullUtterance"
        });

        bind("postProcess", function($context) {
            var $session = $context.session, $temp = $context.temp, $request = $context.request, $parseTree = $context.parseTree, $client = $context.client;

            if ($session.contextPath && !($context.response.nlpClass.indexOf("/Volume") == 0)) {
              $session.contextHistory = [];
            }

            if($context.response.replies){
                var replies = $context.response.replies || [];
                var unitedAnswer = replies.filter(function(reply){
                    return reply.type === "text";
                }).reduce(function(unitedAnswer, current, inx, arr){
                    unitedAnswer = unitedAnswer ?  unitedAnswer + " " + current.text : current.text;
                    if (inx + 1 === arr.length) {
                        current.text = unitedAnswer;
                        return current;
                    }
                    return unitedAnswer;
                }, "");
                replies = replies.filter(function(reply){
                    return reply.type !== "text";
                });
                replies.push(unitedAnswer);
                $context.response.replies = replies;
          }
        });

theme: /Test

    state: Test || noContext = true
      script:
        log($request.query);
