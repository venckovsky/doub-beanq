function spelling(word){
    var res = "";
    var i = 0;
    while (i < word.length - 1){
        if (word[i] != " ") {
            res += capitalize(word[i]) + " - ";
        }
        i++;
    }
    res += capitalize(word[i]);
    return res;
}

function counting(startNumber, number){
    var res = "";
    if (number < 100 && number > 0) {
        for (var i=startNumber; i<number+1;i++){
            res += i + " ";
        }
    } else if (number > 100){
        for (var i=startNumber; i<101;i++){
            res += i + " ";
        }
        res += "Ох, я уже утомилась считать..."
    } else {
        res = "Я не могу посчитать до " + number;
    }
    return res;
}