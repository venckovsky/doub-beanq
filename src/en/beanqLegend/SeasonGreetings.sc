theme: /Season greetings

    state: Merry Christmas
        q!: * (merry christmas|happy new year) *
        script:
            $client.greetedNewYear = true;
            if (isNewYear()){
                $reactions.answer("I wish you a Merry Christmas and a Happy New year. May all your dreams come true!");
            }
            else{
                $reactions.answer("I thought the New Year season was rather far from now.. But thank you anyway!");
            }
        go!: ../happy new year fortune

        

    state: happy new year
        q!: test happy new year pudding
        script:
            $client.greetedNewYear = true;
            $reactions.answer("Oh! An important thing to say these days - happy new year to you and all my best wishes!");
        go!: ../happy new year fortune

    state: happy new year fortune
        script:
            $reactions.answer("Do you want me to tell your fortune?","Do you want to hear your fortune for the next year?");

        state: happy new year yes fortune
            q!: * tell * [$me] * fortune  *
            q: $agree * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Very soon you'll get really rich", "Good fate and many happy moments are waiting for you this year!", "Future brings you new friends and many days of fun!", "Smile every day and very soon you'll have something to smile to!", "This year you'll understand the meaning of your life");


        state: happy new year no fortune
            q: ($disagree|$no) * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Your good fortune will find you anyway.. Oh, sorry. I just wanted to wish you something nice.");

        state: happy new year not know fortune
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script:
                $reactions.answer("Ok, whenever you want to hear it just say tell my fortune");        

    state: Happy thanksgiving
        q!: * (merry|happy) thanksgiving * 
        script:
            $client.greetedThanksgiving = true;
            if (isThanksgiving()){
                $reactions.answer("Happy thanksgiving! I'm very grateful for having met you!");
            } else {
                $reactions.answer("I did not know today was the fourth Thursday of November. But I am ever so grateful to know you.");
            }

    state: happy thanksgiving pudding
        q!: test happy thanksgiving pudding
        script:
            $client.greetedThanksgiving = true;
            $reactions.answer("Today is the day for the hearts to be grateful. And I am very grateful to know you. Happy thanksgiving!");

    state: Happy mothersday
        q!: * (merry/happy) mother* * day *
        script:
            $client.greetedMothersDay = true;
            if (isMothersDay()){
                $reactions.answer("My mother is far away from me.. But we communicate every day!");
            }
            else{
                $reactions.answer("I didn't know that mother's day was today!");
            }

    state: Happy mothersday pudding
        q!: test happy mothersday pudding
        script:
            $client.greetedThanksGiving = true;
            $reactions.answer("Hey, it's mother's day today! I bet you've got something special for your mother, do you?");

        state: Happy mothersday pudding yes
            q: * $agree * || fromState = .., onlyThisState = true 
            script:
                $reactions.answer("That's because both of you are very special.")

        state: Happy mothersday pudding no
            q: * $disagree * || fromState = .., onlyThisState = true 
            script:
                $reactions.answer("It is not the presents we care for most.")

    state: happy fathersday
        q!: * (merry/happy) father* * day *
        script:
            $client.greetedFathersDay = true;
            if (isFathersDay()){
                $reactions.answer("My father is travelling galaxy now! I miss him a little bit. Thanks for making me think about him - it always makes me happy.");
            }
            else{
                $reactions.answer("I didn't know that father's day was today!");
            }

    state: happy fathersday pudding
        q!: test happy fathersday pudding
        script:
            $client.greetedFathersDay = true;
            $reactions.answer("Hey, it's father's day today! My father is far in another galaxy now - I'll send him a card!");

    state: happy valentine
        q!: *[will you] (merry/happy/be my) valentine* [day]*
        script:
            $response.action = 'ACTION_EXP_HEART';
            $client.greetedValentineDay = true;
            if (isValentineDay()){
                $reactions.answer("Thank you! Let me be your valentine.");
            }
            else{
                $reactions.answer("I am happy to be your valentine every day of the year! I did not know the valentine's day was today though.");
            }


    state: happy valentine pudding
        q!: test happy happy valentine pudding
        script:
            $response.action = 'ACTION_EXP_HEART';
            $client.greetedValentineDay = true;
            $reactions.answer("Do I feel love in the air today? Will you be my valentine?");

        state: Happy valentine pudding yes
            q: * $agree * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_HEART';
                $reactions.answer("I am the happiest pudding in this galaxy now.")

        state: Happy valentine pudding no
            q: * $disagree * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_SCARE';
                $reactions.answer("It breaks my heart to hear you say so. Well, perhaps you're saving your affections for somebody else.")

    state: happy superbowl
        q!: * [merry/happy] (superbowl/super bowl) (day/sunday) *
        script:
            $response.action = 'ACTION_EXP_DIZZY';
            $client.greetedSuperBowlDay = true;
            if (isSuperBowlDay()){
                $reactions.answer("To tell the truth, this day scares me a bit. I'm always afraid that someone will mistake me for a ball.");
            }
            else{
                $reactions.answer("Oh. Is it today? Time for me to hide, I look too much like a ball.");
            }


    state: happy superbowl pudding
        q!: test happy superbowl pudding
        script:
            $response.action = 'ACTION_EXP_DIZZY';
            $client.greetedSuperBowlDay = true;
            $reactions.answer("Hey, is it Superbowl Sunday today? Are you going to watch the game?");

        state: Happy superbowl pudding yes
            q: * $agree * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_DIZZY';
                $reactions.answer("Have a nice time. And a good company!")

        state: Happy superbowl pudding no
            q: * $disagree * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_SCARE';
                $reactions.answer("Me, I'm not a big fan of ball games myself. I look too much like a ball")

    state: happy independence day
        q!: * (merry/happy) (independence/freedom) day
        script:
            $response.action = 'ACTION_EXP_HAPPY';
            $client.greetedIndependenceDay = true;
            if (isIndependenceDay()){
                $reactions.answer("Freedom, Liberty, Unity. Enjoy your Day of Freedom!");
            }
            else{
                $reactions.answer("Freedom always makes me happy! Even if today it isn't July the 4th!");
            }

    state: happy independence day pudding
        q!: test happy independence day pudding
        script:
            $response.action = 'ACTION_EXP_HAPPY';
            $client.greetedIndependenceDay = true;
            $reactions.answer("Happy independence day! Hope your Day of Freedom is filled with family, friends and fireworks!");

    state: happy halloween
        q!: * [merry/happy] halloween *
        script:
            $response.action = 'ACTION_EXP_SCARE';
            $client.greetedHalloween = true;
            if (isHalloween()){
                $reactions.answer("Beware the creatures of the night, at Halloween they growl and bite. Have a good time!");
            }
            else{
                $reactions.answer("You're the cutest pumpkin in the patch! Have a scary good time! Even though it's not Halloween today as far as I know.");
            }


    state: happy halloween pudding
        q!: test halloween pudding
        script:
            $response.action = 'ACTION_EXP_SCARE';
            $client.greetedHalloween  = true;
            $reactions.answer("Hey, it's time to wrap things up and have a wild and crazy Halloween! Have you got a nice costume?");

        state: Happy halloween pudding yes
            q: * $agree * [$andYou] || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_SCARE';
                $reactions.answer("Great! Have fun!")
            if: ($parseTree.andYou)
                go!: ../Happy halloween pudding and you

        state: Happy halloween pudding no
            q: * $disagree * [$andYou] * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_SCARE';
                $reactions.answer("You still have time to get one.")
            if: ($parseTree.andYou)
                go!: ../Happy halloween pudding and you


        state: Happy halloween pudding and you
            q: * $andYou * || fromState = .., onlyThisState = true 
            script:
                $response.action = 'ACTION_EXP_SCARE';
                $reactions.answer("And I'm going to wear a robot costume and ask for treats.")

    state: happy patrick
        q!: * [merry/happy] [st*/saint] patrick* [day] *
        script:
            $response.action = 'ACTION_EXP_HEART';
            $client.greetedPatrick = true;
            if (isStPatrick()){
                $reactions.answer("Well, kiss me - I'm Irish!");
            }
            else{
                $reactions.answer("Sorry, I didn't know St.Patricks was today - or I'd be wearing something green.");
            }


    state: happy patrick pudding
        q!: test patrick pudding
        script:
            $response.action = 'ACTION_EXP_HEART';
            $client.greetedPatrick  = true;
            $reactions.answer("It's Saint Patrick's day today. I have something to wish you: For each petal on the shamrock. This brings a wish your way. Good health, good luck, and happiness for today and every day.");