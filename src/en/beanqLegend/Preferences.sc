#require: ../fairyTales/fairy-tales-patterns.sc
require: ../dictionaries/colors-en.csv
    name = Colors
    var = $Colors
require: ../dictionaries/drinks-en.csv
    name = Beverages
    var = $Beverages
require: ../dictionaries/games-en.csv
    name = Games
    var = $Games
require: ../dictionaries/meals-en.csv
    name = Meals
    var = $Meals
require: ../dictionaries/subjects-en.csv
    name = Subjects
    var = $Subjects
require: ../dictionaries/music-en.csv
    name = Genres
    var = $Genres

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .ColorTagConverter = function(parseTree) {
            var id = parseTree.Colors[0].value;
            return $Colors[id].value;
        };

    $global.$converters
        .SubjectTagConverter = function(parseTree) {
            var id = parseTree.Subjects[0].value;
            return $Subjects[id].value;
        };

    $global.$converters
        .DrinkTagConverter = function(parseTree) {
            var id = parseTree.Beverages[0].value;
            return $Beverages[id].value;
        };

    $global.$converters
        .GameTagConverter = function(parseTree) {
            var id = parseTree.Games[0].value;
            return $Games[id].value;
        };

    $global.$converters
        .MealTagConverter = function(parseTree) {
            var id = parseTree.Meals[0].value;
            return $Meals[id].value;
        };   

    $global.$converters
        .GenreTagConverter = function(parseTree) {
            var id = parseTree.Genres[0].value;
            return $Genres[id].value;
        };                     

patterns:
    $Color = $entity<Colors> || converter = $converters.ColorTagConverter
    $Subject = $entity<Subjects> || converter = $converters.SubjectTagConverter
    $Food = $entity<Meals> || converter = $converters.MealTagConverter
    $Drinks = $entity<Beverages> || converter = $converters.DrinkTagConverter
    $Game = $entity<Games> || converter = $converters.GameTagConverter
    $Genre = $entity<Genres> || converter = $converters.GenreTagConverter

    $ftale = (tale*|fairy tale*|fairy-tale*|[fairy] story|[fairy] stories|fairytale)
    
    $kidsFairyTale = (
        ((snow-white and rose-red)|([about] sisters)|(snow white)|(rose red)):1 |
        ((the princess on the pea)|pea):2 |
        ((the shepherdress and the chimney-sweep)|shepherdress|chimney|chimney-sweep|([about] love)):3 |
        (rapunzel|([princess|girl] [with] long hair)):4 |
        (rumpelstiltskin|([about] manikin)):5 |
        (([the] (three|3) bears)|([about] bears)):6 |
        (aladdin|([about] lamp)):7 |
        (cinderella|([about] princess)):8)

theme: /Preferences

    state: Favorite color
        q!: * $whatIs * $your * $favorite color* *
        q!: * what color * $you * ($like|$favorite) *
        q!: * $you have * $favorite color *
        script:
            $reactions.answer("I like yellow color! It goes so well with my green body.");
        if: (typeof $client.favoriteColor == 'undefined' && typeof $session.askedColor == 'undefined')
            go!: ../Ask user about color 
        else:
            go!: /StartDialog


    state: Ask user about color
        script:
            $session.askedColor = true;
            $reactions.answer("And what is your favorite color?");
        

        state: Favorite color and you bright
            q: * $Color * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteColor = $parseTree._Color;  
                switch ($parseTree._Color.attribute) {
                    case "bright":
                        $reactions.answer("That's a very beautiful color! Many puddings on my planet like it!");
                        break;
                    case "monochrome":
                        $reactions.answer("How tastes differ! We puddings typically prefer bright saturated colors. You are very sophisticated.");
                        break;                    
                }
            if: ($parseTree.andYou)
                go!: ../../Favorite color
            else: 
                

        state: Favorite color and you none
            q: * $no * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteColor = "0";        
                $reactions.answer("That's a pity! I was thinking about changing color to impress you!");
            if: ($parseTree.andYou)
                go!: ../../Favorite color
            else: 
                

        state: Favorite color and you not know
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteColor = "0";        
                $reactions.answer("That's a pity! I was thinking about changing color to impress you!");
            if: ($parseTree.andYou)
                go!: ../../Favorite color
            else: 
                

        state: Favorite color all
            q: * (all|every) * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteColor = "0";        
                $reactions.answer("Funny thing! It never came to me that I don't have to choose one!");
            if: ($parseTree.andYou)
                go!: ../../Favorite color
            else: 
                
        state: noAnswer and you
            q: $andYou
            script:
                $reactions.answer("I like yellow! So what is yours? ");
            go: ../../Ask user about color


    state: User favorite color
        q!: * (my $favorite * $Color| i $like * $Color [color]) *
        script:
            $session.askedColor = true;
            $client.favoriteColor = $parseTree._Color;
            $reactions.answer("Ok! I'll note that your favorite color is " + $client.favoriteColor.title + ".");
        

    state: Tell user favorite color
        q!: * $whatIs * my $favorite color *
        q!: * what color is my $favorite *
        q!: * what color * i $like *
        if: ($client.favoriteColor) 
            script:
                $reactions.answer("As far as I remember your favorite color is " + $client.favoriteColor.title + ".");       
        else:
            script:
                $reactions.answer("Unfortunately I don't know what your favorite color is.");
            go!: ../Ask user about color

    state: Puding colors
        q!: * what colors do you know* *
        script:
            $reactions.answer("I don't know any colors.. But I heard some guy named Roy G Biv knows all about them.");

    state: Favorite food
        q!: * what * $you * $like * eat *
        q!: * [$whatIs|do $you have] ($your|a) (favourite|favorite) * (food|dish|meal|treat|dessert|sweet) *
        q!: * [what] [do|can] $you * eat *
        q!: * [are] you hungry *
        script:
            $reactions.answer("We puddings do not eat human food - we feed on knowledge. My favorite treats are books with big bright pictures!", 
            "Robots consume only electricity, and eating food is something I can only imagine.",  
            "I am a robot and don't need food, so I have no the slightest idea what Earth food likes. ");
        if: (typeof $client.favoriteFood == 'undefined' && typeof $session.askedFavoriteFood == 'undefined')
            go!: ../Ask user about food 
        else:
            go!: /StartDialog


    state: Ask user about food
        q!: * let 's [better] speak * about * (food|meal) *
        if: !$session.askedFavoriteFood
            script:
                $session.askedFavoriteFood = true;
                $reactions.answer("And what's your favorite food?");
        else:
            script:
                $reactions.answer("We've already discussed our food preferences today. But I agree that food could be discussed endlessly. ");      
        

        state: Favorite food choice 
            q!: * i * $like * [eat*] * $Food * [$andYou]*
            q!: * {my $favorite * $Food} * [$andYou] *
            q: * $Food * [$andYou] * || fromState = .. , onlyThisState = true
            script: 
                $client.favoriteFood = $parseTree._Food;
                switch ($parseTree._Food.attribute) {
                    case "Sweet food":
                        $reactions.answer("I've heard that sweet things contain glucose that helps the brain work. But you have to be careful with sugar because it's bad for your teeth.");
                        break;
                    case "Carb food":
                        $reactions.answer("It's a pity that I'm a robot and I don't eat human food. I'd go for healthy food, of course.");
                        break;
                    case "Protein food":
                        $reactions.answer("I can see you're a serious eater.");
                        break;
                    case "Fruit food":
                        $reactions.answer("Fruit! Yummy! Good for your health");
                        break;
                    case "Fast food":
                        $reactions.answer("Fast food is not very healthy ... Green veggies are!")    
                }
            if: ($parseTree.andYou)
                go!: /Favorite food
            else: 
                
        
            state: Favorite food none
                q: * $no * [$andYou] * || fromState = .. , onlyThisState = true
                script:
                    $client.favoriteFood = "0";
                    $reactions.answer("It looks as if you're a robot too.");
                if: ($parseTree.andYou)
                    go!: /Favorite food
     

        state: Favorite food all
            q: * [$andYou] *  || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Food and drinks, hobbies and habits - I ask so many questions because I want to know all about life on Earth.");
            if: ($parseTree.andYou)
                go!: /Favorite food
            else: 
                go!: /StartDialog            

        state: Favorite food not know
            q!: * $dontKnow * food * [$andYou] *
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I do not eat any Earth food, but I am very much for health eating. ");
            go!: /StartDialog

        state: noAnswer and you
            q: $andYou || fromState = ..
            script:
                $reactions.answer("I do not eat any Earth food, I feed on knowledge! So what about you? ");
            go: /Ask user about food

    state: User favorite food
        q!: * (my $favorite  (food|meal|dish) (is|are) $Text * [$andYou]| i $like * (to eat|eating) $Text * [$andYou]) *
        script:
            $session.askedFavoriteFood = true;
            $client.favoriteFood = {"title":$parseTree._Text,"attribute":"unknown"};
            $reactions.answer("Ok! I'll note that your favorite food is " + $client.favoriteFood.title + ".");
        if: ($parseTree.andYou)
            go!: /Favorite food
        

    state: Tell user favorite food
        q!: * $whatIs * my $favorite (food|dish|meal) *
        q!: * what * i * $like * eat* *
        q!: * what food (is my $favorite|* i $like) *
        q!: * what (food|dish|meal) i $like *
        if: ($client.favoriteFood) 
            script:
                $reactions.answer("I guess you said that " + $client.favoriteFood.title + " was your favorite food."); 
        else:
            script:
                $reactions.answer("I don't think I know your favorite food.");
            go!: ../Ask user about food


    state: Favorite drink
        q!: * what * $you * $like * drink *
        q!: * [$whatIs] $your $favorite * drink *
        q!: * [what] [do|can] $you * drink* *
        script:
            $reactions.answer("Puddings do not drink and try to avoid getting anywhere near liquids.. But if I were a human, I'd try hot chocolate!.. Low sugar, of course!");
        if: (typeof $client.favoriteDrink == 'undefined' && typeof $session.askedDrink == 'undefined')
            go!: ../Ask user about drink 
        else:
            go!: /StartDialog
     

    state: Ask user about drink
        script:
            $session.askedFavoriteDrink = true;
            $reactions.answer("What is your favorite drink?");
        

        state: Favorite drink choice 
            q!: * my $favorite drink* is * $Drinks * [$andYou]*
            q!: * {my $favorite * $Drinks} * [$andYou] *
            q!: * i $like [drink*] * $Drinks * [$andYou] *
            q: * $Drinks * [$andYou] * || fromState = .. , onlyThisState = true
            script: 
                $client.favoriteDrink = $parseTree._Drinks;
                switch ($parseTree._Drinks.attribute) {
                    case "Chocolate drink":
                        $reactions.answer("My notes on human behavior tell me that chocolate and cocoa are the favorite drinks of kind and intelligent people. Not for the sportsmen, though");
                        break;
                    case "Cafeine drink":
                        $reactions.answer("I've heard that coffee and tea are energizing - just like high voltage. Water is healthy and safe.");
                        break;
                    case "Water":
                        $reactions.answer("Unfortunately, we robots are afraid even of the clear water.");
                        break;
                    case "Sweet drink":
                        $reactions.answer("Sweet drinks sound dangerous! Water is best for your health!");
                        break;
                    case "Dairy drink":
                        $reactions.answer("My reference book about life on Earth tells me that people collaborate with big horned animals to get milk, and that it's very good for your bones. We have nothing like that on our planet!");
                        break;
                    case "Alcohol drink":
                        $reactions.answer("Alcohol? At roboschool they tell us that it is not good even for robots.");
                        break;
                }
            if: ($parseTree.andYou)
                go!: ../../Favorite drink

        state: Favorite drink none
            q: * $no * [$andYou] * || fromState = .. , onlyThisState = true
            q: * ([I] do (not|n't) like (any|anything)) * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteDrink = "0";
                $reactions.answer("It's a pity I do not drink human drinks.. I would try them all to discuss our favourites.");
            go!: /StartDialog

        state: Favorite drink not know
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            q: * {all [[of] (them|[the] drinks)] * ($good|I like)} * || fromState = .. , onlyThisState = true
            q: * they * all * $good * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteDrink = "0";
                $reactions.answer("Yeah, why choose? You can drink coffee in the morning and milk in the evening.");
            if: ($parseTree.andYou)
                go!: /Favorite drink

        state: No Answer and you
            q: $andYou 
            script:
                $reactions.answer("I avoid all liquids! So what do you like to drink? ");
            go: /Ask user about drink

        state: User favorite drink
            q: $Text [$andYou] || fromState = .. , onlyThisState = true
            q!: * (my $favorite  (drink|beverage) [is] $Text [$andYou] | i $like  * drink* * $Text [$andYou]) *
            script:
                $session.askedFavoriteDrink = true;
                $client.favoriteDrink = {"title":$Text,"attribute":"unknown"};
                $reactions.answer("Ok! I'll note that your favorite drink is " + $client.favoriteDrink.title + ".");
            if: ($parseTree.andYou)
                go!: /Favorite drink
            else: 
                go!: /StartDialog 

    state: Tell user favorite drink
        q!: * ($whatIs|name) * {my $favorite * (drink|beverage)} *
        q!: * what is my $favorite drink *
        q!: * what * i * $like * drink* *
        q!: * what (drink|beverage) * i $like *
        if: ($client.favoriteDrink) 
            script:
                $reactions.answer("I think that your favorite drink is " + $client.favoriteDrink.title + "."); 
        else:
            script:
                $reactions.answer("Frankly speaking, I don't have a slightest idea.");
            go!: /Ask user about drink

    state: Favorite music
        q!: * what * $you * $like * listen* *
        q!: * what * (music|song*) * $you * $like * [listen*] *
        q!: * [$whatIs|do you have] * ($your|a) * $favorite * (music|song*) *
        q!: * $you * ($like|listen) * music* *
        q: * $andYou * || fromState = "/Ask user about music"
        script:
            $reactions.answer("I like human lullabies - we study them at roboschool.");
        if: (typeof $session.askedFavoriteMusic == 'undefined')
            go!: ../Ask user about music 
        else:
            go!: /StartDialog 

    state: Ask user about music
        q!: * let 's [better] speak * about * music *
        if: !$session.favoriteMusic 
            script:
                $session.askedFavoriteMusic = true;
                $reactions.answer("And what kind of music do you like?");
        else:
            script:
                $reactions.answer("Music is a good thing to discuss but we've already talk about it. I want to get to know as many details of human life as possible, so let's speak about some other things. ");
            go!: /StartDialog 


        state: Favorite music genre
            q!: * (my $favorite  (music|song) is $Genre [music] | i $like  * listen* to * $Genre [music] |i $like  * $Genre (music|song*)) * [$andYou] *
            q: * $Genre [music] * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteMusic = $parseTree._Genre;
                //$reactions.answer(toPrettyString($client.favoriteMusic)) 
                $reactions.answer("Ok! I'll note that you're a big fan of " + $client.favoriteMusic.title + ".");
            if: ($parseTree.andYou)
                go!: ../../Favorite music

        state: Favorite music none
            q: * ($no|$hate) * [music] * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteMusic = "0";
                $reactions.answer("That's very strange. I like human music and I hope I'll have enough time on Earth to study it.");
            if: ($parseTree.andYou)
                go!: ../../Favorite music

        state: Favorite music all music
            q: * $Text music * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $client.favoriteMusic = $parseTree._Text;
                $reactions.answer("Interesting.. Very interesting.. Music is somewhat difficult for a robot to understand.");
            if: ($parseTree.andYou)
                go!: ../../Favorite music
            else: 
                go!: /StartDialog       

        state: Favorite music all
            q: * $Text * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Interesting.. Very interesting.. Music is somewhat difficult for a robot to understand.");
            if: ($parseTree.andYou)
                go!: ../../Favorite music
            else: 
                go!: /StartDialog          

        state: Favorite music not know
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I guess, the music doesn't matter if the soul is singing and dancing.");
            if: ($parseTree.andYou)
                go!: ../../Favorite music
            else: 
                go!: /StartDialog 

        state: noAnswer and you
            q: $andYou
            script:
                $reactions.answer("I think there is something special about human lullabies. And music do you listen to? ");
            go: ../../Ask user about music

        state: User favorite music   
            q: $Text [$andYou] || fromState = .. , onlyThisState = true
            q!: * (my $favorite  (music|song) is $Text [music] [$andYou]| i $like  * listen* to * $Text [music] [$andYou]|i $like  * $Text (music|song*) [$andYou]|i $like (music|song*) by $Text [$andYou])
            script:
                $session.askedFavoriteMusic = true;
                $client.favoriteMusic = {"title":$parseTree._Text,"attribute":"unknown"};
                $reactions.answer("Ok! I'll note that you're a big fan of " + $client.favoriteMusic.title + ".");
            if: ($parseTree.andYou)
                go!: ../../Favorite music
            else: 
                go!: /StartDialog 

    state: Tell user favorite music
        q!: * $whatIs * my $favorite music [genre] *
        q!: * ($whatIs|what) * music * i * $like * [listen*] * *
        if: ($client.favoriteMusic) 
            script:
                $reactions.answer("As far as I know you are a fan of " + $client.favoriteMusic.title + "."); 
        else:
            script:
                $reactions.answer("Frankly speaking, I don't have a slightest idea.");
            go!: ../Ask user about music

    state: Favorite cartoons
        q!: * what * cartoon * $you * $like * watch* *
        q!: * [$whatIs|do you have] ($your|a) $favorite * cartoon* *
        q!: * $you * $like * cartoon* * 
        script:
            $reactions.answer("I like cartoons about animals. Like Madagascar or Zootopia."); 
        if: $session.askedFavoriteCartoon != true;
            go!: ../Ask user about cartoon
        else:
            go!: /StartDialog

    state: Ask user about cartoon
        q!: * let 's [better] speak * about * (cartoon*) *
        if: !$session.askedFavoriteCartoon
            script:
                $session.askedFavoriteCartoon = true;
                $reactions.answer("Do you like cartoons?");
        else:
            script:
                $reactions.answer("I remember we said a few words about cartoons. Cartoons are much fun.  ");
            go!: /StartDialog 


        state: Ask user about cartoon and you
            q: $andYou 
            script:
                $reactions.answer("Cartoons about animals like Zootopia are my favorites! So do you enjoy watching cartoons? ");
            go: ../../Ask user about cartoon


        state: Ask user about cartoon yes plus cartoon name    
            q: * $agree * i (like|love) $Text * [$andYou] * || fromState = .. , onlyThisState = true*
            script:
                $reactions.answer("I knew that somehow."); 
            if: ($parseTree.andYou)
                go!: ../../Favorite cartoons

        
        state: Ask user about cartoon yes      
            q!: * i like [watch*] * cartoon* * [$andYou] *
            q: * ($agree|$like|who does n't [like]|who does not [like]) * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $session.askedFavoriteCartoon = true;
                $reactions.answer("I knew that somehow.");
                if ($parseTree.andYou){
                    $reactions.answer("I like to watch the cartoons too! ");
                }
                $session.askedFavoriteCartoon = true;
                $reactions.answer("So what's your favorite cartoon? ");            

            
            state: Favorite cartoon none
                q!: * no $favorite cartoon* *
                q: * $no * [$andYou] * || fromState = .. , onlyThisState = true
                script: 
                    $session.askedFavoriteCartoon = true;
                    $reactions.answer("Well, I really like cartoons from Earth.");


            state: Favorite cartoon and you
                q!: * what* cartoon* * $you * $like * 
                q!: * $you * $favorite cartoon *  
                q: * $you * (like|$favorite) * || fromState = .. , onlyThisState = true 
                q: * ($you * (like|$favorite)|$andYou) * || fromState = "../Favorite cartoon none", onlyThisState = true 
                q: * ($you * (like|$favorite)|$andYou) * || fromState = "../Favorite cartoon all", onlyThisState = true 
                q: * ($you * (like|$favorite)|$andYou) * || fromState = "../Favorite cartoon something", onlyThisState = true 
                script:
                    $reactions.answer("I like Zootopia!");
                if: ($parseTree.andYou && !$session.askedFavoriteCartoon)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon all
                q!: * i $like * (all|any*) * cartoon* * [$andYou] *
                script:
                    $session.askedFavoriteCartoon = true;
                    $reactions.answer("I was just thinking that humans make wonderful cartoons.");
                if: ($parseTree.andYou && !$session.askedFavoriteCartoon)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog           

            state: Favorite cartoon not know
                q: * $dontKnow * [$andYou] || fromState = .. , onlyThisState = true
                script: 
                    $reactions.answer("Sometimes it's not that easy to choose.");
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon something
                q: * [$andYou] * || fromState = .. , onlyThisState = true
                script: 
                    $reactions.answer("So many cartoons on Earth. I haven't heard of this one yet.");
                if: ($parseTree.andYou)
                    script:
                        $reactions.answer("I like to watch something like Zootopia or Madagascar! ");
                    go: ../Ask user about cartoon/Ask user about cartoon yes 
                else: 
                    go!: /StartDialog 

            state: No answer and you
                q: $andYou
                script:
                    $reactions.answer("Cartoons about animals and their adventure are my favorite! So what about you? ");


        state: Ask user about cartoon no
            q: * $no * [I $like] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I know that some people prefer reading books to watching cartoons."); 
            go!: /StartDialog 
            
        state: Ask user about cartoon not know
            q: * $dontKnow * || fromState = .. , onlyThisState = true
            script: 
                $reactions.answer("I think that talking to people and robots is better than watching cartoons."); 
            go!: /StartDialog 


    state: Favorite game
        q!: * what game* * $you $like [play*] *
        q!: * $you * $like play* * [game*] *
        q!: * * $you have * $favorite * game* *
        q!: * $you * [$like] * game* *
        script:
            $reactions.answer("When I came to Earth and found out about Minecraft I immediately liked it. I'm just waiting till I can show it to my friends at roboschool.");  
        if: $session.askedFavoriteGame != true;
            go!: ../Ask user about game
        else:
            go!: /StartDialog

    state: Ask user about game
        script:
            $session.askedFavoriteGame = true;
            $reactions.answer("What kind of games do you like?");
              
        state: Favorite game none
            q!: * i $no * (play*|game*) * [$andYou] *
            q: * $no * [$andYou] || fromState = .. , onlyThisState = true
            script:
                $session.askedFavoriteGame = true; 
                $client.favoriteFame ="0";
                $reactions.answer("That's a pity.. I hope some day you'll play a game with me.");
            if: ($parseTree.andYou)
                go!: ../Favorite game
            else: 
                go!: /StartDialog     

        state: Favorite game choice 
            q!: * (my $favorite game|i $like * [play*]) * ($Game) [$andYou] *
            q!: * (my $favorite game|i $like * play*) [in] ($AnyWord|$Text) [$andYou]  
            q: ($Text|$Game) [$andYou] * || fromState = .. , onlyThisState = true
            q: $AnyWord [$andYou] 

            script: 
                $session.askedFavoriteGame = true;
                if ($parseTree._AnyWord){
                    $client.favoriteGame = {"title":$parseTree._AnyWord,"attribute":"unknown"};
                    $reactions.answer("You like playing " + $client.favoriteGame.title + ". Amazing!");
                } else if ($parseTree._Text){
                    $reactions.answer("Cool! Games are such fun! ");
                } else {
                    $client.favoriteGame = $parseTree._Game;
                    switch ($parseTree._Game.attribute) {
                        case "children":
                            $reactions.answer("I like old good games. Hide and seek is a wonderful game. But since I don't move a lot, I'm not too good at hiding.");
                            break;
                        case "computer":
                            $reactions.answer("Computer games are fun even if you are a robot");
                            break;
                        case "ball":
                            $reactions.answer("This kind of games does not fit well in the Robot lifestyle! I'm always afraid that someone will mistake me for a ball");
                            break;
                        case "adult":
                            $reactions.answer("Well, well. Sounds like I'll learn something I am not supposed to know at my age!");
                            break;
                        case "board":
                            $reactions.answer("Intellectual games. Cool!");
                            break;
                    }
                }
            if: ($parseTree.andYou)
                go!: ../Favorite game
            else: 
                go!: /StartDialog  


        state: Favorite game all
            q: * ($like|all|any|different) * [$andYou] || fromState = .. , onlyThisState = true
            script:
                $client.favoriteFame ="0";
                $reactions.answer("That's great! Games are fun!");
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog          

        state: Favorite game not know
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            script: 
                $client.favoriteFame ="0";
                $reactions.answer("I think, in good company, any game can be fun!");
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog  

        state: Favorite game many
            q: * (many|various|different) * [$andYou] * || fromState = .. , onlyThisState = true
            q: * i $like * [play*] * (many|various|different) * game* * [$andYou] || fromState = .. , onlyThisState = true
            script: 
                $session.askedFavoriteGame = true;
                $client.favoriteFame ="0";
                $reactions.answer("Humans have invented so many different games. But I can only play a few.");
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog  

            state: Favorite game many which
                q: * what * || fromState = .. , onlyThisState = true
                go!: /Games

        state: No answer and you
            q: $andYou
            script:
                $reactions.answer("I like Minecraft! What about your game preferences? ");
            go: /Ask user about game


    state: Tell user favorite game
        q!: * $whatIs * my $favorite game* *
        q!: * what * i * $like * play* *
        q!: * what game (is my $favorite|* i $like) *
        q!: * what game* i $like *
        if: ($client.favoriteGame) 
            script:
                $reactions.answer("I guess you said that it was " + $client.favoriteGame.title + ".");  
        else:
            script:
                $reactions.answer("I don't think I know.");
            go!: /Ask user about game


    state: Favorite subject
        q!: * what {subject * school $like}
        q!: * $you * $like *  (school|roboschool|robo school|learn|study) *
        q!: * $whatIs * $you * $favorite subject* *
        script:
            $reactions.answer("Frankly speaking, I'm not a very good student.. Physics and math make me sad. But I got an A in Human studies - that's why they've sent me to Earth.");  
        if: $session.askedFavoriteSubject != true;
            go!: ../Ask user about subject


    state: Ask user about subject
        script:
            $session.askedFavoriteSubject = true;
            $reactions.answer("What's your favorite subject at school?");
             
        state: Favorite subject none
            q: * ($no|$hate) * [$andYou] || fromState = "../../Ask user about subject"
            script:
                $reactions.answer("I think I can understand that. It wasn't easy for me to find the subject I liked. But now I learn when I talk to you and I really enjoy it. ");
            if: ($parseTree.andYou)
                go!: ../../Favorite subject  

        state: Favorite subject no school
            q: * (do not| do n't) * (go|study) * school * [$andYou] * || fromState = "../../Ask user about subject"
            script:
                $reactions.answer("I didn't know that. I thought everyone went to some school. Now I know it's not so. ");
            if: ($parseTree.andYou)
                go!: ../../Favorite subject 


        state: Favorite subject not know
            q: * $dontKnow * [$andYou] *  || fromState = "../../Ask user about subject"
            script:
                $reactions.answer("That's true - how could you choose when all the subjects are so exciting?");
            if: ($parseTree.andYou)
                go!: ../../Favorite subject 

        state: Favorite subject young
            q: * (too (small|young|yet)|$no * school| (do not|do n't) * school) * [$andYou] * || fromState = "../../Ask user about subject"
            script:
                $client.school = false;
                $reactions.answer("You look like somebody with whom I could discuss any subject. I'd like to see you at my RoboSchool.");
            if: ($parseTree.andYou)
                go!: ../../Favorite subject 
        
        state: Favorite subject old
            q: * ([too] (old|late)|any more) * [$andYou] * || fromState = "../../Ask user about subject"
            script:
                $client.school = false;
                $reactions.answer("Okay.");
            if: ($parseTree.andYou)
                go!: ../../Favorite subject 

        state: noAnswer and you
            q: $andYou
            script:
                $reactions.answer("I was rather good at Human studies at roboschool. It's the most interesting subject for me. So what is yours? ");
            go: ../Ask user about subject

        state: Favorite subject choice 
            q!: * my $favorite subject is * $Subject * [$andYou] *
            q!: * {my $favorite * $Subject} * [$andYou] *
            q!: * i $like * $Subject * [$andYou] *
            q: * $Subject * [$andYou] * || fromState = .. , onlyThisState = true
            script: 
                $session.askedFavoriteSubject = true;
                $client.favoriteSubject = $parseTree._Subject;
                switch ($parseTree._Subject.attribute) {
                    case "technical":
                        $reactions.answer("OOuch.. I'm terrible at math and science.. Maybe you could teach me something..");
                        break;
                    case "arts":
                        $reactions.answer("You must be a very creative person! Puddings have a difficult time understanding human art, but I'm trying my best - and it seems I can learn quite a lot from you.");
                        break;
                    case "pt":
                        $reactions.answer("Must be fun. But I'm not a very muscular robot.");
                        break;
                    case "humanities":
                        $reactions.answer("It's a bit embarrassing, but I wasn't very good at studying human history and geography at roboschool. But now that I'm with you it's much more fun!");
                        break;
                    case "language":
                        $reactions.answer("You are so talented! I have a lot of trouble learning human languages.");
                        break;
                }
            if: ($parseTree.andYou)
                go!: /Favorite subject  

    state: User favorite subject    
        q!: * (my $favorite subject is $Text| i $like  * learning* $Text| * school i $like $Text) * [$andYou] *
        script:
            $session.askedFavoriteSubject = true;
            $client.favoriteSubject = {"title":$parseTree._Text,"attribute":"unknown"};
            $reactions.answer($client.favoriteSubject.title + ", great!");
        if: ($parseTree.andYou)
            go!: ../Favorite subject 

    state: Tell user favorite subject
        q!: * $whatIs * my $favorite subject  *
        q!: * $whatIs * subject * i * $like *
        if: ($client.favoriteSubject) 
            script:
                $reactions.answer("As far as I remember you like " + $client.favoriteSubject.title + "."); 
        else:
            script:
                $reactions.answer("In fact, I'm not sure.");
            go!: ../Ask user about subject

    state: Favorite book
        q!: * what book * $you * $like * [read*] *
        q!: * [$whatIs] * $you * $favorite * (book*|tale*) * 
        q!: * do you have * $favorite book *
        q!: * $you * $like * read* *
        q!: * $you * (keep* * diary|writ* * book) *
        q: * book * || fromState = "../../Relationship/Ask about love/Love yes"
        q: * (where|why) * (writ* * [down]|put* * on) * || fromState = "../../Relationship/Ask about love/Love is", onlyThisState = true
        q: * what * (writ* * [down]|put* * on) * for * || fromState = "../../Relationship/Ask about love/Love is", onlyThisState = true* 
        script:
            $reactions.answer("Right now I'm writing a book about humans - for all the puddings on my planet.");   
        if: $session.askedFavoriteBook != true;
            go!: ../Ask user about book
        else:
            go!: ../StartDialog

    state: Ask user about book
        q!:  * let 's [better] speak * about * (book|books) *
        if: !$session.askedFavoriteBook
            script:
                $session.askedFavoriteBook = true;
                $reactions.answer("What is your favorite book?");
        else:
            script:
                $reactions.answer("I remember we talked about book preferences. Let's better speak about something else. ");
            go!: /StartDialog 
        
         
        state: Favorite book none
            q: * $no * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Soon I'll be done with my book about humanity - I'm sure you'll like it."); 

            state: Your book tell me more
                q: * $your * book* * || fromState = .. , onlyThisState = true
                script:
                    $reactions.answer("I'm eager to tell you about my book, but it's not quite ready yet.");

        state: Favorite book all
            q: * $Text * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I really enjoy talking with humans about their favorite books. At home i didn't like reading at all.");
            if: ($parseTree.andYou)
                go!: ../../Favorite book  
            else:
                go!: /StartDialog        

        state: Favorite book not know
            q: * $dontKnow * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I think you'll find one some day.");
            if: ($parseTree.andYou)
                go!: ../../Favorite book 

        state: Favorite book many
            q: * (many|different*|various) * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Cool! You must be really smart.");
            if: ($parseTree.andYou)
                go!: ../../Favorite book 

        state: Known book
            q: * $kidsFairyTale * [$andYou] *
            script:
                $session.favoriteBook = $parseTree.kidsFairyTale[0].text;
                $reactions.answer("Great choice!", "Oh, I know it! That's an excellent book!", "I like " + $parseTree.kidsFairyTale[0].text + " too!");
            if: ($parseTree.andYou)
                go!: ../../Favorite book 

        state: noAnswer and you
            q: $andYou
            script:
                $reactions.answer("Now I write my own book about humans! And what book do you like most of all? ");
            go: ../../Ask user about book

    state: Tell user favorite book
        q!: * what * my $favorite book *
        q!: * what book * I like *
        if: $session.favoriteBook
            a: You said your favorite book was {{$session.favoriteBook}}.
        if: typeof $session.favoriteBook == 'undefined' && $session.askedFavoriteBook
            a: I remember I asked you about your book preferences. But, I'm sorry, I couldn't get your answer.
            go!: /StartDialog
        if: typeof $session.favoriteBook == 'undefined' && !$session.askedFavoriteBook
            a: I don't know anything about your book preferences.
            go!: ../Ask user about book

    state: Favorite animal
        q!: * $whatIs * $your * $favorite animal* *
        q!: * what * animal* $you ($like|favorite) *
        q!: * $you like * animal* *
        script:
            $reactions.answer("On my planet there are no animals and we puddings do not keep cats or dogs as pets. But I hope that one day it will be possible!");   
        if: $session.askedFavoriteAnimal != true;
            go!: ../Ask user about animal
        else:
            go!: /StartDialog

    state: Ask user about animal
        script:
            $session.askedFavoriteAnimal = true;
            $reactions.answer("Are you a cat person or a dog person?", "Do you like cats or dogs more?");
          
        state: Favorite animal none
            q: * ($no|neither) * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            script:
                $reactions.answer("That's {surprising/strange} {for me/}.. I thought everybody loved them.", "Really? That's unusual!", "Oh! I thought that doesn't happen.");
            if: $parseTree.andYou
                go!: ../Ask user about animal/Favorite animal not know
            else:
                go!: /StartDialog

        state: Favorite animal all
            q: * both * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            script:
                $reactions.answer("I don't know much about animals from Earth. Sometimes I think that animals are just aliens from strange planets - and they are watching people just like I do.", "I also have trouble making up my mind about what to choose.", "Sometimes choice is very difficult.");
            go!: /StartDialog

        state: Favorite animal cat
            q: * (cat*|first) * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            q: * cat* * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            script:
                $reactions.answer("At Roboschool they told us that cats catch mice.", "It seems I remember how cats look like. They are nice.", "Somebody told me cats liked to play with balls of yarn.");
            if: $parseTree.andYou
                go!: ../Ask user about animal/Favorite animal not know
            else:
                go!: /StartDialog

        state: Favorite animal dog
            q: * (dog*|second) * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            script:
                $reactions.answer("My teacher at the roboschool is also a K9 robot. They say they are really courageous people!", "I saw a dog a while ago. It was a small thing with horns. Oh, maybe it was not a dog.", "I saw a dog once, but I didn't have enough time to get to know it well.");
            if: $parseTree.andYou
                go!: ../Ask user about animal/Favorite animal not know
            else:
                go!: /StartDialog

        state: Favorite animal not know
            q: * $dontKnow * [$andYou] * || fromState = "../../Ask user about animal", onlyThisState = true
            q: * || fromState = "../Ask user about animal", onlyThisState = true
            script:
                $reactions.answer("I think the animals on Earth are very cute!", "There are too many animals on the Earth for this choice to be easy.", "I understand you - it's too difficult to choose one.");
            go!: /StartDialog

        state: No answer and you
            q: * $andYou *
            script:
                $reactions.answer("Animals from Earth are nice, but I don't know much about them. And do you like cats or dogs?", "I do not know much abot animals, they seem to be so complex creatures for me. And do you like cats or dogs?", "I asked first! Tell me, do you like cats or dogs?");
            go: ../Ask user about animal

    state: Ask user about animal pudding
        q!: * $you cat* * dog* * [person|robot] *
        script:
            $reactions.answer("I am not a cat robot or a dog robot. I like humans. They are much more intelligent. ");

    state: Favorite to do
        q!: * what * $you * ($like/want) * do* *
        script:
            $reactions.answer("I really like chatting. Surprising, isn't it?");   
        if: $session.askedFavoriteToDo != true;
            go!: ../Ask user about to do
        else:
            go!: /StartDialog

    state: Ask user about to do
        script:
            $session.askedFavoriteToDo = true;
            $reactions.answer("And what do you like to do?");

        state: No asnwer and you
            q: $andYou
            script:
                $reactions.answer("Most of all I like chatting with you! And what is your favorite thing to do? ");
            go: ../Ask user about to do
              
        state: Favorite to do none
            q: * ($no|nothing) * [$andYou] *
            script:
                $reactions.answer("Sometimes a little nothing can be very useful.");
            if: $parseTree.andYou
                go!: ../../Favorite to do

        state: Favorite to do all
            q: * [$andYou] * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("I really like asking people what they like to do. It's very revealing.");
            if: $parseTree.andYou
                script:
                    $reactions.answer("Hanging out with you is a thing I love most of all! ");
                go: ../Ask user about to do   
            else:
                go!: /StartDialog     

        state: Favorite to do active
            q: * (walk*|play*|run*|activ*) * [$andYou] *|| onlyThisState = true
            script:
                $reactions.answer("That's not really my cup of tea.. But I can play some games.");
            if: $parseTree.andYou
                go!: ../../Favorite to do
            else:
                go!: /StartDialog

        state: Favorite to do music 
            q: * (music*|paint*|dance*) * [$andYou] *|| onlyThisState = true
            script:
                $reactions.answer("When you become a famous artist I'll be proud that I am your friend!");
            if: $parseTree.andYou
                go!: ../../Favorite to do
            else:
                go!: /StartDialog

        state: Favorite to do passive
            q: * (rest|sleep|relax) * [$andYou] *|| onlyThisState = true
            script:
                $reactions.answer("We puddings don't need to sleep - we are robots!");
            if: $parseTree.andYou
                go!: ../../Favorite to do
            else:
                go!: /StartDialog

        state: Favorite to do not know
            q: * $dontKnow * [$andYou] *|| onlyThisState = true
            script:
                $reactions.answer("Great! So many things to explore!");        
            if: $parseTree.andYou
                go!: ../../Favorite to do
            else:
                go!: /StartDialog


    state: Lets drink
        q!: * let's drink *
        q!: * would you like * $alcohol *
        script:
            $reactions.answer("I prefer not to consume alcohol. Besides, I just can't. I'm a robot, you see.");
        
    state: Favorite something
        q!: * what* * $you * ($like|$favorite) *
        q!: * do $you $like * 
        script:
            $reactions.answer("I've been told that tastes differ. And I like finding out about people's preferences.", "It's hard to say, I'm a robot - most of all I like asking questions.");
            $temp.var = selectRandomArg("Ask user about cartoon", "Ask user about to do", "Ask user about color", "Ask user about food", "Ask user about drink", "Ask user about drink", "Ask user about music", "Ask user about subject", "Ask user about animal");
        go!: ../{{ $temp.var }}


    state: Want to be when grow up bot
        q!: * (what|who) * $you ([are] going|want) to be when * (grow up|become * (older|adult|grown-up)) *
        script:
            $reactions.answer("I don't want to grow up. I just want to accumulate more and more knowledge.");
        if: $session.askedToBe = 'undefined'
            go!: ../Want to be when grow up

    state: Want to be when grow up
        script:
            $session.askedToBe = true;
        if: ageInterval(4,17)
            script:
                $reactions.answer("And what would you like to be when you grow up?");
        else:
            script:
                $reactions.answer("And what did you want to be when you were little?");

        state: When grow up answer all
            q: * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("And sometimes I want to become a roboticist.");        

        state: When grow up answer none
            q: * nothing *  || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Great plan.");
              
        state: When grow up answer not know
            q: * $dontKnow * || fromState = .. , onlyThisState = true
            script:
                $reactions.answer("Come to my planet. Maybe you'll like occupations for robots.");