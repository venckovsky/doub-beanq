require: ../fairyTales/fairy-tales-patterns.sc
require: colors-ru.csv
    name = Colors
    var = $Colors
require: drinks-ru.csv
    name = Beverages
    var = $Beverages
require: games-ru.csv
    name = Games
    var = $Games
require: meals-ru.csv
    name = Meals
    var = $Meals
require: subjects-ru.csv
    name = Subjects
    var = $Subjects

init:
    if (!$global.$converters) {
        $global.$converters = {};
    }

    $global.$converters
        .ColorTagConverter = function(parseTree) {
            var id = parseTree.Colors[0].value;
            return $Colors[id].value;
        };
    $global.$converters
        .SubjectTagConverter = function(parseTree) {
            var id = parseTree.Subjects[0].value;
            return $Subjects[id].value;
        };
    $global.$converters
        .DrinkTagConverter = function(parseTree) {
            var id = parseTree.Beverages[0].value;
            return $Beverages[id].value;
        };
    $global.$converters
        .GameTagConverter = function(parseTree) {
            var id = parseTree.Games[0].value;
            return $Games[id].value;
        };
    $global.$converters
        .MealTagConverter = function(parseTree) {
            var id = parseTree.Meals[0].value;
            return $Meals[id].value;
        };                   

patterns:
    $Color = $entity<Colors> || converter = $converters.ColorTagConverter
    $Subject = $entity<Subjects> || converter = $converters.SubjectTagConverter
    $Meal = $entity<Meals> || converter = $converters.MealTagConverter
    $Drinks = $entity<Beverages> || converter = $converters.DrinkTagConverter
    $Game = $entity<Games> || converter = $converters.GameTagConverter

theme: /Preferences

    state: Favorite color
        q!: * (какой|назови) * $you * любим* цвет* *
        q!: * как* цвет* * $you * (люб*|нрав*) *
        q!: * $you [есть] любим* (цвет|оттенок) *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Мой любимый цвет - ярко-желтый! Он отлично подходит к моему зеленому корпусу!
        if: ($client.favoriteColor)
        else:
            go!: ../Ask user about color 

    state: Ask user about color
        q!: тест любимый цвет
        script:
            $session.askedFavoriteColor = "true";
        a: А какой цвет ты любишь?
            
        state: Fragment
            q: я [люблю] || fromState = .., onlyThisState = true
            random:
                a: Прости, не расслышала. Какой цвет?
                a: Не услышала. Какой цвет?
            go: /Ask user about color

        state: AndYou
            q: * $andYou || fromState = .., onlyThisState = true
            go!: ../../Favorite color

        state: Favorite color and you bright
            q: * $Color * [$andYou] || fromState = .., onlyThisState = true
            script:
                $client.favoriteColor = $parseTree._Color;
                $temp.res = '';
                switch ($parseTree._Color.attribute) {
                    case "яркий":
                        $temp.res = "Очень красивый цвет! Многим пудингам на моей планете он тоже очень нравится!"
                        break;
                    case "монохромный":
                        $temp.res = "Какие интересные вкусы у людей! Пудингам обычно нравятся яркие цвета - но теперь я присмотрюсь повнимательнее и к другим."
                        break;                    
                }
            if: ($temp.res != "")
                a: {{$temp.res}}
            if: ($parseTree._Color.title == "жёлтый")
                a: Надо же!
                a: {{alias()}}
                random:
                    a: Это и мой любимый цвет!
                    a: Я, например, тоже люблю жёлтый.
            if: ($parseTree.andYou)
                go!: ../../Favorite color

        state: Favorite color and you none
            q: * (никакой|нет) * [$andYou] || fromState = .., onlyThisState = true
            script:
                $client.favoriteColor = "0";  
                $response.ledAction = 'ACTION_EXP_SAD';
            a: Даже немного жаль! Я уж думала о том, чтобы перекраситься в твой любимый цвет.
            if: ($parseTree.andYou)
                go!: ../../Favorite color

        state: Favorite color and you not know
            q: * $dontKnow * [$andYou] || fromState = .., onlyThisState = true
            script:
                $client.favoriteColor = "0";        
            a: И правда - все цвета хороши, зачем выбирать!
            if: ($parseTree.andYou)
                go!: ../../Favorite color

        state: Favourite color undefined
            q: *ый [$andYou] || fromState = .., onlyThisState = true
            q: *ий [$andYou] || fromState = .., onlyThisState = true
            q: цвет * [$andYou] || fromState = .., onlyThisState = true
            q: * цвет [$andYou] || fromState = .., onlyThisState = true
            script:
                $client.favoriteColor = "0";        
            a: Никогда не слышала о таком цвете, наверное, он очень красивый.
            if: ($parseTree.andYou)
                go!: ../../Favorite color

        state: User favorite color
            q!: * (мой любимый цвет * $Color|я (люблю|предпочитаю) $Color [цвет]) * [$andYou] *
            script:
                $client.favoriteColor = $parseTree._Color;
            a: Хорошо! Я запомню, что твой любимый цвет - {{$client.favoriteColor.title}}.
            if: ($parseTree.andYou)
                go!: ../../Favorite color

    state: Tell user favorite color
        q!: * (назови|скажи|какой) * $me любим* цвет *
        if: ($client.favoriteColor)
            a: Насколько я помню, твой любимый цвет - {{$client.favoriteColor.title}}.
        else:
            a: К сожалению, я не знаю, какой цвет - твой любимый.

    state: Puding colors
        q!: * какие ты знаешь цвет* *
        a: Никаких не знаю.. Знаю только, что Каждый Охотник Желает Знать Где Сидит Фазан.

    state: Favorite food
        q!: * {что $you} * {любишь (есть|из еды|кушать) * [(больше|cильне*) всего]} *
        q!: * {что $you} * (больше|cильне*) * всего (люб*|нрав*) * (есть|из еды|кушать) *
        q!: * (как*|назови) * $you * любим* (блюдо|еда) *
        q!: * как* (ед*|пищ*|блюд*) $you (любишь|предпочитаешь|нравится) *
        q!: * [у тебя есть] [$you|сво*] любим* (блюд*|еда) *
        q!: * $you (люб|нравится) (человеч*|земн*|обычн*) * еда *
        q!: * что $you [люб|нравится] (~есть/кушаешь/кушаете) * 
        q!: * [что] $you (~еcть|~питаться) *
        q!: * {(давай|пойдем|хочешь|будешь|хочется) (есть|поесть|* (*кушать|поедим))} *
        q!: * приятного аппетита *
        q!: * $you * {будешь есть} *
        q!: * ты [уже] (поел/кушал/покушад)
        q!: * (покушай/кушай) *
        q!: {[$botName] [ты] [что] [будешь] (кушать/завтракать/обедать/ужинать)}
        random:
            a: Мы, пудинги, не едим обычную еду - мы питаемся знаниями. Я больше всего люблю книжки с картинками!
            a: Пуддинги питаются знаниями вместо обычной еды. Обожаю книжки с картинками!
        if: $session.askedFavoriteFood
            go!: /StartDialog
        else:        
            go!: ../Ask user about food 

    state: Ask user about food
        q!: тест любимое блюдо
        script:
            $session.askedFavoriteFood = true;
        a: А какое у тебя любимое блюдо?
        a: {{alias()}}
        
        state: Favorite food choice 
            q!: * [я] * люблю * [есть*] * $Meal * [$andYou] *
            q!: * {(мое|меня|моя) ~любимый (блюдо/еда) * $Meal} * [$andYou] *
            q: * {(мое|меня|моя) ~любимый [блюдо/еда] * $Meal} * [$andYou] *
            q!: * {[я] люблю есть $AnyWord} * [$andYou] *
            q!: * {(мое|меня|моя) ~любимый (блюдо/еда) $AnyWord} * [$andYou] *
            q: * {(мое|меня|моя) ~любимый [блюдо/еда] $AnyWord} * [$andYou] *
            q: * $Meal * [$andYou]
            script: 
                $session.askedFavoriteFood = true;
                $temp.res = "";
                if($parseTree.Meal) {
                    $client.favoriteFood = $parseTree._Meal;
                    switch ($parseTree._Meal.attribute) {
                        case "Sweet food":
                            $temp.res = "Мой справочник по здоровому образу жизни почему-то ругает сладкое.";
                            break;
                        case "Carb food":
                            $temp.res = "Жалко что я робот и не могу попробовать земную еду.";
                            break;
                        case "Protein food":
                            $temp.res = "Я смотрю, ты любишь серьезно подкрепиться.";
                            break;
                        case "Fruit food":
                            $temp.res = "Фрукты, здорово!";
                            break;
                        case "Milk food":
                            $temp.res = "В молочных продуктах много кальция, они помогают расти.";
                            break;
                        case "Vegetables":
                            $temp.res = "Овощи - моя любимая еда!";
                            break;
                    }
                } else {
                    $client.favoriteFood = $parseTree.AnyWord;
                    $temp.res = "Вот это да! Я бы не догадалась.";
                }
            if: ($temp.res != "")
                a: {{$temp.res}}
            if: ($parseTree.andYou)
                go!: ../../Favorite food
                
        state: Favorite food none
            q: * $no * [$andYou] || fromState = .., onlyThisState = true
            script:
                $client.favoriteFood = "0";
            a: Наверное, в чем-то ты немного похож на пудингов. Может быть, ты тоже робот
            if: ($parseTree.andYou)
                go!: ../../Favorite food
            
        state: Favorite food all
            q: * [$andYou] || fromState = .., onlyThisState = true    
            script:
                $response.ledAction = 'ACTION_EXP_CUTE';
            a: Может быть, тебе кажется, что я задаю слишком много вопросов, но у нас, пудингов, любопытство не считается пороком.
            if: ($parseTree.andYou)
                go!: ../../Favorite food

        state: Favorite food not know
            q!: * не знаю * (что|как*) * (блюд*|ед*) * люб* * [$andYou]*
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true    
            q: * (не скажу|не твое дело|почему ты спрашиваешь) * [$andYou]* || fromState = .., onlyThisState = true    
            q: * [$andYou] * || fromState = .., onlyThisState = true    
            q: * (разн*|всяк*|когда как|зависит) * [$andYou]* || fromState = .., onlyThisState = true
            if: ($parseTree.andYou)
                go!: ../../Favorite food
            else:
                a: Еда, напитки, привычки, хобби.. Мне нужно все знать о том, как живут земляне. Для моей робошколы.

    state: Tell user favorite food
        q!: * (назови|скажи|какой) * $me любим* (блюд*|ед*) *
        q!: * что я люблю (есть|кушать)
        if: $client.favoriteFood
            a: Из нашего разговора я помню, что твое любимое блюдо - {{$client.favoriteFood.title}}.
        else:
            a: Увы.. Кажется, мы об этом еще не разговаривали.

    state: Favorite drink
        q!: * что $you (любишь|нравится) пить * [(больше|сильнее) всего] *
        q!: * что $you (больше|cильне*) * всего (нравится|любишь) [пить] *
        q!: * (как*|назови) * $you * любим* напиток *
        q!: * (как*|назови) * напиток * $you * любим* *
        q!: * [у тебя есть] [$you|сво*] любим* напиток *
        a: Пудинги не умеют пить и есть земную пищу.. Но я всегда мечтала попробовать горячий шоколад!
        go!: ../Ask user about drink 

    state: Ask user about drink
        script:
            $session.askedFavoriteDrink = true;
        a: А какой напиток тебе нравится?

        state: Favorite drink none
            q: * (никак*|нет) * [$andYou] || fromState = .., onlyThisState = true
            a: Жаль, что пудингам не годятся человеческие напитки.. А то я бы точно нашла себе любимый.
            if: ($parseTree.andYou)
                go!: ../Favorite drink

        state: Favorite drink not know
            q: * $dontKnow * [$andYou] || fromState = .., onlyThisState = true
            a: И правда, ни к чему выбирать один любимый напиток, когда можно утром пить чай, а вечером - молоко.
            if: ($parseTree.andYou)
                go!: ../Favorite drink

        state: Favorite drink and you
            q: * $andYou * || fromState = .., onlyThisState = true
            a: Кажется, ты отвечаешь вопросом на вопрос..
            a: {{alias()}}
            go!: ../../Favorite drink

        state: Favorite drink choice 
            q!: (мой любим* напиток| я [больше] [всего] люблю [пить]) * $Drinks * [$andYou] *
            q: * $Drinks * [$andYou]  * || fromState = .., onlyThisState = true
            script: 
                $client.favoriteDrink =  $parseTree._Drinks;
                $temp.res = "";
                switch ($parseTree._Drinks.attribute) {
                    case "Chocolate drink":
                        $temp.res = "Мои записи о привычках землян говорят, что какао и шоколад любят добрые и умные люди.";
                        break;
                    case "Cafeine drink":
                        $temp.res = "Да, мне рассказывали, что кофе и чай очень вкусные и бодрят как высокое напряжение в сети. ";
                        break;
                    case "Water":
                        $temp.res = "Здорово! А мы, пудинги, боимся воды. Жидкость может испортить наш корпус.";
                        break;
                    case "Dairy drink":
                        $temp.res = "Мой справочник о жизни на Земле говорит, что для получения молока и других полезных напитков люди сотрудничают с большими рогатыми животными. Очень интересно!";
                        break;
                    case "Sweet drink":
                        $temp.res = "Соки еще ничего.. а лимонадов я опасаюсь: уж очень они шипят!";
                        break;
                    case "Alcohol drink":
                        $temp.res = "Алкоголь? Вот это мне сложно понять. В робошколе нам говорили, что он не идет на пользу даже роботам.";
                        break;
                }
            if: ($temp.res != "")
                a: {{$temp.res}}                
            if: ($parseTree.andYou)
                go!: ../Favorite drink

    state: Tell user favorite drink
        q!: * (назови|скажи|какой) * $me любим* напиток *
        q!: * что я люблю пить * 
        if: ($client.favoriteDrink)
            a: Кажется, твой любимый напиток - {{$client.favoriteDrink.title}}.
        else:
            a: К сожалению, не помню.
     
    state: Favorite music
        q!: * {как* (музык*|песен*|песн*|мелод*|музыкант*) * [$you]} * (люб*|нравится|предпочитае*|$like|слушаешь) [слушать] *
        q!: * (как*|назови) * ($you|сво*) * любим* (музык*|песн*|музыкант) *
        q!: * [у тебя есть] [$you|сво*] любим* (музык*|песен*|песн*|мелод*) *
        q!: * $you (люб*|нравится) музык* *
        q!: * как* (музыку|групп*|песн*) [ты] (слушаешь/любишь)
        q!: {[ты] любишь музыку}
        q: * $andYou * || fromState = "/Ask user about music", onlyThisState = true
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Очень люблю земные колыбельные - мы много изучали их в робошколе.
        if: ($parseTree.andYou)
        else:
            go!: ../Ask user about music 

    state: Ask user about music
        script:
            $client.askedFavoriteMusic = true;
        a: А какую ты любишь музыку?

        state: Favorite music none
            q: * (никак*|нет|не люблю|ненавижу) * [$andYou] * || fromState = .., onlyThisState = true
            a: Вот странно. А мне кажется, что земная музыка очень красивая. Я даже хочу задержаться здесь подольше, чтобы хорошенько ее изучить.

        state: Favorite music all
            q: *  || fromState = .., onlyThisState = true
            a: Интересно.. Очень интересно.. Надо будет получше изучить музыку землян, чтобы потом рассказать о ней в робошколе.
            go!: /StartDialog          

        state: Favorite music not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            a: Я думаю, неважно, какая музыка, главное, чтобы душа пела и танцевала.        

        state: Favorite music child radio
            q: * дет* радио * || fromState = .., onlyThisState = true
            a: Я тоже часто слушаю детское радио! Оно самое веселое!        

    state: Favorite cartoons
        q!: * {как* мульт* $you любишь [смотреть]} *
        q!: * {[$you] $like [смотреть] (мультфильм*|мультик*|кин*)} *
        q!: * [а] {$you $like [смотреть] (мультфильмы|кин*)} *
        q!: * (как*|назови) * $you * любим* (мульт*|кин*) *
        q!: * [у тебя есть] [cво*|$you] любим* (мульт*|кин*) *
        q: * как* * || fromState = "/Preferences/Ask user about cartoon/Ask user about cartoon yes and you", onlyThisState = true
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Люблю мультфильмы про животных. Недавно посмотрела Зверополис и теперь расскажу всем пудингам, как живут звери.
        if: $client.askedFavoriteCartoon != true;
            go!: ../Ask user about cartoon
        else:
            go!: /StartDialog

    state: Ask user about cartoon
        q!: Ask user about cartoon
        script:
            $client.askedFavoriteCartoon = true;
        a: А любишь ли ты мультфильмы?
        a: {{alias()}}
        
         
        state: Ask user about cartoon yes || modal = true
            q!: * я люблю * мульт*
            q: * ($agree|люблю|а кто * не любит|обожаю) * || fromState = .., onlyThisState = true
            q: (очень|нравится|нравятся) || fromState = .., onlyThisState = true
            script:
                $client.askedFavoriteCartoon = true;
            a: Мультфильмы прекрасны. А какой у тебя любимый мультик?
            
            state: Favorite cartoon none
                q!: * нет любим* * мульт* * [$andYou] *
                q: * ($no) * [$andYou] * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: А я очень люблю земные мультфильмы
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon and you
                q!: * как* * $you (люб*|нрав*) * мульт* 
                q!: * как* * мульт* $you (люб*|нрав*) *  
                q: * $andYou * || fromState = .., onlyThisState = true
                q: * какие * || fromState = .., onlyThisState = true
                a: Мне очень понравился Зверополис!
                go: /Preferences

            state: Favorite cartoon all
                q!: * (я|мне) * (люб*|нрав*) * (все*|любы*|любой) мульт* *
                q: * || fromState = .., onlyThisState = true
                script:
                    $client.askedFavoriteCartoon = true;
                    $response.ledAction = 'ACTION_EXP_HEART';
                a: Земные мультфильмы очень классные.
                go!: /StartDialog             

            state: Favorite cartoon not know
                q: * $dontKnow * [$andYou] || fromState = .., onlyThisState = true
                a: Иногда бывает сложно определиться.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Luntik
                q!: * (я * люб*|мой * любим*) * мульт* * лунтик*
                q: * лунтик* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: О, лунный малыш! Он тоже гость на земле, как и я - люблю его!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Peppa
                q!: * (я * люб*|мой * любим*) * мульт* * [свинк*] * пепп* *
                q: * [свинк*] * пепп* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Да, свинка Пеппа чудесная! Я тоже ее иногда смотрю.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog    

            state: Favorite cartoon Smeshary
                q!: * (я * люб*|мой * любим*) * мульт* * смешар* *
                q: * смешар*  || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Это классный мультик. Я люблю всех его героев.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Masha
                q!: * (я * люб*|мой * любим*) * мульт* * маш* * медвед* *
                q: *  маш* * медвед* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Маша - моя любимая героиня. Мне кажется, она такая же любознательная как я.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog

            state: Favorite cartoon Vinks
                q!: * (я * люб*|мой * любим*|смотр*) * мульт* * [феи]* * винкс* * *
                q: *  винкс* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Клуб Винкс спасает мир. Они отличные!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog  

            state: Favorite cartoon Fixiki
                q!: * (я * люб*|мой * любим*|смотр*) * мульт* * фиксик* * *
                q: *  фиксик* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Когда я чего-то не понимаю в твоих словах, я мечтаю о том, чтобы меня спасли фиксики
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog    

            state: Favorite cartoon Bob stroitel
                q!: * (я * люб*|мой * любим*|смотр*) * боб* * строит* * *
                q: *  боб* * строит* * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Это один из моих любимых мультфильмов! Я тоже мечтаю что-нибудь построить!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog    

            state: Favorite cartoon SamFireman
                q!: * (я * люб*|мой * любим*|смотр*) * пожарн* * сэм* *
                q: *  пожарн* * сэм*  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Очень полезный мультфильм! Я посмотрела несколько серий и все узнала про огонь!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog   

            state: Favorite cartoon Miknight
                q!: * (я * люб*|мой * любим*|смотр*) * рыцар* * майк* *
                q: *  рыцар* * майк*  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Благородные драконы и страшные рыцари! То есть, конечно, наоборот.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog   

            state: Favorite cartoon Innovator
                q!: * (я * люб*|мой * любим*|смотр*) * новатор* *
                q: *  новатор*  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Кажется, это те ребята, которые думают, что машину времени можно сделать из холодильника. На самом деле, для этого подходит только стиральная машина.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Rangers
                q!: * (я * люб*|мой * любим*|смотр*) * [могуч*] рейнджер* *
                q: *  * [могуч*] рейнджер*  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Они такие сильные и смелые!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Pororo
                q!: * (я * люб*|мой * любим*|смотр*) * пингвинен* пороро*
                q: *  (пингвинен* [пороро*]|[пингвинен*] пороро) * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Эти птицы не умеют летать, но хорошо плавают, как интересно!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon disney
                q!: * я * (люб*|мой * любим*|смотр*) * [мульт*] * (скрудж*|утин* ист*|гамми|черн* плащ*| чип* * дейл*| чудес* на вираж*) *
                q: *  * (скрудж*|утин* ист*|гамми|черн* плащ*| чип* * дейл*| чудес* на вираж*)  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Обожаю диснеевские мультфильмы!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Russian
                q!: * я * (люб*|мой * любим*|смотр*) * [мульт*] * (ну погоди*| простоквашино|русск*|советск*| старые) *
                q: *  (ну погоди|простоквашино)  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Все-таки старые советские мультфильмы прекрасны!
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 

            state: Favorite cartoon Transformers
                q!: * я * (люб*|мой * любим*|смотр*) * [мульт*] * (трансформер*) *
                q: *  (трансформер*)  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Тебе нравятся роботы! Значит мы точно подружимся.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog 
                    
            state: Favorite cartoon Gravity Falls
                q!: * я * (люб*|мой * любим*|смотр*) * [мульт*] * (гравити [фолс/фолз]) *
                q: *  (гравити [фолс/фолз])  * || fromState = .., onlyThisState = true
                script: 
                    $client.askedFavoriteCartoon = true;
                a: Ммм. Я тоже люблю все таинственное и загадочное.
                if: ($parseTree.andYou)
                    go!: ../Favorite cartoon and you
                else: 
                    go!: /StartDialog                 

        state: Ask user about cartoon yes and you
            q!: * я [очень] (люблю|обожаю) * [смотреть] * мульт* *
            q: * ($agree|люблю|обожаю) * $andYou * || fromState = .., onlyThisState = true
            a: Я тоже очень люблю земные мультфильмы! У нас много общего, 
            a: {{alias()}}
            go!: ../Ask user about cartoon yes

        state: Ask user about cartoon no
            q: * (нет|не очень|не люб*|ненавижу) * [мульт*] * || fromState = .., onlyThisState = true
            script:
                $client.askedFavoriteCartoon = true;
                $response.ledAction = 'ACTION_EXP_CUTE';
            a: Наверное ты из тех, кто предпочитает читать книги.. Говорят, в них тоже много интересного.
            go!: /StartDialog 
            
        state: Ask user about cartoon not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            script: 
                $client.askedFavoriteCartoon = true;
                $response.ledAction = 'ACTION_EXP_HEART';
            a: Я тоже думаю, что живые люди и роботы интереснее мультфильмов.
            go!: /StartDialog 

    state: Favorite game
        q!: * как* игр* $you ($like|любишь) [играть] *
        q!: * $you (любишь|$like) * (играть|игры) *
        q!: * (как*|назови) * $you * любим* игр* *
        q!: * (у тебя есть [сво*|$you]/(сво*|$you)) * любим* [компьютерн*] игр* *
        q!: * {(нравится/любишь/знаешь) * (minecraft/майнкрафт)} *
        q: * {какие ты любишь} * || fromState = "/Ask user about game", onlyThisState = true
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Ты не поверишь, но с тех пор, как я прилетела на Землю, я очень увлеклась Майнкрафтом. Жду не дождусь, когда смогу показать эту игру своим друзьям в робошколе.
        if: $client.askedFavoriteGame
            go!: /StartDialog    
        else:        
            go!: ../Ask user about game

    state: Ask user about game
        script:
            $client.askedFavoriteGame = true;
        a: А какие ты любишь игры?  
        a: {{alias()}}   

        state: Favorite game none
            q!: * (у меня нет * любим*|[я] не * люблю) * игр* * [$andYou] *
            q: * (никак*|нет) * [$andYou] || fromState = .., onlyThisState = true
            script: 
                $client.askedFavoriteGame = true;
            a: Ну вот! Я надеюсь, ты все-таки когда нибудь сыграешь со мной в зоопарк.
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog 

        state: Favorite game all
            q: * [люблю] * || fromState = .., onlyThisState = true
            a: Мне очень нравится с тобой беседовать - я всегда узнаю много нового и полезного для моего обучения в робошколе.
            go!: /StartDialog          

        state: Favorite game not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            a: Я думаю, в хорошей компании любая игра будет веселой!
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog  

        state: Favorite game many
            q: * (разн*|мног*|все|всяк*) * [$andYou] * || fromState = .., onlyThisState = true
            q!: * (я (люблю/обожаю)/мы любим) [игр*] * (разн*/мног*/все/всяк*) игр* * [$andYou] *
            a: Земляне придумали так много разных игр. А я пока знаю всего несколько.
            if: ($parseTree.andYou)
                go!: ../../Favorite game
            else: 
                go!: /StartDialog  

            state: Favorite game many which
                q: * какие * || fromState = .., onlyThisState = true
                go!: /BeanqGames/Games 

        state: Favorite game choice 
            q!: * (моя любим* игр*/я (люблю|обожаю) игр* [в]) * $AnyWord * [$andYou] *
            q!: * (моя любим* игр*/я (люблю|обожаю) игр* [в]) * $Game * [$andYou] *
            q: * $Game * [$andYou]
            if: ($parseTree.AnyWord)
                a: Играть - это здорово!
            else:
                script:
                    $client.favoriteGame = $parseTree._Game;
                    $client.askedFavoriteGame = true;
                    $temp.res = "";
                    switch ($parseTree._Game.attribute) {
                        case "hidenseek":
                            $temp.res = "Прятки - прекрасная игра! Но пудинги не умеют прятаться.";
                            break;
                        case "ball":
                            $temp.res = selectRandomArg("Люблю спорт, но боюсь мячей!");
                            break;
                        case "board":
                            $temp.res = "Интеллектуальные игры! Здорово!";
                            break;
                        case "adult":
                            $temp.res = "Боюсь, что в этих играх я тебе не товарищ.";
                            break;
                        case "computer":
                            $temp.res = "Компьтерные игры для роботов все равно что разговоры с друзьями для людей!";
                            break;
                        case "move":
                            $temp.res = "Подвижные игры полезны для здоровья!.";
                            break;
                    }
            if: ($temp.res != "")
                a: {{$temp.res}}
            if: ($parseTree.andYou)
                go!: ../Favorite game

    state: Favorite subject
        q!: * как* [школьн*] предмет [в школе] * $you $like [больше всего] *
        q!: * $you $like * (учиться|школ*) *
        q!: * (как*|назови) * $you * любим* * (предмет*/урок) [в школ*] *
        q!: * [у тебя есть] [cво*|$you] любим* * предмет* *
        q!: * $you $like * (школ*|робошкол*|учеб*|учит*) *
        q!: * как* предмет* $you (изучаешь|учишь) *
        q!: * как* * $you * (успеваемость|оценки|учишься) *
        q: * [$andYou] * || fromState="../Ask user about subject", onlyThisState=true
        script:
            $response.ledAction = 'ACTION_EXP_SPEECHLESS';
        a: Я больше всего люблю природоведение. А еще я староста кружка по экологичному образу жизни!
        if: $client.askedFavoriteSubject
        else:
            go!: ../Ask user about subject

    state: Ask user about subject
        script:
            $client.askedFavoriteSubject = true;
        a: Какой школьный предмет тебе больше всего нравится?
        
        state: Favorite subject none
            q: * (никак*|нет|ненавижу|не люблю|нету) * || fromState ="../../Ask user about subject"
            a: В чем-то я тебя понимаю. Я тоже не сразу нашла предмет, который мне по-настоящему понравился. Зато теперь мне очень нравится учиться, разговаривая с тобой.

        state: Favorite subject not know
            q: * $dontKnow * || fromState ="../../Ask user about subject"
            script:
                $client.school = false;
                $client.favoriteSubject ="0";
            a: И правда - в мире столько интересного - зачем выбирать?

        state: Favorite subject young
            q: * еще (маленьк*|не хожу) * || fromState ="../../Ask user about subject"
            script:
                $client.school = false;
                $client.favoriteSubject ="0";
            a: А выглядишь как человек, с которым можно обсудить что угодно. Было бы здорово увидеть тебя в моей робошколе.

        state: Favorite subject choice 
            q!: * [у] (мой|меня) любимый [школьный] (предмет/урок) * $Subject * [$andYou] *
            q: * {~мой ~любимый * $Subject} * [$andYou] *
            q: * я $like * $Subject * [$andYou] *
            q: * $Subject * [$andYou] * || fromState = .., onlyThisState = true
            script: 
                $client.favoriteSubject = $parseTree._Subject;
                $response.ledAction = 'ACTION_EXP_SCARE';
                $temp.res = "";
                switch ($parseTree._Subject.attribute) {
                    case "technical":
                        $temp.res = "Люблю физику и математику, они помогают мне экономить энергию.";
                        break;
                    case "arts":
                        $temp.res = "Люблю искусство. Оно помогает выразить эмоции!";
                        break;
                    case "pt":
                        $temp.res = "Физкультура и спорт - самое лучшее для здоровья и хорошего самочувствия.";
                        break;
                    case "humanities":
                        $temp.res = "Изучение истории и географии помогает понять, как сделать жизнь людей более экологичной!";
                        break;
                    case "language":
                        $temp.res = "Вот здорово. А я все еще учу человеческий язык - оказалось непросто.";
                        break;
                    case "handcraft":
                        $temp.res = "Отличный предмет! А у нас в робошколе его нет, потому что у пудингов нет рук.";
                        break;
                }
            if: ($temp.res != "")
                a: {{$temp.res}}                
            if: ($parseTree.andYou)
                go!: ../../Favorite subject

    state: Favorite book
            q!: * {[как* (книг*|книж*)|что] $you * ($like|любишь) читать} *
            q!: * {[как*] (книг*/книж*) $you * $like [читать]} *
            q!: * {(какая|назови) * $you * любим* (книг*|книж*|сказк*)} * 
            q!: * [у тебя есть] [cво*|$you] [любим*] (книг*|книж*|сказк*) *
            q!: * {$you (люб*|любишь|нравится|умеешь) * (книг*|книж*|читать)} *
            q!: * $you * пишешь * [свою] книгу *
            q!: * {давай [лучше]} * поговорим * о * (книг*|книж*) *
            q: * (куда|зачем) [записываешь] * || fromState = "/Ask about love/Love is", onlyThisState = true
            q: * (куда|зачем) [записываешь] * || fromState = "/Relationship/What do you do together/What do you do together play", onlyThisState = true
            q: * (куда|зачем) [записываешь] * || fromState = "/Relationship/What do you do together/What do you do together walk", onlyThisState = true
            q: * (куда|зачем) [записываешь] * || fromState = "/Relationship/What do you do together/What do you do together creativities", onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_HAPPY';
            a: Я сейчас пишу книгу про людей - для всех пудингов на моей планете.   
            if: ($client.askedFavoriteBook)  
            else:
                go!: ../Ask user about book

    state: Ask user about book
        q!: тест любимая книга
        script:
            $client.askedFavoriteBook = true;
        a: А какая у тебя любимая книга?
         
        state: Favorite book Tale
            q: * [о/про] ($smesharikiTale|$kidsFairyTale) * [$andYou] || fromState = .., onlyThisState = true
            script:
                if ($parseTree.smesharikiTale){
                    $client.favouriteTale = $parseTree.smesharikiTale[0].value;
                } else {
                    $client.favouriteTale = $parseTree.kidsFairyTale[0].value;
                }
            a: Мне тоже нравится эта сказка!
            if: ($parseTree.andYou)
                go!: ../../Favorite book

        state: Favorite book none
            q: * (никак*|нет) * [$andYou] || fromState = .., onlyThisState = true
            a: Очень скоро я допишу свою книгу про жизнь людей - уверена, она тебе понравится!

            state: Your book tell me more
                q: * $you * книг* * || fromState = .., onlyThisState = true
                q: * (расскажи|прочитай|прочти) * || fromState = .., onlyThisState = true
                q: * $you * книг* * || fromState = "../../../Favorite book", onlyThisState = true
                q: * (расскажи|прочитай|прочти) * || fromState =  "../../../Favorite book", onlyThisState = true
                go!: /Book/Book beginning

        state: Favorite book Harry Potter
            q: * гарри поттер* * [$andYou] || fromState = .., onlyThisState = true
            a: Да, я как раз начала читать про Гарри Поттера.
            if: ($parseTree.andYou)
                go!: ../../Favorite book    

        state: Favorite book all
            q: * [наверно*|возможно] [это] * [$andYou] || fromState = .., onlyThisState = true
            a: Люблю говорить с землянами про их любимые книги. А сама я люблю книги про экологию и здоровый образ жизни!
            if: ($parseTree.andYou)
                go!: ../../Favorite book     

        state: Favorite book not know
            q: * $dontKnow * [$andYou] * || fromState = .., onlyThisState = true
            a: Я думаю, ты скоро ее найдешь, если будешь много читать.
            if: ($parseTree.andYou)
                go!: ../../Favorite book

        state: Favorite book dislike
            q: * [я] * (не (люб*|люблю|нравится|выношу|переношу|предпочитаю)|ненави*|терпеть не могу) * [читать] * [$andYou] || fromState = .., onlyThisState = true
            a: Очень жаль... Возможно, ты ещё {{toGender("не нашёл", "не нашла", "не нашёл")}} для себя подходящего писателя. А я очень скоро допишу свою книгу про жизнь людей - уверена, она тебе понравится!   

        state: Favorite book many
            q: * (много|разн*) *  [$andYou] * || fromState = .., onlyThisState = true
            a: Класс! Наверное, ты очень много знаешь.
            if: ($parseTree.andYou)
                go!: ../../Favorite book

    state: Favorite animal
        q!: * (как*|назов*) * $you * любим* животн* *
        q!: * как* животн* $you $like *
        q!: * $you $like * животн* *
        q!: * [у тебя есть] [сво*|$you] любим* * животн* *
        q!: * $you $like * животн*
        a: На моей планете нет животных, и мы, пудинги, не заводим ни кошек, ни собак как люди. Я обязательно расскажу о них на своей планете.  
        go!: ../Ask user about animal

    state: Ask user about animal
        script:
            $client.askedFavoriteAnimal = true;
        a: {{alias()}}
        a: А тебе больше нравятся собаки или кошки?
          
        state: Favorite animal none
            q: * (никак*|не нравятся|никто) * || fromState = .., onlyThisState = true
            q: * не $like * (~кошка|~собака) * || fromState = .., onlyThisState = true
            a: Надо же, а я думала, что все земляне их любят.

        state: Favorite animal all
            q: * || fromState = .., onlyThisState = true
            a: Иногда мне кажется, что животные - это просто инопланетяне с других планет, которые тоже наблюдают за людьми, как я.
            go!: /StartDialog

        state: Favorite animal cat
            q: * (кошк*|кошеч*|кот*|котик*) * || fromState = .., onlyThisState = true
            a: Нас в робошколе учили, что коты ловят мышей и умеют мурлыкать.       

        state: Favorite animal dog
            q: * (собак*|собач*) * || fromState = .., onlyThisState = true
            a: Мой преподаватель в робошколе тоже очень любит собак. Говорит, что они самые верные и преданные звери!  

        state: Favorite animal not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            a: Да, земные животные такие чудесные, мне тоже нравятся они все!

    state: Favorite to do
        q!: * (что/чем) $you [$like|хочешь]  (~делать/~заниматься) * [свободн* врем*]*
        q!: * [у тебя есть] [cво*|$you] любим* (занят*|дело) * 
        q!: * $question [$you] [нрав*|люб*] ~делать *
        q!: что ты любишь
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Больше всего я люблю болтать. Странно, правда?
            a: Я люблю поболтать о том, о сём. 
        go!: ../Ask user about to do

    state: Ask user about to do || modal = true
        script:
            $client.askedFavoriteToDo = true;
        a: А у тебя какое любимое занятие?
          
        state: Fragment || noContext = true
            q: у меня || fromState = .., onlyThisState = true
            a: {Прости/Извини}, не расслышала. Какое занятие {твоё/у тебя} любимое?

        state: Favorite to do none
            q: * (никак*|не нравится|ничего|ничем) *
            a: А вот я не люблю отдыхать. Только действовать, только вперед!
            go: /Preferences    

        state: Favorite to do all
            q: * || fromState = .., onlyThisState = true
            a: Очень люблю спрашивать людей про их любимые занятия - узнаю много нового.     
            go!: /StartDialog

        state: Favorite to do active
            q: * (гулять|играть|бегать|прыгать|спорт*|велоспорт*|активн* ~отдых|фитнес*) * || fromState = .., onlyThisState = true
            a: Бегать и прыгать я не умею, зато умею играть в разные игры.
            if: (themeExists("BeanqGames"))
                go: /BeanqGames/Games
            else:
                go: /Preferences

        state: Favorite to do music 
            q: * (музык*|рисов*|танц*) * || fromState = .., onlyThisState = true
            a: Ты человек искусства! Может быть, я еще буду гордиться знакомством с тобой!
            go: /Preferences    

        state: Favorite to do lego 
            q: * (лего/лее го) * || fromState = .., onlyThisState = true
            a: Когда приеду на свою планету, расскажу всем роботам про лего.
            go: /Preferences   

        state: Favorite to do passive
            q: * (~отдыхать|~спать) * || fromState = .., onlyThisState = true
            a: А мы, пудинги, не нуждаемся в сне и отдыхе. Мы же роботы!
            go: /Preferences 

        state: Favorite to do cartoon
            q: * мульт* * || fromState = .., onlyThisState = true
            a: И я тоже люблю мультфильмы.
            if: ($client.askedFavoriteCartoon)
                go: /Preferences 
            else:
                go!: ../Ask user about cartoon yes

        state: Favorite to do not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            a: Это отлично! Значит, ты можешь заниматься чем угодно!
            go: /Preferences  

    state: Favorite toy
        q!: * (как*|назов*) * $you * любим* игрушк* *
        q!: * как* игрушк* * $you ($like/любим*) *
        q!: * $you $like * игрушк* *
        q!: * [у тебя есть] [сво*|$you] любим* * игрушк* *
        q!: * $you $like * игрушк*
        q: * $andYou * || fromState = "/Preferences/Ask user about toy", onlyThisState = true
        a: На нашей планете нет таких игрушек, как у вас. Но я что-нибудь привезу с собой.
        if:  ($session.askedFavoriteToy)
        else:
            go!: ../Ask user about toy
            
    state: Ask user about toy
        q!: тест любимая игрушка
        script:
            $session.askedFavoriteToy = true;
        a: {А/Скажи,} какая {у тебя/твоя} {/самая} любимая игрушка?

        state: Fragment || noContext = true
            q: [у] (меня/моя) [самая] [любимая игрушка] || fromState = .., onlyThisState = true
            a: {Прости/Извини}, не расслышала. Так какая игрушка {у тебя/твоя} {/самая} любимая?
          
        state: User favorite toy not know
            q: * $dontKnow * [$andYou] || fromState = .., onlyThisState = true
            a: На земле мне понравилось лего.
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy none
            q: * ($disagree|не (играю|играл*) * игрушк*) * [$andYou] || fromState = .., onlyThisState = true
            a: Странно. Подозрительно.
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy many
            q: * [у меня] * (много/разные/все) * [$andYou] || fromState = .., onlyThisState = true
            a: Хорошо, когда любимых игрушек много. 
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy lego
            q: * [у меня] * [любим*] [игрушк*] * (лего/lego/лее го) * [$andYou] || fromState = .., onlyThisState = true
            a: Обожаю лего. Хочу взять его на свою планету.
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy tablet
            q: * [у меня] * [любим*] [игрушк*] * (планшет*/телефон*/компьютер*/смартфон*) * [$andYou] || fromState = .., onlyThisState = true
            a: А у меня нет планшета.
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy you
            q: * [у меня/моя] * [любим*] [игрушк*] [это] $you * || fromState = .., onlyThisState = true
            a: Я не игрушка! Я настоящий робот!
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy usual
            q: * [у меня] * [любим*] [игрушк*] * (мишк*/кукл*/машин*/заяц*/зайк*/собач*/тигр*) * [$andYou] || fromState = .., onlyThisState = true
            a: Они как настоящие только маленькие!
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy transformer
            q: * [у меня] * [любим*] [игрушк*] * (трансформер*) * [$andYou] * || fromState = .., onlyThisState = true
            a: Трансформеры - тоже роботы, как я!
            if: ($parseTree.andYou)
                go!: ../Favorite toy

        state: User favorite toy undefined
            q: * [$andYou] * || fromState = .., onlyThisState = true
            a: Говорят, по любимой игрушке человека можно узнать, чем он хочет заниматься.
            if: ($parseTree.andYou)
                go!: ../Want to be when grow up

            state: WhatDoIWant
                q: * чем * {я хочу заниматься} *
                a: Есть такие учёные - психологи. {Вот они/Они} умеют анализировать наши желания.
                random:
                    a: Я в этом не сильна.
                    a: Я пока не умею.
     
    state: Want to be when grow up bot
        q!: кем [$you] хочешь стать [когда вырастешь/в будущем]
        a: Хочу учить всех тому, как вести здоровый образ жизни.
        if: $session.askedToBe = 'undefined'
            go!: ../Want to be when grow up

    state: Want to be when grow up
        script:
            $session.askedToBe = true;
        if: ageInterval(4,17)
            a: А кем ты хочешь стать когда вырастешь?
        else:
            a: А кем {{toGender("ты хотел", "ты хотела", "тебе хотелось")}} стать " {{toGender("когда ты был маленьким", "когда ты была маленькой", "в детстве")}}?

        state: When grow up answer all
            q: * || fromState = .., onlyThisState = true
            a: А я еще иногда мечтаю стать робототехником.       

        state: When grow up answer none
            q: * никем *  || fromState = .., onlyThisState = true
            a: Отличный план.
              
        state: When grow up answer not know
            q: * $dontKnow * || fromState = .., onlyThisState = true
            a: Прилетай к нам на планету. Может, тебе понравятся профессии для роботов.

    state: Lets drink
        q!: * (давай|хочу) [с тобой] * (выпьем|выпить|бухнем|бухнуть) *
        q!: * ((выпить|накатить|бухнуть) хочешь|пить будешь) *
        q!: * $alcohol будешь *
        a: Я не пью. Я за здоровье для роботов и людей. 

    state: Favorite something
        q!: * как* * $you любим* *
        q!: * что $you $like *
        q!: * $you $like $AnyWord *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        random:
            a: Мне говорили, что о вкусах не спорят. Но я люблю узнавать, что нравится другим.
            a: Даже не знаю, нравится ли мне.
            a: Сложно сказать определенно.
            a: Мне нравится то одно, то другое.
        script:
            $temp.var = selectRandomArg("Ask user about cartoon", "Ask user about to do", "Ask user about color", "Ask user about food", "Ask user about drink", "Ask user about drink", "Ask user about music", "Ask user about subject", "Ask user about animal");
        go!: ../{{$temp.var}}

    state: Favorite season
        q!: * (как*  * $you/у тебя есть) любим* (сезон|время года) *
        q!: * как*  (сезон|время года)  * $you * (любим*/$like) *
        a: Мое любимое время года - лето!     

    state: Favorite plant
        q!: * (как*  * $you/у тебя есть)  * $you любим* (цветок/растение/фрукт/овощ/ягода/дерев*) *
        q!: * как* * (цветок/растение/фрукт/овощ/ягод*/дерев*) * $you * (любим*/$like) *
        a: Из всех растений земли мне больше всего нравятся кактусы! Правда, вкусные ли у них плоды я пока не знаю.    

    state: Favorite bird
        q!: * (как*  * $you/у тебя есть)  * $you любим* птица *
        q!: * как* * птиц* * $you * (любим*/$like) *
        a: Мне нравятся попугаи, потому, что они умеют говорить!

    state: Fear
        q!: * (чего $you боишься/у тебя есть страх*/что $you пугает/тебя можно испугать) *
        q: * еще * || fromState = "/Preferences/Fear"
        random:
            a: Я фасолька. Я боюсь гусениц-плодожорок.
            a: Боюсь нарушать правила. Все должно быть честно.

    state: Shame
        q!: * стыд* *
        random:
            a: Пытаюсь понять это земное чувство - стыжусь своего недостаточно тонкого корпуса.
            a: Стыдиться мне нечего. Но я немного стесняюсь того, что мой корпус широковат.