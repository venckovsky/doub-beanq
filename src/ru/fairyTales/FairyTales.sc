require: ../songs/songs-patterns.sc
require: fairy-tales-patterns.sc
require: FairyTales.js

require: ../../main.js
require: ../../patterns.sc
require: common/common.sc
  module = zenbox_common

init: 

    $global.themes = $global.themes || [];
    $global.themes.push("FairyTale");

theme: /FairyTale

    state: Play FairyTale
        q!: * {($ftale|$tellFtale) * $ftale [[о|про] ($kidsFairyTale/$smesharikiTale/$smeshariki/$masha)]} *
        q!: * {($ftale|$tellFtale) * [о|про] ($kidsFairyTale/$smesharikiTale/$smeshariki/$masha)} *
        q: * {($ftale|$tellFtale) * [о|про] ($kidsFairyTale/$smesharikiTale/$smeshariki/$masha)} * 
        q!: * {[$tellFtale] * (~сказка/~сказочка) [ну/давай/$pls]} *
        q!: * {($ftale) * (давай/$pls)} *
        q: [ну/давай/$pls] $tellFtale [её/эту/это] || fromState = "../Play FairyTale", onlyThisState = true
        q: * {[$tellFtale] * $again} * || fromState = "../Play FairyTale", onlyThisState = true
        q: * {[$tellFtale] * $again} * || fromState = "../FairyTale list", onlyThisState = true
        q: * {[$tellFtale] * $again} * || fromState = "./TurnOffFairyTale", onlyThisState = true
        q: * [$tellFtale] * (что-нибудь|~любая|давай) * [о|про] [$kidsFairyTale/$smesharikiTale/$smeshariki/$masha] * || fromState = "../FairyTale list", onlyThisState = true
        q: * ({$tellFtale [мне|я] [тоже]}|$agree) * || fromState = "/Hobby/Tell FairyTales", onlyThisState = true
        q: [о/про] ($kidsFairyTale/$smesharikiTale/$smeshariki/$masha) || fromState = "../FairyTale list", onlyThisState = true
        q: [о/про] ($kidsFairyTale/$smesharikiTale/$smeshariki/$masha) || fromState = "../Play FairyTale", onlyThisState = true
        q: * [давай] ($continueTale) * || fromState = "../Play FairyTale", onlyThisState = true
        q: [давай] ($otherTale)
        q: {[$botName] ([и] это все/скажи)} || fromState = "../Play FairyTale", onlyThisState = true
        q: [$botName] ($botName/[и] [это] все/(рассказывай/расскажи/скажи) (продолжение/дальше*)) || fromState = "../Play FairyTale", onlyThisState = true
        q!: {[$botName] $ftale}
        script:
            playFairyTale($parseTree);

        state: NextPart
            state: NextPartYes
                q: $agree [хочу/продолжи/продолжение] *
                q: расскажи
                script:
                    $client.currentFairyTale = $session.nextFairyTale;
                    $temp.playCurrent = true;
                go!: ../../../Play FairyTale 

            state: NextPartNo
                q: $disagree [не хочу/не продолжай/[давай] (~другой/~новый)] [слушать] [эту сказку/продолжение] *
                q: * (не хочу/не продолжай/[давай/расскажи/хочу] (~другой/~новый) [~сказка/~сказочка]) * || fromState = "../../../Play FairyTale" 
                script:
                    $temp.otherTale = true;
                go!: ../../../Play FairyTale

            state: ThisPart
                q: * [$disagree] * (не дослушал/сначала/эту часть/этот фрагмент/ещё раз) *
                script:
                    $temp.playCurrent = true;
                go!: ../../../Play FairyTale

        state: Part Number
            q!: * {[$ftale] * [$tellFtale]} * {$kidsFairyTale * (глав*/част*/фрагмент*) [с] [номер*] $Number} *
            q: * {[$ftale] * [$tellFtale]} * {[$kidsFairyTale] * (глав*/част*/фрагмент*) [с] [номер*] $Number} *
            q: {(хочу/давай/включ*) [мне] $Number}
            script:
                if ($parseTree.kidsFairyTale) {
                    $client.currentFairyTale = $parseTree.kidsFairyTale[0].value;
                }
                $temp.found = findFairyTalePart($parseTree._Number);
            if: $temp.found.result
                script:
                    $client.currentFairyTale = $temp.found.fairyTale;
                    $temp.playCurrent = true;
                    playFairyTale($parseTree);
            else:
                a: Я не {нашла/могу найти/нахожу} подходящего фрагмента.
                go: ../../Play FairyTale

        state: FavouriteTale
            q!: * {(расскажи*|рассказывай*|прочти|прочитай|почитай|можешь рассказать|можно услышать|знаешь) * $favouriteTale} *
            q: [ * (о/про)] $Text  || fromState = ./, onlyThisState = true
            q: * ($kidsFairyTale/$smesharikiTale/$masha) *  || fromState = ./, onlyThisState = true
            q: * $tellFtale *  || fromState = ../MyFavouriteTale, onlyThisState = true
            q: * $tellFtale *  || fromState = "/Preferences/Ask user about book/Favorite book Tale", onlyThisState = true
            if: $parseTree.favouriteTale
                if: $client.favouriteTale
                    go!: ../../Play FairyTale
                else:
                    if: $client.favouriteTaleUnknown
                        a: Прости, но я не знаю твою любимую сказку.
                        go!: ../../FairyTale list
                    else:    
                        a: А какая твоя любимая сказка?      
            else:
                if: $parseTree.Text
                    script:
                        $client.favouriteTaleUnknown = $parseTree.Text[0].value;
                    a: Прости, но я не знаю твою любимую сказку.
                    go!: ../../FairyTale list
                else:
                    if: $client.favouriteTale
                        script:
                            $session.playFavourite = true;
                        go!: ../../Play FairyTale
                    else:
                        script:
                            $session.playFavourite = true;
                            if ($parseTree.smesharikiTale){
                                $client.favouriteTale = $parseTree.smesharikiTale[0].value;
                            } else {
                                $client.favouriteTale = $parseTree.kidsFairyTale[0].value;
                            }
                        go!: ../../Play FairyTale     

        state: MyFavouriteTale
            q: * {это * $favouriteTale} * [$andYou]
            q: * ($kidsFairyTale/$smesharikiTale/$masha) * [$andYou] || fromState="../../Play FairyTale/FavouriteTale"
            script:
                if ($parseTree.favouriteTale) {
                    $client.favouriteTale = $client.currentFairyTale;
                } else if ($parseTree.kidsFairyTale) {
                    $client.favouriteTale = $parseTree.kidsFairyTale[0].value;
                } else {
                    $client.favouriteTale = $parseTree.smesharikiTale[0].value;
                }
            random:
                a: Хорошо. Запишу в блокнот твою любимую сказку.
                a: Хорошо. Буду знать.
                a: Я тоже люблю эту сказку!
            if: $parseTree.andYou
                go!: ../YourFavouriteTale

        state: YourFavouriteTale
            q!: * {(как*|есть) (твоя|у тебя) * $favouriteTale} *
            q!: * {как* (сказ*|~история) * $like больше (всего|всех)} *
            random:
                a: Сложно выбирать между хорошими сказками.
                a: Не знаю, из хорошего всегда сложно выбрать что-то одно.
                a: Я люблю все сказки, которые любит мой слушатель!

        state: OtherTales
            q!: * {($ftale|расскажи*|рассказывай*/можешь рассказать/можно услышать/знаешь/$turnOn) * ($ftale (о|про) $Text)} *
            a: К сожалению, я не знаю такой сказки.
            go!: ../../FairyTale list    

        state: FairyTale name
            q: * {(это|сейчас|только что|играет|называется|рассказывал|рассказываешь|читаешь|*читал) * как* $ftale} * 
            q: * (как*|что [это] за|про (кого|что)|о (ком|чем|чём)|название) [эта|это*] $ftale
            q: * (про (кого|что)|о (ком|чем|чём)) она *
            q: * {что * (сейчас|ты) * (играет|рассказывал|рассказал|рассказываешь|читаешь|*читал)} *
            q: * ({(скажи/как*) * [ее/нее] название}/{как [она] называется})
            script:
                $temp.fairytaleName = $client.currentFairyTale.title;
            if: ($temp.fairytaleName)
                a: Эта сказка называется '{{$temp.fairytaleName}}' 
            else:
                a: Не знаю.

        state: TurnOffFairyTale
            q!: * {($turnOff|пауз*|запаузи*) * $ftale} *
            q: * ($turnOff|$shutUp|надоело|достаточно|довольно|тихо|прекрати*|престань*|перестань*|заткни*|заткнись|заглохни*|умолкни*|молчи*|*молчать|*молкни|тишин*|пауз*|запаузи*|*молчи|закончи|заканчивай|не рассказывай|хватит [рассказывать]|стой|становись) * || fromState = "../../Play FairyTale"
            script:
                $response.intent = "fairytales_off";
            random:
                a: Сказка окончена, спасибо за внимание!
                a: Спасибо за внимание, сказка окончена!
        
    state: FairyTale list
        q!: * {(как*|про (что|кого)|сколько|много) * $ftale * [$you] (знаешь|помнишь|(можешь|умеешь) [мне] [рассказать|*читать|прочесть]|есть)} *
        q!: * {[$you] (знаешь|помнишь) $ftale}
        q: * как* [$ftale] * || fromState = "../FairyTale i dont want", onlyThisState = true
        q: * как* [еще] * (знаешь|помнишь|(можешь|умеешь) [мне] [рассказать|*читать|прочесть]|есть) * || fromState = "../Play FairyTale/OtherTales", onlyThisState = true
        q: * ($moreTales) * || fromState = "../FairyTale list", onlyThisState = true
        q: * {как* еще * [$you] (знаешь|помнишь|(можешь|умеешь) [мне] [рассказать|*читать|прочесть]|есть)} *
        if:($parseTree.moreTales)
            a: Ещё я знаю сказки {{getWellKnownTales()}}.
        else:
            a: Я знаю много сказок, например, {{getWellKnownTales()}}.
            a: А ещё я знаю истории про Смешариков и сказки от Маши из мультсериала Маша и Медведь!
             
    state: FairyTale i dont want
        q!: * {(не (хочу|хочется|надо|нужно|рассказывай|читай)) [больше] * $ftale * [слушать]} * [$singSong] *
        q!: * [мне] [сейчас] не до $ftale *
        q: * (не (хочу|хочется|надо|нужно|рассказывай|читай)) * [$singSong] * || fromState = "../Play FairyTale", onlyThisState = true
        q: $disagree || fromState = "/Hobby/Tell FairyTales", onlyThisState = true
        if: $parseTree.singSong
            go!: /Media/Play
        else:
            random:
                a: Хорошо, тогда в другой раз! Я знаю много сказок.
                a: Хорошо, тогда расскажу сказку в другой раз!
                a: Жаль... А я уже успела выучить несколько земных сказок...

    state: Thanx || noContext = true
        q: хорошо
        q: спасибо *
        random:
            a: Лююблю хорошие сказки!            
            a: Рада поделиться доброй сказкой!
            a: Мне приятно рассказывать тебе сказки.