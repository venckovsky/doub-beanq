function getWellKnownTales(){
    var $client = $jsapi.context().client;
    $client.well_known_tales = ($client.well_known_tales) ?  $client.well_known_tales : [];
    if (!$client.well_known_tales || $client.well_known_tales.length == 0){
        for (var i=0;i<Object.keys($FairyTales).length;i++){
            if ($FairyTales[i].value.wellKnown) {
                $client.well_known_tales.push($FairyTales[i].value.id);
            }
        }
    }
    var taleId = selectRandomArg.apply(this, $client.well_known_tales);
    var taleName = $FairyTales[taleId].value.title.replace(/\. Фрагмент 1/g,'');
    var list_of_names = taleName + ", ";
    $client.well_known_tales.splice($client.well_known_tales.indexOf(taleId), 1);
    taleId = selectRandomArg.apply(this, $client.well_known_tales);
    taleName = $FairyTales[taleId].value.title.replace(/\. Фрагмент 1/g,'');
    list_of_names += taleName + " и ";
    $client.well_known_tales.splice($client.well_known_tales.indexOf(taleId), 1);
    taleId = selectRandomArg.apply(this, $client.well_known_tales);
    taleName = $FairyTales[taleId].value.title.replace(/\. Фрагмент 1/g,'');
    list_of_names += taleName;
    $client.well_known_tales.splice($client.well_known_tales.indexOf(taleId), 1);

    return list_of_names;
}


function tellFairyTale(parseTree) {
    var $temp = $jsapi.context().temp;
    var $client = $jsapi.context().client;
    var $response = $jsapi.context().response;
    $response.audio = ($client.currentFairyTale) ? $client.currentFairyTale.stream : '';
    if ($response.audio != '') {
        var fairytaleName = $client.currentFairyTale.title;
        var fairyTaleClass = $client.currentFairyTale.class;
        if (parseTree.again) {
            $reactions.answer("Ну, слушай ещё раз! Это " + fairyTaleClass + " '"  +  fairytaleName + "'.", "Сейчас снова расскажу! Это " + fairyTaleClass + " '" +  fairytaleName + "'.");
        } else  if ($temp.otherTale) {
            $reactions.answer("Ну, слушай другую сказку! Она называется '" + fairytaleName + "'.");
        } else {
            $reactions.answer("Ну, слушай! Это " + fairyTaleClass + " '" +   fairytaleName + "'.", "Сейчас расскажу! Это "  + fairyTaleClass + " '" +  fairytaleName + "'.");
        }
    } else {
        $reactions.answer("Что-то я забыла все сказки.");
    }
}

function playFairyTale(parseTree) {
    var $temp = $jsapi.context().temp;
    var $client = $jsapi.context().client;
    var $response = $jsapi.context().response;
    var $session = $jsapi.context().session;
    $response.ledAction = 'ACTION_EXP_CUTE';
    $response.intent = "fairytales_on"; 
    if (parseTree.again || $temp.playCurrent) {
        if ($client.currentFairyTale) {
            tellFairyTale(parseTree);
        } else {
            $reactions.answer("Что-то я забыла, какую сказку рассказывал.");
        }
    } else {
        if ($session.playFavourite){
            $session.playFavourite = false;
            $client.currentFairyTale = $client.favouriteTale;
        } else if (parseTree.kidsFairyTale) {
            $client.currentFairyTale = parseTree.kidsFairyTale[0].value;           
        } else if (parseTree.smesharikiTale) {
            $client.currentFairyTale = parseTree.smesharikiTale[0].value;           
        } else if (parseTree.smeshariki){
            var taleN = selectRandomArg.apply(this, Object.keys($SmesharikiTales));
            $client.currentFairyTale = $SmesharikiTales[taleN].value;
        } else if (parseTree.masha) {
            var taleN = selectRandomArg.apply(this, Object.keys($MashaBearTales));
            $client.currentFairyTale = $MashaBearTales[taleN].value;
        } else if (parseTree.continueTale) {
            if ($client.currentFairyTale.nextPart) {
                if ($client.currentFairyTale.type=="Смешарики"){
                    $client.currentFairyTale = $SmesharikiTales[$client.currentFairyTale.nextPart].value;
                } else {
                    $client.currentFairyTale = $FairyTales[$client.currentFairyTale.nextPart].value;
                }
            } else {
                if ($client.well_known_tales == undefined) {
                    getWellKnownTales();
                }
                var taleN = selectRandomArg.apply(this, $client.well_known_tales);
                $client.currentFairyTale = $FairyTales[taleN].value;
                
            }
        } else {
            if ($client.askForTaleTime && !$temp.otherTale && !parseTree.otherTale){
                //проверка, что с прошлого запроса не прошла неделя
                if (currentDate().format('X')-moment($client.askForTaleTime).format('X') < 604800) {
                    if ($client.currentFairyTale.nextPart) {
                        $temp.askAboutSequel = true;
                        if ($client.currentFairyTale.type=="Смешарики"){
                            $session.nextFairyTale = $SmesharikiTales[$client.currentFairyTale.nextPart].value;
                        } else {
                            $session.nextFairyTale = $FairyTales[$client.currentFairyTale.nextPart].value;
                        }
                    } 
                } 
            } 

            if ($temp.askAboutSequel == undefined) {
                if ($client.well_known_tales  == undefined) {
                    getWellKnownTales();
                }
                var taleN = selectRandomArg.apply(this, $client.well_known_tales);
                try{
                    $client.currentFairyTale = $FairyTales[taleN].value;
                } catch(ex) {
                    delete $client.well_known_tales;
                    getWellKnownTales();
                    taleN = selectRandomArg.apply(this, $client.well_known_tales);
                    $client.currentFairyTale = $FairyTales[taleN].value;
                }
                $client.well_known_tales.splice($client.well_known_tales.indexOf(taleN), 1);
            }
           
        }
        if ($temp.askAboutSequel) {
            $reactions.answer("Мы с тобой слушали сказку '" + $client.currentFairyTale.title + "', хочешь ли ты послушать продолжение?");
            //$temp.nextContext = "/FairyTale/Play FairyTale/NextPart";
            $reactions.transition({value: "/FairyTale/Play FairyTale/NextPart", deferred: true});
        } else {
            tellFairyTale(parseTree);
            $client.askForTaleTime = currentDate();
        }     
    }    
}

function findFairyTalePart(number){
    var $client = $jsapi.context().client;
    var part, nextPart, prevPart, result = false;
    var re = new RegExp(".* Фрагмент " + number + "$", 'g');
    if ($client.currentFairyTale && ($client.currentFairyTale.nextPart || $client.currentFairyTale.sequel)) {
        for (var i=parseInt($client.currentFairyTale.id)-1; i>=0;i--){
            part = $FairyTales[i].value;
            nextPart = $FairyTales[i+1].value; 
            if (part.nextPart && part.nextPart == nextPart.id) {
                if (part.title.match(re)) {
                    result = true;
                    break;
                }
            } else {
                break;
            }
        }

        if (!result && $client.currentFairyTale.title.match(re)) {
            part = $client.currentFairyTale;
            result = true;
        }
 
        if (!result) {
            for (var i=parseInt($client.currentFairyTale.id)+1; i<Object.keys($FairyTales).length;i++){
                part = $FairyTales[i].value;
                prevPart = $FairyTales[i-1].value;            
                if (prevPart.nextPart == part.id) {            
                    if (part.title.match(re)) {
                        result = true;
                        break;
                    }              
                } else {
                    break;
                }
            }              
        }
  
    }

    return {result: result,
        fairyTale: part};
}