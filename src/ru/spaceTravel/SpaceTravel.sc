require: common/common.sc
    module = zenbox_common

require: common.js
    module = zb_common

require: patterns.sc
    module = zb_common

require: SpaceTravel.js

require: SpaceTravelThemes.csv
    name = Planet
    var = Planet

require: SpaceTravelCreatures.csv
    name = Creatures
    var = Creatures

init:
    $global.themes = $global.themes || [];
    $global.themes.push("SpaceTravel");

patterns:
    $planet = $entity<Planet> || converter = function(pt) {
        var id = pt.Planet[0].value;
        return Planet[id].value;
        }
    $creature = $entity<Creatures> || converter = function(pt) {
        var id = pt.Creatures[0].value;
        return Creatures[id].value;
        }

    $notCreature = не $creature
    $meet = (встречу|(встрети*|повстреча*) [бы]|{(мог*|мож*) [бы] (встрети*|повстреча*)})
    $dontMeet = (не $meet|(встрети*|повстреча*) не (мог*|мож*) [бы])

theme: /SpaceTravel
    state: StartSpaceTravel || modal = true
        q!: * {[давай/хочу/[давай/хочу] (~лететь/~полететь)] * ([~другой] планета~)} *
        q!: * {[давай/хочу/[давай/хочу] (игра*/сыгра*/поигра*)] * (кто живет на [этой/другой] планете)} *
        q!: * {(давай/хочу/[давай/хочу] (игра*/сыгра*/поигра*)) * [что-нибудь] * [про|о|об|в|на] (~космос/~космический ~путешествие/[~другой] ~планета/~инопланетянин/~летающая ~тарелка/~неопознанный ~летающий ~объект/нло)} *
        q: [$botName] $agree [$botName] || fromState = "/BeanqGames/Games/How to play SpaceTravel", onlyThisState = true
        q: * (сыграем|хочу|попробую|еще|готов*|начинай) * || fromState = "/BeanqGames/Games/How to play SpaceTravel", onlyThisState = true
        script:
            $session.allCreatures = arrayUnseal(Creatures);
            $session.allPlanets = arrayUnseal(Planet);

            $session.randomPlanet = getRandomPlanet($session.allPlanets);
            $session.catchAllCount = 0;
            $session.repeat = true;

            $session.allPlanets[$session.randomPlanet.idx] = undefined;

            $session.ruPlanet = $session.randomPlanet.name;
            $session.ruPlanetSingular = $session.randomPlanet.nameSingular;
            $session.theme = $session.randomPlanet.theme;
            $session.planet = $session.randomPlanet.planet;
            $session.singularNumber = $session.randomPlanet.singularNumber;

        if: $temp.catchAll
            a: Кажется, мы отвлеклись. Я как второй пилот нашей ракеты считаю, что если играть, то играть по правилам. Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?
        else:
            a: Давай представим, что мы с тобой отправляемся в космическое путешествие на планету, где {{($session.singularNumber) ? "живет одна" : "живут одни"}} {{$session.ruPlanet}}. Закрывай глаза, приключение начинается. Пока мы летим, давай подумаем, кого мы могли бы там встретить? Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?

        state: Yes
            q: * ($agree|$maybe) *
            q: * (*ехали|поехал*|едем|вперед|начина*|погнали|продолж*|пофантазируем|полетел*) *
            random:
                a: Отлично! Вперед!
                a: Супер!
                a: Я рад. Тогда начнем.
            script:
                var paths = ['../../GameWay1', '../../GameWay2'];
                $reactions.transition(paths[$reactions.random(paths.length)]);

        state: No
            q: ($disagree|$stopGame)
            q: * $no *
            a: Как-нибудь мы познакомимся с обитателями всех этих планет! Во что ты хочешь поиграть сейчас?
            go: /

        state: OtherPlanet
            q: * {(не (хочу|надо)|лучше|давай|~полететь) * [про|о|об|в|на] ([~планета] $planet|~планета)} *
            q: * [хочу/давай] * (~другой|~следующий|~сменить|~заменить) ~планета *
            a: Мы не можем улететь на другую планету, пока не угадаем всех инопланетян на этой!
            random:
                a: Давай еще пофантазируем?
                a: Может, пофантазируем еще?
                a: Продолжим?
                a: Давай продолжим?

            state: Yes
                q: * ($agree|$maybe) *
                q: * (*ехали|поехал*|едем|вперед|начина*|погнали|продолж*|пофантазируем|полетел*) *
                go!: ../../Yes

            state: No
                q: ($disagree|$stopGame|* $dontKnow *)
                q: * $no *
                a: Как-нибудь мы познакомимся с обитателями всех этих планет! Во что ты хочешь поиграть сейчас?
                go: /

        state: CatchAll
            q: *
            q: * $dontKnow *
            script:
                if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($request.query, "/");
                    $reactions.transition(res.targetState);
                }
                $session.catchAllCount += 1;
                $temp.catchAll = true;
                $temp.rep = true;

            if: $session.catchAllCount <= 2
                a: Кажется, мы отвлеклись. Я как второй пилот нашей ракеты считаю, что если играть, то играть по правилам. Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?
                go: ../
            else:
                go!: ../../CatchAll

    state: GameWay1 || modal = true
        q: * ($agree|$maybe) *
        q: (*хочу|загадай*|вперед|начина*)
        script:
            $session.catchAllCount = 0;
            $session.answered = false;
            $session.dontKnow = 0;

            var oppositeCreature = getRandomOppositeCreature($session.allCreatures, $session.theme);
            var creature = getRandomCreature($session.allCreatures, $session.theme);

            $session.allCreatures[oppositeCreature.id] = undefined;
            $session.allCreatures[creature.id] = undefined;

            $session.oppositeCreature = oppositeCreature.value.value;
            $session.creature = creature.value.value;

            $session.oppositeCreatureAccs = oppositeCreature.value.accs;
            $session.creatureAccs = creature.value.accs;
        random:
            script:
                $session.spaceLastAnswer = "Как ты думаешь, кого мы можем встретить на {{$session.planet}}: {{$session.creatureAccs}} или {{$session.oppositeCreatureAccs}}?";
            script:
                $session.spaceLastAnswer = "Как ты считаешь, кого мы можем повстречать на {{$session.planet}}: {{$session.oppositeCreatureAccs}} или {{$session.creatureAccs}}?";
            script:
                $session.spaceLastAnswer = "Как ты думаешь, кого мы можем встретить на {{$session.planet}}: {{$session.creatureAccs}} или {{$session.oppositeCreatureAccs}}?";
            script:
                $session.spaceLastAnswer = "Как ты считаешь, кого мы можем повстречать на {{$session.planet}}: {{$session.oppositeCreatureAccs}} или {{$session.creatureAccs}}?";
        a: {{$session.spaceLastAnswer}}

        state: Repeat
            q: * {(повтори*/еще раз/снова/заново) * [вопрос*/вариант*]} *
            q: * {((~какой/что) [был*]) * (вопрос*/вариант*)} *
            q: * не (услышал*|расслышал*|понял*|понят*) *
            random:
                a: Хорошо, повторяю вопрос.
                a: Давай попробую ещё раз задать вопрос.
                a:
            a: {{$session.spaceLastAnswer}}
            go: /SpaceTravel/GameWay1

        state: DontKnow
            q: * $dontKnow *
            q: * (возможн*/вероятн*/(черт/фиг/хрен) [его] знает) *
            script:
                $session.dontKnow += 1;
            if: $session.dontKnow >= 2
                go!: ../AnswerGiven
            else:
                random:
                    a: Подумай еще.
                    a: Подумай.
                a: {{$session.spaceLastAnswer}}

        state: ChooseVariant
            script:
                $temp.rep = true;
            a: Выбери, пожалуйста, один вариант из двух: {{$session.creatureAccs}} или {{$session.oppositeCreatureAccs}}?
            go: ../

        state: AnswerGiven
            q: * {[$meet] * ($notCreature|$creature)} *
            q: * {$dontMeet * $creature} *
            if: ($parseTree.creature && ($parseTree._creature.value !== $session.creature && $parseTree._creature.value !== $session.oppositeCreature)) || ($parseTree.notCreature && ($parseTree._notCreature.value !== $session.creature && $parseTree._notCreature.value !== $session.oppositeCreature))
                go!: ../ChooseVariant
            else:
                script:
                    $session.answered = true;
                if: (!$parseTree.dontMeet && $parseTree.creature && ($parseTree._creature.value == $session.creature)) || ($parseTree.notCreature && ($parseTree._notCreature.value == $session.oppositeCreature)) || ($parseTree.dontMeet && $parseTree.creature && ($parseTree._creature.value == $session.oppositeCreature))
                    random:
                        a: Да, верно! Молодец.
                        a: Согласен с тобой.
                        a: Я тоже так думаю.
                        a: Это точно!

                elseif: $session.dontKnow >= 2
                    random:
                        a: Правильный ответ: {{$session.creatureAccs}}.
                        a: Мы встретим {{$session.creatureAccs}}.
                else:
                    random:
                        a: На этой планете {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}. А это не {{$session.ruPlanetSingular}}, едва ли этот инопланетянин нам повстречается сегодня.
                        a: Это возможно. Но на другой планете. А на {{$session.planet}} только {{$session.ruPlanet}} {{($session.singularNumber) ? "живет" : "живут"}}.
                if: getRandomCreature($session.allCreatures, $session.theme)
                    random:
                        a: Давай еще пофантазируем?
                        a: Может, пофантазируем еще?
                        a: Продолжим?
                        a: Давай продолжим?
                    go: /SpaceTravel/StartSpaceTravel
                else:
                    script:
                        $session.randomPlanet = getRandomPlanet($session.allPlanets);
                        $session.allPlanets[$session.randomPlanet.idx] = undefined;

                    if: $session.randomPlanet
                        a: Отлично пофантазировали! Думаю, мы достаточно узнали о планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}. Заходим на посадку!
                        script:
                            $session.allCreatures = arrayUnseal(Creatures);
                            $session.ruPlanet = $session.randomPlanet.name;
                            $session.ruPlanetSingular = $session.randomPlanet.nameSingular;
                            $session.theme = $session.randomPlanet.theme;
                            $session.planet = $session.randomPlanet.planet;
                            $session.singularNumber = $session.randomPlanet.singularNumber;

                        a: Хочешь полететь на другую планету, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}?
                        go: /SpaceTravel/StartSpaceTravel
                    else:
                        a: Кажется, мы изучили все планеты в этой галактике. Когда-нибудь мы совершим еще одно космическое путешествие на другие неисследованные планеты. А теперь возвращаемся на Землю! Во что хочешь поиграть сейчас?
                        go: /

        state: OtherPlanet
            q: * {(не (хочу|надо)|лучше|давай|~полететь) * [про|о|об|в|на] ([~планета] $planet|~планета)} *
            q: * [хочу/давай] * (~другой|~следующий|~сменить|~заменить) ~планета *
            a: Мы не можем улететь на другую планету, пока не угадаем всех инопланетян на этой!
            random:
                a: Давай еще пофантазируем?
                a: Может, пофантазируем еще?
                a: Продолжим?
                a: Давай продолжим?

            state: Yes
                q: * ($agree|$maybe) *
                q: * (*ехали|поехал*|едем|вперед|начина*|погнали|продолж*|пофантазируем|полетел*) *
                a: {{$session.spaceLastAnswer}}
                go: ../../

            state: No
                q: ($disagree|$stopGame|* $dontKnow *)
                q: * $no *
                a: Как-нибудь мы познакомимся с обитателями всех этих планет! Во что ты хочешь поиграть сейчас?
                go: /

        state: LocalCatchAll
            q: *
            script:
                if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($request.query, "/");
                    $reactions.transition(res.targetState);
                }
                $session.catchAllCount += 1;
                $session.repeat = false;
                $temp.catchAll = true;
                $temp.rep = true;

            if: $session.catchAllCount <= 2
                if: $session.answered
                    go!: /SpaceTravel/StartSpaceTravel
                else:
                    a: Кажется, мы отвлеклись. Я как второй пилот нашей ракеты считаю, что если играть, то играть по правилам. Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?
                    # go: /SpaceTravel/GameWay1
            else:
                go!: ../../CatchAll

            state: Yes
                q: * ($agree|$maybe) *
                q: (*хочу|загадай*|вперед|начина*)
                go!: /SpaceTravel/GameWay1/Repeat

            state: No
                q: ($disagree|$stopGame|* $dontKnow *)
                go!: ../../../CatchAll

            state: Ans
                q: * ($notCreature|$creature) *
                go!: /SpaceTravel/GameWay1/AnswerGiven

    state: GameWay2 || modal = true
        q: justai gameway2 || fromState = "/SpaceTravel/StartSpaceTravel", onlyThisState = true
        script:
            $session.catchAllCount = 0;
            $session.dontKnow = 0;
            $session.answered = false;
        random:
            script:
                $session.creature = getRandomCreature($session.allCreatures, $session.theme);
                $session.allCreatures[$session.creature.id] = undefined;

                $session.spaceLastAnswer = "Как ты думаешь, на {{$session.planet}} мы повстречаем {{$session.creature.value.accs}}?";
                $session.yesPattern = true;
            script:
                $session.oppositeCreature = getRandomOppositeCreature($session.allCreatures, $session.theme);
                $session.allCreatures[$session.oppositeCreature.id] = undefined;

                $session.spaceLastAnswer = "Как ты думаешь, на {{$session.planet}} мы повстречаем {{$session.oppositeCreature.value.accs}}?";
                $session.yesPattern = false;
            script:
                $session.creature = getRandomCreature($session.allCreatures, $session.theme);
                $session.allCreatures[$session.creature.id] = undefined;

                $session.spaceLastAnswer = "На {{$session.planet}} мы бы встретили {{$session.creature.value.accs}}?";
                $session.yesPattern = true;

            script:
                $session.oppositeCreature = getRandomOppositeCreature($session.allCreatures, $session.theme);
                $session.allCreatures[$session.oppositeCreature.id] = undefined;

                $session.spaceLastAnswer = "На {{$session.planet}} мы бы встретили {{$session.oppositeCreature.value.accs}}?";
                $session.yesPattern = false;
        a: {{$session.spaceLastAnswer}}

        state: AnswerGiven
            q: ($agree|$disagree|$maybe)
            q: * {$meet * [$creature]} *
            q: * {$dontMeet * [$creature]} *
            script:
                $session.answered = true;
            if: ((($parseTree._agree || $parseTree._maybe || $parseTree.meet) && $session.yesPattern) || (($parseTree._disagree || $parseTree.dontMeet) && !$session.yesPattern))
                random:
                    a: Да, верно! Молодец.
                    a: Согласен с тобой.
                    a: Я тоже так думаю.
            elseif: (($parseTree._no || $parseTree._disagree || $parseTree.dontMeet) && $session.yesPattern)
                a: Кажется, мы можем встретить этого инопланетянина сегодня! Ведь это {{$session.ruPlanetSingular}}!
            elseif: $session.dontKnow >= 2
                if: $session.yesPattern
                    a: Да, встретим.
                else:
                    a: Нет, не встретим.
            else:
                random:
                    a: На этой планете {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}. А это не {{$session.ruPlanetSingular}}, едва ли этот инопланетянин нам повстречается сегодня.
                    a: Это возможно. Но на другой планете. А на {{$session.planet}} только {{$session.ruPlanet}} {{($session.singularNumber) ? "живет" : "живут"}}.
            if: getRandomCreature($session.allCreatures, $session.theme)
                random:
                    a: Давай еще пофантазируем?
                    a: Может, пофантазируем еще?
                    a: Продолжим?
                    a: Давай продолжим?
                go: /SpaceTravel/StartSpaceTravel
            else:
                script:
                    $session.randomPlanet = getRandomPlanet($session.allPlanets);
                    $session.allPlanets[$session.randomPlanet.idx] = undefined;

                if: $session.randomPlanet
                    a: Отлично пофантазировали! Думаю, мы достаточно узнали о планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}. Заходим на посадку!
                    script:
                        $session.allCreatures = arrayUnseal(Creatures);
                        $session.ruPlanet = $session.randomPlanet.name;
                        $session.ruPlanetSingular = $session.randomPlanet.nameSingular;
                        $session.theme = $session.randomPlanet.theme;
                        $session.planet = $session.randomPlanet.planet;
                        $session.singularNumber = $session.randomPlanet.singularNumber;

                    a: Хочешь полететь на другую планету, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}?
                    go: /SpaceTravel/StartSpaceTravel
                else:
                    a: Кажется, мы изучили все планеты в этой галактике. Когда-нибудь мы совершим еще одно космическое путешествие на другие неисследованные планеты. А теперь возвращаемся на Землю! Во что хочешь поиграть сейчас?
                    go: /

        state: LocalCatchAll
            q: *
            script:
                if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                    var res = $nlp.match($request.query, "/");
                    $reactions.transition(res.targetState);
                }
                $session.catchAllCount += 1;
                $temp.catchAll = true;
                $temp.rep = true;

            if: $session.catchAllCount <= 2
                if: $session.answered
                    go!: /SpaceTravel/StartSpaceTravel
                else:
                    a: Кажется, мы отвлеклись. Я как второй пилот нашей ракеты считаю, что если играть, то играть по правилам. Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?
            else:
                go!: ../../CatchAll

            state: Yes
                q: * ($agree|$maybe) *
                q: (*хочу|загадай*|вперед|начина*)
                go!: /SpaceTravel/GameWay2/Repeat

            state: No
                q: ($disagree|$stopGame|* $dontKnow *)
                go!: ../../../CatchAll

        state: Repeat
            q: * {(повтори*/еще раз/снова/заново) * [вопрос*/вариант*]} *
            q: * {((~какой/что) [был*]) * (вопрос*/вариант*)} *
            q: * не (услышал*|расслышал*|понял*|понят*) *
            random:
                a: Хорошо, повторяю вопрос.
                a: Давай попробую ещё раз задать вопрос.
            a: {{$session.spaceLastAnswer}}
            go: /SpaceTravel/GameWay2

        state: DontKnow
            q: * $dontKnow *
            q: * (возможн*/вероятн*/(черт/фиг/хрен) [его] знает) *
            script:
                $session.dontKnow += 1;
            if: $session.dontKnow >= 2
                go!: ../AnswerGiven
            else:
                random:
                    a: Подумай еще.
                    a: Подумай.
                a: {{$session.spaceLastAnswer}}

        state: OtherPlanet
            q: * {(не (хочу|надо)|лучше|давай|~полететь) * [про|о|об|в|на] ([~планета] $planet|~планета)} *
            q: * [хочу/давай] * (~другой|~следующий|~сменить|~заменить) ~планета *
            a: Мы не можем улететь на другую планету, пока не угадаем всех инопланетян на этой!
            random:
                a: Давай еще пофантазируем?
                a: Может, пофантазируем еще?
                a: Продолжим?
                a: Давай продолжим?

            state: Yes
                q: * ($agree|$maybe) *
                q: * (*ехали|поехал*|едем|вперед|начина*|погнали|продолж*|пофантазируем|полетел*) *
                a: {{$session.spaceLastAnswer}}
                go: ../../

            state: No
                q: ($disagree|$stopGame|* $dontKnow *)
                q: * $no *
                a: Как-нибудь мы познакомимся с обитателями всех этих планет! Во что ты хочешь поиграть сейчас?
                go: /

    state: StopGame
        q: * $stopGame * || fromState = "../GameWay1"
        q: * $stopGame * || fromState = "../GameWay2"
        a: Как-нибудь мы познакомимся с обитателями всех этих планет! Во что ты хочешь поиграть сейчас?
        go: /

    state: CatchAll
        q: *
        script:
            if ($request.query.indexOf("хочу ")  > -1 || $request.query.indexOf("давай ")  > -1 || $request.query.indexOf("включи ")  > -1 || $request.query.indexOf("поиграем")  > -1 || $request.query.indexOf("поиграй ")  > -1 || $request.query.indexOf("поговори ")  > -1 || $request.query.indexOf("расскажи ")  > -1 || $request.query.indexOf("скажи ")  > -1 || $request.query.indexOf("создай ")  > -1 || $request.query.indexOf("загадай ")  > -1 || $request.query.indexOf("открой ")  > -1 || $request.query.indexOf("запусти ")  > -1 || $request.query.indexOf("поставь ")  > -1) {
                var res = $nlp.match($request.query, "/");
                $reactions.transition(res.targetState);
            }

            $session.catchAllCount += 1;
            $temp.catchAll = true;
            $temp.rep = true;

        if: $session.catchAllCount <= 2
            if: $session.answered
                go!: /SpaceTravel/StartSpaceTravel
            else:
                a: Кажется, мы отвлеклись. Я как второй пилот нашей ракеты считаю, что если играть, то играть по правилам. Я буду тебе называть двух инопланетян, а ты говори, кого из них мы можем встретить на планете, где {{($session.singularNumber) ? "живет" : "живут"}} только {{$session.ruPlanet}}, а кого нет. Поехали?
                go: /SpaceTravel/StartSpaceTravel
        else:
            a: Кажется, стало скучновато, и мы летим не на ту планету. Сыграем в что-нибудь другое? У меня много других полезных и интересных игр.
            script:
                nextGame();
                $session.nextGame = $temp.nextTheme;

        state: Yes
            q: * ($agree|$maybe) *
            q: (*хочу|загадай*|вперед|начина*)
            go!: /{{$session.nextGame}}

        state: No
            q: ($disagree|$stopGame|* $dontKnow *)
            a: А во что ты хочешь поиграть?
            go: /

        state: Ans
            q: * ($notCreature|$creature) *
            go!: /SpaceTravel/GameWay1/AnswerGiven
