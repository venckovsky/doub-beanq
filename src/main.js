var global_locale = "ru";

function processStreamContent(){
    var $response = $jsapi.context().response;
    var $client = $jsapi.context().client;
    var $temp = $jsapi.context().temp;
    if ($response.audio || $response.stream) {
        $client.audioHint = ($client.audioHint) ? ($client.audioHint) : 0;
        $client.audioHintMoment = ($client.audioHintMoment) ? ($client.audioHintMoment) : 0;
        var interval = currentDate() - $client.audioHintMoment;
        if ($client.audioHint < 6 && interval >= 1200000) {
            switch($response.intent) {
                case "fairytales_on":
                    $temp.contentType = "сказку";
                    break;
                case "poems_on":
                    $temp.contentType = "чтение";
                    break;   
                default: 
                    $temp.contentType = "музыку";
                    break;                                    
            }

            $reactions.answer("Чтобы остановить {{$temp.contentType}}, громко скажи 'Фасолик'.");
            $client.audioHint += 1;
            $client.audioHintMoment = currentDate();
        }
    }
}

bind("postProcess", processStreamContent);

function getFullReplyText(){
    var $response = $jsapi.context().response;
    var res = '';
    
    if ($response.replies && $response.replies.length > 0) {
        for (var i=0; i<$response.replies.length; i++){
            if ($response.replies[i].text) {
                if (res != '') {
                    res += ' ';
                }
                res += $response.replies[i].text;
            }
        }        
    }

    return res;
}

function checkBotRepetition(){
    var $client = $jsapi.context().client;
    var $temp = $jsapi.context().temp;
    if ($client.lastAnswer) {
        if ($client.lastAnswer == getFullReplyText() && $client.lastAnswer != '' && typeof $temp.rep == 'undefined') {
            $reactions.answer("Что-то не так. " + selectRandomArg("Мы ходим по кругу.","Это я уже говорил.","Я это уже говорил.","Я повторяюсь.","Я снова повторяюсь."));
        } else {
            $client.lastAnswer = getFullReplyText();
        }               
    } else {
        $client.lastAnswer = getFullReplyText();
    }
}

bind("postProcess", checkBotRepetition);

function setNextContext(){
    var $temp = $jsapi.context().temp;
    if ($temp.nextContext) {
        $reactions.transition({value: $temp.nextContext, deferred: true});
    }
}

bind("postProcess", setNextContext);


function ttsReplacements() {
    var $response = $jsapi.context().response;
    if ($response.replies && $response.replies.length > 0) {
        for (var i=0; i<$response.replies.length; i++){
            if ($response.replies[i].text) {
                $response.replies[i].text.toString().replace(/\bДанечка\b/g,'Данеч ка');
            }
        }        
    }
}

bind("postProcess", ttsReplacements);

bind("postProcess", function($context) {
    $context.session.lastActiveTime = $jsapi.currentTime();
});

bind("preProcess", function($context) {
    if ($context.session.lastActiveTime) {
        var interval = $jsapi.currentTime() - $context.session.lastActiveTime;
        if (interval > 30*60*1000) {
            $reactions.newSession( {message: $context.parseTree.text, data: $context.request.data } );
        }
    }    
});

bind("preMatch", function() {
    var $response = $jsapi.context().response;

    if ($response.beanq && $response.beanq.event && $response.beanq.event.type && $response.beanq.event.packageName) {
        if ($response.beanq.event.type === "START_APPLICATION") {
            if ($response.beanq.event.packageName === "com.justai.beanq.skill.animalquiz") {
                $jsapi.context().temp.targetState = '/animalsAndBirds/Enter';
            }
        }
    }
});

bind("preMatch", function() {
    var $response = $jsapi.context().response;
    if ($response.beanq && $response.beanq.event && $response.beanq.event.type && $response.beanq.event.packageName) {
        if ($response.beanq.event.type === "STOP_APPLICATION") {
            if ($response.beanq.event.packageName === "com.justai.beanq.skill.animalquiz") {
                $jsapi.context().temp.targetState = "/";
            }
        }
    }
});


function fillResponse(actionType, packageName, payload) {
    var $response = $jsapi.context().response;

    $response.beanq = $response.beanq || {};
    $response.beanq.actions = $response.beanq.actions || [];

    $response.beanq.actions.push({
        "actionType": actionType , 
        "packageName": packageName,
        "payload": payload
    });
}