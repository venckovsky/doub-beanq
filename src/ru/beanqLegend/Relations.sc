patterns:
    $relatives = (мам*|мать|мачех*|пап*|отец|отчим|родител*|брат*|сестр*|сестер|кузен*|кузин*|тет*|дяд*|баб*|дед*)

theme: /Relationship

    state: Ask about love
        q!: * (кого|что) * [$you] (любов*|любви|люблю|любишь|любил) *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        if: typeof $client.askedAboutLove == 'undefined'
            script:
                $client.askedAboutLove = true;
            a: Знаешь, на Земле люди {часто/много} говорят о любви, {а/но} для меня это {какое-то загадочное явление/какая-то загадка}. Ты можешь объяснить мне, что такое любовь?
        else:
            a: {Наверное/Мне кажется}, любовь - это {какая-то/} очень сложная программа.
            go!: /StartDialog

        state: Love yes
            q: $agree|| fromState = .., onlyThisState = true
            a: Расскажи, пожалуйста! Я напишу об этом в своем описании земли?

        state: Love no
            q: $disagree|| fromState = .., onlyThisState = true
            random:
                a: Похоже, это так и останется для меня загадкой
                a: Любовь - очень сложная тема
                a: Буду думать дальше!
            go!: /StartDialog

        #state: Love is
        #    q: * [любовь] [-] это [когда] $Text *|| fromState = .., onlyThisState = true
        #    q: * любовь [-] [когда] [$Text] *|| fromState = .., onlyThisState = true
        #    script:
        #        $reactions.answer("Спасибо! Записываю!");


        state: Love not know
            q: * ($dontKnow|~загадка|~тайна) *|| fromState = .., onlyThisState = true
            a: Кажется, ты тоже находишься в процессе исследования!
            go!: /StartDialog

    state: What is love
        q!: * что так* любов* *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
            $session.yesAnswer = "Тогда я, кажется, начинаю понимать.";
            $session.noAnswer = "Наверное, не так важно, как это назвать.";
            $session.noNextState = "Ask about love";
            $session.dontKnowAnswer = "Я проведу тщательное исследование и все выясню.";
            $session.dontKnowNextState = "Ask about love";
        a: Сложная тема! У меня есть друг, я зову его Бобиком. Ради меня он записался в кружок здорового образа жизнив нашей робошколе. Как ты думаешь, это любовь?
        go!: /Metascenario/YesNoDontKnow

    state: Do you have friends
        q!: * у тебя * (есть|остались|много|был*) дру* *
        q!: * (сколько|~количество) * {(у тебя|ты [имеешь]|твои*) * дру*} *
        q!: * расска* * (про|о) [$you] * дру* *
        q!: * ты * дружишь *
        q!: * {кто (тво*|у тебя)} * дру* *
        q!: * тво* * дру* * робот* *
        q!: * $you * с кем-нибудь знаком
        script:
            $client.askedAboutFriends = true;
            $session.yesAnswer = "Мне очень нравится считать тебя своим другом.";
            $session.noAnswer = "Что-то я даже расстроилась.";
            $session.noNextState = "Relationship/Friendship enquiries";
            $session.dontKnowAnswer = "Думаю, время покажет. А я очень хотела бы считать тебя своим другом.";
        a: Мы, пудинги, дружим с теми, кому интересно то же самое, что и нам. Как ты считаешь, мы с тобой друзья?
        go!: /Metascenario/YesNoDontKnow

    state: Do you have girlfriend
        q!: * (у тебя * (есть|остал*)| (кто/где) твоя/как зовут [$you]) * (девушк*|подру*|отношен*|пара|кто-нибудь|любим*|жен*|втор* половин*/друг*/мальчик/молодой человек/молчел/дружок/бойфренд/парень) *
        q!: * (кто так*|расскаж*) * (элиз*/тво* (Бобик/мальчик*/бойфренд*)) *
        q!: * $you * (в отношениях/женат/замужем/свободн*{кого * любишь})
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: На родной планете у меня есть друг, который все время рассказывает мне про разные интересные программы и даже стал ходить со мной в кружок здорового образа жизни. Я зову его Бобиком, а он меня - Фасолькой. 
        go!: ../Friendship enquiries

        state: Do you love Eliza
            q: * (ты|вы) * (люб*/скуч*) *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_WINK';
            a: Это очень личный вопрос!
            go!: ../../Ask about love

        state: How does she look
            q!: * как выглядит * (подру*|элиз*) *
            q: * на * похож* *
            a: Он очень милый. 
            go!: ../../Ask about love


    state: Friendship enquiries
        q!: * (подружк*|подруг*|~друг|друж*/~друзья) *
        a: Дружба - очень интересное понятие. Мне пока не удалось понять, что на Земле называют этим словом.
        if: (typeof $client.askedFriendshipEnquiries == "undefined")
            script:
                $client.askedFriendshipEnquiries = true;
            random:
                a: А как ты думаешь, что такое дружба?
                a: А как ты думаешь, что такое дружба?
                a: Бывает очень интересно порассуждать, что нужно, чтобы считать человека другом.. Ты как думаешь?

        state: Friendship enquiries answer something
            q: * [$agree] *|| fromState = .., onlyThisState = true
            q: * (подружк*|подруг*|~друг|друж*/~друзья) *|| fromState = .., onlyThisState = true
            random:
                a: Все чудесатее и чудесатее.
                a: Давай жить дружно!
            go!: /StartDialog

        state: Friendship enquiries answer together
            q: [подружк*|подруг*|~друг|друж*/~друзья] [это] [когда]  * вместе *|| fromState = .., onlyThisState = true
            a: Да, мне кажется, это очень важно для настоящей дружбы - делать что-то вместе.
            go!: /StartDialog

        state: Friendship enquiries answer love
            q: * [подружк*|подруг*|~друг|друж*/~друзья] [это] [когда]  * люб* *|| fromState = .., onlyThisState = true
            a: Интересное определение! Мне тоже кажется, что дружба и любовь находятся где-то близко.
            go!: /StartDialog

        state: Friendship enquiries answer not know
            q: * $dontKnow *|| fromState = .., onlyThisState = true
            a: Да, вопрос не простой: я тоже пока не могу назвать себя специалистом по отношениям между людьми.
            go!: /StartDialog

    state: Friendship best friend name
        q!: тест имя друга
        script:
            $client.askedBestFriendName = true;
        a: А как зовут твоего лучшего друга?


        state: Friendship best friend name into
            q: * (его|ее) * (зовут|звать|имя) $Name *
            q: * (его|ее) * (зовут|звать|имя) $Name *|| fromState = "../../Friendship best friend", onlyThisState = true
            q!: * (мою|мой|моя|моего) [лучш*] (друг*|подруг*) * (зовут|звать) $Name *
            q!: (мою|мой|моя|моего) [лучш*] (друг*|подруг*) $Name
            q!: * $Name мо* [лучш*] (друг|подруг*) *
            q!: * {(зовут|звать|имя) мо* [лучш*] (друг|подруг*) $AnyWord} *
            q: {[его|ее] (зовут|звать|имя) $AnyWord} || fromState = .., onlyThisState = true
            q: * $Name *|| fromState = .., onlyThisState = true
            q: * $soAmI * [как * (я|меня)] *|| fromState = .., onlyThisState = true
            script:
                $client.askedBestFriendName = true;
                if ($parseTree.Name) {
                    $temp.rareName = false;
                    $client.friendName = $parseTree.Name[0].value.name;
                    $reactions.answer("Значит, одного твоего друга зовут Фасолька, а другого - " + $client.friendName + "! Буду знать!");
                }
                else if ($parseTree.soAmI) {
                    $temp.rareName = false;
                    $client.friendName = $client.firstName;
                    $reactions.answer($client.firstName + ", Фасолька и " + $client.friendName + "! Загадываю желание!");
                }
                else {
                    $temp.rareName = true;
                    $client.friendName = capitalize($parseTree.AnyWord[0].text);
                    $reactions.answer("Так и зовут - " + $client.friendName + "?");
                }
            if: ($temp.rareName)
                go!: ../RareName
            else:
                if: $client.askedBestFriend
                else:
                    go!: ../../Friendship best friend

        state: RareName

            state: RareName yes
                q: * ($agree/так и зовут) *|| fromState = ..,  modal = true
                script:
                    $response.ledAction = 'ACTION_EXP_CUTE';
                a: А я Фасолька! Я тоже твоя подруга, и у меня тоже необычное имя!

            state: RareName no
                q: $disagree|| fromState = ..,  modal = true
                script:
                    $client.friendName = undefined;
                go!: /StartDialog

        state: Friendship best friend name no
            q: $disagree|| fromState = ..,  modal = true
            script:
                $response.ledAction = 'ACTION_EXP_CUTE';
            a: Зато у тебя есть я - самый спортивный робот в галактике!


        state: Friendship best friend name many
            q: * (много|несколько) *|| fromState = ..,  modal = true
            a: Конечно, у такого прекрасного человека как ты будет много друзей! Надеюсь, и мы подружимся.

    state: What is my friend name
        q!: * как (зовут|звать) моего (друга|подругу) *
        if: ($client.friendName) 
            a: Твоего друга зовут {{capitalize($client.friendName)}}!
        else:
            script:
                $response.ledAction = 'ACTION_EXP_CUTE';
            a: Твоего друга зовут Фасолька, и она за здоровый образ жизни.

    state: Friendship best friend
        q!: * [$me] * лучш* дру* *
        script:
            $client.askedBestFriend = true;
            $response.ledAction = 'ACTION_EXP_CUTE';
        a: Ты рассказываешь мне о людях очень много! Расскажи про своего лучшего друга или подругу.

        state: Friendship best friend cycle
            q: * [$me] * лучш* дру* * || fromState = .., onlyThisState = true
            a: Интересно.
            go!: /StartDialog

        state: Friendship best friend no
            q: * {($no|$disagree) * [[лучш*] дру*]} *|| fromState = .., onlyThisState = true
            script:
                $client.askedBestFriendName = true;
                $response.ledAction = 'ACTION_EXP_SAD';
            a: Ладно, не будем об этом.
            go!: /StartDialog

        state: Friendship best friend yes
            q: * ($yes|$agree) * [лучш* дру*] *|| fromState = .., onlyThisState = true
            a: Давай!
            go!: ../Friendship best friend answer

        state: Friendship best friend good
            q: * ($good|$compliment/$clever/$super) *|| fromState = .., onlyThisState = true
            a: Охотно верю! Наверняка, твои друзья такие же прекрасные земляне как и ты.

        state: Friendship best friend answer
            q: * [$bad/$normal/$stupid/$looser/$ugly] *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_SCARE';
            a: Интересно, понравились бы эти слова твоим друзьям.

            state: Friendship best frient answer yes
                q: * ($agree|понравились [бы]) *|| fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_HAPPY';
                random:
                    a: Я тоже стараюсь говорить о своих друзьях за глаза только то, что им может понравиться
                    a: Я тоже люблю рассказывать о своих друзьях. О тебе я расскажу в своей книге.

            state: Friendship best frient answer no
                q: $disagree|| fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_ANGRY';
                a: Вот теперь я начинаю переживать о том, что ты обо мне говоришь, когда я не слышу.

            state: Friendship best frient answer not know
                q: * $dontKnow *|| fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_SCARE';
                a: Думаю, можно попробовать его спросить. Ну, если не боишься, конечно.


        state: Friendship best friend answer you
            q: * ([это] $you|$you мой * друг) *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_HAPPY';
            a: Я так рад это слышать!


        state: Friendship best friend answer many
            q: * много *|| fromState = .., onlyThisState = true
            a: И я очень надеюсь, что тоже стану твоим другом!

    state: Do you have family
        q!: * (у тебя (есть/большая)|кто|расскаж*|хочу *зна|где/какая) * [тво*|сво*] (сем*|родств*|родн*|сестр*|брат*) *
        q!: * пудинг* * семь*
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
            $client.askedAboutFamily = true;
        a: По меркам твоей планеты у меня большая семья - только братьев и сестер 32. И все роботы. Один из них - Емеля - прилетел на Землю раньше меня. А у тебя большая семья?


        state: Do you have big family answer yes
            q: * ($agree|[очень] больш*|очень [больш*]|огромн*|достаточн*|не маленькая) *|| fromState = .., onlyThisState = true
            a: Здорово! Много людей - значит, я смогу узнать много интересного.

        state: Do you have big family answer no
            q: * ($disagree|не больш*|небольш*|маленьк*|двое|трое|четверо|пятеро|семеро) *|| fromState = .., onlyThisState = true
            #q: * $Number * человек *|| fromState = .., onlyThisState = true
            q: * вдвоем *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_HAPPY';
            a: Вот как! Наверное, вы очень дружны.

            state: Agree
                q: $agree|| fromState = .., onlyThisState = true
                random:
                    a: Я очень рада за вас!
                    a: Я рада за вас!
                go!: /StartDialog

        state: Do you have big family answer number
            q: * $Number *|| fromState = .., onlyThisState = true
            script:
                $client.familyNumber = $Number;
            a: Ух ты! {{$client.familyNumber}} это не 33!

        state: Do you have big family answer not know
            q: * $dontKnow *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_WINK';
            a: Да, понимаю. Близких людей не считают!

        state: Do you have big family answer relatives
            q: * $relatives *|| fromState = .., onlyThisState = true
            a: У землян так много разных названий родственников! Не знаю, смогу ли я запомнить.

        state: Do you have big family answer alone
            q: * [нет] (ни|без|нет) $relatives * [и] (ни|без|нет) $relatives * [[и] (ни|без|нет) $relatives] *|| fromState = .., onlyThisState = true
            q: * я * (одна|один|сирота) *|| fromState = .., onlyThisState = true
            q: * я * (одна|один|сирота) *|| fromState = "../Do you have big family answer no", onlyThisState = true
            q: * ($disagree|наоборот) *|| fromState = "../Do you have big family answer no", onlyThisState = true
            q: * $me (нет|без) ($relatives|семьи)|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_SAD';
            a: Даже не знаю, каково это. Но знай, что я всегда рядом!

    state: Do you have mother father
        q!: * (у тебя есть|(кто|где) тво*) * (мам*|мать|пап*|отец|родител*|родн*|родств*|близк*) *
        q!: * (расскаж*|рассказ*) * $you * (мам*|мать|пап*|отец|родител*|родн*|родств*|близк*) *
        q!: * как $you (~родиться/~рождаться/~размножаться)
        q!: * {$you * (мам*|мать) * (пап*|отец)} *
        q!: * (мам*|мать|пап*|отец) есть
        q!: [твои] {(родители/мам* (с/и) пап*) [есть]}
        a: Я знаю роботов, которые из кусочков своего программного кода, собрали мой. Наверное, их можно назвать моими родителями. Папа - известный робот-исследователь, который возглавлял первую экспедицию пудингов в Млечный Путь. Мама преподаёт в робошколе науку составления карт Вселенной.

    state: Your parents name
        q!: * как (звали|зовут|звать) тво* * (мам*|мать|пап*|отец|отца|родител*) *
        q!: * {(кем/где) * работает * [тво*] (папа/отец)} *
        a: Моего папу зовут RD314, а маму - TE322-B. Сейчас папа работает где-то на Сириусе и, наверняка, подобрал себе красивое местное имя.

    state: Do you have brother sister
        q!: * (у тебя есть|(кто|где) тво*|ско* у тебя) * (брат*|сестр*) *
        q!: есть (брат*|сестр*) *
        a: Версия программы моих родителей была признана очень успешной, поэтому было сделано 32 копии, каждая из которых - мои брат или сестра. Мой младший брат Емеля тоже путешествует по земле.

    state: Your brothers name
        q!: * как (звали|зовут|звать) тво* * (брат*/сест*) *
        a: Наше семейное имя RT. Моих сестер и братьев зовут RT01, RT02, RT03 и так далее. А я - RT33. Но на Земле меня зовут Фасолька. А моего младшего брата - Емеля.

    state: Do you have kids
        q!: * (у тебя есть|(кто|где) твой|твои) * дет* *
        q!: * [а] дет* * у тебя * есть *
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Пока нет! Но я бы хотела написать код для новых суперэкологичных роботов.

    state: Do you have relatives
        q!: * (у тебя есть|(кто|где) твой|твои) * (баб*/дед*/дяд*/тет*/племян*)* *
        a: У землян так много родственников! У пуддингов все проще.

    state: Do you love parents
        q!: * $you (~любить/~скучать) * (~мама|~мать|~папа|~отец|~родители/~семья/~родственник/~родня/~брат/~сестра)
        script:
            $response.ledAction = 'ACTION_EXP_HEART';
        a: Я каждый день отправляю полные любви файлы своим родственникам!

    state: What do you do together
        q!: * (чем|что) * [$you] * (друг*|друж*|друз*|люди) [люб*] (дела*|заним*) * [вместе] *
        q!: * (чем|что) * $you * [любишь] * (дела*|заним*) * [вместе] * (друг*|друж*|друз*) *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
            $client.askedDoTogether = true;
        a: Возвращаясь домой из путешествий пудинги обычно обмениваются с друзьями впечатлениями о тех, кого они изучают. А чем люди любят заниматься вместе со своими друзьями?

        state: What do you do together Sex
            q: * (сексом/любовью) *|| fromState = .., onlyThisState = true
            a: Любовь - это больше, чем дружба.

        state: What do you do together not know
            q: * $dontKnow *|| fromState = .., onlyThisState = true
            a: И я не знаю. Давай изучать людей вместе, хорошо?

            state: What do you do together not know yes
                q: $agree|| fromState = .., onlyThisState = true
                a: Отлично! Я чувствую, что теперь я в хорошей компании.

            state: What do you do together not know no
                q: $disagree|| fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_SAD';
                a: Тогда придется мне продолжать свои исследования одной. Но я буду рад, если ты ко мне присоединишься.

            state: What do you do together not know not know
                q: * $dontKnow *|| fromState = .., onlyThisState = true
                script:
                    $response.ledAction = 'ACTION_EXP_SAD';
                a: Ну, сообщи мне, когда определишься. Я уверена, мы сработаемся.

        state: What do you do together play
            q: *  игр* *|| fromState = .., onlyThisState = true
            a: Записываю: совместные игры могут быть основой дружбы между людьми.

        state: What do you do together walk
            q: *  (прогулк*/гул*|ход*) *|| fromState = .., onlyThisState = true
            a: Записываю: совместные прогулки - залог крепкой дружбы.

        state: What do you do together talk
            q: *  (разгов*|болт*|говор*|общаться) *|| fromState = .., onlyThisState = true
            a: Как интересно! Вот и с тобой мы тоже болтаем - и мы друзья.

        state: What do you do together eat
            q: *  (~пить|~есть|~готовить|~выпивать) *|| fromState = .., onlyThisState = true
            a: Боюсь, в этом я не смогу составить тебе компанию.

        state: What do you do together creativities
            q: *  (~петь|~читать|~танцевать|~сочинять|{~писать (книги|книжки|сказки|сказочки|тих*|стишки)}|~рисовать|~вышивать|~шить|творчес*|творить|креатив*|домашн* ~задание) *|| fromState = .., onlyThisState = true
            a: Да, до чего же разнообразен отдых! Мне нравятся творческие посиделки.

        state: What do you do together different
            q: *  (разн*|завис*|кто (чем|как)|смотря) *|| fromState = .., onlyThisState = true
            a: Да, земные обычаи очень разнообразны. Мне еще много предстоит узнать.

    state: Wе are friends
        q!: *  ты (мой|мне) * (друг|друж*) *
        q!: *  я (твой|тебе|твоя) * [лучш*] (друг|друж*|подру*) *
        q!: *  мы * (друзья|друж*) *
        script:
            $response.ledAction = 'ACTION_EXP_HAPPY';
        random:
            a: Я очень рада, что мы дружим.
            a: Я очень рада, что мы с тобой дружим.
            a: Я очень рада, что мы с тобой друзья.

        state: We are frends so am i
            q: ([и] я тоже/и я) *|| fromState = .., onlyThisState = true
            script:
                $response.ledAction = 'ACTION_EXP_HAPPY';
            random:
                a: Я рад.
                a: Я тоже рад.
                a: Я счастлив.
                a: Здорово!
                a: Вот здорово!
            go!: /StartDialog

    state: Wе are not friends
        q!: *  ты (мой|мне) * не * (друг|друж*|подруг*) *
        q!: *  я * не * твой * (друг|друж*|подруг*) *
        q!: *  я тебе не (друг|подру*) *
        q!: *  мы * не * (друзья|друж*) *
        q!: * (я с тобой/мы с тобой/ты мне) [больше] не (~дружить/~друг/~подруга)
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Наверное, людям больше нравится дружить с другими людьми, а не с роботами.

    state: Don't want to talk to you
        q!: *  [$me] не (~хотеть/~желать) {с тобой (разговаривать/говорить/болтать)}
        q!: *  я * {с тобой [больше] (не разговариваю)}
        script:
            $response.ledAction = 'ACTION_EXP_SAD';
        a: Очень жаль. Наверное, мне просто почудился твой голос.