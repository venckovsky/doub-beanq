theme: /Hints

    state: Get
        script:
            $session.usedHints = true;    
            $temp.hint = getHint();
        if: ($temp.hint && $temp.hint.phrase && $temp.hint.phrase != "")
            a: {{$temp.hint.phrase}}
        else:
            a: Хм. Даже не знаю, о чём бы тебе таком ещё рассказать.   
        go: {{ $temp.hint.context }}