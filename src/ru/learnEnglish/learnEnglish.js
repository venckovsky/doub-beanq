var LE = (function(){
    function init(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        if (!$client.wordsToLearn  || Object.keys($client.wordsToLearn).length == 0){
            //|| Object.keys($client.wordsToLearn).length == 2
            $client.wordsToLearn = {};
            $client.wellKnownWords = [];
            $session.currentId = -1;
            for (var i in  Object.keys(learnEnglish)) {
                $client.wordsToLearn[i] = 0;
            }
        }
        $session.currentWords = ($session.currentWords) ? $session.currentWords : shuffle(Object.keys($client.wordsToLearn));
        $session.currentPhrases = ($session.currentPhrases) ? $session.currentPhrases : [];
        }

    function getWord(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        //$session.rightInRow = 2; $session.phraseId = 0; $session.currentPhrases = [learnEnglish[11].value.phrase];//-----------------------------for tests

        if ($session.rightInRow && $session.rightInRow === 2) {
            $session.phraseTranslation = true;
            $session.currentPhrase = $session.currentPhrases[$session.phraseId].original;
            $session.currentPhraseTranslation = $session.currentPhrases[$session.phraseId].translation;
            $session.rightInRow = undefined;
            $session.tips = undefined;
            return $session.currentPhrases[$session.phraseId].original;
        } else {

            if ($session.currentId + 1 === $session.currentWords.length) {
                $session.currentWords = shuffle(Object.keys($client.wordsToLearn));
                $session.currentId = 0;
            }
            if ($session.currentId === -1 || $session.currentId == undefined) {
                $session.currentId = 0;
            } else if ($session.currentId < $session.currentWords.length - 1) {
                $session.currentId = $session.currentId + 1;
            } 
            if ($session.currentId > -1) {

                var id = $session.currentWords[$session.currentId];
                //var id = $session.currentWords[43];//---------------------------------------------------------------for tests
                $session.currentWord = learnEnglish[id].value.word;
                $session.currentTranslation = learnEnglish[id].value.translation;
                $session.phrase = learnEnglish[id].value.phrase;
                $session.tips = undefined;
                $session.phraseTranslation = false;
                return $session.currentWord
            } 
        }
    }   
    
    function noRightAnswer(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        $temp.nextState = undefined;
        $session.rightInRow = undefined;
        var id = $session.currentWords[$session.currentId];
        if ($client.wordsToLearn[id] != 0){
            $client.wordsToLearn[id] -= 1;
        }
    }

    function getTip(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        
        noRightAnswer();
        if ($session.phraseTranslation) {
            $temp.nextState = "../Ask for answer";
        } else if ($session.tips != undefined) { 
            if ($session.tips.length === 0){
                var suggestedVarianOfTranslation = $session.suggestedVarianOfTranslation ? ($session.currentTranslation.indexOf($session.suggestedVarianOfTranslation) ? $session.suggestedVarianOfTranslation : $session.currentTranslation[0]) : $session.currentTranslation[0];
                toAnswerRu("У меня уже не осталось подсказок. Правильный ответ: " + suggestedVarianOfTranslation + ".");
                $temp.nextState = "../../../Start";
            } else if ($session.tips.length === 2) {
                toAnswerRu("У меня есть ещё одна подсказка: ", "Послушай следующую подсказку: ");
                if ($session.nextTip === 1){
                    toAnswerEn($session.tips[1][0]);
                    toAnswerRu($session.tips[1][1]);
                } else {
                    toAnswerEn($session.tips[0][0]);
                    toAnswerRu($session.tips[0][1]);
                    toAnswerEn($session.tips[0][2]);
                }
                $session.tips = [];
            }
        } else {
            toAnswerRu("У меня есть подсказка: ", "Послушай подсказку: ");

            var firstTip = [$session.phrase.original,"Итак, как будет по-русски ", $session.currentWord + "?"];
            
            var shuffledWords = shuffle(Object.keys(learnEnglish));
            var id = (shuffledWords[0] === $session.currentWords[$session.currentId]) ? shuffledWords[1] : shuffledWords[0];
            $session.alternativeTranslation = learnEnglish[id].value.translation[0];
    
            if (selectRandomArg(0, 1) === 0){
                $session.suggestedVarianOfTranslation = $session.currentTranslation[0];
                var translationVariants = $session.suggestedVarianOfTranslation + " или " + $session.alternativeTranslation;
                $session.rightAnswer = 1;
            } else {
                $session.suggestedVarianOfTranslation = $session.currentTranslation[0];
                var translationVariants = $session.alternativeTranslation + " или " + $session.suggestedVarianOfTranslation;
                $session.rightAnswer = 2;
            }
            var secondTip = [$session.currentWord, " - это " + translationVariants + "?"];
    
            $session.tips = [firstTip, secondTip];
            if (selectRandomArg(0, 1) === 1) {
                $session.nextTip = 0;
                toAnswerEn($session.tips[1][0]);
                toAnswerRu($session.tips[1][1]);
            } else {
                $session.nextTip = 1;
                toAnswerEn($session.tips[0][0]);
                toAnswerRu($session.tips[0][1]);
                toAnswerEn($session.tips[0][2]);
            }
        }
    }
    
    function formAnswer(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        toAnswerRu("Закончили!", "Спасибо за урок!", "Отлично позанимались!");
        if (!$session.learnedWords) {$session.learnedWords = [];}
        if ($session.learnedWords.length > 0 || ($session.currentId - $session.learnedWords.length + 1) > 0){
            toAnswerRu("У нас ");
        }
        if ($session.learnedWords.length > 0) {
            var right = $session.learnedWords.length;
            var ru__answers = right % 10 === 1 && right % 100 !== 11 ? 'выученное слово' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ? 'выученных слова' : 'выученных слов');
            toAnswerRu(right + " " + ru__answers);
            } 
    
        if (($session.currentId - $session.learnedWords.length + 1) > 0) {
            if ($session.learnedWords.length  > 0){
                toAnswerRu(" и ");
            }
            var right = $session.currentId - $session.learnedWords.length + 1;
            var ru__answers = right % 10 === 1 && right % 100 !== 11 ? 'слово, которое' : (right % 10 >= 2 && right % 10 <= 4 && (right % 100 < 10 || right % 100 >= 20) ?  'слова, которые' : 'слов, которые');
            toAnswerRu(right + " " + ru__answers + " нужно повторить.");
        }    
    }
    
    function checkAnswer(){
        var $session = $jsapi.context().session;
        var $temp = $jsapi.context().temp;
        var $client = $jsapi.context().client;
        var $parseTree = $jsapi.context().parseTree;
        if ($session.phraseTranslation) {
            var translation = $session.currentPhrases[$session.phraseId].translation;
            var translationLower = [];
            for (var i in translation){
                translationLower.push(translation[i].toLowerCase());
            }
            if (translationLower.indexOf($parseTree.text.toLowerCase()) > -1) {
                toAnswerRu("Верно!");
            } else {
                toAnswerRu("Правильный перевод: " + translation + ".");        
            }
            $temp.nextState = "../../";
        } else if ($parseTree.Translation || $temp.probablyTranslation) {
            var translationCheck = $parseTree.Translation || [{'value':{'translation':[$temp.probablyTranslation]}}];
            var rightAnswer = false;
            for (var i in $session.currentTranslation) {
                if (translationCheck[0].value.translation.indexOf($session.currentTranslation[i]) > -1) {
                    rightAnswer = true;
                    break
                }
            } 
            if (translationCheck[1]) {
                if (rightAnswer){
                    rightAnswer = false;
                    for (i in $session.currentTranslation) {
                        if (translationCheck[1].value.translation.indexOf($session.currentTranslation[i]) > -1) {
                            rightAnswer = true;
                        }    
                    }
                } 

                if (!rightAnswer) {
                    toAnswerRu("Назови, пожалуйста, только один перевод.");
                    $temp.nextState = "../";
                }
            } else if ($parseTree.or) {
                toAnswerRu("Назови, пожалуйста, только один перевод.");
                $temp.nextState = "../";
                rightAnswer = false;

            } else if ($parseTree.not) {
                if ($session.alternativeTranslation) {
                    if (translationCheck[0].value.translation.indexOf($session.alternativeTranslation) > -1){
                        rightAnswer = true;
                    } else {
                        rightAnswer = false;
                    }              
                } else {
                    rightAnswer = false;
                }
            } else if (($parseTree.words).length > 1 && !$temp.probablyTranslation){
                if (($parseTree.LEdoubts[0].text).length > 0){
                    rightAnswer = rightAnswer;
                } else {
                    rightAnswer = false;
                    var skip = true;
                    $temp.rememberThis = false;
                    toAnswerRu('Уточни, пожалуйста.');
                    
                }
            }
            if (rightAnswer) {
                if (!$session.tips) {
                    var id = $session.currentWords[$session.currentId];
                    if (!(id in $client.wordsToLearn)){
                        $client.wordsToLearn[id] = 0;
                    }
                    $client.wordsToLearn[id] += 1;
                    $session.rightInRow = ($session.rightInRow != undefined) ? $session.rightInRow + 1 : 0;
                    if ($session.currentPhrases.indexOf($session.phrase) == -1 ) {
                        $session.currentPhrases.push($session.phrase);
                        $session.phraseId = ($session.phraseId != undefined) ? $session.phraseId + 1 : -1;
                    }
                    if ($client.wordsToLearn[id] === 3){
                        $client.wellKnownWords.push(id);
                        delete $client.wordsToLearn[id];
                        $session.learnedWords = ($session.learnedWords) ? $session.learnedWords : [];
                        $session.learnedWords.push(learnEnglish[id].value.word);
                    }
                } 
                toAnswerRu("Верно!");
                $temp.nextState = "../../";
            } else {
                if (!$temp.nextState && skip != true) {
                    toAnswerRu("Не совсем так.");
                    $temp.nextState = "../Tip";
                } else {
                    $temp.nextState = "../";
                }
            }
        } else if ($parseTree.order) {
            if ($session.rightAnswer != undefined) {
                if ($parseTree.order[0].value == $session.rightAnswer) {
                    toAnswerRu("Верно!");
                    $temp.nextState = "../../";
                } else {
                    toAnswerRu("Не совсем так.");
                    $temp.nextState = "../Tip";                    
                }
            } else {
                toAnswerRu("Не совсем так.");
                $temp.nextState = "../Tip";
            }
        } else if ($parseTree.Text){
            toAnswerRu("Не совсем так.");
            $temp.nextState = "../Tip";
        } else {
            toAnswerRu("Не совсем так.");
            $temp.nextState = "../Tip";
        }
        $session.rightAnswer = undefined;
        $session.alternativeTranslation = undefined;
    }        


    return {
        init : init, 
        getWord : getWord,
        getTip : getTip,
        formAnswer : formAnswer,
        checkAnswer : checkAnswer,
        noRightAnswer : noRightAnswer
    }
})();



function toAnswerRu() {
    var newText = Array.isArray(arguments[0]) ? selectRandomArg.apply(this, arguments[0]) : selectRandomArg.apply(this, arguments);
    //newText = resolveVariables(resolveInlineDictionary((newText)));
    //$temp.specialAnswerProcessing = true;
    //if (!$temp.answer) {
    //    $temp.answer = [];
    //} 
    $reactions.answer(newText);

    //$temp.answer.push([newText,"ru"]);
}

function toAnswerEn() {
    var newText = Array.isArray(arguments[0]) ? selectRandomArg.apply(this, arguments[0]) : selectRandomArg.apply(this, arguments);
    //newText = resolveVariables(resolveInlineDictionary((newText)));
    //$temp.specialAnswerProcessing = true;
    //if (!$temp.answer) {
    //    $temp.answer = [];
    //} 
    $reactions.answer({value: newText, lang: "en"});

    //$temp.answer.push([newText,"en"])
}

function shuffle(array) {
    if (testMode()) {
        return array;
    } else {
        var maxIdx = array.length;
        var newArray = [];
        var randomNumber;
        for (var idx = 0; idx < maxIdx; ++idx) {
            randomNumber = Math.floor(Math.random() * (array.length));
            newArray[idx] = array[randomNumber];
            array.splice(randomNumber,1)
        }
        return newArray
    }
}

function learnEnglishRememberQuestion() {
    var $session = $jsapi.context().session;
    var $temp = $jsapi.context().temp;
    var $response = $jsapi.context().response;

    if ($temp.rememberThis && $response && $response.replies && $response.replies.length > 1) {
        var arr = JSON.parse(JSON.stringify($response));
        arr = arr.replies.slice(1);
        for (var i = 0; i<arr.length; ++i) {
            arr[i].state = "/Learn English/Start/GetAnswer/Repeat";
        }
        $session.learnEnglishPreviousAnswer = arr;
    }
    $session.learnEnglishPreviousAnswer = $session.learnEnglishPreviousAnswer ? $session.learnEnglishPreviousAnswer : false;
}
bind("postProcess", learnEnglishRememberQuestion);