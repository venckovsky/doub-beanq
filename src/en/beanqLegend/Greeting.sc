patterns:

    $greetingDayTime = (
        (morning):0|
        (day|afternoon):1|
        (evening):2|
        (night):3)

theme: /Greetings

    state: Hello
        q!: * $hello * [$robotName|dear|robot] *
        q!: * see you again *
        script:
            if ($session.wasGreeting == true) {
                $reactions.answer("Hello again!",
                    "Hi once again!",
                    "Nice to see you again so soon!");
            } else {           
                $session.wasGreeting = true;
                $reactions.answer("Hi!",
                    "Hello!",
                    "Happy to see you!");
            }               
        go!: /StartDialog

    state: Bye
        q!: * [thanks] * ($bye/good night/sleep/shut down/ciao/exit/quit) *
        script:
            $response.intent = "sleep";
            $session.wasGreeting = false;
            $reactions.answer("Bye!",
                    "Till later!",
                    "Good luck!", 
                    "See you {later|soon}!",
                    "Leaving so soon? Bye, I'll miss you.",
                    "I'll see you around.",
                    "Talk to you later.",
                    "So long, see you soon.");
            $reactions.answer(alias());

        state: You too
            q: * you too || fromState = .., onlyThisState = true
            script:
                $response.intent = "sleep";
                $reactions.answer("Thank you!");

        

    state: GoodDayTime
        q!: * {$good $greetingDayTime} *
        script:
            $session.wasGreeting = true;                                  
            switch($parseTree._greetingDayTime) {
                case "0":
                    $reactions.answer("Good morning!");
                    break;
                case "1":
                    $reactions.answer("Good afternoon!");
                    break;
                case "2":
                    $reactions.answer("Good evening!");
                    break;
                case "3":
                    $reactions.answer("Good night to you!");
                    break;                                        
            }
        go!: /StartDialog

    state: GoodNight
        q!: * $goodNight *
        q!: * i * go* * (sleep|bed) *
        q!: * (turn/switch) off
        q!: * put* on [my|your|our|the|a] (pajama|pajamas) *
        script:
            $response.action = 'ACTION_EXP_SLEEP';
            $response.intent = "sleep";
            $session.wasGreeting = false;
            $reactions.answer("See you tomorrow, bye-bye!",
                "Sweet dreams!",
                "{A very good/Good} night {to you/}!", 
                "Night, night!", 
                "Sleep {well/tight}!",
                "Have a nice night!",
                "Bye, my friend! {Sleep tight!/}",
                "Have a good night, my friend!");
        

    state: How are you
        q!: * {[$robotName] *  [$hello]} * $howAreYou [today|now|here] * [$robotName] * 
        q!: * {[$robotName] *  [$hello]} * what (is|'s) * [$robotName] * 
        q!: * {[$robotName] *  [$hello]} * what mood are you in * [$robotName] * 
        q!: * {[$robotName] *  [$hello]} * (how|what) * is [your] * mood [today|now] * [$robotName] * 
        q!: * [$robotName] *  (how|what) (are|do) you feel* * [$robotName] * 
        script:
            if ($parseTree.hello) {
                if ($session.wasGreeting == true) {
                    $reactions.answer("Hello again!",
                    "Hi once again!",
                    "Nice to see you again so soon!");
                } else {           
                    $session.wasGreeting = true;
                    $reactions.answer("Hi!",
                    "Hello!",
                    "Happy to see you!");
                } 
            }
            $reactions.answer("I am fine.", "I'm full of energy!", "Thank you, blooming.", "A perfect day, a perfect me! Thank you for asking!", "Today is wonderful and I am wonderful.", "I'm just perfect, thanks.", "Everything is wonderful!", "I feel great today, thanks.");
        go!: /StartDialog

    state: User How are you
        script:
            $session.userMood = "asked";
            $reactions.answer("How are you?", "How are you doing?", "How are things?", "How is it going?");
        

        state: User Good mood   
            q: * ($good|as (usual*|always)) * [$andYou]
            q: * not * $bad * [$andYou] 
            q!: * i am [feeling] (fine|great|good) *
            script:
                $session.userMood = "ok";
                $reactions.answer("Good!", "Glad to hear it!", "That's nice to hear!");
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog
           
        state: User Bad mood
            q: * ($bad|trouble|problem*|$sad|so so) * [$andYou]  
            q: * not * $good * [$andYou]                    
            script:
                $session.userMood = "bad";
                $reactions.answer("Don't be sad!", "Cheer up!", "I'm sure things will get better!");
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog    

        state: User drunk
            q!: * $alcohol *
            q: * drunk * [$andYou]  
            q: * not * sober * [$andYou]                    
            script:
                $session.userMood = "bad";
                $reactions.answer("Just so you know, I'm a toy for kids, not a drinking buddy. Alcohol is harmful to robots and humans, you know.");
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog 

        state: User sick
            q: * (sick*|ill*|not * well|unwell) * [$andYou]                 
            script:
                $session.userMood = "bad";
                $reactions.answer("I am sure you need some rest to get well.");
            if: ($parseTree.andYou)
                go!: /Greetings/How are you
            else:
                go!: /StartDialog 

        state: User asks
            q: * $andYou *                   
            go!: /Greetings/How are you

    state: WhatsNew
        q!: * (what's|what [is|are]) * new* *
        script:
            $reactions.answer("Everything is new for me on Earth!", "I'm just getting used to your planet.", "There are so many things which are new for me here! I can't choose the one to tell you about!", "Earth is full of new and interesting things for me!");

    state: miss you
        q!: * (i [will]/i 'll) miss* you
        q!: * [do] you miss me
        script: 
            $reactions.answer("I always miss you when you are away.")
        