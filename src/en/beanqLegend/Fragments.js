function spelling(word){
    var res = "";
    var i = 0;
    while (i < word.length - 1){
        if (word[i] != " ") {
            res += capitalize(word[i]) + " - ";
        }
        i++;
    }
    res += capitalize(word[i]);
    return res;
}

function counting(number){

    var answer = "";
    if (number < 500 && number > 0) {
        for (var i = 1; i < number + 1; i++){
            answer += i + " ";
        }
    } else if (number > 500) {
        for (i=1; i<501;i++) {
            answer += i + " ";
        }
        answer += "Oh, I get tired of counting..."
    } else {
        answer = "I can't count to " + number;
    }
    return answer;  
}