function fillBeanqGames() {
    var $session = $jsapi.context().session;
    $session.beanqGames = [];

    if (themeExists("Food quiz")) {$session.beanqGames.push(["буфет", "в буфет", "/Food quiz/Food game pudding start/Food game yes"]);}
    if (themeExists("Gorodagame")) {$session.beanqGames.push(["города", "в города", "/Gorodagame/Goroda game intro"]);}
    if (themeExists("Math game")) {$session.beanqGames.push(["математика", "в математику", "/Math game/Math game pudding start/Math game yes"]);}
    if (themeExists("Geography quiz")) {$session.beanqGames.push(["география", "в географию", "/Geography quiz/Geography game pudding start/Geography game yes"]);}
    if (themeExists("Odd word game")) {$session.beanqGames.push(["найди лишнее", "в найди лишнее", "/Odd word game BeanQ/YesOrNo/Yes Odd Word game"]);}
    if (themeExists("Calendar quiz")) {$session.beanqGames.push(["календарь", "в календарь", "/Calendar quiz/Pudding start/Yes"]);}
    if (themeExists("Guess Number Game")) {$session.beanqGames.push(["угадай число", "в угадывание чисел", "/Guess Number Game/Guess/Yes"]);}
    if (themeExists("Opposites")) {$session.beanqGames.push(["перевёртыши", "в перевёртыши", "/Opposites/Fasolik/Play"]);}
    if (themeExists("animalsAndBirds")) {$session.beanqGames.push(["зоопарк", "в зоопарк", "/animalsAndBirds/Get"]);}
    if (themeExists("Riddles")) {$session.beanqGames.push(["загадки", "в загадки", "/Riddles/Get"]);}
    if (themeExists("BeanqJokes")) {$session.beanqGames.push(["шутки и анекдоты", "в шутки и анекдоты", "/BeanqJokes/Jokes Start/Jokes Yes"]);}
    if (themeExists("SpaceTravel")) {$session.beanqGames.push(["кто живет на этой планете", "в кто живет на этой планете", "/Akinator/Enter"]);}
    if (themeExists("Akinator")) {$session.beanqGames.push(["акинатор", "в акинатор", "/Akinator/Enter"]);}
    if (themeExists("FindSyllable")) {$session.beanqGames.push(["найди слог", "в найди слог", "/Akinator/Enter"]);}
}

function getRandomGame() {
    var $session = $jsapi.context().session;
    var $temp = $jsapi.context().temp;

    // Если конкретная игра была вызвана сразу, а не через общий диалог, надо заполнить массив
    if (!$session.beanqGames)
        fillBeanqGames();

    var games = $session.beanqGames;

    // Если вызов из конкретной игры, то её исключаем из предлагаемых далее
    if ($temp.currentGame) {
        var games = $session.beanqGames.filter(function(game) {
            return game[0] !== $temp.currentGame;
        });
    }

    var id = $reactions.random(games.length);
    return games[id];
}
