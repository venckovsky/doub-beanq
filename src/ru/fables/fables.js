function getRand(min, max) {
    var index;
    if (testMode()) {
        index = Math.floor((min+max)/2);
    } else {
        index = Math.floor(Math.random() * (max - min)) + min;
    }
    return index;
}

var fables = (function () {

    function init() {
        var $session = $jsapi.context().session;
        var $client = $jsapi.context().client;

        $session.catchAll = 0
        if ($session.insideSkill == undefined || $session.insideSkill == false) {
            $session.allFablesIDs = [];
            for (var id = 1; id < Object.keys($fables).length + 1; id++) {
                var fableID = $fables[id].value.id;
                $session.allFablesIDs.push(fableID);
            }
            $session.insideSkill = true;
        }

        if (!$client.fables) {
            $client.fables = {};
            $client.fables.fablesTold = [];
        }
    }

    function ifFableTold() {
        var $session = $jsapi.context().session;
        var $client = $jsapi.context().client;

        if ($client.fables.fablesTold.indexOf($session.currentFable.id) != -1) {
            fables.chooseStepNumber();
        } else {
            if ($session.lastState == "/") {
                $session.stepNumber = 2;
                $reactions.transition("/Fables/Enter/RandomPath");
            }
            else {
                $reactions.transition("/Fables/Enter/TellFable");
            }
        }
    }

    function chooseFable() {
        var $parseTree = $jsapi.context().parseTree;
        var $session = $jsapi.context().session;

        if ($parseTree.fableName) {
            var currentFableID = $parseTree.fableName[0].value.id;

            $session.currentFable = {
                id: currentFableID,
                answers: $fablesAnswers[currentFableID],
                audio: $parseTree.fableName[0].value.audio,
                index: 0,
                subIndex: -1
            }
            return true;
        } else {
            if ($session.allFablesIDs.length === 0) {
                $session.insideSkill = false;

                return false;
            }
            else {
                var currentFableID = selectRandomArg($session.allFablesIDs);
                var currentFableData = $fables[currentFableID].value;

                $session.currentFable = {
                    id: currentFableData.id,
                    answers: $fablesAnswers[currentFableData.id],
                    audio: currentFableData.audio,
                    index: 0,
                    subIndex: -1
                }
                return true;
            }
        }
    }

    function chooseStepNumber() {
        var $session = $jsapi.context().session;

        $session.stepNumber = getRand(0,3);
        $reactions.transition("/Fables/Enter/RandomPath");
    }


    function catchInsideDialogue(pt) {
        var $request = $jsapi.context().request;
        var $session = $jsapi.context().session;
        var res = $nlp.match($request.query, "/");

        if (pt.changeThemeWords) {
            if (res.targetState) {
                $session.insideSkill = false;
                $reactions.transition(res.targetState);

                return true;
            }
        }
        else if (pt.stopGame) {
            $session.insideSkill = false;
            $reactions.transition("/Fables/Enter/Exit");

            return true;
        }
        else {
            return false;
        }
    }

    function answer(type) {
        var $session = $jsapi.context().session;
        var index = $session.currentFable.index;

        // Если особый ответ на «не знаю» не предусмотрен, отвечаем как на «нет»
        if (type === "d" && !$session.currentFable.answers[index].hasOwnProperty("d")) {
            type = "n";
        }
        var answer = $session.currentFable.answers[index][type];

        if (typeof answer === 'object' && $session.currentFable.subType == null)
        {
            $session.currentFable.subType = type;
            $session.currentFable.subIndex = -1;
            $session.currentFable.subAnswer = answer;
        }
        if ($session.currentFable.subType != null)
        {
            $session.currentFable.subIndex++;
            if ($session.currentFable.subIndex == 0)
            {
                return undefined;
            }
            if ($session.currentFable.subIndex < $session.currentFable.subAnswer.length + 1)
            {
                var index = $session.currentFable.subIndex-1;
                var localAnswer = $session.currentFable.subAnswer[index][type];
                if ($session.currentFable.subIndex == $session.currentFable.subAnswer.length)
                {
                    $session.currentFable.subIndex = -1;
                    $session.currentFable.subType = null;
                    $session.currentFable.subAnswer = undefined;
                }
                return localAnswer;
            }
        }
        else {
            return answer;
        }
    }

    function clear() {
        var $session = $jsapi.context().session;

        var lastFableIndex = $session.allFablesIDs.indexOf($session.currentFable.id);
        $session.allFablesIDs.splice(lastFableIndex, 1);

        delete $session.currentFable;
        delete $session.offtopic;
    }


    return {
        init:init,
        chooseFable:chooseFable,
        catchInsideDialogue:catchInsideDialogue,
        chooseStepNumber:chooseStepNumber,
        answer:answer,
        clear:clear,
        ifFableTold:ifFableTold
    }

})()